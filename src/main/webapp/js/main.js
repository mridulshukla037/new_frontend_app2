var jaduApp = angular.module('jaduApp', ["ngRoute", "ngResource", "ngCookies", "satellizer",'ngTable', 'ezplus', 'moment-picker','bootstrapLightbox','ui.bootstrap','ngMaterial', 'ui.grid.autoFitColumns','ui.grid.pagination', 'ui.grid.resizeColumns', 'md.data.table', 'xeditable'])//,'angular-elevate-zoom'
.config(['$authProvider', '$httpProvider', '$provide', function($authProvider, $httpProvider, $provide) {
  var countAjaxRequests = 0;

  $authProvider.oauth2({
    name: 'jadu',
    url: 'http://wimwisure.com/RestServer/oauth/token',
    clientId: 'jadu-android-app',
    redirectUri: window.location.origin,
    authorizationEndpoint: 'login.html'
  });

  $authProvider.httpInterceptor = true;

  // register the interceptor as a service
  $provide.factory('myHttpInterceptor', function($q, $injector) {

    return {
      // optional method
      'request': function(config) {

        var fileType = config.url.split('.').pop();
        if(fileType == "html"){
          return config
        }

        if(config.type != 'refresh'){
          if(fileType !== "html"){
            $(".loader").css("display", "table");

            if(config.spinnerText)
            $(".loader-text").html(config.spinnerText);

            ++countAjaxRequests;
          }
        }

        var authService = $injector.get("authService");
        config.headers.Authorization = "Bearer " + (authService.getAccessToken());

        // do something on success

        return config;
      },

      // optional method
      'requestError': function(rejection) {
        $(".loader").css("display", "none");
        // do something on error
        return $q.reject(rejection);
      },

      // optional method
      'response': function(response) {
        var fileType = response.config.url.split('.').pop();
        if(fileType == "html"){
          return response
        }

        if(response.config.type != 'refresh'){//on refresh do nothing else show loader
          if(fileType !== "html"){
            --countAjaxRequests;

            if(countAjaxRequests === 0)
              $(".loader").css("display", "none");
          }
        }

        // check if unauthorized here
        // do something on success
        return response;
      },

      // optional method
      'responseError': function(rejection) {
        // console.log('REJECTION ', rejection);
        if(rejection.status === 401){
          var authService = $injector.get("authService");
          var deferred = $q.defer();

          var fileType = rejection.config.url.split('.').pop();
          console.log('---- Status  '+ rejection.status);
          var refreshPromise = new Promise(function(resolve, reject){
            authService.refreshAccessToken()
            .then(
              function(res){
                authService.setAccessToken(res);
                var $http = $injector.get('$http');
                if(rejection.config.type != 'refresh'){//on refresh do nothing else show loader
                  if(fileType !== "html"){
                    --countAjaxRequests;

                    if(countAjaxRequests === 0)
                      $(".loader").css("display", "none");
                  }
                }

                return $http(rejection.config)
              },
              function(err){
                window.location = "login";
                console.log("=================Refresh token err!", err)
              }
            )
            .then( //catching re-request from return $http(rejection.config)
              function(res){
                console.log('Refresh Promise Resolved');

                resolve(res)
              },
              function(err){
                console.log('Refresh Promise Rejected');

                console.log("=================Refresh CALL err!", err)
                reject();
              }
            );
          })

          return refreshPromise;

        }
        else{
          // do something on error
          console.log('inside response error', rejection);
          if(rejection.config.type != 'refresh'){//on refresh do nothing else show loader
            if(fileType !== "html"){
              --countAjaxRequests;

              if(countAjaxRequests === 0)
                $(".loader").css("display", "none");
            }
          }
          return $q.reject(rejection);
        }

      }

    };
  });

  $httpProvider.interceptors.push('myHttpInterceptor');

}])
.run(['$rootScope', '$location', '$auth', 'authService', 'editableOptions',  function ($rootScope, $location,     $auth, authService, editableOptions) {
	editableOptions.theme = 'bs4';
	$rootScope.$on('$routeChangeStart', function (event) {
    /*if($location.path() !== "/login"){
    console.log("In here")
    authService.isAuthenticated();
  }*/
});
}])
.config(['momentPickerProvider', function (momentPickerProvider) {
  momentPickerProvider.options({
    /* Picker properties */
    locale:        'en',
    format:        'YYYY/MM/DD HH:mm:ss',
    minView:       'decade',
    maxView:       'hour',
    startView:     'day',
    autoclose:     true,
    today:         false,
    keyboard:      false,

    /* Extra: Views properties */
    leftArrow:     '&larr;',
    rightArrow:    '&rarr;',
    yearsFormat:   'YYYY',
    monthsFormat:  'MMM',
    daysFormat:    'D',
    hoursFormat:   'HH:[00]',
    minutesFormat: moment.localeData().longDateFormat('LT').replace(/[aA]/, ''),
    secondsFormat: 'ss',
    minutesStep:   15,
    secondsStep:   60,
    hoursStart:8,
    hoursEnd:20,
  });
}]);;

jaduApp.factory('GoogleMaps',[  function() {
  var map = null;
  var myLatlng = {
    lat: -25.363,
    lng: 131.044
  };
  var marker;
  var conf = {
    isLocationSet : false,
    mapEnabled : "geolocation" in navigator ? true  : false
  };


  function updateCurrentPosition(position){
    myLatlng.lat = position.coords.latitude;
    myLatlng.lng = position.coords.longitude;
    map.panTo(myLatlng);
    marker.setPosition(myLatlng);
    conf.isLocationSet = true;
  }

  function updateMap(){

  }

  function addMultiMarkers(map, locations){

    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i].lat, locations[i].long),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i].tag);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  }

  function addMap(lat, lng, mapId) {
    if("geolocation" in navigator){

      if(lat)
      myLatlng.lat = lat;

      if(lng)
      myLatlng.lng = lng;
      if(mapId)
      map = new google.maps.Map(document.getElementById(mapId), {
        center: myLatlng,
        zoom: 16
      });

      marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        draggable:true,
        animation: google.maps.Animation.DROP
      });

      map.addListener("click", function(position){
        console.log(position.latLng.lat());
        updateTimeout = setTimeout(function(){
          marker.setPosition({
            lat : position.latLng.lat(),
            lng : position.latLng.lng()
          });
        }, 200);
      });

      map.addListener("dblclick", function(position){
        clearTimeout(updateTimeout);
      });

      google.maps.event.addListener(marker, 'dragend', function(mk){
        // updateCurrentPosition(marker.position)
        console.log(marker.position.lat(), marker.position.lng());
        console.log('#marker lat lng ', mk.latLng);

        // console.log('#myLatlng ', myLatlng.lat(), myLatlng.lng());

      });

      // getCurrentLocation(function(){
      //     console.log(myLatlng);
      // }, function(){
      //
      // });

      return map;
    }else{
      return null;
    }
  };
  var countRequests = 0;

  // function reRequest(successCallback, errorCallback){
  //   getCurrentLocation(successCallback, errorCallback);
  // }

  function getCurrentLocation(successCallback, errorCallback){
    countRequests++;
    // console.log('Fetching Location');
    navigator.geolocation.getCurrentPosition(function(pos){
      // console.log("---------------->Got location", pos)
      myLatlng.lat = pos.coords.latitude;
      myLatlng.lng = pos.coords.longitude;
      successCallback(myLatlng);
    },
    function(e){
      console.log("Unable to get location",e)
      errorCallback();
      // if(countRequests < 10){
      //   setTimeout(function(){reRequest(successCallback,errorCallback);},2500)
      // }
    },
    {
      timeout:2500,
      enableHighAccuracy:true
    });
  }


  function addInputField(input, callback){
    var searchBox = new google.maps.places.SearchBox(
      (input));

      searchBox.addListener('places_changed',function() {
        var places = searchBox.getPlaces();
        if(places.length > 0){
          var place = places[0];
          myLatlng = place.geometry.location;

          if(!map)
          addMap(myLatlng.lat, myLatlng.lng);

          map.panTo(myLatlng);
          map.setZoom(16);
          marker.setPosition(myLatlng);
          callback && callback();
        }


      });
    }

    return {
      addMap: addMap,
      config : function(){
        return conf;
      },
      addInputField : addInputField,
      getCurrentLocation : getCurrentLocation,
      getLatLong : function(){
        return myLatlng;
      }
    };
  }]);

  jaduApp.config(['$routeProvider', '$locationProvider',  function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/dashboard", {
      templateUrl: 'templates/pages/index.html',
      controller: "HomeController"
    })
    .when("/login", {
      templateUrl: 'login.html',
      controller: "LoginController"
    })
    .when("/qc", {
      templateUrl: 'templates/pages/qc.html',
      controller: "QCController"
    })
    .when("/qc-check/:id", {
      templateUrl: 'templates/pages/qc-check.html',
      controller: "QCCheckController"
    })
    .when("/report/:id", {
      templateUrl: 'templates/pages/report.html',
      controller: "ReportController"
    })
    .when("/create-case", {
      templateUrl: 'templates/pages/create-case.html',
      controller: "CreateCaseController"
    })
    .when("/create-case-agent", {
      templateUrl: 'templates/pages/create-case-agent.html',
      controller: "CreateCaseAgentController"
    })
    .when("/case-requests", {
      templateUrl: 'templates/pages/case-requests.html',
      controller: "CaseRequestsController"
    })
    .when("/address-requests", {
      templateUrl: 'templates/pages/address-requests.html',
      controller: "AddressRequestsController"
    })
    .when("/address-pending", {
      templateUrl: 'templates/pages/address-pending.html',
      controller: "AddressPendingController"
    })
    .when("/allocation-pending", {
      templateUrl: 'templates/pages/allocation-pending.html',
      controller: "AllocationPendingController"
    })
    .when("/completed-cases", {
      templateUrl: 'templates/pages/completed-cases.html',
      controller: "CompletedCasesController"
    })
    .when("/all-cases", {
      templateUrl: 'templates/pages/all-cases.html',
      controller: "AllCasesController"
    })
    .when("/cancelled-cases", {
      templateUrl: 'templates/pages/cancelled-cases.html',
      controller: "CancelledCasesController"
    })
    .when("/closed-cases", {
      templateUrl: 'templates/pages/closed-cases.html',
      controller: "ClosedCasesController"
    })
    .when("/scheduled-cases", {
      templateUrl: 'templates/pages/scheduled-cases.html',
      controller: "ScheduledCasesController"
    })
    .when("/scheduled-cases-agent", {
      templateUrl: 'templates/pages/scheduled-cases-agent.html',
      controller: "ScheduledCasesAgentController"
    })
    .when("/manage-insurance-companies", {
      templateUrl: 'templates/pages/insurance-companies.html',
      controller: "InsuranceCompaniesController"
    })
    .when("/inspectors", {
      templateUrl: 'templates/pages/inspectors.html',
      controller: "InspectorsController"
    })
    .when("/create-agent", {
      templateUrl: 'templates/pages/create-agent.html',
      controller: "CreateAgentController"
    })
    .when("/all-agents", {
      templateUrl: 'templates/pages/all-agents.html',
      controller: "AllAgentsController"
    })
    .when("/all-customers", {
      templateUrl: 'templates/pages/all-customers.html',
      controller: "AllCustomersController"
    })
    .when("/add-vehicle", {
      templateUrl: 'templates/pages/add-vehicle.html',
      controller: "AddVehicleController"
    })
    .when("/agent-home", {
      templateUrl: 'templates/pages/agent-home.html',
      controller: "AgentHomeController"
    })
    .when("/all-cases-agent", {
      templateUrl: 'templates/pages/agent-all-cases.html',
      controller: "AllCasesForAgentController"
    })
    .when("/", {
      templateUrl: 'templates/pages/root.html'
    })
    .when("/404", {
      templateUrl: 'templates/pages/not-found.html'
    })
    .otherwise({ redirectTo: '/404' });
    // use the HTML5 History API
    //$locationProvider.html5Mode(true);
  }]);
