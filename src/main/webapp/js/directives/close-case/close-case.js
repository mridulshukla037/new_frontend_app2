angular.module('jaduApp').directive('closeCase', ['$http', '$location', '$timeout','$compile', '$window','ajaxService', function ($http, $location, $timeout, $compile, $window, ajaxService) {
    return {
        replace: true,
        restrict: 'EA',
        scope: {
            inspectionCase : "=",
            callback: "&"
        },
        templateUrl: "js/directives/close-case/close-case.html",
        controller: function ($scope, $element) {

        },
        link: function(scope, element, attrs){
            scope.closeReasons = [
                {id : "Customer not interested"},
                {id : "Inpector not avilable"},
                {id : "Inspection already done"},
                {id : "Agent asked to cancel"}
            ];

            scope.closeCase = function(){
                var dataObj = {
                    case_id:scope.inspectionCase.id,
                    reason:scope.inspectionCase.closeReason
                };

                ajaxService.closeCase(dataObj,'Please Wait...')
                .then(function(res){
                    alert('Case Closed!');
                    scope.callback();
                })
                .catch(function(e){
                    console.error('Case Rescheduling Error',e);
                });
            };

        }
    };
}]);
