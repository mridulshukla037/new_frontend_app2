angular.module('jaduApp').directive('cancelCase', ['$http', '$location', '$timeout','$compile', '$window','ajaxService', function ($http, $location, $timeout, $compile, $window, ajaxService) {
    return {
        replace: true,
        restrict: 'EA',
        scope: {
            inspectionCase : "=",
            callback: "&"
        },
        templateUrl: "js/directives/cancel-case/cancel-case.html",
        controller: function ($scope, $element) {

        },
        link: function(scope, element, attrs){
            scope.cancelReasons = [
                {id : "Customer not interested"},
                {id : "Inpector not avilable"},
                {id : "Inspection already done"},
                {id : "Agent asked to cancel"}
            ];

            scope.cancelCase = function(){
                alert("Working");
            };

        }
    };
}]);
