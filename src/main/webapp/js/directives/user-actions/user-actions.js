/* global ModalWindow, angular, atqUTIL, ATQConfirmDialog */

angular
		.module('jaduApp')
		.directive(
				'userActions',
				[
						'$http',
						'$location',
						'$timeout',
						'$compile',
						'$window',
						'ajaxService',
						function($http, $location, $timeout, $compile, $window,
								ajaxService) {
							return {
								replace : true,
								restrict : 'EA',
								scope : {
									user : "=",
									callback : "&"
								},
								templateUrl : "js/directives/user-actions/user-actions.html",

								controller : function($scope, $element,
										$mdDialog) {
									if ($scope.user){
										$scope.userCopy = JSON.parse(JSON
												.stringify($scope.user));
										$scope.branchListCopy = $scope.$parent.branchList;
										$scope.branchListCopy.sort();
									}
									$scope.updateFirstName = function() {
										ajaxService
												.updateFirstName(
														{
															username : $scope.userCopy.username,
															firstname : $scope.userCopy.firstName
														})
												.then(
														function(data) {
															alert("First name updated");
															_
																	.extend(
																			$scope.user,
																			data);
															$scope.callback();
														},
														function(err) {
															console.log(err);
															alert(err.data.error_message);
														});
									};

									$scope.updateLastName = function() {
										ajaxService
												.updateLastName(
														{
															username : $scope.userCopy.username,
															lastname : $scope.userCopy.lastName
														})
												.then(
														function(data) {
															alert("Last name updated");
															_
																	.extend(
																			$scope.user,
																			data);
															$scope.callback();
														},
														function(err) {
															console.log(err);
															alert(err.data.error_message);
														});
									};

									$scope.updateAgentCode = function() {
										ajaxService
												.updateAgentCode(
														{
															username : $scope.userCopy.username,
															agentcode : $scope.userCopy.agentCode
														})
												.then(
														function(data) {
															alert("Agent code updated");
															_
																	.extend(
																			$scope.user,
																			data);
															$scope.callback();
														},
														function(err) {
															console.log(err);
															alert(err.data.error_message);
														});
									};
									$scope.updateBranch = function() {
										ajaxService
												.updateBranch(
														{
															username : $scope.userCopy.username,
															branch : $scope.userCopy.branchName
														})
												.then(
														function(data) {
															alert("Branch updated");
															_
																	.extend(
																			$scope.user,
																			data);
															$scope.callback();
														},
														function(err) {
															console.log(err);
															alert(err.data.error_message);
														});
									};

									$scope.updateEmail = function() {
										ajaxService
												.updateEmail(
														{
															username : $scope.userCopy.username,
															email : $scope.userCopy.email
														})
												.then(
														function(data) {
															alert("Email updated");
															_
																	.extend(
																			$scope.user,
																			data);
															$scope.callback();
														},
														function(err) {
															console.log(err);
															alert(err.data.error_message);
														});
									};

									$scope.updatePhoneNumber = function() {
										ajaxService
												.updatePhoneNumber(
														{
															username : $scope.userCopy.username,
															phoneNumber : $scope.userCopy.phoneNumber
														})
												.then(
														function(data) {
															alert("Phone number updated");
															_
																	.extend(
																			$scope.user,
																			data);
															$scope.callback();
														},
														function(err) {
															console.log(err);
															alert(err.data.error_message);
														});
									};

									$scope.updateEnabled = function() {
										ajaxService
												.updateEnabled(
														{
															username : $scope.userCopy.username,
															enabled : $scope.userCopy.enabled
														})
												.then(
														function(data) {
															alert("Account has been "
																	+ (data.enabled ? " enabled!"
																			: " disabled!"));
															_
																	.extend(
																			$scope.user,
																			data);
															$scope.callback();
														},
														function(err) {
															console.log(err);
															alert(err.data.error_message);
														});
									};

									$scope.updateDeviceId = function() {
										ajaxService
												.updateDeviceId(
														{
															username : $scope.userCopy.username,
															deviceId : $scope.userCopy.deviceId
														})
												.then(
														function(data) {
															alert("IMEI number has been updated!");
															_
																	.extend(
																			$scope.user,
																			data);
															$scope.callback();
														},
														function(err) {
															console.log(err);
															alert(err.data.error_message);
														});
									};

									$scope.updateDeviceLocked = function() {
										ajaxService
												.updateDeviceLocked(
														{
															username : $scope.userCopy.username,
															deviceLocked : $scope.userCopy.deviceLocked
														})
												.then(
														function(data) {
															alert("Account has been "
																	+ (data.deviceLocked ? " locked"
																			: " unlocked")
																	+ " with IMEI number!");
															_
																	.extend(
																			$scope.user,
																			data);
															$scope.callback();
														},
														function(err) {
															console.log(err);
															alert(err.data.error_message);
														});
									};

									$scope.resetPassword = function() {
										ajaxService
												.resetPassword(
														{
															username : $scope.userCopy.username
														},
														"Resetting user's password. Please wait!")
												.then(
														function(data) {
															alert("Password has been reset to user's phone number!");
															_
																	.extend(
																			$scope.user,
																			data);
															$scope.callback();
														},
														function(err) {
															console.log(err);
															alert(err.data.error_message);
														});
									}

									$scope.getOTP = function() {
										ajaxService
												.getUnexpiredOTP(
														{
															username : $scope.userCopy.username
														},
														"Fetching data. Please wait!")
												.then(
														function(data) {
															$scope.otps = (data);
														},
														function(err) {
															console.log(err);
															alert(err.data.error_message);
														});
									}
								}
							};
						} ]);
