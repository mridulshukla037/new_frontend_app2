
/* global ModalWindow, angular, atqUTIL, ATQConfirmDialog */

angular.module('jaduApp').directive('lossPhotos', ['$http', '$location', '$timeout','$compile', '$window','ajaxService', function ($http, $location, $timeout, $compile, $window, ajaxService) {
    return {
        replace: true,
        restrict: 'EA',
        scope: {
        	lossAssessmentPhotos : "=",
            photoTypes: "=",
            caseId: "=",
            callback: "&"
        },
        templateUrl: "js/directives/loss-photos/loss-photos.html",
        controller: function ($scope, $element) {

        },
        link: function(scope, element, attrs){
            scope.ImageURLS = [];
            for(var i=0; i<scope.photoTypes.length; i++){
            	
                scope.ImageURLS.push({
                	url:"util/case-image/"+scope.caseId+"/"+scope.photoTypes[i].fileName,
                	name:scope.photoTypes[i].photoType
                })
            }
        }
    };
}]);
