
/* global ModalWindow, angular, atqUTIL, ATQConfirmDialog */

angular.module('jaduApp').directive('retagPhotos', ['$http', '$location', '$timeout','$compile', '$window','ajaxService', function ($http, $location, $timeout, $compile, $window, ajaxService) {
    return {
        replace: true,
        restrict: 'EA',
        scope: {
            casePhotos : "=",
            photoTypes: "=",
            caseId: "=",
            callback: "&"
        },
        templateUrl: "js/directives/retag-photos/retag-photos.html",
        controller: function ($scope, $element) {

        },
        link: function(scope, element, attrs){
            scope.casePhotosCopy = JSON.parse(JSON.stringify(scope.casePhotos));
            for(var i=0; i<scope.casePhotosCopy.length; i++){
                scope.casePhotosCopy[i].photoTypeUpdated = scope.casePhotosCopy[i].photoType;
                scope.casePhotosCopy[i].url= "util/case-image/"+scope.caseId+"/"+scope.casePhotosCopy[i].filename
            }
            
            scope.updateTag = function(){
                var data = [];
                
                for(var i=0; i<scope.casePhotosCopy.length; i++){
                    if(scope.casePhotosCopy[i].photoTypeUpdated !== scope.casePhotosCopy[i].photoType){
                        data.push({
                            id: scope.casePhotosCopy[i].id,
                            photoType: scope.casePhotosCopy[i].photoTypeUpdated
                        });
                    }
                }
                
                ajaxService.retagPhotos({
                    case_id : +scope.caseId,
                    payload : JSON.stringify(data)
                }, "Updating please wait!").then(function(){
                    alert("Updated");
                    for(var i=0; i<scope.casePhotosCopy.length; i++){
                        scope.casePhotosCopy[i].photoType = scope.casePhotosCopy[i].photoTypeUpdated;
                    }
                    scope.casePhotos =    JSON.parse(JSON.stringify(scope.casePhotosCopy)); 
                    scope.callback();
                }, function(a, b){
                    console.log(a, b)
                    alert("Unable to update. Please try again later!");
                });
            }
        }
    };
}]);
