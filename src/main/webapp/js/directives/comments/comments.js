angular.module('jaduApp').directive('caseComments', ['$http', '$location', '$timeout','$compile', '$window','ajaxService', function ($http, $location, $timeout, $compile, $window, ajaxService) {
    return {
        replace: true,
        restrict: 'EA',
        scope: {
            inspectionCase : "="
        },
        templateUrl: "js/directives/comments/comments.html",
        controller: function ($scope, $element) {

        },
        link: function(scope, element, attrs){
            console.log(scope.inspectionCase);
            
                
            
            scope.getComments = function(){
                ajaxService.getComments({
                    case_id : scope.inspectionCase.id
                }, "Fetching comments. Please wait!").then(function(result){
                    var data = result.data;
                    scope.comments = data;
                }, function(err){
                    console.log(err);
                    alert("Unable to fetch comments. Please try again later.");
                });
            };
            
            scope.addComments = function(){
                ajaxService.addComment({
                    case_id : scope.inspectionCase.id,
                    comment : scope.comment
                }, "Adding comment. Please wait!").then(function(data){
                    alert("Comment added");
                    scope.comment = null;
                    scope.getComments()
                }, function(err){
                    console.log(err);
                    alert("Unable to add comment. Please try again later.");
                });
            };
            
            
            scope.getComments();
        }
    };
}]);
