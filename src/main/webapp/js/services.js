/* global angular */

angular.module('jaduApp').factory('ajaxService', ["$http", "$q", function ($http, $q) {

        var globalData = {
            purposeOfInspections: null
        };
        var globalData = {
            purposeOfSurvey: null
        };

        function sendRequest(method, url, params, spinnerText) {
            if (spinnerText === 'refresh') {
                return $http({
                    method: method,
                    url: url,
                    params: params,
                    type: 'refresh'
                });
            } else {
                return $http({
                    method: method,
                    url: url,
                    params: params,
                    spinnerText: spinnerText
                });
            }
        }

        function updateRequest(method, url, data, spinnerText) {
            return $http({
                method: method,
                url: url,
                data: data,
                spinnerText: spinnerText
            });
        }

        function sendRefresh(method, url, params) {
            sendRequest(method, url, params, 'refresh');
        }

        function sendRequestFile(method, url, params, spinnerText) {
            return $http({
                method: method,
                url: url,
                data: params,
                spinnerText: spinnerText,
                headers: {'Content-Type': undefined},
                // prevents serializing payload. don't do it.
                transformRequest: angular.identity
            }).then(function (result) {
                // resolve the promise as the data
                return result.data;
            });
        }

        return {
            getAllocationRequestedCases: function (params, spinnerText) {
                return sendRequest("GET", "qc/cases/address-requested-cases", params, spinnerText);
            },
            getAddressRequests: function (params, spinnerText) {
                return sendRequest("GET", "qc/cases/address-requested-cases", params, spinnerText);
            },
            getInspectorsInRadius: function (params, spinnerText) {
                return sendRequest("GET", "qc/get-inspectors-in-radius", params, spinnerText);
            },
            assignCaseToInspector: function (params, spinnerText) {
                return sendRequest("POST", "qc/assign-case-to-inspector", params, spinnerText);
            },
            getQCQuestions: function (params, spinnerText) {
                return sendRequest("GET", "qc/get-vehicle-types", params, spinnerText);
            },
            getQCCases: function (params, spinnerText) {
                return sendRequest("GET", "qc/cases/qc-cases", params, spinnerText);
            },
            getAllBranches: function (data, spinnerText) {
                return sendRequest("GET", "util/get-branches-by-user", data, spinnerText);
            },
            getVehicleTypes: function (data, spinnerText) {
                return sendRequest("GET", "util/get-vehicle-types", data, spinnerText);
            },
            getVehicleFuelTypes: function (data, spinnerText) {
                return sendRequest("GET", "util/get-vehicle-fuel-types", data, spinnerText);
            },
            getVehicleMakeModels: function (data, spinnerText) {
                return sendRequest("GET", "util/get-vehicle-make-models", data, spinnerText);
            },
            getVahanDetails: function (data, spinnerText) {
                return sendRequest("GET", "util/get-vahan-details", data, spinnerText);
            },
            getCostConfigurationRecord: function (data, spinnerText) {
                return sendRequest("POST", "claims/cost-configuration-record", data, spinnerText);
            },
            sendClaimCaseImage:function(data,spinnerText){
                return sendRequest("POST",data,spinnerText);
            },
            updateCaseVehicleDetails: function (caseId, data, spinnerText) {
                return updateRequest("POST", "claims/vehicle-details?caseId=" + caseId, data, spinnerText);
            },
            updateCustomerPhoneNumber: function (params, spinnerText) {
                return sendRequest("POST", "qc/update-customer-phone-number", params, spinnerText);
            },
            updateAgentPhoneNumber: function (params, spinnerText) {
                return sendRequest("POST", "qc/update-inspector-phone-number", params, spinnerText);
            },
            updateVehicleNumber: function (params, spinnerText) {
                return sendRequest("POST", "qc/update-vehicle-number", params, spinnerText);
            },
            getPurposeOfSurvey: function (params, spinnerText) {
                if (globalData.purposeOfSurvey)
                    return globalData.purposeOfSurvey;
                return globalData.purposeOfSurvey = sendRequest(
                        "GET", "util/get-purpose-of-survey", params, spinnerText);
            },
            getBranches: function (params, spinnerText) {
                return  sendRequest(
                        "GET", "util/get-branches-by-company", params, spinnerText);
            },
            getCasePhotos: function (params, spinnerText) {
                return sendRequest("GET", "qc/cases/get-case-photos", params, spinnerText);
            },
            getCaseQuestions: function (params, spinnerText) {
                return sendRequest("GET", "qc/cases/get-qc-questions", params, spinnerText);
            },
            getClaimsCaseQuestions: function (params, spinnerText) {
                return sendRequest("GET", "qc/survey/cases/get-qc-questions", params, spinnerText);
            },
            getCaseDetails: function (params, spinnerText) {
                return sendRequest("GET", "qc/cases/case-details", params, spinnerText);
            },
            getClaimsCaseDetails: function (params, spinnerText) {
                return sendRequest("GET", "qc/survey/cases/case-details", params, spinnerText);
            },
            getQuestionOptions: function (params, spinnerText) {
                return sendRequest("GET", "qc/cases/get-qc-options", params, spinnerText);
            },
            getCaseRequests: function (params, spinnerText) {
                return sendRequest("GET", "qc/cases/requested-cases", params, spinnerText);
            },
            getEstimatedPrices: function (params, spinnerText) {
                return sendRequest("POST", "claims/loss-assessement-details", params, spinnerText);
            },
            getInsuranceDetails: function (params, spinnerText) {
                return sendRequest("POST", "claims/insurance-details", params, spinnerText);
            },
            getVehicleDetails: function (params, spinnerText) {
                return sendRequest("POST", "claims/vehicle-details", params, spinnerText);
            },
            getDriverDetails: function (params, spinnerText) {
                return sendRequest("POST", "claims/driver-details", params, spinnerText);
            },
            getScheduledCases: function (params, spinnerText) {
                return sendRequest("GET", "qc/survey/cases/scheduled-cases", params, spinnerText);
            },
            getScheduledCasesAgent: function (params, spinnerText) {
                return sendRequest("GET", "survey/cases/scheduled-cases", params, spinnerText);
            },
            getCompletedCases: function (params, spinnerText) {
                return sendRequest("GET", "qc/cases/completed-cases", params, spinnerText);
            },
            getAllCases: function (params, spinnerText) {
                return sendRequest("GET", "qc/cases/all-cases", params, spinnerText);
            },
            getAllClaimCases: function (params, spinnerText) {
                return sendRequest("GET", "qc/survey/cases/all-cases", params, spinnerText);
            },
            getAllCasesAgent: function (params, spinnerText) {
                return sendRequest("GET", "agent/cases/all-cases", params, spinnerText);
            },
            getRequestedCases: function (params, spinnerText) {
                return sendRequest("GET", "search/cases?" + params.field + "=" + params.value);
            },
            getCancelledCases: function (params, spinnerText) {
                return sendRequest("GET", "qc/cases/cancelled-cases", params, spinnerText);
            },
            getClosedCases: function (params, spinnerText) {
                return sendRequest("GET", "qc/cases/closed-cases", params, spinnerText);
            },
            getAgentByPhoneNumber: function (phone_number, spinnerText) {
                return sendRequest("GET", "util/get-agent-by-phone-number", {phone_number: phone_number}, spinnerText);
            },
            getUserByPhoneNumber: function (phone_number, spinnerText) {
                return sendRequest("GET", "util/get-user-by-phone-number", {phone_number: phone_number}, spinnerText);
            },
            createCase: function (data, spinnerText) {
                return sendRequest("POST", "qc/create-case", data, spinnerText);
            },
            createCaseAgent: function (data, spinnerText) {
                return sendRequest("POST", "survey/create-case", data, spinnerText);
            },
            sendInspectionReport: function (data, spinnerText) {
                return sendRequest("POST", "qc/cases/submit-qc-report", data, spinnerText);
            },
            getQCAnswers: function (data, spinnerText) {
                return sendRequest("GET", "qc/cases/qc-answers", data, spinnerText);
            },
            getBCData: function (data, spinnerText) {
                return sendRequest("GET", "bc/get-public-blockchain", data, spinnerText);
            },
            uploadFileGetMD5: function (data, spinnerText) {
                return sendRequest("POST", "bc/upload-file-get-md5", data, spinnerText);
            },
            checkAccessTokenValidity: function (spinnerText) {
                return sendRequest("GET", "user/check-access-token-validity", {}, spinnerText);
            },
            refreshAccessToken: function (data, spinnerText) {
                return sendRequest("POST", "oauth/token", data, spinnerText);
            },
            updateInspectionTime: function (data, spinnerText) {
                return sendRequest("POST", "oauth/token", data, spinnerText);
            },
            getMakeModelVariants: function (data, spinnerText) {
                return sendRequest("GET", "claims/config/make-model-variant", data, spinnerText);
            },
            getVehicleBodyTypes: function (data, spinnerText) {
                return sendRequest("GET", "claims/vehicle-body-types", data, spinnerText);
            },
            getVehicleClassTypes: function (data, spinnerText) {
                return sendRequest("GET", "claims/vehicle-class-types", data, spinnerText);
            },
            getVehiclePermitTypes: function (data, spinnerText) {
                return sendRequest("GET", "claims/vehicle-permit-types", data, spinnerText);
            },
            getDriverLicenseTypes: function (data, spinnerText) {
                return sendRequest("GET", "claims/driver-license-types", data, spinnerText);
            },
            getThirdPartyDamageTypes: function (data, spinnerText) {
                return sendRequest("GET", "claims/third-party-damage-types", data, spinnerText);
            },
            getVehicleLabourCost: function (data, spinnerText) {
                return sendRequest("GET", "claims/config/vehicle-labour-cost", data, spinnerText);
            },
            getVehiclePartCost: function (data, spinnerText) {
                return sendRequest("GET", "claims/config/vehicle-part-cost", data, spinnerText);
            },
            getVehicleRepairType: function (data, spinnerText) {
                return sendRequest("GET", "claims/config/vehicle-repair-type", data, spinnerText);
            },
            getVehicleDamageType: function (data, spinnerText) {
                return sendRequest("GET", "claims/config/vehicle-damage-type", data, spinnerText);
            },
            getVehiclePartType: function (data, spinnerText) {
                return sendRequest("GET", "claims/config/vehicle-part-type", data, spinnerText);
            },
            getVehiclePartSide: function (data, spinnerText) {
                return sendRequest("GET", "claims/config/vehicle-part-side", data, spinnerText);
            },
            getVehicleParticularType: function (data, spinnerText) {
                return sendRequest("GET", "claims/config/vehicle-part-particular-type", data, spinnerText);
            },
            getClaimsConfigDetails: function (data, spinnerText) {
                return sendRequest("GET", "claims/config-details", data, spinnerText);
            },
            getReportPoliceItems: function (data, spinnerText) {
                return sendRequest("GET", "claims/report-to-police-items", data, spinnerText);
            },
            getPunchnamaCarriedOutItems: function (data, spinnerText) {
                return sendRequest("GET", "claims/punchnama-carried-out-items", data, spinnerText);
            },
            updateClaimsData: function (data, spinnerText) {
                return updateRequest("POST", "claims/update-claim-case", data, spinnerText);
            },
            updateClaimsInsuranceDetails: function (caseId, data, spinnerText) {
                return updateRequest("POST", "claims/insurance-details?caseId=" + caseId, data, spinnerText);
            },
            updateClaimsAccidentDetails: function (caseId, data, spinnerText) {
                return updateRequest("POST", "claims/accident-details?caseId=" + caseId, data, spinnerText);
            },
            updateClaimsLossAssessmentDetails: function (caseId, data, spinnerText) {
                return updateRequest("POST", "claims/loss-assessement-details?caseId=" + caseId, data, spinnerText);
            },
            updateClaimsDriverDetails: function (caseId, data, spinnerText) {
                return updateRequest("POST", "claims/driver-details?caseId=" + caseId, data, spinnerText);
            },
            getAssessmentSummary: function (caseId, data, spinnerText) {
                return sendRequest("GET", "claims/assessment-summary?caseId=" + caseId, spinnerText);
            },
            updateAssessmentSummary: function (caseId, data, spinnerText) {
                return updateRequest("POST", "claims/assessment-summary?caseId=" + caseId, data, spinnerText);
            },
            getNearbyAgents: function (data, spinnerText) {
                // return sendRequest("GET", "util/get-nearby-agents", data,
                // spinnerText);
                return new Promise(function (resolve, reject) {
                    var dummyData = [
                        {
                            id: 1,
                            firstName: 'fname1.1',
                            lastName: 'lname1.2',
                            contactNumber: 98989898989,
                            location: {
                                lat: 75,
                                long: 34
                            },
                            status: 'active',
                            lastInspectionTime: 1524127300
                        },
                        {
                            id: 2,
                            firstName: 'fname2.1',
                            lastName: 'lname2.2',
                            contactNumber: 98989898989,
                            location: {
                                lat: 74,
                                long: 34
                            },
                            status: 'inactive',
                            lastInspectionTime: 1524127300
                        },
                        {
                            id: 3,
                            firstName: 'fname3.1',
                            lastName: 'lname3.2',
                            contactNumber: 98989898989,
                            location: {
                                lat: 75,
                                long: 34
                            },
                            status: 'active',
                            lastInspectionTime: 1524127300
                        },
                        {
                            id: 4,
                            firstName: 'fname4.1',
                            lastName: 'lname4.2',
                            contactNumber: 98989898989,
                            location: {
                                lat: 72,
                                long: 33
                            },
                            status: 'active',
                            lastInspectionTime: 1524127300
                        },
                        {
                            id: 5,
                            firstName: 'fname5.1',
                            lastName: 'lname5.2',
                            contactNumber: 98989898989,
                            location: {
                                lat: 76,
                                long: 37
                            },
                            status: 'active',
                            lastInspectionTime: 1524127300
                        }
                    ];
                    resolve(dummyData);
                });

            },
            schdeuleCase: function (data, spinnerText) {
                return sendRequest("POST", "util/submit-address-qc", data, spinnerText);
            },
            rescheduleCase: function (data, spinnerText) {
                return sendRequest("POST", "qc/update-inspection-time", data, spinnerText);
            },
            closeCase: function (data, spinnerText) {
                return sendRequest("POST", "qc/close-case", data, spinnerText);
            },
            cancelCase: function (data, spinnerText) {
                return sendRequest("POST", "qc/cancel-case", data, spinnerText);
            },
            allotCaseManual: function (data, spinnerText) {
                return sendRequest("POST", "qc/cases/allot-user-manual", data, spinnerText);
            },
            getPhotoTypes: function (data, spinnerText) {
                return sendRequest("GET", "util/get-claims-photo-types", data, spinnerText);
            },
            getClaimsPhotoTypes: function (data, spinnerText) {
                return sendRequest("GET", "claims/claims-photos", data, spinnerText);
            },
            getInsuranceCompanies: function (data, spinnerText) {
                return sendRequest("GET", "util/get-insurance-companies-auth", data, spinnerText);
            },
            getBranchDivisionByCompany: function (company, spinnerText) {
                return sendRequest("GET", "util/get-branch-division-by-company-auth", {company: company}, spinnerText);
            },
            getDashboardStats: function (data, spinnerText) {
                return sendRequest("POST", "admin/get-dashboard-stats", data, spinnerText);
            },
            getDashboardTableFields: function (data, spinnerText) {
                return sendRequest("GET", "admin/get-dashboard-table-fields", data, spinnerText);
            },
            getDashboardTableData: function (data, spinnerText) {
                return sendRequest("POST", "admin/get-dashboard-table-data", data, spinnerText);
            },
            createAgent: function (data, spinnerText) {
                return sendRequest("POST", "admin/create-agent-without-kyc", data, spinnerText);
            },
            getReqAgentsDetails: function (params, spinnerText) {
                return sendRequest("GET", "search/agents?" + params.field + "=" + params.value);
            },
            getAllAgents: function (data, spinnerText) {
                return sendRequest("GET", "admin/get-agents", data, spinnerText);
            },
            getAllAgentsByCompany: function (data, spinnerText) {
                return sendRequest("GET", "admin/get-agents-by-company", data, spinnerText);
            },
            getMappedAgents: function (data, spinnerText) {
                return sendRequest("GET", "util/get-agent-by-company", data, spinnerText);
            },
            getTopAgents: function (data, spinnerText) {
                return sendRequest("GET", "admin/get-top-agents", data, spinnerText);
            },
            getAllAgentList: function (data, spinnerText) {
                return sendRequest("GET", "admin/get-all-agents", data, spinnerText);
            },
            getProfileDetails: function (data, spinnerText) {
                return sendRequest("GET", "user/profile-details", data, spinnerText);
            },
            getConfigurations: function (data, spinnerText) {
                return sendRequest("GET", "qc/get-configurations", data, spinnerText);
            },
            subscribeWebDevice: function (data, spinnerText) {
                return sendRequest("POST", "user/subscribe-web-device", data, spinnerText);
            },
            updateEnabled: function (data, spinnerText) {
                return sendRequest("POST", "admin/update-enabled", data, spinnerText);
            },
            updateFirstName: function (data, spinnerText) {
                return sendRequest("POST", "admin/update-firstName", data, spinnerText);
            },
            updateLastName: function (data, spinnerText) {
                return sendRequest("POST", "admin/update-lastName", data, spinnerText);
            },
            updateAgentCode: function (data, spinnerText) {
                return sendRequest("POST", "admin/update-agentCode", data, spinnerText);
            },
            updateBranch: function (data, spinnerText) {
                return sendRequest("POST", "admin/update-branch", data, spinnerText);
            },
            updateEmail: function (data, spinnerText) {
                return sendRequest("POST", "admin/update-email", data, spinnerText);
            },
            updatePhoneNumber: function (data, spinnerText) {
                return sendRequest("POST", "admin/update-phone-number", data, spinnerText);
            },
            updateDeviceId: function (data, spinnerText) {
                return sendRequest("POST", "admin/update-device-id", data, spinnerText);
            },
            updateDeviceLocked: function (data, spinnerText) {
                return sendRequest("POST", "admin/update-device-locked", data, spinnerText);
            },
            resetPassword: function (data, spinnerText) {
                return sendRequest("POST", "admin/reset-password", data, spinnerText);
            },
            getBucketObjects: function (spinnerText) {
                return sendRequest("GET", "qc/get-bucket-objects", {}, spinnerText);
            },
            manualQC: function (data, spinnerText) {
                return sendRequest("POST", "qc/manual-qc", data, spinnerText);
            },
            getUnexpiredOTP: function (data, spinnerText) {
                return sendRequest("POST", "admin/get-unexpired-otp", data, spinnerText);
            },
            getInspectionTypes: function (spinnerText) {
                return sendRequest("GET", "qc/get-inspection-types", spinnerText);
            },
            submitNewVehicle: function (spinnerText) {
                return sendRequest("POST", "qc/vehicle", spinnerText);
            },
            toggleVehicle: function (spinnerText) {
                return sendRequest("POST", "qc/vehicle/toggle", spinnerText);
            },
            getComments: function (data, spinnerText) {
                return sendRequest("GET", "qc/cases/comments", data, spinnerText);
            },
            agentComments: function (data, spinnerText) {
                return sendRequest("GET", "qc/agents/comments", data, spinnerText);
            },
            addComment: function (data, spinnerText) {
                return sendRequest("POST", "qc/cases/comments", data, spinnerText);
            },
            uploadInspectionFile: function (data, spinnerText) {
                return sendRequestFile("POST", "qc/cases/inspection", data, spinnerText);
            },
            reupload: function (data, spinnerText) {
                return sendRefresh("POST", "/test/do-qc", {case_id: data}, spinnerText);
            },
            reopenCase: function (data, spinnerText) {
                return sendRequest("POST", "qc/case/reopen", data, spinnerText);
            },
            reopenCaseQC: function (data, spinnerText) {
                return sendRequest("POST", "qc/case/reopen-qc", data, spinnerText);
            },
            replacePhoto: function (data, spinnerText) {
                return sendRequestFile("POST", "qc/replace-photo", data, spinnerText);
            },
            addPhoto: function (data, spinnerText) {
                return sendRequestFile("POST", "qc/add-photo", data, spinnerText);
            },
            downloadUploadedFile: function (data) {
                return sendRequestFile("GET", "qc/cases/download-uploaded-zip/" + data, {}, "");
            },
            retagPhotos: function (data, spinnerText) {
                return sendRequest("POST", "qc/cases/retag-photos", data, spinnerText);
            },
            moveToQC: function (data, spinnerText) {
                return sendRequest("POST", "qc/survey/move-to-qc", data, spinnerText);
            },
            bulkUploadPhoto: function (data, spinnerText) {
                return sendRequestFile("POST", "qc/cases/bulk-upload", data, spinnerText);
            },
            claimBulkUploadPhoto: function (data, spinnerText) {
                return sendRequestFile("POST", "qc/claims/cases/bulk-upload", data, spinnerText);
            },
            sendReport: function (data, spinnerText) {
                return sendRequest("GET", "qc/send-report", data, spinnerText);
            },
            deletePhoto: function (data, spinnerText) {
                return sendRequest("POST", "qc/cases/delete-case-photo", data, spinnerText);
            },
            updateCustomerName: function (data, spinnerText) {
                return sendRequest("POST", "qc/cases/update-customer-name", data, spinnerText);
            },
            reminder: function (data, spinnerText) {
                return sendRequest("POST", "reconcile/case/reminder", data, spinnerText);
            },
            fetchReportDetails: function (data, spinnerText) {
                return sendRequest("GET", "qc/report-data", data, spinnerText);
            }
        };
    }]);

angular.module('jaduApp').factory('authService', ["$http", "$q", 'ajaxService', function ($http, $q, ajaxService) {
        var authTokens = {
            access_token: localStorage.getItem("access_token") ? localStorage.getItem("access_token") : undefined,
            refresh_token: localStorage.getItem("refresh_token") ? localStorage.getItem("refresh_token") : undefined
        };

        return {
            isAuthenticated: function () {
                return ajaxService.checkAccessTokenValidity();
            },
            getAccessToken: function () {
                return localStorage.getItem("access_token");
                // return authTokens.access_token;
            },
            refreshAccessToken: function () {
                return ajaxService.refreshAccessToken({
                    grant_type: "refresh_token",
                    refresh_token: localStorage.getItem("refresh_token"),
                    client_id: "jadu-android-app"
                }, 'Refreshing App. Please Wait!');
            },
            setAccessToken: function (res) {
                localStorage.setItem("access_token", res.data.access_token);
                localStorage.setItem("refresh_token", res.data.refresh_token);

                authTokens.access_token = res.data.access_token;
                authTokens.access_token = res.data.refresh_token;
            }
        };
    }]);

angular.module('jaduApp').service('fcm', function () {

    var self = this;
    var messaging = "";
    self.init = function (config) {

        try {
            firebase.initializeApp(config);
            // console.log('app initialized');
        }
        catch (err) {
        }
        ;
        messaging = firebase.messaging();

        return messaging;
    };

    self.requestPermission = function () {
        // console.log('fired');
        return messaging.requestPermission()
                .then(function () {
                    return 1; // permisiion granted
                })
                .catch(function (err) {
                    return err; // permission denied
                });
    };

    self.requestToken = function () {
        return messaging.getToken()
                .then(function (currentToken) {
                    if (currentToken) {
                        // console.log('currentToken',currentToken);
                        return currentToken;
                    } else {
                        // Show permission request.
                        console.log('No Instance ID token available. Request permission to generate one.');
                        return 0;
                    }
                })
                .catch(function (err) {
                    console.log('An error occurred while retrieving token. ', err);
                });
    };

    self.tokenRefresh = function () {
        // [START refresh_token]
        // Callback fired if Instance ID token is updated.
        return  messaging.onTokenRefresh(function () {
            messaging.getToken().then(function (refreshedToken) {
                console.log('Token refreshed.');
                // Indicate that the new Instance ID token has not yet been sent
                // to the
                // app server.
                setTokenSentToServer(false);
                // Send Instance ID token to app server.
                sendTokenToServer(refreshedToken);
                // [START_EXCLUDE]
                // Display new Instance ID token and clear UI of all previous
                // messages.
                resetUI();
                // [END_EXCLUDE]
            }).catch(function (err) {
                console.log('Unable to retrieve refreshed token ', err);
                showToken('Unable to retrieve refreshed token ', err);
            });
        });
        // [END refresh_token]
    };



});


angular.module('jaduApp').service('gmap', function () {

    var self = this;
    var map = null;
    var marker = null;
    var cityCircle = null;
    var radius = null;
    var myLatlng = {
        lat: null,
        lng: null
    };
    var geocoder = new google.maps.Geocoder;

    navigator.geolocation.getCurrentPosition(function (pos) {
        myLatlng.lat = parseFloat(pos.coords.latitude);
        myLatlng.lng = parseFloat(pos.coords.longitude);
    },
            function (e) {
                console.log('Unable to fetch Location', e);
            },
            {
                timeout: 1000,
                enableHighAccuracy: true
            });

    this.initMap = function (element, lat, lng) {
        if (lat !== null)
            myLatlng.lat = lat;
        if (lng !== null)
            myLatlng.lng = lng;

        map = new google.maps.Map(element, {
            center: myLatlng,
            zoom: 10
        });

        marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Case Location',
            label: 'X',
            draggable: true,
            animation: google.maps.Animation.DROP
        });

        return map;
    };

    this.addInputField = function (element, map) {
        var searchBox = new google.maps.places.SearchBox(element);

        marker.addListener('dragend', function (evt) {
            myLatlng.lat = evt.latLng.lat();
            myLatlng.lng = evt.latLng.lng();

            reverseGeocode(myLatlng, element);

        });

        reverseGeocode(myLatlng, element);

        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length === 0) {
                return;
            }

            var bounds = new google.maps.LatLngBounds();

            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }

                myLatlng.lat = place.geometry.location.lat();
                myLatlng.lng = place.geometry.location.lng();

                self.addCirlce(map, myLatlng.lat, myLatlng.lng, radius);
                map.panTo(place.geometry.location);
                map.setZoom(9);
                marker.setPosition(place.geometry.location);

                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                }
                else {
                    bounds.extend(place.geometry.location);
                }
            });

            map.fitBounds(bounds);
        });
    };

    this.addCirlce = function (map, lat, lng, r) {
        if (r !== null) {
            radius = r;
        }
        else
        if (r === null) {
            r = radius;
        }
        if (cityCircle !== null) {
            cityCircle.setMap(null);
        }
        cityCircle = new google.maps.Circle({
            strokeColor: '#69f0ae',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#ffff00',
            fillOpacity: 0.25,
            map: map,
            center: {lat: lat, lng: lng},
            radius: r * 1000
        });
    };

    this.getCurrentPosition = function () {
        return myLatlng;
    };

    function reverseGeocode(myLatlng, element) {
        geocoder.geocode({'location': myLatlng}, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    element.value = results[0].formatted_address;
                }
                else {
                    console.log('No results found for reverse geocode');
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
    }
});
