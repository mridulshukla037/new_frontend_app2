function distanceBetweenLatLongs(lat1, lon1, lat2, lon2, unit) {
	var radlat1 = Math.PI * lat1/180;
	var radlat2 = Math.PI * lat2/180;
	var theta = lon1-lon2;
	var radtheta = Math.PI * theta/180;
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	dist = Math.acos(dist);
	dist = dist * 180/Math.PI;
	dist = dist * 60 * 1.1515;
	if (unit==="K") { dist = dist * 1.609344 };
	if (unit==="N") { dist = dist * 0.8684 };
	return dist;
}


var downloadImagePDF = function($scope){
    	generateHeader = {
    	      	margin: 30,
    	          columnGap: 20,
    	          columns: [
    	          	{
    	              	 image:'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAASABIAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx7/2wBDAQUFBQcGBw4ICA4eFBEUHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh7/wAARCACnAMgDASIAAhEBAxEB/8QAHQABAAIDAAMBAAAAAAAAAAAAAAgJBQYHAgMEAf/EAEgQAAECBQEEBgYFCwMCBwAAAAECAwAEBQYRBwgSITETQVFhcYEUIjJCkdEVFlWUoRgjJDNSU1ZykpOxQ4LBCSUXJlRjoqTh/8QAGwEBAAIDAQEAAAAAAAAAAAAAAAMEAQIFBgf/xAAsEQACAgIBAgQGAwADAAAAAAAAAQIDBBESEyEFMUFRBhQiQmGBMnHhFZGh/9oADAMBAAIRAxEAPwCZcIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBGv6i3ZTbIsup3PVlhMtIsle7nBcV7qB3k4EbBEKdubUduvXLLadUp7ekqUsTFUWk+qt7Hqt/7RxPeYkqrc5JI1lLitmpPbT2sD8w9MMzlKl2nllbbKmMlpJPBOfCPH8prWX7SpP3eOPEknPbHipbafadbHioR1lj1r0KnVkdj/Ka1l+0qT93h+U1rL9pUn7vHHAtsjIdb/qEfu+3+9b/qEOhX7DqzOxflNay/aVJ+7w/Ka1l+0qT93jju8j943/UI/QUnktJ8DGehX7DqyOyyu1Bq7KzsvNzc3S5mVYdSt9hLGC6jPrJz1cInVZlwSF1WrTbipjgclJ+XS82QeWRxB7weEVYDgeIyOuJZ7A9//o9Q0zqLxK5YqnKWVH2mifXQPA8fMxUyqFGPKKJarG3pks4QhHPLAhCEAIQhACEIQAhHyTFUpks+WJioSrLoGShbyUqA8CY9ZrVHAJNVkcD/AN9PzjOmD74Ri/rHb/23TfvKPnH4q5bdScKrtMB75pHzhpgysIxP1mtz7epn3pHzjweuu2GUb7txUlCe1U2gD/MNMbMzCMD9c7R3kpFz0YqUoJSPTW8knkBxjO5G7vZGMZzDTQNE141BlNNtN6jcTykKmwjoZFkni6+rgkY68cz3CK2H35qbmX52feU/Ozbqn5lxRyVuKOTHXNrHUleoGpr1OkpjfoFvrUxLBJyl5/ktzvxyHhHICpKQpazhKRlR7o6mJVwjyfqVLp7ejMWTbdSvG8aValIBE3UXgguAfqWh7az4DMTto+zVpBISbDTlqszbzbYQt95xRU4rHFR44yY0DYX0zcpdFmNR61KhE7VkdHTULHFqW/a7io/gO+JQRUyL3KWovsTVQ4rucke2cNG3XS4qzZYE9SXFgfDMet3Zp0aW2UfVBpOetLywf8x1+PwEHkQYg5y9yTSON/kx6M/wt/8AZX84wV7bKum01bVRTbVNeptYLRVJPiYUoIcAyAQTjBPA+MSChGVbNPexxRU28zMy0w/JzzKmZyVdUxMtqGChxJwYylmXJPWbeFJu2mk+kUt8OKSDjpGuS0HuIzHaduCwvq1qNL3nIshFNuBPRzW6OCJpI5n+YY8wY4ByOFDI5ER1q5K2vuUpJwkWpWjXZC57Zp1fpjodlJ+XS+0oHqIzjxHKMpETdgnUEql5/TKou5clAZymKUfaaJ9dHkTnwJiWUciyDhJpl2L2tiEIRoZEIQgBGDv65qfZ1nVS5am4luWkJdTqsn2iBwSO8nA84zkQv27dRVVa4pXTWnPfoUhuzdVKT7bvuNnwHHxPdElVbnJJGspcVsjtc1bqd13FULmrE1Mqnqi+p5QDqgEJJ9VI7gIx3RH/ANRN/wB9Ue0kqPjHukpCqT6FOU6jVKeaScF2Xl1LTnxAjtKMYrRS5SbPj6Bv9t/+8qPxUqwrivpVHvdJjKfQdx/wvXPuS/lHsat26XU77Vp1tac4z6KocfhGNxM6mYb0OW/YX/cMfhkpU8C2pXcVkxnPqzdv8H1z7sr5Q+rN4FSUN2hW+lcIQ2DLKwVHgOqG4IamdE2RdMJS+NVkT03J71FoATMTOclLr3+mjyPEjuiUm13qadP9OVU+mLT9OVzelJMA8WkEYW55A4HeYzOz3Y0ppVpDLStR6NidU0Z6rPqP+oRlWT2JHDyiD+t9/wA1qVqRULiccUqnMrVK0pvkEMg+1jtVzjnxXXt36FhvpwNIbbDLSWgoq3eZPvHrMbtojYb2pOplNtgBQkEETVTcA9lhJzu/7jw840l1wNNKdUN7d5J7T1CJ/wCyNpgiwNOmqjUWR9YK2lM1OrI9ZtJGUNd2AePeYtZNvThpENMeUts7HJSzElJsycq0lphhtLbaEjASkDAA8o0zVjUGXsuTZYYYE5VZoHoGCcJSBzWo9kbzEbdott5Go7a3TlDsknoeHUCciMeC4leVlKFvlpv+znfEWfbg4Mraf5dlv2/Jr9b1CvKqOremq67LIVw6OW9RIHYO2MSxcFwMOpLVdqjS+Y3nFDPxj79PanRqNd0tUK/JelyKEkbu7vdGvqWR14jtt0TOnN+0UUxNakWH18Zd9GEONK88fCPX5ORXhTjWqPofm0uy/wDDwGDi3+JVSueVqxeSb/00DTrViuUyry0lcUz6fTH1hsvKGHGCTwOesRIpJCkhSTkEZBji3/gRS5iVSWbnm15A/OJSkgntGI7FTZYydPl5RTy3iy2lsuK5qwMZMeV8aswrZRnjefqta/Z7n4eq8RorlXm916Pe/wBGna7WLL6h6Y1a3HUJMytouyazzQ+nigjz4ecVpqbmGHHZWcbU1NSzimJhChgpWk4OYtliC+29p+m19QZe8qcwG6ZcH5uaCRwbmkjn3bw4+IMUMO3jLi/U7t0NrZxezbkn7Nu6lXbTCfSaW+HFIBx0rXvoPiMxZ3aNdkbntinXBTXA5KT8uh9sg5wFDOPEcvKKr+RwRnqIiWOwRqAeiqOmlSfJVLZnKXvHm0T66B4Hj5mJsyra5o0on9pLSEIRzSyIQhAGqatXnI2Bp/VbonlJxKMnoUE8XHTwQkeJxFZdSqE/WKpOVqquqen6g8qYmFqOTlRzjwEd8239RPrNfLNjU2YC6VQlB2d3DwcmjySf5R+JMR95kknHWTHUw6uMeT9SrfPb0fZb9FqFy3DTbapLZXPVSYTLtAe6CfWUe4DJizqwbUpdnWhTLcpjDaZeRl0tb26MrUBxUe8nJiM2wbpyV+l6n1Rn9aFSlJSoey2Dhbg8Tw+MS4irlW85aXkiWqHGJ4dE1+7R/SIBpocm0f0iPOEVSU8ejb/dp+EOjb/dp+EeUIAjlt43bV6JpxJ2/TmXmpeuzHQTc6k4S22OJb8Vf4zEJt0JAQkYSkboHdFmGuFjS2ommdWth/CXnmuklXMcW3k8UH4jHgTFaLzE1KTD8jPNKZnJR1UvMNqHFK0nBjpYUo8WvUq3p+Z1XZQsim31rDLs1d9kSVFbE8ZVZG9NLB9UAdaQeJiw8AAAAYAirCybmnbLvKk3dTiov0x8LcQDjpWjwWg9xGYs+tmsyNw2/IVymvIek55hLzS0nIIUM/8A5EOZFqe2SUtcTIxrGoVlUm9KYmWnwtp9k70vMt+20f8Akd0bPEddcZ256HqC76PW6jLyU6ylyXShwhAI4KSIn8JxrMjISqnxku6ZzfHMyrExHO6vnF9mj5q9o5d1PUpUiqWqbXUUncV8DGkV63qvRFhNao0xJAq3Q4tHqE/zDhG6aXakT1Ar7q7jqM7PU6ZQEqUtRWWFD3gOyN61C1Us2ctmap8kBWH5lstpaLZCEkj2lE8sc49asrxLHvjVOHNP1S1/nY8H/wAf4Nl40siqzpteje/9OO2vdtxWrMB+j1B3oknK5R1RU04OsYPLxESqtKtMXDbklWZZJSiaaC9080nrHkYhwfzMtlZzupxntMSt0dpczR9OqVKTaFNvlvpFoVzTvHOIp/E9FKqjalqW9f2i/wDBOXkTtnTJtwS/6Zt0aTrfYsrqJprVbafQkvutFyUWebb6eKFZ6uPDzjdo5DtW6lDTvTOY9AeCa7VcylOQD6wJHrOf7Qc+OI8bFNtaPoj8ivqZl5qSm5inz7fRzsm6qXmUZzhaTgxkLTuOoWfdVLuulKKZulvh3GeDjfvoPcRmMWkKSDvrU44olTjijkrUeJJMbRpVZM7qLqBTbQkt5Lcwrpp94D9TLpI3j4nkPGO1PSrfIox/l9JZVZ9ekbotenXDTF78pPy6X2z2Ajl5HhCPooVLkaJRpOkU1hLEnJspZZbSOCUpGBCOI/wXz7Y0XXe/JbTrTOq3G6oekpbLUk3nit9XBAHnx8o3qIEbZeoSrx1PNuSMz0lFt07hCVZQ7NH2j37vL4xJTX1JpGk5cVs4it2Zfddmp11Ts3MuKfmHFHJUtRyYy9j2tUb3vKl2jSkkzFRdCXFjk0yOK1nuxmMOSACpRwkDKj3RMfYS05NNt2a1Fq0sUz9XHRSAWOLUqDzHZvH8AI6d9iqh2Ktcects6jet9WVoTZ9Bp1UYnGaaECUlvRZffAKE9eORPONKoG1np5WLip9GYp1bacnplMu245LgJBUcJJ45xkiPft2UgT+hr88hgrep020+lY9xOcK/zEF2ZxyTnKfUmlkLlptp9JHMEKBipTRGyDk/MnnNxaRa3U56WptNmajNr6OXlmlOuq7EpGT/AIiO42xdNzvf9rr+AogES6TkDr5xu20ldJo+znWash1KHp6noZaJPNToA4d+CYrwl8syzLQCjhI3sDO73nsjGNjqxNyFlnHyLENEdeLX1VrlQo9JkZ+Rm5NkP7s0kDpEZxkY7MiMvrjqxRNJ6LIVOtSc3NiemOgablgCrIGSTnqxEMdkitCi7QlGUteG6iw5KK48yRw/ER0v/qHTyV3BZtLSoFQQ9MKAPEDIA4fGEsdK5Q9DKs3DkdS0w2lba1AvyStOj2/WUPzSFr6ZxKQlsJGSVceXfHEduPT5Nt31K3vTmNynV09FO7o9VEyBwUf5h/gx69gylLn9YqvVzjcp1O3ATz3lnGB8Il3q1ZNO1CsCp2rUQEpm2vzLuMll0cULHgf+YxJqi36Qlzh3KxeRIIz1ERKvYN1F6IzmmNVmCdzem6QVnmg8VtjwPH4xF+u0mo2/Xahb1YZLVRpj6pd9J68clDuIwfOP2gVqpW3XpC46M6pqo014PskH2gD6yD3EZEXroK2vsV65OEu5azGCvW06Pd1K+j6uwVBJ3mnUHC2ldqTHp01u6mX1ZNMuekupXLzrIUoA8W1+8g9hByI2OOVCydU1KD00WbKoXQcJrafocAquhdcZcUaXW5aZbz6ofQUqx3kR8TGiV3KdCXZ2mtIPNYJJHlEjIR2Y/Eeclraf6POS+EfDJS3xa/Zy2xdG6TRZ1qpVmaNWnGjvNoUjdabV27vWfGOpQhHLysu7Knztltncw8HHwq+nRHij1zT7MrLOzMy6lplpBW4tRwEpAySTFb+0NqGvUvVCcrTK1GjyGZOlIzwKAfWc/wBx4/CJKbcuoyqDZjNi0mZ3arXR+kbivWalR7RPZvHh8YhQlKUJS2gYSkYSImw6vvZvfP7QtaWm1Or9lAyYnTsWaa/VHT/60VSXCa5cAD6yoesyx/po7uHE+IiMezTpy5qTqhLyky2TQ6QUzdSVjKXCD6jXmfwBixhpCGm0tNoShCAEpSBgADkBDMt2+CFENLkeUIQigWDAajCvmxa0LW6P6aMm56H0nLf3Tjz7O/EVbIDyekTNBwTYdWJoO+2Hc+tvd+YtoiCO2np0q09RRd9PlymjXAr8+Up9VmaHPPZvc/HMXMOxRlp+pDdHcThLBk0zkoqpocXTUzLZnUt+0Wd4bwHlFqFou0h+1qW7Qej+ilSrZlOj9not0buPKKrSBkhQyORHbEtNhHUnel5nTCrzBU9LBUzSFrV7bPvND+U8R3Z7Imza20pIjokvI7/rjRjcGkdz0lKd9T9Pc3R2kDeH+Iq/VvOUQ73BSE8e4pMW3zTKJiVdYcSFIcQUKB6wRiKq7vpSqRdlyUJxJbVK1F9rdI5JKjj8MRphS84m968mSE2o7nVPbPWmdITMbzlUaafdT1lLbYGT3ZjBbMNkpuew9T6m5T0zZXIehSZWngHAkqJSe0YEclvC6524KfQWJwEM29TjJsA+8OeYm9sYWwKJoHTTMIWHasXJt0KGOCzhP/xxG1q6VevyINTltEE7FqDtJuq2qohZbdkamzvKzjGFhJjse2/OJm9dpVtOT0FHaJ7PWyeHxjl2qlvqtvU66LdUlbaJWpKdazz3Cd5P4Yj572uap3ZXnLgq696ZEoiXA7ENpwP8RYUOUoz/AAROXFOJKH/p4UpAod015WN9+dSw32hKU5P4mJXRxHYkpIpmgNJeLKUOTzrsypQGCrKuBPwjctetQJTTfTWpXC8pJm9zoZFoni6+oYSB4cz3COZc+djLUe0SH+2lV7cq2tqmqCwkTkhKhirzKD6rznup7ykcCY4qCEneJwBxJ7BH647MzDz03OuqenJlxT8w4o5K3FHJJ+MZ7Tm0J6/b5pVoyCV/proM04gZ6GXBytR7OEdSC6Vff0Kcvrl2JZbAdDuKn6eVOq1B1bVEqc30tMlFjiAOC3R2BRx8MxJWOO6/X7J6K6SSsrQGmE1FSESFIliOAwAN8jrCRxiL7e09rG22lC5ykOqA4r6DGfKOcqp3NyRac1DsywKEQElNqbV1h3fc+hZhOPYU0QPwj7Pys9Vfsygf0K+cZ+UsHVj7k74xt0Vun23bs/Xao8lmSkWFPOrJxwAzjxPKIUtbW+p6WwldCoDihzV6wz+Manqxrze+pdrItqtSkhTpEvB18yZVl/HJByeUFiWb7mHbFLzNIv67KjfV6VS76otRen3T0DZPBlgHCEDsGIwgDqlIbl2lPTDqw2w2kZK1ngABH7zPAdwEd52KdOlXZqEu8qhL79Gt9WJfeHqvTZHDHbujj8I6E5KmvsVop2SJN7Mum7em2mMnITDafpiexNVJzrLqh7PgkcI6jCEcZvb2y8loQhCMARqWr1lSOoOn1VteeSn9KZPQOEfqnRxQoeBxG2wjKeu4KoKjIT1Jqc5RqqypmoU99UtMoUOIUk4z5x77erVRtq4abclIcKJ+lzCX2iPeAPrJPcRwiR23XpsabWZfU2ky59Fmt2VrCUD2V8kO+fInuERkBwrIOcfjHYqmra+5SnHhLsWi6d3XTb2symXPSnAuWnmA5ge4r3knvByIgjtYW5PUvX+vuStMn5hioNtTQUxLqWN4pweIHdG67DOopoN2zGnlSfxT6sTMU0qPBt8D1kD+YdXaO+JrKabUreU2hR5ZKY56bx7Cz2siVTylCrVWnpSks0SqpcnZltkFUosAAqAPHEWlWxTGqNblOpLOejk5ZthOexKQP+I+4MsgghpGRy9UR5xrde7TMIKHkQg24rKqktqzIXLS6POzkvVpLo3jLNFf51vhxxy9XEcCnKDcplHUC162CoboJk144+UWtqQhWN5KVY5ZGY8eia/do/pESV5bhHjo1lUm9mq6N0hdB0rtqkuoLbkvTmkrSoYKVbuSCPOIY7YmowvjUs0Knvb9FtxSmgQfVemT7au/HLyiVO09qM3pxpdOz0u4n6XngZSmt9ZcUOKvBIyfhFdKErSj86suOqJW6snJUs8ST5xviV8pc2a3S0tI8iQApazhKRlR7omZsNWAKBZs7qLW0BibrKP0fpOHQyieIV3b2M+AERh0csd/UXUqlWohK/RFLExUXE+5LpOSM9RPLziyCr23SqlaL9qusqapb0r6IW2Vbm63jGARy4RtmW/YjWiH3FfO0fqMdTNT5mpyzn/ZaXvSdMST7YB9d3HefwxHN8d4+MTzGyppAGgj6Im+Axn0pWY8fyT9G/smo/f1xmvKrhFR0ZlS5PeyB+O8fGGD3fGJ2q2TNHSokU2qJ7hPq+Uep/ZI0iWkBqWrDJB4lM8Tn4iN/nYexr0H7kF909kN1XZE41bIulBSoA1xJIwD6byPbyjEfkb2Lk/+Y6/jPD84n5Rn5ysx0JEPaPSqlXK1IUGkMl2o1J9MvLoA6yeKj3AcYsv0jsmn6e2BS7Wp4BTKtDpnMcXXTxWs+JzGiaO7OtoabXa5c0lOz9SnAyWpf0vdIYz7RTjrPKOzxTyL+o+3kTV18EIQhFYlEIQgBCEIAxF6W9T7rtWpW7VGw5KT8uplwY5ZHAjvB4xWLeNt1Czbtqdp1VJE1THi2FH/AFWvcWPEYi1GIvbdmm5qFCltSKRL709Sk9FUUpHF2WJ9o96Sfge6LOLbwlp+TIrYckQ9YmJqUmWJ6QeUzOyjqZiWcScFLiTkRY/otqjQr907p1fXUZOWnC2G55hx5KFNPp4KGD1E8R4xW6CCApJylQyk90etbKFrKt51BPPcWUg/CL9+OrSvXZwLWfp2h/bNO+8o+cPp2h/bNO+8o+cVS+jo/ezH95Xzh6Oj97Mf3lfOK/yP5Jeui1d24rfaSFOVymoB4AmaR848PrTbWCfp+l4AJP6UjkPOKqjLNH2lzB8XlfOHorHa9/dV84fI/kddHWdqLUUai6pvuyL4dodFBlaeU+y4r33O/J5dwEcqWtLbanVn1UDJ74/QAlISkAJAwAI3/Z+sBzUfVSm0RxlS6TJKE5VVdQbSfVR4qOB8Yt/TTX/RD3skSo2J9OFWpp+q6qozu1i4QHiFDi1Lj9Wjz5+YiQEeDDTbDKGWUJQ22kJQlIwEgcABHnHGlJye2XUtLQhCEamRAnAzjMIQAHhiEIQAhCEAIQhACEIQAhCEAI+aqyMrU6bM06eZS9KzLSmnW1DIUlQwRH0wgCs/VbTO4LE1DqNrS1GqVQlAsvU11hgr6RhXEDh1jl5Rrn1buz+D6/8Ac1fKLT1sMrdS6tptTiRhKikEgdxjy3EfsJ+EXI5korWiF0xb2VXfVu7P4Pr/ANzV8oC2rsJAFnV/J4Aehq+UWo7iP2E/CG4jOdxPwjb56XsOhEq2+pt8fwPcX3JXyj9bsq+3F7jdi3CVdnoavlFpUIx89L2HQiVbuWRfrXFdh3CD1D0NXH8Im9si6YP6e6fGcrDRRXq0oTM4hQ4sjHqN+Q595jtUIityJWLTNoVqPdCEIRXJBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIA//9k=',
    	                   // image: '../../images/basketball.jpg',
    	          		 width: 100,
    	                   height: 95
    	                 },
    	                 {
    	                       width: '10%',
    	                       text:''
    	                 },
    	                 {
    	              	   ul: [
//    	              		{text:$scope.reportDetails.data.reportHeaderInfo.companyHeader,listType: 'none',style: 'header',alignment: 'center'},
//    	         				{text:$scope.reportDetails.data.reportHeaderInfo.subHeaders[0],listType: 'none',style: 'subheader',alignment: 'center'},
//    	         				{text:$scope.reportDetails.data.reportHeaderInfo.subHeaders[1],listType: 'none',style: 'subheader',alignment: 'center'},
//    	         				{text:$scope.reportDetails.data.reportHeaderInfo.subHeaders[2],listType: 'none',style: 'subheader',alignment: 'center'},
//    	         				{text:$scope.reportDetails.data.reportHeaderInfo.subHeaders[3],listType: 'none',style: 'subheader',alignment: 'center'}
    	         				],
    	         				alignment: 'center'
    	                 }			                                   
    	       
    	           ]
    	        }
    }

    
var downloadPDF = function($scope){

              // Header in report
            generateHeader = {
    margin: 30,
      columnGap: 20,
      columns: [
            {
             image:'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAASABIAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx7/2wBDAQUFBQcGBw4ICA4eFBEUHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh7/wAARCACnAMgDASIAAhEBAxEB/8QAHQABAAIDAAMBAAAAAAAAAAAAAAgJBQYHAgMEAf/EAEgQAAECBQEEBgYFCwMCBwAAAAECAwAEBQYRBwgSITETQVFhcYEUIjJCkdEVFlWUoRgjJDNSU1ZykpOxQ4LBCSUXJlRjoqTh/8QAGwEBAAIDAQEAAAAAAAAAAAAAAAMEAQIFBgf/xAAsEQACAgIBAgQGAwADAAAAAAAAAQIDBBESEyEFMUFRBhQiQmGBMnHhFZGh/9oADAMBAAIRAxEAPwCZcIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBGv6i3ZTbIsup3PVlhMtIsle7nBcV7qB3k4EbBEKdubUduvXLLadUp7ekqUsTFUWk+qt7Hqt/7RxPeYkqrc5JI1lLitmpPbT2sD8w9MMzlKl2nllbbKmMlpJPBOfCPH8prWX7SpP3eOPEknPbHipbafadbHioR1lj1r0KnVkdj/Ka1l+0qT93h+U1rL9pUn7vHHAtsjIdb/qEfu+3+9b/qEOhX7DqzOxflNay/aVJ+7w/Ka1l+0qT93jju8j943/UI/QUnktJ8DGehX7DqyOyyu1Bq7KzsvNzc3S5mVYdSt9hLGC6jPrJz1cInVZlwSF1WrTbipjgclJ+XS82QeWRxB7weEVYDgeIyOuJZ7A9//o9Q0zqLxK5YqnKWVH2mifXQPA8fMxUyqFGPKKJarG3pks4QhHPLAhCEAIQhACEIQAhHyTFUpks+WJioSrLoGShbyUqA8CY9ZrVHAJNVkcD/AN9PzjOmD74Ri/rHb/23TfvKPnH4q5bdScKrtMB75pHzhpgysIxP1mtz7epn3pHzjweuu2GUb7txUlCe1U2gD/MNMbMzCMD9c7R3kpFz0YqUoJSPTW8knkBxjO5G7vZGMZzDTQNE141BlNNtN6jcTykKmwjoZFkni6+rgkY68cz3CK2H35qbmX52feU/Ozbqn5lxRyVuKOTHXNrHUleoGpr1OkpjfoFvrUxLBJyl5/ktzvxyHhHICpKQpazhKRlR7o6mJVwjyfqVLp7ejMWTbdSvG8aValIBE3UXgguAfqWh7az4DMTto+zVpBISbDTlqszbzbYQt95xRU4rHFR44yY0DYX0zcpdFmNR61KhE7VkdHTULHFqW/a7io/gO+JQRUyL3KWovsTVQ4rucke2cNG3XS4qzZYE9SXFgfDMet3Zp0aW2UfVBpOetLywf8x1+PwEHkQYg5y9yTSON/kx6M/wt/8AZX84wV7bKum01bVRTbVNeptYLRVJPiYUoIcAyAQTjBPA+MSChGVbNPexxRU28zMy0w/JzzKmZyVdUxMtqGChxJwYylmXJPWbeFJu2mk+kUt8OKSDjpGuS0HuIzHaduCwvq1qNL3nIshFNuBPRzW6OCJpI5n+YY8wY4ByOFDI5ER1q5K2vuUpJwkWpWjXZC57Zp1fpjodlJ+XS+0oHqIzjxHKMpETdgnUEql5/TKou5clAZymKUfaaJ9dHkTnwJiWUciyDhJpl2L2tiEIRoZEIQgBGDv65qfZ1nVS5am4luWkJdTqsn2iBwSO8nA84zkQv27dRVVa4pXTWnPfoUhuzdVKT7bvuNnwHHxPdElVbnJJGspcVsjtc1bqd13FULmrE1Mqnqi+p5QDqgEJJ9VI7gIx3RH/ANRN/wB9Ue0kqPjHukpCqT6FOU6jVKeaScF2Xl1LTnxAjtKMYrRS5SbPj6Bv9t/+8qPxUqwrivpVHvdJjKfQdx/wvXPuS/lHsat26XU77Vp1tac4z6KocfhGNxM6mYb0OW/YX/cMfhkpU8C2pXcVkxnPqzdv8H1z7sr5Q+rN4FSUN2hW+lcIQ2DLKwVHgOqG4IamdE2RdMJS+NVkT03J71FoATMTOclLr3+mjyPEjuiUm13qadP9OVU+mLT9OVzelJMA8WkEYW55A4HeYzOz3Y0ppVpDLStR6NidU0Z6rPqP+oRlWT2JHDyiD+t9/wA1qVqRULiccUqnMrVK0pvkEMg+1jtVzjnxXXt36FhvpwNIbbDLSWgoq3eZPvHrMbtojYb2pOplNtgBQkEETVTcA9lhJzu/7jw840l1wNNKdUN7d5J7T1CJ/wCyNpgiwNOmqjUWR9YK2lM1OrI9ZtJGUNd2AePeYtZNvThpENMeUts7HJSzElJsycq0lphhtLbaEjASkDAA8o0zVjUGXsuTZYYYE5VZoHoGCcJSBzWo9kbzEbdott5Go7a3TlDsknoeHUCciMeC4leVlKFvlpv+znfEWfbg4Mraf5dlv2/Jr9b1CvKqOremq67LIVw6OW9RIHYO2MSxcFwMOpLVdqjS+Y3nFDPxj79PanRqNd0tUK/JelyKEkbu7vdGvqWR14jtt0TOnN+0UUxNakWH18Zd9GEONK88fCPX5ORXhTjWqPofm0uy/wDDwGDi3+JVSueVqxeSb/00DTrViuUyry0lcUz6fTH1hsvKGHGCTwOesRIpJCkhSTkEZBji3/gRS5iVSWbnm15A/OJSkgntGI7FTZYydPl5RTy3iy2lsuK5qwMZMeV8aswrZRnjefqta/Z7n4eq8RorlXm916Pe/wBGna7WLL6h6Y1a3HUJMytouyazzQ+nigjz4ecVpqbmGHHZWcbU1NSzimJhChgpWk4OYtliC+29p+m19QZe8qcwG6ZcH5uaCRwbmkjn3bw4+IMUMO3jLi/U7t0NrZxezbkn7Nu6lXbTCfSaW+HFIBx0rXvoPiMxZ3aNdkbntinXBTXA5KT8uh9sg5wFDOPEcvKKr+RwRnqIiWOwRqAeiqOmlSfJVLZnKXvHm0T66B4Hj5mJsyra5o0on9pLSEIRzSyIQhAGqatXnI2Bp/VbonlJxKMnoUE8XHTwQkeJxFZdSqE/WKpOVqquqen6g8qYmFqOTlRzjwEd8239RPrNfLNjU2YC6VQlB2d3DwcmjySf5R+JMR95kknHWTHUw6uMeT9SrfPb0fZb9FqFy3DTbapLZXPVSYTLtAe6CfWUe4DJizqwbUpdnWhTLcpjDaZeRl0tb26MrUBxUe8nJiM2wbpyV+l6n1Rn9aFSlJSoey2Dhbg8Tw+MS4irlW85aXkiWqHGJ4dE1+7R/SIBpocm0f0iPOEVSU8ejb/dp+EOjb/dp+EeUIAjlt43bV6JpxJ2/TmXmpeuzHQTc6k4S22OJb8Vf4zEJt0JAQkYSkboHdFmGuFjS2ommdWth/CXnmuklXMcW3k8UH4jHgTFaLzE1KTD8jPNKZnJR1UvMNqHFK0nBjpYUo8WvUq3p+Z1XZQsim31rDLs1d9kSVFbE8ZVZG9NLB9UAdaQeJiw8AAAAYAirCybmnbLvKk3dTiov0x8LcQDjpWjwWg9xGYs+tmsyNw2/IVymvIek55hLzS0nIIUM/8A5EOZFqe2SUtcTIxrGoVlUm9KYmWnwtp9k70vMt+20f8Akd0bPEddcZ256HqC76PW6jLyU6ylyXShwhAI4KSIn8JxrMjISqnxku6ZzfHMyrExHO6vnF9mj5q9o5d1PUpUiqWqbXUUncV8DGkV63qvRFhNao0xJAq3Q4tHqE/zDhG6aXakT1Ar7q7jqM7PU6ZQEqUtRWWFD3gOyN61C1Us2ctmap8kBWH5lstpaLZCEkj2lE8sc49asrxLHvjVOHNP1S1/nY8H/wAf4Nl40siqzpteje/9OO2vdtxWrMB+j1B3oknK5R1RU04OsYPLxESqtKtMXDbklWZZJSiaaC9080nrHkYhwfzMtlZzupxntMSt0dpczR9OqVKTaFNvlvpFoVzTvHOIp/E9FKqjalqW9f2i/wDBOXkTtnTJtwS/6Zt0aTrfYsrqJprVbafQkvutFyUWebb6eKFZ6uPDzjdo5DtW6lDTvTOY9AeCa7VcylOQD6wJHrOf7Qc+OI8bFNtaPoj8ivqZl5qSm5inz7fRzsm6qXmUZzhaTgxkLTuOoWfdVLuulKKZulvh3GeDjfvoPcRmMWkKSDvrU44olTjijkrUeJJMbRpVZM7qLqBTbQkt5Lcwrpp94D9TLpI3j4nkPGO1PSrfIox/l9JZVZ9ekbotenXDTF78pPy6X2z2Ajl5HhCPooVLkaJRpOkU1hLEnJspZZbSOCUpGBCOI/wXz7Y0XXe/JbTrTOq3G6oekpbLUk3nit9XBAHnx8o3qIEbZeoSrx1PNuSMz0lFt07hCVZQ7NH2j37vL4xJTX1JpGk5cVs4it2Zfddmp11Ts3MuKfmHFHJUtRyYy9j2tUb3vKl2jSkkzFRdCXFjk0yOK1nuxmMOSACpRwkDKj3RMfYS05NNt2a1Fq0sUz9XHRSAWOLUqDzHZvH8AI6d9iqh2Ktcects6jet9WVoTZ9Bp1UYnGaaECUlvRZffAKE9eORPONKoG1np5WLip9GYp1bacnplMu245LgJBUcJJ45xkiPft2UgT+hr88hgrep020+lY9xOcK/zEF2ZxyTnKfUmlkLlptp9JHMEKBipTRGyDk/MnnNxaRa3U56WptNmajNr6OXlmlOuq7EpGT/AIiO42xdNzvf9rr+AogES6TkDr5xu20ldJo+znWash1KHp6noZaJPNToA4d+CYrwl8syzLQCjhI3sDO73nsjGNjqxNyFlnHyLENEdeLX1VrlQo9JkZ+Rm5NkP7s0kDpEZxkY7MiMvrjqxRNJ6LIVOtSc3NiemOgablgCrIGSTnqxEMdkitCi7QlGUteG6iw5KK48yRw/ER0v/qHTyV3BZtLSoFQQ9MKAPEDIA4fGEsdK5Q9DKs3DkdS0w2lba1AvyStOj2/WUPzSFr6ZxKQlsJGSVceXfHEduPT5Nt31K3vTmNynV09FO7o9VEyBwUf5h/gx69gylLn9YqvVzjcp1O3ATz3lnGB8Il3q1ZNO1CsCp2rUQEpm2vzLuMll0cULHgf+YxJqi36Qlzh3KxeRIIz1ERKvYN1F6IzmmNVmCdzem6QVnmg8VtjwPH4xF+u0mo2/Xahb1YZLVRpj6pd9J68clDuIwfOP2gVqpW3XpC46M6pqo014PskH2gD6yD3EZEXroK2vsV65OEu5azGCvW06Pd1K+j6uwVBJ3mnUHC2ldqTHp01u6mX1ZNMuekupXLzrIUoA8W1+8g9hByI2OOVCydU1KD00WbKoXQcJrafocAquhdcZcUaXW5aZbz6ofQUqx3kR8TGiV3KdCXZ2mtIPNYJJHlEjIR2Y/Eeclraf6POS+EfDJS3xa/Zy2xdG6TRZ1qpVmaNWnGjvNoUjdabV27vWfGOpQhHLysu7Knztltncw8HHwq+nRHij1zT7MrLOzMy6lplpBW4tRwEpAySTFb+0NqGvUvVCcrTK1GjyGZOlIzwKAfWc/wBx4/CJKbcuoyqDZjNi0mZ3arXR+kbivWalR7RPZvHh8YhQlKUJS2gYSkYSImw6vvZvfP7QtaWm1Or9lAyYnTsWaa/VHT/60VSXCa5cAD6yoesyx/po7uHE+IiMezTpy5qTqhLyky2TQ6QUzdSVjKXCD6jXmfwBixhpCGm0tNoShCAEpSBgADkBDMt2+CFENLkeUIQigWDAajCvmxa0LW6P6aMm56H0nLf3Tjz7O/EVbIDyekTNBwTYdWJoO+2Hc+tvd+YtoiCO2np0q09RRd9PlymjXAr8+Up9VmaHPPZvc/HMXMOxRlp+pDdHcThLBk0zkoqpocXTUzLZnUt+0Wd4bwHlFqFou0h+1qW7Qej+ilSrZlOj9not0buPKKrSBkhQyORHbEtNhHUnel5nTCrzBU9LBUzSFrV7bPvND+U8R3Z7Imza20pIjokvI7/rjRjcGkdz0lKd9T9Pc3R2kDeH+Iq/VvOUQ73BSE8e4pMW3zTKJiVdYcSFIcQUKB6wRiKq7vpSqRdlyUJxJbVK1F9rdI5JKjj8MRphS84m968mSE2o7nVPbPWmdITMbzlUaafdT1lLbYGT3ZjBbMNkpuew9T6m5T0zZXIehSZWngHAkqJSe0YEclvC6524KfQWJwEM29TjJsA+8OeYm9sYWwKJoHTTMIWHasXJt0KGOCzhP/xxG1q6VevyINTltEE7FqDtJuq2qohZbdkamzvKzjGFhJjse2/OJm9dpVtOT0FHaJ7PWyeHxjl2qlvqtvU66LdUlbaJWpKdazz3Cd5P4Yj572uap3ZXnLgq696ZEoiXA7ENpwP8RYUOUoz/AAROXFOJKH/p4UpAod015WN9+dSw32hKU5P4mJXRxHYkpIpmgNJeLKUOTzrsypQGCrKuBPwjctetQJTTfTWpXC8pJm9zoZFoni6+oYSB4cz3COZc+djLUe0SH+2lV7cq2tqmqCwkTkhKhirzKD6rznup7ykcCY4qCEneJwBxJ7BH647MzDz03OuqenJlxT8w4o5K3FHJJ+MZ7Tm0J6/b5pVoyCV/proM04gZ6GXBytR7OEdSC6Vff0Kcvrl2JZbAdDuKn6eVOq1B1bVEqc30tMlFjiAOC3R2BRx8MxJWOO6/X7J6K6SSsrQGmE1FSESFIliOAwAN8jrCRxiL7e09rG22lC5ykOqA4r6DGfKOcqp3NyRac1DsywKEQElNqbV1h3fc+hZhOPYU0QPwj7Pys9Vfsygf0K+cZ+UsHVj7k74xt0Vun23bs/Xao8lmSkWFPOrJxwAzjxPKIUtbW+p6WwldCoDihzV6wz+Manqxrze+pdrItqtSkhTpEvB18yZVl/HJByeUFiWb7mHbFLzNIv67KjfV6VS76otRen3T0DZPBlgHCEDsGIwgDqlIbl2lPTDqw2w2kZK1ngABH7zPAdwEd52KdOlXZqEu8qhL79Gt9WJfeHqvTZHDHbujj8I6E5KmvsVop2SJN7Mum7em2mMnITDafpiexNVJzrLqh7PgkcI6jCEcZvb2y8loQhCMARqWr1lSOoOn1VteeSn9KZPQOEfqnRxQoeBxG2wjKeu4KoKjIT1Jqc5RqqypmoU99UtMoUOIUk4z5x77erVRtq4abclIcKJ+lzCX2iPeAPrJPcRwiR23XpsabWZfU2ky59Fmt2VrCUD2V8kO+fInuERkBwrIOcfjHYqmra+5SnHhLsWi6d3XTb2symXPSnAuWnmA5ge4r3knvByIgjtYW5PUvX+vuStMn5hioNtTQUxLqWN4pweIHdG67DOopoN2zGnlSfxT6sTMU0qPBt8D1kD+YdXaO+JrKabUreU2hR5ZKY56bx7Cz2siVTylCrVWnpSks0SqpcnZltkFUosAAqAPHEWlWxTGqNblOpLOejk5ZthOexKQP+I+4MsgghpGRy9UR5xrde7TMIKHkQg24rKqktqzIXLS6POzkvVpLo3jLNFf51vhxxy9XEcCnKDcplHUC162CoboJk144+UWtqQhWN5KVY5ZGY8eia/do/pESV5bhHjo1lUm9mq6N0hdB0rtqkuoLbkvTmkrSoYKVbuSCPOIY7YmowvjUs0Knvb9FtxSmgQfVemT7au/HLyiVO09qM3pxpdOz0u4n6XngZSmt9ZcUOKvBIyfhFdKErSj86suOqJW6snJUs8ST5xviV8pc2a3S0tI8iQApazhKRlR7omZsNWAKBZs7qLW0BibrKP0fpOHQyieIV3b2M+AERh0csd/UXUqlWohK/RFLExUXE+5LpOSM9RPLziyCr23SqlaL9qusqapb0r6IW2Vbm63jGARy4RtmW/YjWiH3FfO0fqMdTNT5mpyzn/ZaXvSdMST7YB9d3HefwxHN8d4+MTzGyppAGgj6Im+Axn0pWY8fyT9G/smo/f1xmvKrhFR0ZlS5PeyB+O8fGGD3fGJ2q2TNHSokU2qJ7hPq+Uep/ZI0iWkBqWrDJB4lM8Tn4iN/nYexr0H7kF909kN1XZE41bIulBSoA1xJIwD6byPbyjEfkb2Lk/+Y6/jPD84n5Rn5ysx0JEPaPSqlXK1IUGkMl2o1J9MvLoA6yeKj3AcYsv0jsmn6e2BS7Wp4BTKtDpnMcXXTxWs+JzGiaO7OtoabXa5c0lOz9SnAyWpf0vdIYz7RTjrPKOzxTyL+o+3kTV18EIQhFYlEIQgBCEIAxF6W9T7rtWpW7VGw5KT8uplwY5ZHAjvB4xWLeNt1Czbtqdp1VJE1THi2FH/AFWvcWPEYi1GIvbdmm5qFCltSKRL709Sk9FUUpHF2WJ9o96Sfge6LOLbwlp+TIrYckQ9YmJqUmWJ6QeUzOyjqZiWcScFLiTkRY/otqjQr907p1fXUZOWnC2G55hx5KFNPp4KGD1E8R4xW6CCApJylQyk90etbKFrKt51BPPcWUg/CL9+OrSvXZwLWfp2h/bNO+8o+cPp2h/bNO+8o+cVS+jo/ezH95Xzh6Oj97Mf3lfOK/yP5Jeui1d24rfaSFOVymoB4AmaR848PrTbWCfp+l4AJP6UjkPOKqjLNH2lzB8XlfOHorHa9/dV84fI/kddHWdqLUUai6pvuyL4dodFBlaeU+y4r33O/J5dwEcqWtLbanVn1UDJ74/QAlISkAJAwAI3/Z+sBzUfVSm0RxlS6TJKE5VVdQbSfVR4qOB8Yt/TTX/RD3skSo2J9OFWpp+q6qozu1i4QHiFDi1Lj9Wjz5+YiQEeDDTbDKGWUJQ22kJQlIwEgcABHnHGlJye2XUtLQhCEamRAnAzjMIQAHhiEIQAhCEAIQhACEIQAhCEAI+aqyMrU6bM06eZS9KzLSmnW1DIUlQwRH0wgCs/VbTO4LE1DqNrS1GqVQlAsvU11hgr6RhXEDh1jl5Rrn1buz+D6/8Ac1fKLT1sMrdS6tptTiRhKikEgdxjy3EfsJ+EXI5korWiF0xb2VXfVu7P4Pr/ANzV8oC2rsJAFnV/J4Aehq+UWo7iP2E/CG4jOdxPwjb56XsOhEq2+pt8fwPcX3JXyj9bsq+3F7jdi3CVdnoavlFpUIx89L2HQiVbuWRfrXFdh3CD1D0NXH8Im9si6YP6e6fGcrDRRXq0oTM4hQ4sjHqN+Q595jtUIityJWLTNoVqPdCEIRXJBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIA//9k=',
               // image: '../../images/basketball.jpg',
                     width: 100,
               height: 95
             },
             {
                   width: '10%',
                   text:''
             },
             {
               ul: [
                   
                {text:"Hedgerow Technologies Private Limited",listType: 'none',style: 'header',alignment: 'center'},
                {fontSize: 10, text: "Claims Services with Value Addition",listType: 'none',style: 'subheader',alignment: 'center'},
                {fontSize: 10, text:"WZ 33A, Dayal Sar Road, Uttam Nagar West, New Delhi, 110059",listType: 'none',style: 'subheader',alignment: 'center'},
                {fontSize: 10, text: "PAN Number: AAECH3682G   GSTIN: 07AAECH3682G1Z",listType: 'none',style: 'subheader',alignment: 'center'},
                {fontSize: 10, text: "Phone Number: +91 7290049100, Email: info@wimwisure.com",listType: 'none',style: 'subheader',alignment: 'center'}
                ],
                alignment: 'center'
             }			                                   

       ]
    }

              // sub-section
              referenceContent = [
                      {
                              width: '25%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                              [ {
                                                  stack: [
                                                      {						
                                                          text: [
                                                              //{ text: 'Reference ID:', fillColor: 'lightgreen', bold: true,alignment: 'center' }
                                                              { text: 'Reference ID:', fillColor: 'lightgreen', bold: true,alignment: 'center' }
                                                          ]
                                                  }
                                                ]
                                              }]
                                      ]
                              }
                      },
                      {
                              width: '25%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                            [{ text: $scope.claimCase.id,fillColor: '#fff', bold: true,alignment: 'center' }]
                                      ]
                              }
                      },
                      {
                              width: '25%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                              [ {
                                                  stack: [
                                                      {						
                                                          text: [
                                                              { text: 'Date: ',fillColor: '#fff', bold: true,alignment: 'center' }
                                                          ]
                                                  }
                                                ]
                                              }]
                                      ]
                              }
                      },
                      {
                              width: '25%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                              [ {
                                                  stack: [
                                                      {						
                                                          text: [
                                                              { text: formatDate(new Date()),fillColor: 'lightgreen', bold: true,alignment: 'center' }
                                                          ]
                                                  }
                                                ]
                                              }]
                                      ]
                              }
                      }
    ]

              // Report Heading
              reportHeading = {
        width: '100%',
        style: 'tableExample',
        table: {
            headerRows: 1,
            widths: ['*'],
            body: [
                [{ text: 'MOTOR LOSS ASSESSMENT REPORT',fillColor: '#fbd4b4', bold: true,alignment: 'center' }]
             ]
        }
              }

              // ID Heading
              idHeading = {
        width: '100%',
        style: 'tableExample',
        table: {
            headerRows: 1,
            widths: ['*'],
            body: [
                [{ text: 'INSURANCE DETAILS',fillColor: 'lightblue', bold: true,alignment: 'center' }]
             ]
        }
              }

              // ID Body
              idContent = [
                      {
                              width: '50%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                              [ {
                                                  stack: [
                                                      {
                                                          text: [
                                                              { text: 'Insurer                          :  '+ $scope.claimCase.insuranceDetails.insurer.id+'\n',alignment: 'left'},
                                                              { text: 'Policy No                     :  '+ $scope.claimCase.insuranceDetails.policyNumber +'\n', alignment: 'left' },
                                                              { text: 'Policy Valid From       :  '+ formatDate($scope.claimCase.insuranceDetails.startDate) +'\n', alignment: 'left' },
                                                              { text: 'Policy Valid Till           :  '+ formatDate($scope.claimCase.insuranceDetails.endDate) +'\n', alignment: 'left' },
                                                              { text: 'Claim Number            :  '+ $scope.claimCase.claimNumber +'\n\n',  alignment: 'left'}
                                                          ]
                                                  }
                                                ]
                                              }]
                                      ]
                              }
                      },
                      {
                              width: '50%',
                              style: 'tableExample',
                              table: {
                                        headerRows: 1,
                                        widths: ['*'],
                                      body: [
                                        [ {
                                            stack: [
                                              {
                                                      text: [
                                                              { text: 'Sum Insured        		   :  '+ $scope.claimCase.insuranceDetails.sumInsured +'\n',alignment: 'left'},
                                                              { text: 'IDV            				       :  '+ $scope.claimCase.insuranceDetails.idv +'\n', alignment: 'left' },
                                                              { text: 'Insured Name                :  '+ $scope.claimCase.insuranceDetails.insuredName +'\n', alignment: 'left' },
                                                              { text: 'Insured Address   		 :  '+ $scope.claimCase.insuranceDetails.insuredAddress +'\n', alignment: 'left' },
                                                              { text: 'Insured Mobile No         :  '+ $scope.claimCase.insuranceDetails.insuredMobileNumber +'\n', alignment: 'left'}    
                                                  ]
                                              }
                                            ]
                                        }]
                                      ]
                              }
                      }	
    ]

            // ID Heading
              vdHeading = {
        width: '100%',
        style: 'tableExample',
        table: {
            headerRows: 1,
            widths: ['*'],
            body: [
                [{ text: 'VEHICLE DETAILS',fillColor: 'lightblue', bold: true,alignment: 'center' }]
             ]
        }
              }

              // VehicleDetails Body
              vdContent = [
                      {
                              width: '50%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                              [ {
                                                  stack: [
                                                      {
                                                          text: [
                                                              { text: 'Registered Owner Name  :  '+ $scope.claimCase.vehicleDetails.registeredOwnerName+'\n',alignment: 'left'},
                                                              { text: 'Registered No.			       :  '+ $scope.claimCase.vehicleDetails.id +'\n', alignment: 'left' },
                                                              { text: 'Date of Registration		 :  '+ formatDate($scope.claimCase.vehicleDetails.dateOfRegistration) +'\n', alignment: 'left' },
                                                              { text: 'Chassis No.			   		:  '+ $scope.claimCase.vehicleDetails.chassisNumber +'\n', alignment: 'left' },
                                                              { text: 'Engine No.			         	:  '+ $scope.claimCase.vehicleDetails.engineNumber +'\n',  alignment: 'left'},
                                                              { text: 'Make			                	   :  '+ $scope.claimCase.vehicleDetails.makeModelVariant.make +'\n', alignment: 'left' },
                                                              { text: 'Model			   			      :  '+ $scope.claimCase.vehicleDetails.makeModelVariant.model +'\n', alignment: 'left' },
                                                              { text: 'Type of Body			         :  '+ ($scope.claimCase.vehicleDetails.vehicleBodyType ? $scope.claimCase.vehicleDetails.vehicleBodyType.bodyType : "") +'\n',  alignment: 'left'}
                                                          ]
                                                  }
                                                ]
                                              }]
                                      ]
                              }
                      },
                      {
                              width: '50%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                        [ {
                                            stack: [
                                              {
                                                      text: [
                                                      { text: 'Class of Vehicle (Use)           		:  '+ $scope.claimCase.vehicleDetails.makeModelVariant.type+'\n',alignment: 'left'},
                                                      { text: 'Fitness Certificate No.				  :  '+ $scope.claimCase.vehicleDetails.fitnessCertificateNumber +'\n', alignment: 'left' },
                                                      { text: 'Fitness Valid upto					      :  '+ formatDate($scope.claimCase.vehicleDetails.validUpto) +'\n',  alignment: 'left'},
                                                      { text: 'Permit No.					   			    :  '+ $scope.claimCase.vehicleDetails.permitNumber +'\n', alignment: 'left' },
                                                      { text: 'Type of Permit					       	  :  '+ ($scope.claimCase.vehicleDetails.vehiclePermitType ? $scope.claimCase.vehicleDetails.vehiclePermitType.permitType : "") +'\n', alignment: 'left' },
                                                      { text: 'Passenger Carrying Capacity	   :  '+ $scope.claimCase.vehicleDetails.passengerCarryingCapacity +'\n', alignment: 'left' },
                                                      { text: 'Tax paid upto					         	  :  '+  formatDate($scope.claimCase.vehicleDetails.taxPaidUpto) +'\n',  alignment: 'left'}
                                                  ]
                                              }
                                            ]
                                        }]
                                      ]
                              }
                      }
    ]

            // DD Heading
              ddHeading = {
        width: '100%',
        style: 'tableExample',
        table: {
            headerRows: 1,
            widths: ['*'],
            body: [
                [{ text: 'DRIVER DETAILS',fillColor: 'lightblue', bold: true,alignment: 'center' }]
             ]
        }
              }

              // Driver Details Body
              ddContent = [
                      {
                              width: '50%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                              [ {
                                                  stack: [
                                                      {
                                                          text: [
                                                              { text: 'Name of the Driver			           :  '+ $scope.claimCase.driverDetails.driverName+'\n',alignment: 'left'},
                                                              { text: 'Motor Driving License No.		  :  '+ $scope.claimCase.driverDetails.licenseNumber +'\n', alignment: 'left' },
                                                              { text: 'Date of issue			                     :  '+ formatDate($scope.claimCase.driverDetails.issueDate) +'\n', alignment: 'left' },
                                                              { text: 'Valid up to			    	 		         :  '+ formatDate($scope.claimCase.driverDetails.expiryDate) +'\n', alignment: 'left' }
                                                          ]
                                                  }
                                                ]
                                              }]
                                      ]
                              }
                      },
                      {
                              width: '50%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                        [ {
                                            stack: [
                                              {
                                                      text: [	
                                                      { text: 'Issuing Authority					           :  '+ $scope.claimCase.driverDetails.issuingAuthority+'\n',alignment: 'left'},
                                                      { text: 'Type of License.			         		   :  '+ $scope.claimCase.driverDetails.licenseType +'\n', alignment: 'left' },
                                                      { text: 'Badge No.			                		       :  '+ $scope.claimCase.driverDetails.badgeNumber +'\n', alignment: 'left' },
                                                      { text: 'Endorsement of License			      :  '+ $scope.claimCase.driverDetails.licenseEndorsement +'\n', alignment: 'left' }
                                                  ]
                                              }
                                            ]
                                        }]
                                      ]
                              }
                      }
    ]

            // ID Heading
              adHeading = {
        width: '100%',
        style: 'tableExample',
        table: {
            headerRows: 1,
            widths: ['*'],
            body: [
                [{ text: 'ACCIDENT DETAILS',fillColor: 'lightblue', bold: true,alignment: 'center' }]
             ]
        }
              }

              // Accident Details Body
              adContent = [
                      {
                              width: '50%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                              [ {
                                                  stack: [
                                                      {			
                                                          text: [
                                                              { text: 'Date of Accident			           :  '+ formatDate($scope.claimCase.accidentDetails.dateOfAccident) +'\n',alignment: 'left'},
                                                              { text: 'Time of Accident			          :  '+ $scope.claimCase.accidentDetails.id +'\n', alignment: 'left' },
                                                              { text: 'Place of Accident			         :  '+ $scope.claimCase.accidentDetails.accidentLocation +'\n', alignment: 'left' },
                                                              { text: 'Date of Intimation				     :  '+ formatDate($scope.claimCase.accidentDetails.dateOfIntimation) +'\n', alignment: 'left' },
                                                              { text: 'Place of survey			             :  '+ $scope.claimCase.accidentDetails.locationOfSurvey +'\n',  alignment: 'left'},
                                                              { text: 'Date & Time of Inspection	  :  '+ formatDate($scope.claimCase.accidentDetails.dateOfSurvey) +'\n', alignment: 'left' }
                                                          ]
                                                  }
                                                ]
                                              }]
                                      ]
                              }
                      },
                      {
                              width: '50%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                        [ {
                                            stack: [
                                              {		
                                                      text: [
                                                      { text: 'Third Paty Damage			           :  '+ $scope.claimCase.accidentDetails.thirdPartyDamage+'\n',alignment: 'left'},
                                                      { text: 'Type of Third Party Damage	   :  '+ $scope.claimCase.accidentDetails.thirdPartyDamageType +'\n', alignment: 'left' },
                                                      { text: 'Whether reported to the Police  :  '+ $scope.claimCase.accidentDetails.reportedToPolice +'\n', alignment: 'left' },
                                                      { text: 'Name of the Police Station		 :  '+ $scope.claimCase.accidentDetails.policeStationName +'\n', alignment: 'left' },
                                                      { text: 'Station Diary Entry No.			     :  '+ $scope.claimCase.accidentDetails.stationDiaryEntryNumber +'\n',  alignment: 'left'},
                                                      { text: 'Panchanama carried out		     :  '+ $scope.claimCase.accidentDetails.punchnamaCarriedOut +'\n', alignment: 'left' }
                                                  ]
                                              }
                                            ]
                                        }]
                                      ]
                              }
                      }
    ]


            // ID Heading
              cnnHeading = {
        width: '100%',
        table: {
              style: 'tableExample',
            headerRows: 1,
            widths: ['*'],
            body: [
                [{ text: 'CAUSE,NATURE & NOTES',fillColor: 'lightblue', bold: true,alignment: 'center' }]
             ]
        }
              }

            // Accident Details Body
              cnnContent = [
                      {
                              width: '25%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                              [ {
                                                  stack: [
                                                      {						
                                                          text: [
                                                              { text: 'Accident Observations			 :  '+ '\n\n',alignment: 'left',layout: 'noBorders'},
                                                              { text: 'Third Party Damage Observatons	           :  '+ '\n\n', alignment: 'left',layout: 'noBorders' },
                                                              { text: 'Other Remarks			             :  '+ '\n', alignment: 'left' }
                                                          ]
                                                  }
                                                ]
                                              }]
                                      ]
                              }
                      },
                      {
                              width: '75%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                        [ {
                                            stack: [
                                              {		
                                                      text: [
                                                      { text:  $scope.claimCase.accidentDetails.accidentRemarks+'\n\n',alignment: 'left'},
                                                      { text:  $scope.claimCase.accidentDetails.thirdPartyDamageRemarks +'\n\n', alignment: 'left' },
                                                      { text:  $scope.claimCase.accidentDetails.otherRemarks +'\n\n\n', alignment: 'left' }
                                                  ]
                                              }
                                            ]
                                        }]
                                      ]
                              }
                      }
    ]


            // ID Heading
              assesmentLossHeading = {
        width: '100%',
        table: {
              style: 'tableExample',
            headerRows: 1,
            widths: ['*'],
            body: [
                [{ text: 'ASSESMENT OF LOSS',fillColor: 'lightblue', bold: true,alignment: 'center' }]
             ]
        }
              }

            // Accident Details Body
              assesmentLossContent = [
                      {
                              width: '80%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                              [ {
                                                  stack: [
                                                      {						
                                                          text: [
                                                              { text: "Assessed for Spare Part of Accident Vehicle Bearing Reg. Number: ",alignment: 'left'}
                                                          ]
                                                  }
                                                ]
                                              }]
                                      ]
                              }
                      },
                      {
                              width: '20%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                        [ {
                                            stack: [
                                              {		
                                                      text: [
                                                          { text:  $scope.claimCase.vehicleNumber,alignment: 'left'}
                                                      ]								              
                                              }
                                            ]
                                        }]
                                      ]
                              }
                      }
    ]

              var assesmentLossHead = [[
          { text: 'S.No',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'PartName',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'Assesd for INR',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'TAX %(GST)',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'Total Amount in INR',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'Rate of Dep.',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'Final Amount in INR',fillColor: 'lightgrey', bold: true, alignment: 'center' }
    ]];
              var assesmentLossFoot = [ [{text: ' Total', colSpan: 7, bold: true,fillColor: 'lightgrey', alignment: 'right' },{}, {}, {},{},{},{}, {text: "total", bold: true,fillColor: 'lightgrey'}] ]
              assesmentLossTable = [];

              $scope.claimCase.lossAssessments.forEach(function(sourceRow,index) {
                      var dataRow = [];
                      assesmentLossTable.push([
                                      { text: index+1},
                                      { text: sourceRow.vehiclePartParticularType.id},
                                      { text: sourceRow.partCost},
                                      { text: sourceRow.gstRateForPart},
                                     // { text: sourceRow.partCost*(sourceRow.gstRateForPart/100)},
                                      { text: parseInt(sourceRow.partCost)+parseInt(sourceRow.partCost*(sourceRow.gstRateForPart/100))},
                                      { text: sourceRow.depRateForPart},
                                      { text: parseInt(sourceRow.partCost)+parseInt(sourceRow.partCost*(sourceRow.gstRateForPart/100))}
                                      ])
                    });
             assesmentLossTable = assesmentLossHead.concat(assesmentLossTable);
              console.log("assesmentLossTable",assesmentLossTable);
              // End of assesment table


              // starting labour charges

              // ID Heading
              labourChargesHeading = {
        width: '100%',
        table: {
              style: 'tableExample',
            headerRows: 1,
            widths: ['*'],
            body: [
                [{ text: 'LABOUR CHARGES',fillColor: 'lightblue', bold: true,alignment: 'center' }]
             ]
        }
              }
              
              
                      assesmentLossContentLabour = [
                      {
                              width: '80%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                              [ {
                                                  stack: [
                                                      {						
                                                          text: [
                                                              { text: "Assessed for Labour Charges of Accident Vehicle Bearing Reg. Number: ",alignment: 'left'}
                                                          ]
                                                  }
                                                ]
                                              }]
                                      ]
                              }
                      },
                      {
                              width: '20%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                        [ {
                                            stack: [
                                              {		
                                                      text: [
                                                          { text:  $scope.claimCase.vehicleNumber,alignment: 'left'}
                                                      ]								              
                                              }
                                            ]
                                        }]
                                      ]
                              }
                      }
    ]
    

              var labourChargesHead = [[
          { text: 'S.No',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'Description',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'Assesd for INR',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'TAX %(GST)',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'Total Amount in INR',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'Rate of Dep.',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'Final Amount in INR',fillColor: 'lightgrey', bold: true, alignment: 'center' },
    ]];
              var labourChargesFoot = [ [{text: ' Total', colSpan: 7, bold: true,fillColor: 'lightgrey', alignment: 'right' },{}, {}, {},{},{},{}, {text: "total", bold: true,fillColor: 'lightgrey'}] ]
              labourChargesBody = [];

              $scope.claimCase.lossAssessments.forEach(function(sourceRow,index) {
                      var dataRow = [];
                      labourChargesBody.push([
                                      { text: index+1},
                                      { text: sourceRow.vehiclePartParticularType.id},
                                      { text: sourceRow.labourCost},
                                      { text: sourceRow.gstRateForLabour},
                                      { text: parseInt(sourceRow.labourCost)+parseInt(sourceRow.labourCost*(sourceRow.gstRateForLabour/100))},
                                      { text: sourceRow.depRateForLabour},
                                      { text: parseInt(sourceRow.labourCost)+parseInt(sourceRow.labourCost*(sourceRow.gstRateForLabour/100))},
                                      ])
                    });
              labourChargesTable = labourChargesHead.concat(labourChargesBody);
              console.log("labourChargesTable",labourChargesTable);
              //  End of labour charges
              
              
              
        
        
              // ID Heading
              paintChargesHeading = {
        width: '100%',
        table: {
              style: 'tableExample',
            headerRows: 1,
            widths: ['*'],
            body: [
                [{ text: 'PAINT CHARGES',fillColor: 'lightblue', bold: true,alignment: 'center' }]
             ]
        }
              }
              
              
                      assesmentLossContentPaint = [
                      {
                              width: '80%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                              [ {
                                                  stack: [
                                                      {						
                                                          text: [
                                                              { text: "Assessed for Paint Charges of Accident Vehicle Bearing Reg. Number: ",alignment: 'left'}
                                                          ]
                                                  }
                                                ]
                                              }]
                                      ]
                              }
                      },
                      {
                              width: '20%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                        [ {
                                            stack: [
                                              {		
                                                      text: [
                                                          { text:  $scope.claimCase.vehicleNumber,alignment: 'left'}
                                                      ]								              
                                              }
                                            ]
                                        }]
                                      ]
                              }
                      }
    ]
    

              var paintChargesHead = [[
          { text: 'S.No',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'Description',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'Assesd for INR',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'TAX %(GST)',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'Total Amount in INR',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'Rate of Dep.',fillColor: 'lightgrey', bold: true, alignment: 'center' },
          { text: 'Final Amount in INR',fillColor: 'lightgrey', bold: true, alignment: 'center' },
    ]];
              var labourChargesFoot = [ [{text: ' Total', colSpan: 7, bold: true,fillColor: 'lightgrey', alignment: 'right' },{}, {}, {},{},{},{}, {text: "total", bold: true,fillColor: 'lightgrey'}] ]
              paintChargesBody = [];

              $scope.claimCase.lossAssessments.forEach(function(sourceRow,index) {
                      var dataRow = [];
                      paintChargesBody.push([
                                      { text: index+1},
                                      { text: sourceRow.vehiclePartParticularType.id},
                                      { text: sourceRow.paintCost},
                                      { text: sourceRow.gstRateForPaint},
                                      { text: parseInt(sourceRow.paintCost)+parseInt(sourceRow.paintCost*(sourceRow.gstRateForPaint/100))},
                                      { text: sourceRow.depRateForPaint},
                                      { text: parseInt(sourceRow.paintCost)+parseInt(sourceRow.paintCost*(sourceRow.gstRateForPaint/100))},
                                      ])
                    });
              paintChargesTable = paintChargesHead.concat(paintChargesBody);
              console.log("labourChargesTable",paintChargesTable);
              //  End of labour charges
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        

              // Starting summarry of assesments
             // assesment summary Heading
              assesmentSummaryHeading = {
        width: '100%',
        table: {
              style: 'tableExample',
            headerRows: 1,
            widths: ['*'],
            body: [
                [{ text: 'SUMMARY OF ASSESSMENT',fillColor: 'lightblue', bold: true,alignment: 'center' }]
             ]
        }
              }

            // assessment summary Body
              assesmentSummaryContent = [
                      {
                              width: '50%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                              [ {
                                                  stack: [
                                                      {						
                                                          text: [                                                     
                                                              
                                                              { text: 'Assessed Cost Of Parts:' + $scope.claimCase.assessmentSummary.totalPartCharges + '\n\n',alignment: 'left'},
                                                              { text: 'Supplementary Parts:' + $scope.claimCase.assessmentSummary.supplementryPartCharges + '\n\n',alignment: 'left'},
                                                              { text: 'Assessed Labour Charges:' + $scope.claimCase.assessmentSummary.totalLabourCharges + '\n\n',alignment: 'left'},
                                                              { text: 'Supplementary Labour:' + $scope.claimCase.assessmentSummary.supplementryLabourCharges + '\n\n',alignment: 'left'},
                                                              { text: 'Assessed Paint Charges:' + $scope.claimCase.assessmentSummary.totalPaintCost + '\n\n',alignment: 'left'},
                                                              { text: 'Supplementary Paint:' + $scope.claimCase.assessmentSummary.supplementaryPaintCost + '\n\n',alignment: 'left'}
                                                          ]
                                                  }
                                                ]
                                              }]
                                      ]
                              }
                      },
                      {
                              width: '50%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                        [ {
                                            stack: [
                                              {		
                                                      text: [
                                                        { text: 'Total Labour Charges:' + ($scope.claimCase.assessmentSummary.totalLabourCharges + $scope.claimCase.assessmentSummary.supplementryLabourCharges) + '\n\n',alignment: 'left'},
                                                        { text: 'Total cost of parts :' + ($scope.claimCase.assessmentSummary.totalPartCharges + $scope.claimCase.assessmentSummary.supplementryPartCharges) + '\n\n',alignment: 'left'},
                                                        { text: 'Total cost of paint :' + ($scope.claimCase.assessmentSummary.totalPaintCost + $scope.claimCase.assessmentSummary.supplementaryPaintCost) + '\n\n',alignment: 'left'},
                                                        { text: 'Less Excess (-) :' + $scope.claimCase.assessmentSummary.lessExcess + '\n\n',alignment: 'left'},
                                                        { text: 'Less Salvage cost (-):' + $scope.claimCase.assessmentSummary.lessSalavage + '\n\n',alignment: 'left'},
                                                        { text: 'Total:' + $scope.claimCase.assessmentSummary.total + '\n\n',alignment: 'left'}
                                                      ]
                                              }
                                            ]
                                        }]
                                      ]
                              }
                      }
//                      {
//                              width: '33.3%',
//                              style: 'tableExample',
//                              table: {
//                                      headerRows: 1,
//                                      widths: ['*'],
//                                      body: [
//                                          [{ text: 'Test',alignment: 'left'}]
//                                           // [{ text: '\n\n\n\n'+$scope.reportDetails.data.assessmentSummary.summaryNotes+'\n\n\n',fillColor: 'lightblue', bold: true,alignment: 'center' }]
//                                      ]
//                              }
//                      }
    ]

              // Summary remarks

            // assesment summary remarks Heading
              assesRemrkHeading = {
        width: '100%',
        table: {
              style: 'tableExample',
            headerRows: 1,
            widths: ['*'],
            body: [
                [{ text: 'Remarks',fillColor: 'lightblue', bold: true,alignment: 'center' }]
             ]
        }
              }

            // assessment summary Body
             assesRemrkContent = [
                      {
                              width: '100%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                              [ {
                                                  stack: [
                                                      {						
                                                          text: [
                                                              { text: '1:	' + $scope.claimCase.accidentDetails.accidentRemarks + '\n\n',alignment: 'left'},
                                                              { text: '2:	' + $scope.claimCase.vehicleDetails.remarks + '\n\n',alignment: 'left'},
                                                              { text: '3:	' + $scope.claimCase.driverDetails.remarks + '\n\n',alignment: 'left'}
                                                          ]
                                                  }
                                                ]
                                              }]
                                      ]
                              }
                      }
    ]

              // End summmary of assesments


              // Enclosed section

              enclosedContent = [
                      {
                              width: '40%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 2,
                                      widths: ['*'],
                                      body: [				 						
                                              [{ text: 'Enclosed please find here with:',fillColor: 'lightpink', bold: true,alignment: 'center' }],
                                              [ {
                                                  stack: [
                                                      {						
                                                          text: [
                                                              { text: '1. Digital Photographs.' + '\n',alignment: 'left'},
                                                              { text: '2. Loss Assessment Fee bill in duplicate.'  + '\n',alignment: 'left'},
                                                              { text: '3. Claim form & Estimate.' + '\n',alignment: 'left'},
                                                              { text: '4. Copies of the vehicular documents duly verified from original as produced.' + '\n',alignment: 'left'},
                                                              { text: '5. Repair Invoice.' + '\n\n\n',alignment: 'left'}
                                                          ]
                                                  }
                                                ]
                                              }]
                                      ]
                              }
                      },
                      {
                              width: '30%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 2,
                                      widths: ['*'],
                                      body: [
                                        [ {
                                            stack: [
                                              {		
                                                  columns: [{
                                                      image:'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAASABIAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx7/2wBDAQUFBQcGBw4ICA4eFBEUHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh7/wAARCACnAMgDASIAAhEBAxEB/8QAHQABAAIDAAMBAAAAAAAAAAAAAAgJBQYHAgMEAf/EAEgQAAECBQEEBgYFCwMCBwAAAAECAwAEBQYRBwgSITETQVFhcYEUIjJCkdEVFlWUoRgjJDNSU1ZykpOxQ4LBCSUXJlRjoqTh/8QAGwEBAAIDAQEAAAAAAAAAAAAAAAMEAQIFBgf/xAAsEQACAgIBAgQGAwADAAAAAAAAAQIDBBESEyEFMUFRBhQiQmGBMnHhFZGh/9oADAMBAAIRAxEAPwCZcIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBGv6i3ZTbIsup3PVlhMtIsle7nBcV7qB3k4EbBEKdubUduvXLLadUp7ekqUsTFUWk+qt7Hqt/7RxPeYkqrc5JI1lLitmpPbT2sD8w9MMzlKl2nllbbKmMlpJPBOfCPH8prWX7SpP3eOPEknPbHipbafadbHioR1lj1r0KnVkdj/Ka1l+0qT93h+U1rL9pUn7vHHAtsjIdb/qEfu+3+9b/qEOhX7DqzOxflNay/aVJ+7w/Ka1l+0qT93jju8j943/UI/QUnktJ8DGehX7DqyOyyu1Bq7KzsvNzc3S5mVYdSt9hLGC6jPrJz1cInVZlwSF1WrTbipjgclJ+XS82QeWRxB7weEVYDgeIyOuJZ7A9//o9Q0zqLxK5YqnKWVH2mifXQPA8fMxUyqFGPKKJarG3pks4QhHPLAhCEAIQhACEIQAhHyTFUpks+WJioSrLoGShbyUqA8CY9ZrVHAJNVkcD/AN9PzjOmD74Ri/rHb/23TfvKPnH4q5bdScKrtMB75pHzhpgysIxP1mtz7epn3pHzjweuu2GUb7txUlCe1U2gD/MNMbMzCMD9c7R3kpFz0YqUoJSPTW8knkBxjO5G7vZGMZzDTQNE141BlNNtN6jcTykKmwjoZFkni6+rgkY68cz3CK2H35qbmX52feU/Ozbqn5lxRyVuKOTHXNrHUleoGpr1OkpjfoFvrUxLBJyl5/ktzvxyHhHICpKQpazhKRlR7o6mJVwjyfqVLp7ejMWTbdSvG8aValIBE3UXgguAfqWh7az4DMTto+zVpBISbDTlqszbzbYQt95xRU4rHFR44yY0DYX0zcpdFmNR61KhE7VkdHTULHFqW/a7io/gO+JQRUyL3KWovsTVQ4rucke2cNG3XS4qzZYE9SXFgfDMet3Zp0aW2UfVBpOetLywf8x1+PwEHkQYg5y9yTSON/kx6M/wt/8AZX84wV7bKum01bVRTbVNeptYLRVJPiYUoIcAyAQTjBPA+MSChGVbNPexxRU28zMy0w/JzzKmZyVdUxMtqGChxJwYylmXJPWbeFJu2mk+kUt8OKSDjpGuS0HuIzHaduCwvq1qNL3nIshFNuBPRzW6OCJpI5n+YY8wY4ByOFDI5ER1q5K2vuUpJwkWpWjXZC57Zp1fpjodlJ+XS+0oHqIzjxHKMpETdgnUEql5/TKou5clAZymKUfaaJ9dHkTnwJiWUciyDhJpl2L2tiEIRoZEIQgBGDv65qfZ1nVS5am4luWkJdTqsn2iBwSO8nA84zkQv27dRVVa4pXTWnPfoUhuzdVKT7bvuNnwHHxPdElVbnJJGspcVsjtc1bqd13FULmrE1Mqnqi+p5QDqgEJJ9VI7gIx3RH/ANRN/wB9Ue0kqPjHukpCqT6FOU6jVKeaScF2Xl1LTnxAjtKMYrRS5SbPj6Bv9t/+8qPxUqwrivpVHvdJjKfQdx/wvXPuS/lHsat26XU77Vp1tac4z6KocfhGNxM6mYb0OW/YX/cMfhkpU8C2pXcVkxnPqzdv8H1z7sr5Q+rN4FSUN2hW+lcIQ2DLKwVHgOqG4IamdE2RdMJS+NVkT03J71FoATMTOclLr3+mjyPEjuiUm13qadP9OVU+mLT9OVzelJMA8WkEYW55A4HeYzOz3Y0ppVpDLStR6NidU0Z6rPqP+oRlWT2JHDyiD+t9/wA1qVqRULiccUqnMrVK0pvkEMg+1jtVzjnxXXt36FhvpwNIbbDLSWgoq3eZPvHrMbtojYb2pOplNtgBQkEETVTcA9lhJzu/7jw840l1wNNKdUN7d5J7T1CJ/wCyNpgiwNOmqjUWR9YK2lM1OrI9ZtJGUNd2AePeYtZNvThpENMeUts7HJSzElJsycq0lphhtLbaEjASkDAA8o0zVjUGXsuTZYYYE5VZoHoGCcJSBzWo9kbzEbdott5Go7a3TlDsknoeHUCciMeC4leVlKFvlpv+znfEWfbg4Mraf5dlv2/Jr9b1CvKqOremq67LIVw6OW9RIHYO2MSxcFwMOpLVdqjS+Y3nFDPxj79PanRqNd0tUK/JelyKEkbu7vdGvqWR14jtt0TOnN+0UUxNakWH18Zd9GEONK88fCPX5ORXhTjWqPofm0uy/wDDwGDi3+JVSueVqxeSb/00DTrViuUyry0lcUz6fTH1hsvKGHGCTwOesRIpJCkhSTkEZBji3/gRS5iVSWbnm15A/OJSkgntGI7FTZYydPl5RTy3iy2lsuK5qwMZMeV8aswrZRnjefqta/Z7n4eq8RorlXm916Pe/wBGna7WLL6h6Y1a3HUJMytouyazzQ+nigjz4ecVpqbmGHHZWcbU1NSzimJhChgpWk4OYtliC+29p+m19QZe8qcwG6ZcH5uaCRwbmkjn3bw4+IMUMO3jLi/U7t0NrZxezbkn7Nu6lXbTCfSaW+HFIBx0rXvoPiMxZ3aNdkbntinXBTXA5KT8uh9sg5wFDOPEcvKKr+RwRnqIiWOwRqAeiqOmlSfJVLZnKXvHm0T66B4Hj5mJsyra5o0on9pLSEIRzSyIQhAGqatXnI2Bp/VbonlJxKMnoUE8XHTwQkeJxFZdSqE/WKpOVqquqen6g8qYmFqOTlRzjwEd8239RPrNfLNjU2YC6VQlB2d3DwcmjySf5R+JMR95kknHWTHUw6uMeT9SrfPb0fZb9FqFy3DTbapLZXPVSYTLtAe6CfWUe4DJizqwbUpdnWhTLcpjDaZeRl0tb26MrUBxUe8nJiM2wbpyV+l6n1Rn9aFSlJSoey2Dhbg8Tw+MS4irlW85aXkiWqHGJ4dE1+7R/SIBpocm0f0iPOEVSU8ejb/dp+EOjb/dp+EeUIAjlt43bV6JpxJ2/TmXmpeuzHQTc6k4S22OJb8Vf4zEJt0JAQkYSkboHdFmGuFjS2ommdWth/CXnmuklXMcW3k8UH4jHgTFaLzE1KTD8jPNKZnJR1UvMNqHFK0nBjpYUo8WvUq3p+Z1XZQsim31rDLs1d9kSVFbE8ZVZG9NLB9UAdaQeJiw8AAAAYAirCybmnbLvKk3dTiov0x8LcQDjpWjwWg9xGYs+tmsyNw2/IVymvIek55hLzS0nIIUM/8A5EOZFqe2SUtcTIxrGoVlUm9KYmWnwtp9k70vMt+20f8Akd0bPEddcZ256HqC76PW6jLyU6ylyXShwhAI4KSIn8JxrMjISqnxku6ZzfHMyrExHO6vnF9mj5q9o5d1PUpUiqWqbXUUncV8DGkV63qvRFhNao0xJAq3Q4tHqE/zDhG6aXakT1Ar7q7jqM7PU6ZQEqUtRWWFD3gOyN61C1Us2ctmap8kBWH5lstpaLZCEkj2lE8sc49asrxLHvjVOHNP1S1/nY8H/wAf4Nl40siqzpteje/9OO2vdtxWrMB+j1B3oknK5R1RU04OsYPLxESqtKtMXDbklWZZJSiaaC9080nrHkYhwfzMtlZzupxntMSt0dpczR9OqVKTaFNvlvpFoVzTvHOIp/E9FKqjalqW9f2i/wDBOXkTtnTJtwS/6Zt0aTrfYsrqJprVbafQkvutFyUWebb6eKFZ6uPDzjdo5DtW6lDTvTOY9AeCa7VcylOQD6wJHrOf7Qc+OI8bFNtaPoj8ivqZl5qSm5inz7fRzsm6qXmUZzhaTgxkLTuOoWfdVLuulKKZulvh3GeDjfvoPcRmMWkKSDvrU44olTjijkrUeJJMbRpVZM7qLqBTbQkt5Lcwrpp94D9TLpI3j4nkPGO1PSrfIox/l9JZVZ9ekbotenXDTF78pPy6X2z2Ajl5HhCPooVLkaJRpOkU1hLEnJspZZbSOCUpGBCOI/wXz7Y0XXe/JbTrTOq3G6oekpbLUk3nit9XBAHnx8o3qIEbZeoSrx1PNuSMz0lFt07hCVZQ7NH2j37vL4xJTX1JpGk5cVs4it2Zfddmp11Ts3MuKfmHFHJUtRyYy9j2tUb3vKl2jSkkzFRdCXFjk0yOK1nuxmMOSACpRwkDKj3RMfYS05NNt2a1Fq0sUz9XHRSAWOLUqDzHZvH8AI6d9iqh2Ktcects6jet9WVoTZ9Bp1UYnGaaECUlvRZffAKE9eORPONKoG1np5WLip9GYp1bacnplMu245LgJBUcJJ45xkiPft2UgT+hr88hgrep020+lY9xOcK/zEF2ZxyTnKfUmlkLlptp9JHMEKBipTRGyDk/MnnNxaRa3U56WptNmajNr6OXlmlOuq7EpGT/AIiO42xdNzvf9rr+AogES6TkDr5xu20ldJo+znWash1KHp6noZaJPNToA4d+CYrwl8syzLQCjhI3sDO73nsjGNjqxNyFlnHyLENEdeLX1VrlQo9JkZ+Rm5NkP7s0kDpEZxkY7MiMvrjqxRNJ6LIVOtSc3NiemOgablgCrIGSTnqxEMdkitCi7QlGUteG6iw5KK48yRw/ER0v/qHTyV3BZtLSoFQQ9MKAPEDIA4fGEsdK5Q9DKs3DkdS0w2lba1AvyStOj2/WUPzSFr6ZxKQlsJGSVceXfHEduPT5Nt31K3vTmNynV09FO7o9VEyBwUf5h/gx69gylLn9YqvVzjcp1O3ATz3lnGB8Il3q1ZNO1CsCp2rUQEpm2vzLuMll0cULHgf+YxJqi36Qlzh3KxeRIIz1ERKvYN1F6IzmmNVmCdzem6QVnmg8VtjwPH4xF+u0mo2/Xahb1YZLVRpj6pd9J68clDuIwfOP2gVqpW3XpC46M6pqo014PskH2gD6yD3EZEXroK2vsV65OEu5azGCvW06Pd1K+j6uwVBJ3mnUHC2ldqTHp01u6mX1ZNMuekupXLzrIUoA8W1+8g9hByI2OOVCydU1KD00WbKoXQcJrafocAquhdcZcUaXW5aZbz6ofQUqx3kR8TGiV3KdCXZ2mtIPNYJJHlEjIR2Y/Eeclraf6POS+EfDJS3xa/Zy2xdG6TRZ1qpVmaNWnGjvNoUjdabV27vWfGOpQhHLysu7Knztltncw8HHwq+nRHij1zT7MrLOzMy6lplpBW4tRwEpAySTFb+0NqGvUvVCcrTK1GjyGZOlIzwKAfWc/wBx4/CJKbcuoyqDZjNi0mZ3arXR+kbivWalR7RPZvHh8YhQlKUJS2gYSkYSImw6vvZvfP7QtaWm1Or9lAyYnTsWaa/VHT/60VSXCa5cAD6yoesyx/po7uHE+IiMezTpy5qTqhLyky2TQ6QUzdSVjKXCD6jXmfwBixhpCGm0tNoShCAEpSBgADkBDMt2+CFENLkeUIQigWDAajCvmxa0LW6P6aMm56H0nLf3Tjz7O/EVbIDyekTNBwTYdWJoO+2Hc+tvd+YtoiCO2np0q09RRd9PlymjXAr8+Up9VmaHPPZvc/HMXMOxRlp+pDdHcThLBk0zkoqpocXTUzLZnUt+0Wd4bwHlFqFou0h+1qW7Qej+ilSrZlOj9not0buPKKrSBkhQyORHbEtNhHUnel5nTCrzBU9LBUzSFrV7bPvND+U8R3Z7Imza20pIjokvI7/rjRjcGkdz0lKd9T9Pc3R2kDeH+Iq/VvOUQ73BSE8e4pMW3zTKJiVdYcSFIcQUKB6wRiKq7vpSqRdlyUJxJbVK1F9rdI5JKjj8MRphS84m968mSE2o7nVPbPWmdITMbzlUaafdT1lLbYGT3ZjBbMNkpuew9T6m5T0zZXIehSZWngHAkqJSe0YEclvC6524KfQWJwEM29TjJsA+8OeYm9sYWwKJoHTTMIWHasXJt0KGOCzhP/xxG1q6VevyINTltEE7FqDtJuq2qohZbdkamzvKzjGFhJjse2/OJm9dpVtOT0FHaJ7PWyeHxjl2qlvqtvU66LdUlbaJWpKdazz3Cd5P4Yj572uap3ZXnLgq696ZEoiXA7ENpwP8RYUOUoz/AAROXFOJKH/p4UpAod015WN9+dSw32hKU5P4mJXRxHYkpIpmgNJeLKUOTzrsypQGCrKuBPwjctetQJTTfTWpXC8pJm9zoZFoni6+oYSB4cz3COZc+djLUe0SH+2lV7cq2tqmqCwkTkhKhirzKD6rznup7ykcCY4qCEneJwBxJ7BH647MzDz03OuqenJlxT8w4o5K3FHJJ+MZ7Tm0J6/b5pVoyCV/proM04gZ6GXBytR7OEdSC6Vff0Kcvrl2JZbAdDuKn6eVOq1B1bVEqc30tMlFjiAOC3R2BRx8MxJWOO6/X7J6K6SSsrQGmE1FSESFIliOAwAN8jrCRxiL7e09rG22lC5ykOqA4r6DGfKOcqp3NyRac1DsywKEQElNqbV1h3fc+hZhOPYU0QPwj7Pys9Vfsygf0K+cZ+UsHVj7k74xt0Vun23bs/Xao8lmSkWFPOrJxwAzjxPKIUtbW+p6WwldCoDihzV6wz+Manqxrze+pdrItqtSkhTpEvB18yZVl/HJByeUFiWb7mHbFLzNIv67KjfV6VS76otRen3T0DZPBlgHCEDsGIwgDqlIbl2lPTDqw2w2kZK1ngABH7zPAdwEd52KdOlXZqEu8qhL79Gt9WJfeHqvTZHDHbujj8I6E5KmvsVop2SJN7Mum7em2mMnITDafpiexNVJzrLqh7PgkcI6jCEcZvb2y8loQhCMARqWr1lSOoOn1VteeSn9KZPQOEfqnRxQoeBxG2wjKeu4KoKjIT1Jqc5RqqypmoU99UtMoUOIUk4z5x77erVRtq4abclIcKJ+lzCX2iPeAPrJPcRwiR23XpsabWZfU2ky59Fmt2VrCUD2V8kO+fInuERkBwrIOcfjHYqmra+5SnHhLsWi6d3XTb2symXPSnAuWnmA5ge4r3knvByIgjtYW5PUvX+vuStMn5hioNtTQUxLqWN4pweIHdG67DOopoN2zGnlSfxT6sTMU0qPBt8D1kD+YdXaO+JrKabUreU2hR5ZKY56bx7Cz2siVTylCrVWnpSks0SqpcnZltkFUosAAqAPHEWlWxTGqNblOpLOejk5ZthOexKQP+I+4MsgghpGRy9UR5xrde7TMIKHkQg24rKqktqzIXLS6POzkvVpLo3jLNFf51vhxxy9XEcCnKDcplHUC162CoboJk144+UWtqQhWN5KVY5ZGY8eia/do/pESV5bhHjo1lUm9mq6N0hdB0rtqkuoLbkvTmkrSoYKVbuSCPOIY7YmowvjUs0Knvb9FtxSmgQfVemT7au/HLyiVO09qM3pxpdOz0u4n6XngZSmt9ZcUOKvBIyfhFdKErSj86suOqJW6snJUs8ST5xviV8pc2a3S0tI8iQApazhKRlR7omZsNWAKBZs7qLW0BibrKP0fpOHQyieIV3b2M+AERh0csd/UXUqlWohK/RFLExUXE+5LpOSM9RPLziyCr23SqlaL9qusqapb0r6IW2Vbm63jGARy4RtmW/YjWiH3FfO0fqMdTNT5mpyzn/ZaXvSdMST7YB9d3HefwxHN8d4+MTzGyppAGgj6Im+Axn0pWY8fyT9G/smo/f1xmvKrhFR0ZlS5PeyB+O8fGGD3fGJ2q2TNHSokU2qJ7hPq+Uep/ZI0iWkBqWrDJB4lM8Tn4iN/nYexr0H7kF909kN1XZE41bIulBSoA1xJIwD6byPbyjEfkb2Lk/+Y6/jPD84n5Rn5ysx0JEPaPSqlXK1IUGkMl2o1J9MvLoA6yeKj3AcYsv0jsmn6e2BS7Wp4BTKtDpnMcXXTxWs+JzGiaO7OtoabXa5c0lOz9SnAyWpf0vdIYz7RTjrPKOzxTyL+o+3kTV18EIQhFYlEIQgBCEIAxF6W9T7rtWpW7VGw5KT8uplwY5ZHAjvB4xWLeNt1Czbtqdp1VJE1THi2FH/AFWvcWPEYi1GIvbdmm5qFCltSKRL709Sk9FUUpHF2WJ9o96Sfge6LOLbwlp+TIrYckQ9YmJqUmWJ6QeUzOyjqZiWcScFLiTkRY/otqjQr907p1fXUZOWnC2G55hx5KFNPp4KGD1E8R4xW6CCApJylQyk90etbKFrKt51BPPcWUg/CL9+OrSvXZwLWfp2h/bNO+8o+cPp2h/bNO+8o+cVS+jo/ezH95Xzh6Oj97Mf3lfOK/yP5Jeui1d24rfaSFOVymoB4AmaR848PrTbWCfp+l4AJP6UjkPOKqjLNH2lzB8XlfOHorHa9/dV84fI/kddHWdqLUUai6pvuyL4dodFBlaeU+y4r33O/J5dwEcqWtLbanVn1UDJ74/QAlISkAJAwAI3/Z+sBzUfVSm0RxlS6TJKE5VVdQbSfVR4qOB8Yt/TTX/RD3skSo2J9OFWpp+q6qozu1i4QHiFDi1Lj9Wjz5+YiQEeDDTbDKGWUJQ22kJQlIwEgcABHnHGlJye2XUtLQhCEamRAnAzjMIQAHhiEIQAhCEAIQhACEIQAhCEAI+aqyMrU6bM06eZS9KzLSmnW1DIUlQwRH0wgCs/VbTO4LE1DqNrS1GqVQlAsvU11hgr6RhXEDh1jl5Rrn1buz+D6/8Ac1fKLT1sMrdS6tptTiRhKikEgdxjy3EfsJ+EXI5korWiF0xb2VXfVu7P4Pr/ANzV8oC2rsJAFnV/J4Aehq+UWo7iP2E/CG4jOdxPwjb56XsOhEq2+pt8fwPcX3JXyj9bsq+3F7jdi3CVdnoavlFpUIx89L2HQiVbuWRfrXFdh3CD1D0NXH8Im9si6YP6e6fGcrDRRXq0oTM4hQ4sjHqN+Q595jtUIityJWLTNoVqPdCEIRXJBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIA//9k=',
    	                   // image: '../../images/basketball.jpg',
    	          		 width: 100,
    	                   height: 95,
                           alignment: 'center'
                                                  }]
//                                                      text: [
//                                                              { text: '' + '\n\n',alignment: 'left'},
//                                                      { text: '' + '\n\n',alignment: 'left'},
//                                                      { text: '' + '\n\n\n',alignment: 'left'}								              
//                                                  ]

                                              }
                                            ]
                                        }],
                                        [{ text: '(Hedgerow Technologies Pvt. Ltd.)',fillColor: 'lightpink', bold: true,alignment: 'center' }],
                                      ]
                              }
                      },
                      {
                              width: '30%',
                              style: 'tableExample',
                              table: {
                                      headerRows: 1,
                                      widths: ['*'],
                                      body: [
                                                [ {
                                                    stack: [
                                                      {	
                                                          columns: [{
                                                      image:'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAASABIAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx7/2wBDAQUFBQcGBw4ICA4eFBEUHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh7/wAARCACnAMgDASIAAhEBAxEB/8QAHQABAAIDAAMBAAAAAAAAAAAAAAgJBQYHAgMEAf/EAEgQAAECBQEEBgYFCwMCBwAAAAECAwAEBQYRBwgSITETQVFhcYEUIjJCkdEVFlWUoRgjJDNSU1ZykpOxQ4LBCSUXJlRjoqTh/8QAGwEBAAIDAQEAAAAAAAAAAAAAAAMEAQIFBgf/xAAsEQACAgIBAgQGAwADAAAAAAAAAQIDBBESEyEFMUFRBhQiQmGBMnHhFZGh/9oADAMBAAIRAxEAPwCZcIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBGv6i3ZTbIsup3PVlhMtIsle7nBcV7qB3k4EbBEKdubUduvXLLadUp7ekqUsTFUWk+qt7Hqt/7RxPeYkqrc5JI1lLitmpPbT2sD8w9MMzlKl2nllbbKmMlpJPBOfCPH8prWX7SpP3eOPEknPbHipbafadbHioR1lj1r0KnVkdj/Ka1l+0qT93h+U1rL9pUn7vHHAtsjIdb/qEfu+3+9b/qEOhX7DqzOxflNay/aVJ+7w/Ka1l+0qT93jju8j943/UI/QUnktJ8DGehX7DqyOyyu1Bq7KzsvNzc3S5mVYdSt9hLGC6jPrJz1cInVZlwSF1WrTbipjgclJ+XS82QeWRxB7weEVYDgeIyOuJZ7A9//o9Q0zqLxK5YqnKWVH2mifXQPA8fMxUyqFGPKKJarG3pks4QhHPLAhCEAIQhACEIQAhHyTFUpks+WJioSrLoGShbyUqA8CY9ZrVHAJNVkcD/AN9PzjOmD74Ri/rHb/23TfvKPnH4q5bdScKrtMB75pHzhpgysIxP1mtz7epn3pHzjweuu2GUb7txUlCe1U2gD/MNMbMzCMD9c7R3kpFz0YqUoJSPTW8knkBxjO5G7vZGMZzDTQNE141BlNNtN6jcTykKmwjoZFkni6+rgkY68cz3CK2H35qbmX52feU/Ozbqn5lxRyVuKOTHXNrHUleoGpr1OkpjfoFvrUxLBJyl5/ktzvxyHhHICpKQpazhKRlR7o6mJVwjyfqVLp7ejMWTbdSvG8aValIBE3UXgguAfqWh7az4DMTto+zVpBISbDTlqszbzbYQt95xRU4rHFR44yY0DYX0zcpdFmNR61KhE7VkdHTULHFqW/a7io/gO+JQRUyL3KWovsTVQ4rucke2cNG3XS4qzZYE9SXFgfDMet3Zp0aW2UfVBpOetLywf8x1+PwEHkQYg5y9yTSON/kx6M/wt/8AZX84wV7bKum01bVRTbVNeptYLRVJPiYUoIcAyAQTjBPA+MSChGVbNPexxRU28zMy0w/JzzKmZyVdUxMtqGChxJwYylmXJPWbeFJu2mk+kUt8OKSDjpGuS0HuIzHaduCwvq1qNL3nIshFNuBPRzW6OCJpI5n+YY8wY4ByOFDI5ER1q5K2vuUpJwkWpWjXZC57Zp1fpjodlJ+XS+0oHqIzjxHKMpETdgnUEql5/TKou5clAZymKUfaaJ9dHkTnwJiWUciyDhJpl2L2tiEIRoZEIQgBGDv65qfZ1nVS5am4luWkJdTqsn2iBwSO8nA84zkQv27dRVVa4pXTWnPfoUhuzdVKT7bvuNnwHHxPdElVbnJJGspcVsjtc1bqd13FULmrE1Mqnqi+p5QDqgEJJ9VI7gIx3RH/ANRN/wB9Ue0kqPjHukpCqT6FOU6jVKeaScF2Xl1LTnxAjtKMYrRS5SbPj6Bv9t/+8qPxUqwrivpVHvdJjKfQdx/wvXPuS/lHsat26XU77Vp1tac4z6KocfhGNxM6mYb0OW/YX/cMfhkpU8C2pXcVkxnPqzdv8H1z7sr5Q+rN4FSUN2hW+lcIQ2DLKwVHgOqG4IamdE2RdMJS+NVkT03J71FoATMTOclLr3+mjyPEjuiUm13qadP9OVU+mLT9OVzelJMA8WkEYW55A4HeYzOz3Y0ppVpDLStR6NidU0Z6rPqP+oRlWT2JHDyiD+t9/wA1qVqRULiccUqnMrVK0pvkEMg+1jtVzjnxXXt36FhvpwNIbbDLSWgoq3eZPvHrMbtojYb2pOplNtgBQkEETVTcA9lhJzu/7jw840l1wNNKdUN7d5J7T1CJ/wCyNpgiwNOmqjUWR9YK2lM1OrI9ZtJGUNd2AePeYtZNvThpENMeUts7HJSzElJsycq0lphhtLbaEjASkDAA8o0zVjUGXsuTZYYYE5VZoHoGCcJSBzWo9kbzEbdott5Go7a3TlDsknoeHUCciMeC4leVlKFvlpv+znfEWfbg4Mraf5dlv2/Jr9b1CvKqOremq67LIVw6OW9RIHYO2MSxcFwMOpLVdqjS+Y3nFDPxj79PanRqNd0tUK/JelyKEkbu7vdGvqWR14jtt0TOnN+0UUxNakWH18Zd9GEONK88fCPX5ORXhTjWqPofm0uy/wDDwGDi3+JVSueVqxeSb/00DTrViuUyry0lcUz6fTH1hsvKGHGCTwOesRIpJCkhSTkEZBji3/gRS5iVSWbnm15A/OJSkgntGI7FTZYydPl5RTy3iy2lsuK5qwMZMeV8aswrZRnjefqta/Z7n4eq8RorlXm916Pe/wBGna7WLL6h6Y1a3HUJMytouyazzQ+nigjz4ecVpqbmGHHZWcbU1NSzimJhChgpWk4OYtliC+29p+m19QZe8qcwG6ZcH5uaCRwbmkjn3bw4+IMUMO3jLi/U7t0NrZxezbkn7Nu6lXbTCfSaW+HFIBx0rXvoPiMxZ3aNdkbntinXBTXA5KT8uh9sg5wFDOPEcvKKr+RwRnqIiWOwRqAeiqOmlSfJVLZnKXvHm0T66B4Hj5mJsyra5o0on9pLSEIRzSyIQhAGqatXnI2Bp/VbonlJxKMnoUE8XHTwQkeJxFZdSqE/WKpOVqquqen6g8qYmFqOTlRzjwEd8239RPrNfLNjU2YC6VQlB2d3DwcmjySf5R+JMR95kknHWTHUw6uMeT9SrfPb0fZb9FqFy3DTbapLZXPVSYTLtAe6CfWUe4DJizqwbUpdnWhTLcpjDaZeRl0tb26MrUBxUe8nJiM2wbpyV+l6n1Rn9aFSlJSoey2Dhbg8Tw+MS4irlW85aXkiWqHGJ4dE1+7R/SIBpocm0f0iPOEVSU8ejb/dp+EOjb/dp+EeUIAjlt43bV6JpxJ2/TmXmpeuzHQTc6k4S22OJb8Vf4zEJt0JAQkYSkboHdFmGuFjS2ommdWth/CXnmuklXMcW3k8UH4jHgTFaLzE1KTD8jPNKZnJR1UvMNqHFK0nBjpYUo8WvUq3p+Z1XZQsim31rDLs1d9kSVFbE8ZVZG9NLB9UAdaQeJiw8AAAAYAirCybmnbLvKk3dTiov0x8LcQDjpWjwWg9xGYs+tmsyNw2/IVymvIek55hLzS0nIIUM/8A5EOZFqe2SUtcTIxrGoVlUm9KYmWnwtp9k70vMt+20f8Akd0bPEddcZ256HqC76PW6jLyU6ylyXShwhAI4KSIn8JxrMjISqnxku6ZzfHMyrExHO6vnF9mj5q9o5d1PUpUiqWqbXUUncV8DGkV63qvRFhNao0xJAq3Q4tHqE/zDhG6aXakT1Ar7q7jqM7PU6ZQEqUtRWWFD3gOyN61C1Us2ctmap8kBWH5lstpaLZCEkj2lE8sc49asrxLHvjVOHNP1S1/nY8H/wAf4Nl40siqzpteje/9OO2vdtxWrMB+j1B3oknK5R1RU04OsYPLxESqtKtMXDbklWZZJSiaaC9080nrHkYhwfzMtlZzupxntMSt0dpczR9OqVKTaFNvlvpFoVzTvHOIp/E9FKqjalqW9f2i/wDBOXkTtnTJtwS/6Zt0aTrfYsrqJprVbafQkvutFyUWebb6eKFZ6uPDzjdo5DtW6lDTvTOY9AeCa7VcylOQD6wJHrOf7Qc+OI8bFNtaPoj8ivqZl5qSm5inz7fRzsm6qXmUZzhaTgxkLTuOoWfdVLuulKKZulvh3GeDjfvoPcRmMWkKSDvrU44olTjijkrUeJJMbRpVZM7qLqBTbQkt5Lcwrpp94D9TLpI3j4nkPGO1PSrfIox/l9JZVZ9ekbotenXDTF78pPy6X2z2Ajl5HhCPooVLkaJRpOkU1hLEnJspZZbSOCUpGBCOI/wXz7Y0XXe/JbTrTOq3G6oekpbLUk3nit9XBAHnx8o3qIEbZeoSrx1PNuSMz0lFt07hCVZQ7NH2j37vL4xJTX1JpGk5cVs4it2Zfddmp11Ts3MuKfmHFHJUtRyYy9j2tUb3vKl2jSkkzFRdCXFjk0yOK1nuxmMOSACpRwkDKj3RMfYS05NNt2a1Fq0sUz9XHRSAWOLUqDzHZvH8AI6d9iqh2Ktcects6jet9WVoTZ9Bp1UYnGaaECUlvRZffAKE9eORPONKoG1np5WLip9GYp1bacnplMu245LgJBUcJJ45xkiPft2UgT+hr88hgrep020+lY9xOcK/zEF2ZxyTnKfUmlkLlptp9JHMEKBipTRGyDk/MnnNxaRa3U56WptNmajNr6OXlmlOuq7EpGT/AIiO42xdNzvf9rr+AogES6TkDr5xu20ldJo+znWash1KHp6noZaJPNToA4d+CYrwl8syzLQCjhI3sDO73nsjGNjqxNyFlnHyLENEdeLX1VrlQo9JkZ+Rm5NkP7s0kDpEZxkY7MiMvrjqxRNJ6LIVOtSc3NiemOgablgCrIGSTnqxEMdkitCi7QlGUteG6iw5KK48yRw/ER0v/qHTyV3BZtLSoFQQ9MKAPEDIA4fGEsdK5Q9DKs3DkdS0w2lba1AvyStOj2/WUPzSFr6ZxKQlsJGSVceXfHEduPT5Nt31K3vTmNynV09FO7o9VEyBwUf5h/gx69gylLn9YqvVzjcp1O3ATz3lnGB8Il3q1ZNO1CsCp2rUQEpm2vzLuMll0cULHgf+YxJqi36Qlzh3KxeRIIz1ERKvYN1F6IzmmNVmCdzem6QVnmg8VtjwPH4xF+u0mo2/Xahb1YZLVRpj6pd9J68clDuIwfOP2gVqpW3XpC46M6pqo014PskH2gD6yD3EZEXroK2vsV65OEu5azGCvW06Pd1K+j6uwVBJ3mnUHC2ldqTHp01u6mX1ZNMuekupXLzrIUoA8W1+8g9hByI2OOVCydU1KD00WbKoXQcJrafocAquhdcZcUaXW5aZbz6ofQUqx3kR8TGiV3KdCXZ2mtIPNYJJHlEjIR2Y/Eeclraf6POS+EfDJS3xa/Zy2xdG6TRZ1qpVmaNWnGjvNoUjdabV27vWfGOpQhHLysu7Knztltncw8HHwq+nRHij1zT7MrLOzMy6lplpBW4tRwEpAySTFb+0NqGvUvVCcrTK1GjyGZOlIzwKAfWc/wBx4/CJKbcuoyqDZjNi0mZ3arXR+kbivWalR7RPZvHh8YhQlKUJS2gYSkYSImw6vvZvfP7QtaWm1Or9lAyYnTsWaa/VHT/60VSXCa5cAD6yoesyx/po7uHE+IiMezTpy5qTqhLyky2TQ6QUzdSVjKXCD6jXmfwBixhpCGm0tNoShCAEpSBgADkBDMt2+CFENLkeUIQigWDAajCvmxa0LW6P6aMm56H0nLf3Tjz7O/EVbIDyekTNBwTYdWJoO+2Hc+tvd+YtoiCO2np0q09RRd9PlymjXAr8+Up9VmaHPPZvc/HMXMOxRlp+pDdHcThLBk0zkoqpocXTUzLZnUt+0Wd4bwHlFqFou0h+1qW7Qej+ilSrZlOj9not0buPKKrSBkhQyORHbEtNhHUnel5nTCrzBU9LBUzSFrV7bPvND+U8R3Z7Imza20pIjokvI7/rjRjcGkdz0lKd9T9Pc3R2kDeH+Iq/VvOUQ73BSE8e4pMW3zTKJiVdYcSFIcQUKB6wRiKq7vpSqRdlyUJxJbVK1F9rdI5JKjj8MRphS84m968mSE2o7nVPbPWmdITMbzlUaafdT1lLbYGT3ZjBbMNkpuew9T6m5T0zZXIehSZWngHAkqJSe0YEclvC6524KfQWJwEM29TjJsA+8OeYm9sYWwKJoHTTMIWHasXJt0KGOCzhP/xxG1q6VevyINTltEE7FqDtJuq2qohZbdkamzvKzjGFhJjse2/OJm9dpVtOT0FHaJ7PWyeHxjl2qlvqtvU66LdUlbaJWpKdazz3Cd5P4Yj572uap3ZXnLgq696ZEoiXA7ENpwP8RYUOUoz/AAROXFOJKH/p4UpAod015WN9+dSw32hKU5P4mJXRxHYkpIpmgNJeLKUOTzrsypQGCrKuBPwjctetQJTTfTWpXC8pJm9zoZFoni6+oYSB4cz3COZc+djLUe0SH+2lV7cq2tqmqCwkTkhKhirzKD6rznup7ykcCY4qCEneJwBxJ7BH647MzDz03OuqenJlxT8w4o5K3FHJJ+MZ7Tm0J6/b5pVoyCV/proM04gZ6GXBytR7OEdSC6Vff0Kcvrl2JZbAdDuKn6eVOq1B1bVEqc30tMlFjiAOC3R2BRx8MxJWOO6/X7J6K6SSsrQGmE1FSESFIliOAwAN8jrCRxiL7e09rG22lC5ykOqA4r6DGfKOcqp3NyRac1DsywKEQElNqbV1h3fc+hZhOPYU0QPwj7Pys9Vfsygf0K+cZ+UsHVj7k74xt0Vun23bs/Xao8lmSkWFPOrJxwAzjxPKIUtbW+p6WwldCoDihzV6wz+Manqxrze+pdrItqtSkhTpEvB18yZVl/HJByeUFiWb7mHbFLzNIv67KjfV6VS76otRen3T0DZPBlgHCEDsGIwgDqlIbl2lPTDqw2w2kZK1ngABH7zPAdwEd52KdOlXZqEu8qhL79Gt9WJfeHqvTZHDHbujj8I6E5KmvsVop2SJN7Mum7em2mMnITDafpiexNVJzrLqh7PgkcI6jCEcZvb2y8loQhCMARqWr1lSOoOn1VteeSn9KZPQOEfqnRxQoeBxG2wjKeu4KoKjIT1Jqc5RqqypmoU99UtMoUOIUk4z5x77erVRtq4abclIcKJ+lzCX2iPeAPrJPcRwiR23XpsabWZfU2ky59Fmt2VrCUD2V8kO+fInuERkBwrIOcfjHYqmra+5SnHhLsWi6d3XTb2symXPSnAuWnmA5ge4r3knvByIgjtYW5PUvX+vuStMn5hioNtTQUxLqWN4pweIHdG67DOopoN2zGnlSfxT6sTMU0qPBt8D1kD+YdXaO+JrKabUreU2hR5ZKY56bx7Cz2siVTylCrVWnpSks0SqpcnZltkFUosAAqAPHEWlWxTGqNblOpLOejk5ZthOexKQP+I+4MsgghpGRy9UR5xrde7TMIKHkQg24rKqktqzIXLS6POzkvVpLo3jLNFf51vhxxy9XEcCnKDcplHUC162CoboJk144+UWtqQhWN5KVY5ZGY8eia/do/pESV5bhHjo1lUm9mq6N0hdB0rtqkuoLbkvTmkrSoYKVbuSCPOIY7YmowvjUs0Knvb9FtxSmgQfVemT7au/HLyiVO09qM3pxpdOz0u4n6XngZSmt9ZcUOKvBIyfhFdKErSj86suOqJW6snJUs8ST5xviV8pc2a3S0tI8iQApazhKRlR7omZsNWAKBZs7qLW0BibrKP0fpOHQyieIV3b2M+AERh0csd/UXUqlWohK/RFLExUXE+5LpOSM9RPLziyCr23SqlaL9qusqapb0r6IW2Vbm63jGARy4RtmW/YjWiH3FfO0fqMdTNT5mpyzn/ZaXvSdMST7YB9d3HefwxHN8d4+MTzGyppAGgj6Im+Axn0pWY8fyT9G/smo/f1xmvKrhFR0ZlS5PeyB+O8fGGD3fGJ2q2TNHSokU2qJ7hPq+Uep/ZI0iWkBqWrDJB4lM8Tn4iN/nYexr0H7kF909kN1XZE41bIulBSoA1xJIwD6byPbyjEfkb2Lk/+Y6/jPD84n5Rn5ysx0JEPaPSqlXK1IUGkMl2o1J9MvLoA6yeKj3AcYsv0jsmn6e2BS7Wp4BTKtDpnMcXXTxWs+JzGiaO7OtoabXa5c0lOz9SnAyWpf0vdIYz7RTjrPKOzxTyL+o+3kTV18EIQhFYlEIQgBCEIAxF6W9T7rtWpW7VGw5KT8uplwY5ZHAjvB4xWLeNt1Czbtqdp1VJE1THi2FH/AFWvcWPEYi1GIvbdmm5qFCltSKRL709Sk9FUUpHF2WJ9o96Sfge6LOLbwlp+TIrYckQ9YmJqUmWJ6QeUzOyjqZiWcScFLiTkRY/otqjQr907p1fXUZOWnC2G55hx5KFNPp4KGD1E8R4xW6CCApJylQyk90etbKFrKt51BPPcWUg/CL9+OrSvXZwLWfp2h/bNO+8o+cPp2h/bNO+8o+cVS+jo/ezH95Xzh6Oj97Mf3lfOK/yP5Jeui1d24rfaSFOVymoB4AmaR848PrTbWCfp+l4AJP6UjkPOKqjLNH2lzB8XlfOHorHa9/dV84fI/kddHWdqLUUai6pvuyL4dodFBlaeU+y4r33O/J5dwEcqWtLbanVn1UDJ74/QAlISkAJAwAI3/Z+sBzUfVSm0RxlS6TJKE5VVdQbSfVR4qOB8Yt/TTX/RD3skSo2J9OFWpp+q6qozu1i4QHiFDi1Lj9Wjz5+YiQEeDDTbDKGWUJQ22kJQlIwEgcABHnHGlJye2XUtLQhCEamRAnAzjMIQAHhiEIQAhCEAIQhACEIQAhCEAI+aqyMrU6bM06eZS9KzLSmnW1DIUlQwRH0wgCs/VbTO4LE1DqNrS1GqVQlAsvU11hgr6RhXEDh1jl5Rrn1buz+D6/8Ac1fKLT1sMrdS6tptTiRhKikEgdxjy3EfsJ+EXI5korWiF0xb2VXfVu7P4Pr/ANzV8oC2rsJAFnV/J4Aehq+UWo7iP2E/CG4jOdxPwjb56XsOhEq2+pt8fwPcX3JXyj9bsq+3F7jdi3CVdnoavlFpUIx89L2HQiVbuWRfrXFdh3CD1D0NXH8Im9si6YP6e6fGcrDRRXq0oTM4hQ4sjHqN+Q595jtUIityJWLTNoVqPdCEIRXJBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIA//9k=',
    	                    //image: 'util/case-image/39/1582116760759.jpeg',
    	          		 width: 100,
    	                   height: 95,
                           alignment: 'center'
                                                  }]
//                                                              text: [
//                                                                      { text: '' + '\n\n',alignment: 'left'},
//                                                              { text: '' + '\n\n',alignment: 'left'},
//                                                              { text: '' + '\n\n',alignment: 'left'},
//                                                              { text: '' + '\n\n',alignment: 'left'}								              ]
                                                      }
                                                    ]
                                                }],
                                                [{ text: '(Customer/Garage Signature)',fillColor: 'lightpink', bold: true,alignment: 'center' }],
                                              ]
                              }
                      }
    ]

    generateAllPDF($scope);
}

function spliceArray(a, chunkSize){
    var chunk, result = [];

    while (a.length > 0) {

      chunk = a.splice(0, chunkSize);
      
      if(chunk.length != chunkSize)
          chunk.push({});

      result.push(chunk);

    }
    
    console.log(result);
    
    return result;
}

var generateParticularPDF = function($scope){
    var docDefinition = {

              header: function() {
                    return [
                            generateHeader
                    ]
                },
                footer: function(currentPage, pageCount) { 
                    return [
                        {text: currentPage.toString() + ' of ' + pageCount, alignment: 'center', style: 'footer'}
                    ]
                },
                content: [
                            {
                                  columns: referenceContent,
                                  columnGap: 0
                            },
                        {
                              columns: [
                            reportHeading
                          ],
                          columnGap: 20
                        },
                            ' ',
                            {
                                  columns: [
                                      assesmentLossHeading
                                          ],
                                          columnGap: 20
                                },
                            {
                                  columns: assesmentLossContent,
                                  columnGap: 0
                            },
                            {	
                                style: 'topTable',
                                table: {body:assesmentLossTable},
                                layout: {
                                    paddingLeft: function(i, node) { return 8; },
                                    paddingRight: function(i, node) { return 8; },
                                    paddingTop: function(i, node) { return 6; },
                                    paddingBottom: function(i, node) { return 6; },
                                    fillColor: function (i, node) {
                                        return (i % 2 === 0) ?  '#F5F5F5' : null;
                                    }
                                }
                            },
                            ' ',
                            {
                                  columns: [
                                      labourChargesHeading
                                          ],
                                },
                            {
                                  columns: assesmentLossContentLabour,
                                  columnGap: 0
                            },
                            {	
                                style: 'topTable',
                                table: {body:labourChargesTable},
                                layout: {
                                    paddingLeft: function(i, node) { return 8; },
                                    paddingRight: function(i, node) { return 8; },
                                    paddingTop: function(i, node) { return 6; },
                                    paddingBottom: function(i, node) { return 6; },
                                    fillColor: function (i, node) {
                                        return (i % 2 === 0) ?  '#F5F5F5' : null;
                                    }
                                }
                            },
                            ' ',
                            {
                                  columns: [
                                      paintChargesHeading
                                          ],
                                },
                            {
                                  columns: assesmentLossContentPaint,
                                  columnGap: 0
                            },
                            {	
                                style: 'topTable',
                                table: {body:paintChargesTable},
                                layout: {
                                    paddingLeft: function(i, node) { return 8; },
                                    paddingRight: function(i, node) { return 8; },
                                    paddingTop: function(i, node) { return 6; },
                                    paddingBottom: function(i, node) { return 6; },
                                    fillColor: function (i, node) {
                                        return (i % 2 === 0) ?  '#F5F5F5' : null;
                                    }
                                }
                            },
                            ' ',
                            {
                                  columns: [
                                      assesmentSummaryHeading
                                          ],
                                          columnGap: 20
                                },
                            {
                                  columns: assesmentSummaryContent,
                                  columnGap: 0
                            },
                            ' ',
                            ' ',
                            {
                                  columns: [
                                      assesRemrkHeading
                                          ],
                                          columnGap: 20
                                },
                            {
                                  columns: assesRemrkContent,
                                  columnGap: 0
                            },
                            ' ',
                            {
                                  columns: enclosedContent,
                                  columnGap: 0
                            }
    ],
// pageSize: 'A4',
// pageMargins: [62,80,62,80],
                pageOrientation: 'potrait',
                pageMargins: [10, 130, 15, 30],
                styles: {
                    topHeader: {
                        fontSize: 20,
                        bold: true,
                        margin: [0, 16, 0, 50],
                        alignment: 'left'
                    },
                    table: {
                        fontSize: 8,
                        alignment: 'left',
                        color: 'black',
                        margin: [0, 5, 0, 15]
                    },
                    header: {       
                        fontSize: 16,
                        bold: true,
                        margin: [0, 10, 0, 15],
                        alignment: 'left'
                    },
                    footer: {
                        fontSize: 8,
                        margin: [0, 25, 0, 17],
                        alignment: 'center'
                    }
                }
            };
          //pdfMake.createPdf(docDefinition).open();
          var pdfDocGenerator = pdfMake.createPdf(dd);
        pdfDocGenerator.getDataUrl((dataUrl) => {
          $('#myModal').modal('show');
          $('#modal-body').html('');
          const targetElement = document.querySelector('#modal-body');
          const iframe = document.createElement('iframe');
          iframe.src = dataUrl;
          targetElement.appendChild(iframe);
        });
}

var generateAllPDF = function($scope){
    var docDefinition = {

          header: function() {
                return [
                        generateHeader
                ]
            },
            footer: function(currentPage, pageCount) { 
                return [
                    {text: currentPage.toString() + ' of ' + pageCount, alignment: 'center', style: 'footer'}
                ]
            },
            content: [
                        {
                              columns: referenceContent,
                              columnGap: 0
                        },
                    {
                          columns: [
                        reportHeading
                      ],
                      columnGap: 20
                    },
                    //' ',
                    {
                        columns: [idHeading],
                        columnGap: 20
                    },
                   {
                      columns: idContent,
                      columnGap: 0
                    },
                    ' ',
                    {
                        columns: [
                                  vdHeading
                                ],
                                columnGap: 20
                    },
                    {
                              columns: vdContent,
                              columnGap: 0
                        },
                        ' ',
                        {
                              columns: [
                                        ddHeading
                                      ],
                                      columnGap: 20
                            },
                        {
                              columns: ddContent,
                              columnGap: 0
                            },
                        ' ',
                        {
                              columns: [
                                        adHeading
                                      ],
                                      columnGap: 20
                            },
                        {
                              columns: adContent,
                              columnGap: 0
                            },
                        ' ',
                        {
                              columns: [
                                        cnnHeading
                                      ],
                                  columnGap: 20
                            },
                        {
                              columns: cnnContent,
                              columnGap: 0
                        },
                        ' ',
                        {
                              columns: [
                                  assesmentLossHeading
                                      ],
                                      columnGap: 20
                            },
                        {
                              columns: assesmentLossContent,
                              columnGap: 0
                        },
                        {	
                            style: 'topTable',
                            table: {body:assesmentLossTable},
                            layout: {
                                paddingLeft: function(i, node) { return 8; },
                                paddingRight: function(i, node) { return 8; },
                                paddingTop: function(i, node) { return 6; },
                                paddingBottom: function(i, node) { return 6; },
                                fillColor: function (i, node) {
                                    return (i % 2 === 0) ?  '#F5F5F5' : null;
                                }
                            }
                        },
                        ' ',
                        {
                              columns: [
                                  labourChargesHeading
                                      ],
                            },
                        {
                                  columns: assesmentLossContentLabour,
                                  columnGap: 0
                            },
                        {	
                            style: 'topTable',
                            table: {body:labourChargesTable},
                            layout: {
                                paddingLeft: function(i, node) { return 8; },
                                paddingRight: function(i, node) { return 8; },
                                paddingTop: function(i, node) { return 6; },
                                paddingBottom: function(i, node) { return 6; },
                                fillColor: function (i, node) {
                                    return (i % 2 === 0) ?  '#F5F5F5' : null;
                                }
                            }
                        },
                        ' ',
                        {
                              columns: [
                                  paintChargesHeading
                                      ],
                            },
                        {
                                  columns: assesmentLossContentPaint,
                                  columnGap: 0
                            },
                        {	
                            style: 'topTable',
                            table: {body:paintChargesTable},
                            layout: {
                                paddingLeft: function(i, node) { return 8; },
                                paddingRight: function(i, node) { return 8; },
                                paddingTop: function(i, node) { return 6; },
                                paddingBottom: function(i, node) { return 6; },
                                fillColor: function (i, node) {
                                    return (i % 2 === 0) ?  '#F5F5F5' : null;
                                }
                            }
                        },
                        ' ',
                        {
                              columns: [
                                  assesmentSummaryHeading
                                      ],
                                      columnGap: 20
                            },
                        {
                              columns: assesmentSummaryContent,
                              columnGap: 0
                        },
                        ' ',
                        {
                              columns: [
                                  assesRemrkHeading
                                      ],
                                      columnGap: 20
                            },
                        {
                              columns: assesRemrkContent,
                              columnGap: 0
                        },
                        ' ',
                        {
                              columns: enclosedContent,
                              columnGap: 0
                        },
                        {
                            table: {
			    widths: ["50%","50%"],
				body: spliceArray($scope.imageArray, 2)
                            }
                        }
],
 pageSize: 'A4',
// pageMargins: [62,80,62,80],
            pageOrientation: 'portrait',
            pageMargins: [10, 130, 15, 30],
            styles: {
                topHeader: {
                    fontSize: 20,
                    bold: true,
                    margin: [0, 16, 0, 50],
                    alignment: 'left'
                },
                table: {
                    fontSize: 8,
                    alignment: 'left',
                    color: 'black',
                    margin: [0, 5, 0, 15]
                },
                header: {       
                    fontSize: 16,
                    bold: true,
                    margin: [0, 10, 0, 15],
                    alignment: 'left'
                },
                footer: {
                    fontSize: 8,
                    margin: [0, 25, 0, 17],
                    alignment: 'center'
                }
            }
        };
    //pdfMake.createPdf(docDefinition).open();
      var pdfDocGenerator = pdfMake.createPdf(docDefinition);
        pdfDocGenerator.getDataUrl((dataUrl) => {
          $('#myModal').modal('show');
          $('#modal-body').html('');
          const targetElement = document.querySelector('#modal-body');
          const iframe = document.createElement('iframe');
          iframe.src = dataUrl;
          targetElement.appendChild(iframe);
        });
        
        
};

var formatDate = function(reqdate) {    
    // function for reusability
    var date = new Date(reqdate);
  var d = date.getUTCDate().toString(),           // getUTCDate() returns
                                                                                                          // 1 - 31
      m = (date.getUTCMonth() + 1).toString(),    // getUTCMonth() returns
                                                                                                          // 0 - 11
      y = date.getUTCFullYear().toString(),       // getUTCFullYear()
                                                                                                          // returns a 4-digit
                                                                                                          // year
      formatted = '';
  if (d.length === 1) {                           // pad to two digits if
                                                                                                          // needed
      d = '0' + d;
  }
  if (m.length === 1) {                           // pad to two digits if
                                                                                                          // needed
      m = '0' + m;
  }
  formatted = d + '-' + m + '-' + y;              // concatenate for
                                                                                                          // output
  return formatted;
};