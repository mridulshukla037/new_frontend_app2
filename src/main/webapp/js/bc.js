var jaduApp = angular.module('jaduApp', ["ngRoute", "ngResource", "ngCookies"])
        .config(function($httpProvider, $provide) {
        
        var countAjaxRequests = 0;
        


        // register the interceptor as a service
        $provide.factory('myHttpInterceptor', function($q) {
          return {
            // optional method
            'request': function(config) {
                // do something on success
                return config;
            },

            // optional method
           'requestError': function(rejection) {
                $(".loader").css("display", "none");
                // do something on error
                return $q.reject(rejection);
            },



            // optional method
            'response': function(response) {
                var fileType = response.config.url.split('.').pop();
                if(fileType !== "html"){
                    countAjaxRequests--;
                    if(countAjaxRequests === 0)
                        $(".loader").css("display", "none");
                }
                
                
                // do something on success
                return response;
            },

            // optional method
           'responseError': function(rejection, b, c) {
                console.log(rejection, b, c)
                //alert(rejection.statusText);
                var fileType = rejection.config.url.split('.').pop();
                if(fileType !== "html"){
                    countAjaxRequests--;
                    if(countAjaxRequests === 0)
                        $(".loader").css("display", "none");
                }
                // do something on error
                return $q.reject(rejection);
            }
          };
        });

        $httpProvider.interceptors.push('myHttpInterceptor');

    });


jaduApp.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                
                if(changeEvent.target.files[0])
                    reader.readAsDataURL(changeEvent.target.files[0]);
                else
                    scope.$apply(function () {
                        scope.fileread = null;
                    });
            });
        }
    }
}]);

jaduApp.controller('HomeController',['$scope', '$timeout', '$location', '$compile', '$q','ajaxService','$http',  function($scope, $timeout, $location, $compile, $q, ajaxService, $http) {
        $scope.currentBlock = undefined;
        
        $scope.calculated = null;
        
        ajaxService.getBCData().then(function(data){
            //console.log(data.join(""))
            $scope.result = JSON.parse("[" + data.join(",") + "]");
            console.log($scope.result)
        });
        
        $scope.getBlockForTxnId =  function(){
            $scope.currentBlock = $scope.getTransaction($scope.txnid);
        };
        
        $scope.getTransaction = function(id){
            for(var i=0; i<$scope.result.length; i++){
                var item = $scope.result[i];
                
                if(item.block === id){
                    return item;
                    
                }
            }
            
            return null;
        };
        
        
        $scope.validateFile = function(){
            $scope.calculated = null;
            if(!$scope.myfile){
                alert("Please upload a file!")
                return false;
            }
            
            if(!$scope.mytxnid){
                alert("Please enter a transaction id!")
                return false;
            }
            
            
            
            var txn = $scope.getTransaction($scope.mytxnid);
            
            if(!txn){
                alert("Invalid trnasaction id!")
                return false;
            }
            
            var file = document.getElementById("fileForUpload").files[0];
            
            var formData = new FormData();
                    formData.append("file", file);
                    
            var fd = new FormData();
            fd.append('file', file);
            fd.append('data', 'string');
            
            $.ajax({
                url: "bc/upload-file-get-md5",
                data: fd,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(response) {
                    $scope.calculated = {};

                    //md5($scope.myfile);
                    //var hash = md5.create();
                    //hash.update($scope.myfile);
                    $scope.calculated.checksum = response; //hash.hex();
                   // console.log($scope.myfile)
                    //console.log(hex_md5($scope.myfile))

                    //console.log(txn.time + $scope.calculated.checksum)

                    var hash2 = sha256.create();
                    hash2.update($scope.calculated.checksum);
                    $scope.calculated.payload_hash = hash2.hex();

                    var hash3 = sha256.create();
                    hash3.update("None" + $scope.calculated.payload_hash);
                    $scope.calculated.message_hash = hash3.hex();

                    $scope.calculated.message_hash_bc = txn.messages.hash;

                    $scope.calculated.match = ($scope.calculated.message_hash === $scope.calculated.message_hash_bc);
                    $scope.$apply();
                },
                error: function(error) {

                }
            });
                
        }
}]);