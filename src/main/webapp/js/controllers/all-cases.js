/* global angular, jaduApp */

jaduApp
		.controller(
				'AllCasesController',
				[
						'$scope',
						'$timeout',
						'$location',
						'$compile',
						'ajaxService',
						'$q',
						'$window',
						'NgTableParams',
						function($scope, $timeout, $location, $compile,
								ajaxService, $q, $window, NgTableParams) {

							$scope.showInspectionReport = function(id) {
								$window.open('#/report/' + id, '_blank');
							};

							$scope.getCaseDetailsModel = {
								case_id : "",
								vehicle_id : "",
								customer_phone : "",
								requester_phone : ""
							};

							$scope.clearValues = function() {
								$scope.getCaseDetailsModel = {
									case_id : "",
									vehicle_id : "",
									customer_phone : "",
									requester_phone : ""
								};
							}

							$scope.fetchAllCases = function(spinnerText) {
								$scope.allCases = [];

								$scope.allCasesTable = new NgTableParams({
									count : 50
								// count per page
								}, {
									dataset : $scope.allCases
								});
							};

							$scope.getCaseDetails = function() {
								var fieldName = '';
								var fieldValue = "";
								// To get form value and name
								if ($scope.getCaseDetailsModel.case_id != "") {
									fieldName = "case_id";
									fieldValue = $scope.getCaseDetailsModel.case_id;
								} else if ($scope.getCaseDetailsModel.vehicle_id != "") {
									fieldName = "vehicle_id";
									fieldValue = $scope.getCaseDetailsModel.vehicle_id;
								} else if ($scope.getCaseDetailsModel.customer_phone != "") {
									fieldName = "customer_phone";
									fieldValue = $scope.getCaseDetailsModel.customer_phone;
								} else {
									fieldName = "requester_phone";
									fieldValue = $scope.getCaseDetailsModel.requester_phone;
								}
								ajaxService.getRequestedCases({
									field : fieldName,
									value : fieldValue
								}, "Fetching details. Please wait!").then(
										function(res) {
											$scope.data = res.data;
											$scope.clearValues();
										}, function(err) {
											alert("No results found");
											$scope.clearValues();
										});
							}

							$scope.fetchCompletedCases = function(spinnerText) {
								$q
										.all(
												[ ajaxService
														.getCompletedCases(
																null,
																spinnerText) ])
										.then(
												function(res) {
													$scope.allCompletedCases = res[0].data;

													$scope.completedCasesTable = new NgTableParams(
															{
																count : 50
															// count per page
															},
															{
																dataset : $scope.allCompletedCases
															});
												});
							};

							$scope.fetchClosedCases = function(spinnerText) {
								$q
										.all(
												[ ajaxService.getClosedCases(
														null, spinnerText) ])
										.then(
												function(res) {
													$scope.closedCases = res[0].data;

													$scope.closedCasesTable = new NgTableParams(
															{
																count : 50
															// count per page
															},
															{
																dataset : $scope.closedCases
															});

												});
							};

							$scope.refreshDeamonActive = false;

							$scope.refreshDeamon = function() {
								$timeout(function() {
									$scope.fetchAllCases('refresh');
									$scope.fetchCompletedCases('refresh');
									$scope.fetchClosedCases('refresh');
								}, 60000);
							};

							$scope.fetchAllCases("Fetching data. Please wait!");

							$scope.reopenCaseForQC = function(qcCase) {
								$scope.currentCase = qcCase;

								$("#manual-qc-container").modal('show');
								$("#manual-qc-container .modal-body")
										.html(
												$compile(
														"<reopen-case-qc inspection-case='currentCase' callback='reopenCaseCallback()'></reopen-case-qc>")
														($scope));

							};

							$scope.reopenCaseCallback = function() {
								$("#manual-qc-container").modal('hide');
								$scope
										.fetchCompletedCases("Fetching data. Please wait!");
							};

							$scope.allRemarks = [ {
								id : "recommended",
								title : "Recommended"
							}, {
								id : "not-recommended",
								title : "Not Recommended"
							}, {
								id : "underwriter",
								title : "Underwriter"
							} ];

							$scope.showComments = function(data) {

								$scope.selectedCaseForComment = data;

								$("#manual-qc-container3").modal('show');
								$("#manual-qc-container3 .modal-body")
										.html(
												$compile(
														"<case-comments inspection-case='selectedCaseForComment'></case-comments>")
														($scope));
							};

							$scope.reopenCase = function(qcCase) {
								$scope.currentCase = qcCase;

								$("#reopen-case-container").modal('show');
								$("#reopen-case-container .modal-body")
										.html(
												$compile(
														"<reopen-case inspection-case='currentCase' callback='reopenCaseCallback()'></reopen-case>")
														($scope));
							}

							$scope.sendReport = function(qcCase) {
								ajaxService
										.sendReport({
											case_id : qcCase
										}, "Mailing report. Please wait!")
										.then(
												function() {
													alert("Report sent successfully!");
												},
												function() {
													alert("Unable to send report. Please try again later!");
												});
							}
						} ]);
