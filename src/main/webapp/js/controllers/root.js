jaduApp.controller('RootController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', 'fcm','$rootScope',  function($scope, $timeout, $location, $compile, ajaxService, fcm, $rootScope) {
    // console.log('root controller called', $rootScope);
    // console.log($rootScope.userConfigurations);
    // console.log($rootScope['userConfigurations']);
    // console.log($rootScope.userConfigurations == undefined);
    // console.log('#root controller called', $rootScope);

    $scope.$on("user", function (evt, data) {
        console.log("##############################Inside MyController1 : " ,$rootScope);
    });

    // if($rootScope.userConfigurations == undefined){
    //   window.localStorage.getItem('userConfigurations');
    //   console.log('local item available ');
    // }
    $scope.getGroupClass = function(group){
      var pathList = [
        '/create-case',
        '/address-requests',
        '/case-requests',
        '/scheduled-cases',
        '/qc',
        '/completed-cases',
        '/closed-cases',
        '/all-cases',
        '/all-customers'
      ];

      var groupActive = group + ' active';
      if(pathList.indexOf($location.path()) != -1){
        $scope.isManageCasesActive = 'collapse in';
        return groupActive
      }
      else{
        return group;
      }
    }

    $scope.getClass = function (path) {
      // console.log('$location.path()==> ',$location.path());
        return ($location.path().substr(0, path.length) === path) ? 'active' : '';
    };

    $scope.logout = function(){
        window.localStorage.removeItem("access_token");
        window.localStorage.removeItem("refresh_token");
        window.localStorage.removeItem("userProfile");
        window.localStorage.removeItem("userConfiguration");
        window.location = "login";
    };

    angular.element(document).ready(function () {
        //******************************user-detail-fetch*************

        //******************************

        var config = {
          apiKey: "AIzaSyDyXX2b1GGVwd6G0k_PCYHdacLTYlYEIJI",
          authDomain: "inspection-200812.firebaseapp.com",
          databaseURL: "https://inspection-200812.firebaseio.com",
          projectId: "inspection-200812",
          storageBucket: "inspection-200812.appspot.com",
          messagingSenderId: "998522894848"
          };

          $scope.messaging = fcm.init(config);
          // console.log($scope.messaging);
          $scope.notification ={};

          $scope.showNotification = true;

          $scope.messaging.onMessage(function(payload) {
            console.log('on message payload====>',payload);
            if(typeof(payload.data.notification) == "string"){
              payload.data.notification = JSON.parse(payload.data.notification);
            }
            $scope.$apply(function() {
              $scope.notification = {
                "body":payload.data.notification.body,
                "click_action":payload.data.notification.click_action,
                "icon":"images/notification-icon.png",
                "title":payload.data.notification.title
              };
              $scope.showNotification = true;
            });
          });

          $scope.notificationDismiss = function(){
            $scope.showNotification = false;
          }

          $scope.notificationView = function(url){
            //alert('url');
            $scope.myValue = true;
          }

          $scope.denyPermission = function(){
            $scope.askPermission = 0;
            window.localStorage.notification = 0;
          }

          $scope.requestPermission = function(){
            $scope.askPermission = 0;
            fcm.requestPermission()
              .then(function(permission){
                if(permission){
                  window.localStorage.setItem('notification', 1);
                  $scope.requestToken();
                }
                else{
                  window.localStorage.setItem('notification', 0);
                }
              });
          }

          $scope.requestToken = function(){
            fcm.requestToken()
              .then(function(fcmDeviceToken){
                if(fcmDeviceToken == 0){
                  console.log('No token');
                }
                else{
                  // console.log('fcmDeviceToken', fcmDeviceToken);
                  window.localStorage.setItem('fcmDeviceToken', fcmDeviceToken);
                  $scope.registerDevice(fcmDeviceToken);
                }
              });
          };

          $scope.registerDevice = function(fcmDeviceToken){
            // console.log('to be registered');
            // console.log('FCM Device Token', fcmDeviceToken);
            ajaxService.subscribeWebDevice({device_id:fcmDeviceToken}, 'Please Wait!...')
            .then(function(res){
              // console.info('Device Registered for notification');
            })
            .catch(function(e){
              console.log('Device Regstration Error', e);
            })
            // device.registerDevice(fcmDeviceToken)
            //   .then(function(res){
            //       if(res == false){
            //         window.localStorage.setItem('deviceRegistered', 0);
            //       }
            //       else{
            //         window.localStorage.setItem('deviceRegistered', 1);
            //       }
            //   });
          };

          $scope.askNotificationPermission = function(){
            // console.log('fired this 101 root.js');
            $scope.requestPermission();

            // if( window.localStorage.notification == 0 || window.localStorage.notification == undefined){
            //   console.log('fired this 103 root.js');
            //
            //   // if($scope.notIOS){
            //     $scope.askPermission = 1;
            //   // }
            //   // else {
            //     // $scope.askPermission = 0;
            //   // }
            //
            // }
            // else
            // if( window.localStorage.notification == 1){
            //   $scope.askPermission = 0;
            //   if(window.localStorage.deviceRegistered == 0 || window.localStorage.deviceRegistered == undefined){
            //     $scope.requestPermission();
            //   }
            // }
          }

          var messaging = firebase.messaging();
          /*navigator.serviceWorker.register('./firebase-messaging-sw.js')
          .then((registration) => {
            messaging.useServiceWorker(registration);
            $scope.askNotificationPermission();

            // Request permission and get token.....
          });
*/

//****************************************************************************

      //     const messaging = firebase.messaging();
      //     navigator.serviceWorker.register('./firebase-messaging-sw.js')
      //       .then((registration) => {
      //         messaging.useServiceWorker(registration);
      //
      //         // Request permission and get token.....
      //       });
      //     messaging.usePublicVapidKey("BDgBR52fmOgp-RHBIYiMumKu-MgQ45Edyj8jTa61poYvAbiqCesKHVbqmkKz9Fe_qKBvdzTL9yHofxvMCwgvamw");
      //
      //       messaging.requestPermission().then(function() {
      //           console.log('Notification permission granted.');
      //           messaging.getToken().then(function(currentToken) {
      //               if (currentToken) {
      //                   console.log(currentToken)
      // //                sendTokenToServer(currentToken);
      // //                updateUIForPushEnabled(currentToken);
      //               } else {
      //                 // Show permission request.
      //                 console.log('No Instance ID token available. Request permission to generate one.');
      //                 // Show permission UI.
      // //                updateUIForPushPermissionRequired();
      // //                setTokenSentToServer(false);
      //               }
      //             })
      //             .catch(function(err) {
      //               console.log('An error occurred while retrieving token. ', err);
      // //              showToken('Error retrieving Instance ID token. ', err);
      // //              setTokenSentToServer(false);
      //             })
      //             })
      //                     .catch(function(err) {
      //               console.log('Unable to get permission to notify.', err);
      //             });
      //
      //        // Get Instance ID token. Initially this makes a network call, once retrieved
      //       // subsequent calls to getToken will return from cache.


    });
}]);
