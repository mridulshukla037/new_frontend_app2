jaduApp.controller('AllCustomersController',['$scope', '$timeout', '$location', '$compile' , '$auth', '$q','ajaxService', function($scope, $timeout, $location, $compile, $auth, $q, ajaxService) {

  $scope.customerList = [];

  ajaxService.getAllCustomers({},'Loading Customer Data....')
  .then(function(res){
        $scope.customerList = res.data;
  })
  .catch(function(e){
        alert("Unable to fetch Customer data!");
        console.error('All customer fetch error', e);
  })

  $scope.showCustomerActionsModel = function(agent){
      console.log(agent);
      $scope.currentAgent = agent;
      $('#agent-actions').modal('show');
      $('#agent-actions .modal-body').html($compile("<user-actions  user='currentAgent' callback='updateCallback()'></user-actions>")($scope));
  };

  $scope.updateCallback = function(){
    $('#agent-actions').modal('hide');
  };

  $scope.goToCreateCase = function(){
    $location.url('/create-case');
  };

}]);
