jaduApp.controller('CreateAgentController',['$scope', '$timeout', '$location', '$compile' , '$auth', '$q','ajaxService', function($scope, $timeout, $location, $compile, $auth, $q, ajaxService) {
  console.log('Create Agent Controller Fired');

  $scope.defaultCompany = 'IFFCO Tokio General Insurance Co. Ltd.';


  $scope.newAgent = {
    company_branch_division_id:"",
    first_name:"",
    last_name:"",
    phone_number:"",
    email_id:""
  }

  ajaxService.getInsuranceCompanies({},spinnerText)
  .then(function(res){
    $scope.companyData = res.data;
  })
  .catch(function(e){
    console.error('Get company names error',e);
  });

  $scope.createAgent = function(){
    ajaxService.createAgent($scope.newAgent, "Creating Agent. Please wait!")
    .then(function(res){
      alert('Agent Created!')
      $scope.newAgent = {
        company_branch_division_id:"",
        first_name:"",
        last_name:"",
        phone_number:"",
        email_id:""
      }
    })
    .catch(function(e){
        alert(e.data.error_message);
      console.error("Unable to create Agent!",e);
    })
  }

  // ***********************************************************************************

  $scope.defaultState = 'TELANGANA';
  var spinnerText = 'Loading...';

  $scope.branchFilterDataUniqe = { // to be mapped after data fetch
    states:[],
    divisions:[],
    branches:[]
  };

  $scope.branchFilterData = { // to be updated by user activity
    states:[],
    divisions:[],
    branches:[]
  };

  $scope.branchFilter = {
    company:$scope.defaultCompany,
    state:$scope.defaultState,
    division:null,
    branch:null
  };

  ajaxService.getBranchDivisionByCompany($scope.branchFilter.company,spinnerText)
  .then(function(res){
    console.log(res);
    $scope.locationData = res.data;

    _.map(_.uniqBy(res.data, 'state'), function (item) {
      $scope.branchFilterData.states.push(item.state.trim());
      $scope.branchFilterDataUniqe.states.push(item);
    });
    var temp = [];
    _.map(_.uniqBy($scope.branchFilterData.states), function (item) {
      temp.push(item);
    });
    $scope.branchFilterData.states = temp;

    _.map(_.uniqBy(res.data, 'division'), function (item) {
      $scope.branchFilterDataUniqe.divisions.push(item);
    });

    _.map(_.uniqBy(res.data, 'branch'), function (item) {
      $scope.branchFilterDataUniqe.branches.push(item);
    });
  })
  .catch(function(e){
    console.log('Error Fetching Locations ',e);
  })

  $scope.setDivisionOptions = function(){
    $scope.defaultState = '';
    $scope.branchFilterData.divisions = [];
    _.filter($scope.branchFilterDataUniqe.divisions, function(item) {
      if(item.state == $scope.branchFilter.state){
        $scope.branchFilterData.divisions.push(item.division);
      }
    });
  }

  $scope.setBranchOptions = function(){
    $scope.branchFilterData.branches = [];
    _.filter($scope.branchFilterDataUniqe.branches, function(item) {
      if(item.division == $scope.branchFilter.division){
        $scope.branchFilterData.branches.push(item);
      }
    });
  }

  $scope.setBranchID = function(){
    $scope.newAgent.company_branch_division_id = '';
    $scope.newAgent.company_branch_division_id = $scope.branchFilter.branch;
  }

}]);
