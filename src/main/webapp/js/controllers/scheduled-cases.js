jaduApp.controller('ScheduledCasesController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', '$q','$mdDialog',  function($scope, $timeout, $location, $compile, ajaxService, $q, $mdDialog) {

  $scope.refreshDeamonActive = false;
  
  

  $scope.fetchScheduledCases = function(spinnerText){
    $q.all([
      ajaxService.getScheduledCases(null, spinnerText),
      ajaxService.getVehicleTypes(null, 'Please Wait!...')
    ])
    .then(function(res){
      $scope.qcCases = res[0].data;
      $scope.vehicleTypes = [];
      
      _.map(_.uniqBy(res[1].data, 'id'), function (item) {
              $scope.vehicleTypes.push(item);
            });

      if($scope.qcCases != undefined){

        $scope.qcCases.forEach(function(qcCase, index){
          if(qcCase.inspectionType == 'SELF_INSPECT'){
            $scope.qcCases[index].inspectionTypeDisplay = 'Self';
          }
          else
          if(qcCase.inspectionType == 'ASSIGN_TO_CUSTOMER'){
            $scope.qcCases[index].inspectionTypeDisplay = 'Customer';
            if($scope.qcCases[index].inspectionTime == null){
              $scope.qcCases[index].inspectionTime = $scope.qcCases[index].creationTime;
            }
          }
          else
          if(qcCase.inspectionType == 'ASSIGN_TO_INSPECTOR'){
            $scope.qcCases[index].inspectionTypeDisplay = 'Inspector';
          }
          // console.log('case ', index, qcCase, $scope.qcCases[index]);
        });

        $scope.flagCalculator();
        // $scope.refreshDeamon();
      }
      else{
        console.log('No Scheduled Cases Recieved!');
      }
    });
  };

  angular.element(document).ready(function () {
    $scope.fetchScheduledCases("Fetching scheduled cases. Please wait!");
  });

  $scope.flagCalculator = function(){
    $scope.currentTime = moment((moment().unix())*1000);
      for(i=0;i<$scope.qcCases.length;i++){
        if($scope.qcCases[i].inspectionStartTime == null){//insepection not started
          var insTime = moment($scope.qcCases[i].inspectionTime);
          var diff = $scope.currentTime.diff(insTime, 'minutes', true);
          if(diff >= 15 && diff < 30){
            $scope.qcCases[i].flagStatus = 'b-status-orange';
          }
          else
          if(diff >= 30){
            $scope.qcCases[i].flagStatus = 'a-status-red';
          }
          else{
            $scope.qcCases[i].flagStatus = '';
          }
        }
        else{
          var insStartTime = moment($scope.qcCases[i].inspectionStartTime);

          var diff = $scope.currentTime.diff(insStartTime, 'minutes', true);

          if(diff >= 15 && diff < 30){
            $scope.qcCases[i].flagStatus = 'b-status-orange';
          }
          else
          if(diff >= 30){
            $scope.qcCases[i].flagStatus = 'a-status-red';
          }
          else{
            $scope.qcCases[i].flagStatus = '';
          }
        }
    }



    $timeout(function(){
      $scope.flagCalculator();
    },6000)

  }

  $scope.refreshDeamon = function(){

    $timeout(function(){
      $scope.fetchScheduledCases('refresh');
    },60000)
  }

  $scope.newCaseData = {};

  $scope.editCase = function(id){
       $scope.editCaseId = id;
        $scope.currentCase = getCaseById(id);

        var timestamp = moment.unix($scope.currentCase.inspectionTime/1000);
        $scope.currentCase.inspectionTimeModel = timestamp;
        $scope.currentCase.inspectionTime = ( timestamp.format("YYYY/MM/DD HH:mm:ss") );

        $scope.newCaseData.newCustomerPhoneNumber = $scope.currentCase.customerPhoneNumber;
        $scope.newCaseData.newRequestorPhoneNumber = $scope.currentCase.requestorPhoneNumber;
        $scope.newCaseData.newVehicleNumber = $scope.currentCase.vehicleNumber;
        $("#modal-2").modal('toggle');
    };

  $scope.allotCase = function(id){
        $scope.currentCase = getCaseById(id);

        var timestamp = moment.unix($scope.currentCase.inspectionTime/1000);
        $scope.currentCase.inspectionTimeModel = timestamp;
        $scope.currentCase.inspectionTime = ( timestamp.format("YYYY/MM/DD HH:mm:ss") );
        $("#modal-3").modal('toggle');
    };

  $scope.cancelCase = function(){

      $("#modal-2").modal('hide');
      ajaxService.cancelCase({case_id:$scope.currentCase.id},'Cancelling Case. Please Wait...')
      .then(function(res){
        alert('Case Cancelled!');
        $scope.fetchScheduledCases("Updating Case. Please wait!");
      })
      .catch(function(e){
        console.error('Case Cancellation Error',e);
      })
    };

  $scope.rescheduleCaseCallback = function(){
      console.log('rescheduleCaseCallback called');
      $("#modal-2").modal('hide');
      $scope.fetchScheduledCases("Updating Case. Please wait!");
    };

  $scope.closeCaseCallback = function(){
      console.log('rescheduleCaseCallback called');
      $("#modal-2").modal('hide');
      $scope.fetchScheduledCases("Updating Case. Please wait!");
    };

  function getCaseById(id){
    for(var i=0; i< $scope.qcCases.length; i++){
      if($scope.qcCases[i].id === id)
        return $scope.qcCases[i];
    }
    return null;
  }

  $scope.showTimeline = function(){
      $("#timelineModal").modal('toggle');
    }

//-------------------------------------------------

  $scope.updateCaseDetails = function(){
    var dataCPN = {
      case_id:$scope.editCaseId,
      customer_phone_number:$scope.newCaseData.newCustomerPhoneNumber
    }
    var dataRPN = {
      case_id:$scope.editCaseId,
      phone_number:$scope.newCaseData.newRequestorPhoneNumber
    }
    var dataVN = {
      case_id:$scope.editCaseId,
      vehicle_number:$scope.newCaseData.newVehicleNumber
    }
    console.log('=================>***********',dataVN,dataCPN,dataRPN);
    $q.all([
      ajaxService.updateCustomerPhoneNumber(dataCPN),
      ajaxService.updateAgentPhoneNumber(dataRPN),
      ajaxService.updateVehicleNumber(dataVN)
    ])
    .then(function(res){
      alert('Case Details Updated!');
      $scope.fetchScheduledCases('refresh');
    })
    .catch(function(){
      alert('Error in Case Details!');
    });

  };

  $scope.manualQCInit = function(data){
      $scope.selectedCaseForManualQc = data;
      $("#manual-qc-container").modal('show');
      $("#manual-qc-container .modal-body").html($compile("<bucket-tree callback='onFileSelect(file)'></bucket-tree>")($scope));
  };

  $scope.onFileSelect = function(file){
        $("#manual-qc-container").modal('hide');
        var ask = window.confirm("Do you want to use file: " + file + " for QC of case: " + $scope.selectedCaseForManualQc.id);
        if (ask) {
            ajaxService.manualQC({
                case_id : $scope.selectedCaseForManualQc.id,
                zip_file : file
            }, "Doing QC with file selected. Please wait!").then(function(){
                alert("QC done successfully");
            }, function(err){
                alert("Unable to use file for QC. Please try again later.");
                console.log(err);
            });
        }
  };

    $scope.showComments = function(data){        
      
       $scope.selectedCaseForManualQc = data;
      
       $("#manual-qc-container3").modal('show');
       $("#manual-qc-container3 .modal-body").html($compile("<case-comments inspection-case='selectedCaseForManualQc'></case-comments>")($scope));
    };


    $scope.uploadInspectionFileModel = function(data){
        $scope.selectedCaseForManualQc = data;
      
        $("#manual-qc-container2").modal('show');
    };
  
    $scope.showComments = function(data){        
      
       $scope.selectedCaseForManualQc = data;
      
       $("#manual-qc-container3").modal('show');
       $("#manual-qc-container3 .modal-body").html($compile("<case-comments inspection-case='selectedCaseForManualQc'></case-comments>")($scope));
    };
  
    $scope.uploadFileManually = function(){
        var file = document.getElementById('manualUploadFile').files[0];
        var formData = new FormData();
        formData.append('case_file', file);
        formData.append('case_id', $scope.selectedCaseForManualQc.id);

        console.log(formData)

        $q.all([
                ajaxService.uploadInspectionFile(formData, "Uploading file. Please wait!"),
                
        ]).then(
                function(){
                    ajaxService.reupload($scope.selectedCaseForManualQc.id, "Uploading file. Please wait!");
                    alert("File uploaded successfully!");
                    $("#manual-qc-container2").modal('hide');
                    $location.url('/qc');
                }, function(err){

                    if(err.data.error_message)
                        alert(err.data.error_message);
                    else
                        alert("Unable to upload file!");
                    console.log(err);
                });
    };
    
    $scope.moveToQC  = function(ic){
        $scope.selectedCaseForQC = ic;
        $("#move-to-qc").modal('show');
        $("#move-to-qc .modal-title").html('Move case ' + ic + " to QC!");
    };
    
    $scope.moveToQCSubmit = function(){
        ajaxService.moveToQC({
            case_id: $scope.selectedCaseForQC,
            vehicle_type: $scope.selectedCaseForQCVehicleType
        }, "Moving case "+$scope.selectedCaseForQC+" to QC. Please wait!").then(function(){
            $("#move-to-qc").modal('hide');
            $(".modal-backdrop").hide();
            alert("Successfully moved to QC!");
            
            window.location.href = '#/qc';
        }, function(a){
            console.log(a);
            alert("Unable to update!");
        })
    };
    
    //Trigger
    $scope.radioChanged = function(value){
    	var data = {
    			secret_key:"magic@W!mwisure",
        		last_one_day:true,
        		self_inspection:true
        		}
    	if(value=="self"){
    		data.self_inspection = true;
    	}
    	if(value=="assign"){
    		data.self_inspection = false;
    	}
    	ajaxService.reminder(data).then(function(result){
    		alert("system call has been initiated");
    		console.log(result)
    	}).error(function(){
    		console.log(err)
    	})
    }
    
    

}]);
