jaduApp.controller('QCController', ['$scope', '$timeout', '$location', '$compile', '$auth', '$q', 'ajaxService', 'NgTableParams', function ($scope, $timeout, $location, $compile, $auth, $q, ajaxService, NgTableParams) {
        $scope.ajaxService = ajaxService;

        $scope.getPurposeOfSurvey = function (id) {
            for (var i = 0; i < $scope.purposeOfSurvey.length; i++) {
                if ($scope.purposeOfSurvey[i].id === id)
                    return $scope.purposeOfSurvey[i];
            }

            return null;
        };

        $scope.fetchQCCases = function (spinnerText) {
            angular.element(document).ready(function () {
                $q.all([
                    ajaxService.getQCCases(null, spinnerText),
                    ajaxService.getPurposeOfSurvey(null, spinnerText)]).then(function (res) {
                    $scope.qcCases = res[0].data;

                    $scope.qcCases.forEach(function (qcCase, index) {
                        if (qcCase.inspectionType == 'SELF_INSPECT') {
                            $scope.qcCases[index].inspectionTypeDisplay = 'Self'
                        } else if (qcCase.inspectionType == 'ASSIGN_TO_CUSTOMER') {
                            $scope.qcCases[index].inspectionTypeDisplay = 'Customer'
                        } else if (qcCase.inspectionType == 'ASSIGN_TO_INSPECTOR') {
                            $scope.qcCases[index].inspectionTypeDisplay = 'Inspector'
                        }
                    });

                    $scope.flagCalculator();
                    // if(!$scope.refreshDeamonActive)
                    // $scope.refreshDeamon();

                    $scope.purposeOfInspections = res[1].data;
                    $scope.tableParams = new NgTableParams({}, {dataset: $scope.qcCases});
                    console.log('$scope.qcCases==> ', $scope.qcCases);
                });
            });
        };

        $scope.getPurposeOfSurvey = function (id) {
            for (var i = 0; i < $scope.purposeOfInspections.length; i++) {
                if ($scope.purposeOfInspections[i].id === id)
                    return $scope.purposeOfInspections[i];
            }
            return null;
        };

        $scope.flagCalculator = function () {
            $scope.currentTime = moment((moment().unix()) * 1000);

            for (i = 0; i < $scope.qcCases.length; i++) {
                var insTime = moment($scope.qcCases[i].inspectionSubmitTime);
                var diff = $scope.currentTime.diff(insTime, 'minutes', true);
                if (diff >= 15 && diff < 30) {
                    $scope.qcCases[i].flagStatus = 'b-status-orange';
                } else if (diff >= 30) {
                    $scope.qcCases[i].flagStatus = 'a-status-red';
                } else {
                    $scope.qcCases[i].flagStatus = '';
                }
            }

            $timeout(function () {
                $scope.flagCalculator();
            }, 6000);
        };

        $scope.refreshDeamonActive = false;

        $scope.refreshDeamon = function () {
            // $scope.refreshDeamonActive = false;
            $timeout(function () {
                $scope.fetchQCCases('refresh');
                // $scope.refreshDeamon();
            }, 60000);
        };

        $scope.reopenCase = function (qcCase) {
            $scope.currentCase = qcCase;

            $("#reopen-case-container").modal('show');
            $("#reopen-case-container .modal-body").html($compile("<reopen-case inspection-case='currentCase' callback='reopenCaseCallback()'></reopen-case>")($scope));
        };

        $scope.reopenCaseCallback = function () {
            $("#reopen-case-container").modal('hide');
            $scope.fetchQCCases("Fetching QC cases. Please wait!");
        };

        $scope.showComments = function (data) {

            $scope.selectedCaseForComment = data;

            $("#manual-qc-container3").modal('show');
            $("#manual-qc-container3 .modal-body").html($compile("<case-comments inspection-case='selectedCaseForComment'></case-comments>")($scope));
        };

        $scope.fetchQCCases("Fetching QC cases. Please wait!");

        $scope.downloadUploaded = function (id) {
            ajaxService.downloadUploadedFile(id);
        };
    }]);
