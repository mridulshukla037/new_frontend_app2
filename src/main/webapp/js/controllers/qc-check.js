/* global angular */

jaduApp.controller('QCCheckController', ['$scope', '$timeout', '$location', '$compile', '$auth', '$q', 'ajaxService', 'NgTableParams', '$routeParams', 'Lightbox', '$filter', function ($scope, $timeout, $location, $compile, $auth, $q, ajaxService, NgTableParams, $routeParams, Lightbox, $filter) {

        $scope.caseId = $routeParams.id;

        $scope.activeGroup = "Insurance-details";

        $scope.vehiclePartParticularType = {
            id: null
        };

        $scope.groups = [
            {id: "Insurance-details", value: "Insurance Details", tags: [], active: true},
            {id: "vehicle-details", value: "Vehicle Details", tags: []},
            {id: "Accident-details", value: "Accident Details", tags: []},
            {id: "Driver-details", value: "Driver Details", tags: []},
            {id: "Assessment", value: "Assessment", tags: [], child: [
                    {id: "Assessment-Right", value: "Assessment Right", tags: [], hide: true, partSideId: "Right"},
                    {id: "Assessment-Left", value: "Assessment Left", tags: [], hide: true, partSideId: "Left"},
                    {id: "Assessment-Front", value: "Assessment Front", tags: [], hide: true, partSideId: "Front"},
                    {id: "Assessment-Back", value: "Assessment Back", tags: [], hide: true, partSideId: "Back"},
                    {id: "Assessment-Other", value: "Assessment Other", tags: [], hide: true, partSideId: "Other"}
                ]},
            {id: "Summary", value: "Summary", tags: []},
            {id: "video", value: "Video", tags: ["video"], showCount: false}
        ];

        function toDataURL(url, callback) {
            
            var deferred = $q.defer(); 
            
            var xhr = new XMLHttpRequest();
            xhr.onload = function () {
                var reader = new FileReader();
                reader.onloadend = function () {
                    deferred.resolve(reader.result);
                };
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
            
            return deferred.promise;
        }

        $scope.downloadPDF = function () {
            var requestArray = [];
            for(var i=0; i<$scope.claimCase.claimCasePhotos.length; i++){
                if($scope.claimCase.claimCasePhotos[i].fileName.indexOf(".jpeg") == -1)
                    requestArray.push(toDataURL('util/case-image/'+$scope.claimCase.id+'/' + $scope.claimCase.claimCasePhotos[i].fileName));
            }
            
            for(var i=0; i< $scope.claimCase.lossAssessments.length; i++){
                if($scope.claimCase.lossAssessments[i].lossAssessmentPhotos){
                    for(var j=0; j<$scope.claimCase.lossAssessments[i].lossAssessmentPhotos.length ;j++){
                        requestArray.push(toDataURL('util/case-image/'+$scope.claimCase.id+'/' + $scope.claimCase.lossAssessments[i].lossAssessmentPhotos[j].fileName));
                    }
                }
                
            }
            
            $q.all(requestArray).then(function (dataUrl) {
                
                var result = [];
                for(var i=0; i<dataUrl.length; i++){
                    result.push({ image : dataUrl[i], width: 270});
                }
                
                $scope.imageArray = result;
                
                downloadPDF($scope);
            });    
        };

        $scope.changeActive = function (group, array) {
            $scope.activeGroupData = group;
            $scope.activeGroup = group.id;
            for (var i = 0; i < array.length; i++) {
                array[i].active = false;

                if (array[i].child) {
                    for (var j = 0; j < array[i].child.length; j++) {
                        array[i].child[j].hide = true;
                    }
                }
            }

            if (group.child) {
                for (var i = 0; i < group.child.length; i++) {
                    group.child[i].hide = false;
                }
            }

            group.active = true;
        };

        $scope.getVehiclePartParticularTypeById = function (id) {
            for (var i = 0; i < $scope.claimsConfig.vehiclePartParticularType.length; i++) {
                if ($scope.claimsConfig.vehiclePartParticularType[i].id === id)
                    return $scope.claimsConfig.vehiclePartParticularType[i];
            }
            return null;
        };
        $scope.lossAssessmentPhoto=function(lossAssessment){
            $('#myModal7').modal('show');
            
        }
        $scope.lossAssessmentPhotos = function(lossAssessment){
            console.log(lossAssessment);
            
            $('#myModal2').modal('show');
            
        //
        
        
        
        
        
        $scope.currentLossAssessment = lossAssessment;
            
$("myModal2 .modal-body").html($compile('<div ng-repeat="item incurrentLossAssessment.lossAssessmentPhotos">')
($scope));
        };
        
        $scope.useExisting =function(la){
            $('#myModal3').modal('show');
            
        //
        
        
        
        
        
        $scope.currentCaseAssessment = la;
            
$("myModal3 .modal-body").html($compile('<div ng-repeat="item incurrentCaseAssessment">')
($scope));
$scope.arrayOfImages=[];
        }
        
        $scope.addClaimImage=function(Image){
           
            $scope.arrayOfImages.push({fileName:Image})
        }
        $scope.addPhoto = function(){
        var file = document.getElementById('uploadFileIdentifier').files[0];
        
        if(!file){
            alert("Please choose a file!");
            return false;
        }
        
        var formData = new FormData();
        formData.append('case_file', file);
        formData.append('case_id', $scope.caseId);
        formData.append('comment', $scope.uploadFileComment);
        formData.append('photo_type', $scope.uploadFileType);

 

        ajaxService.addPhoto(formData, "Uploading file. Please wait!").then(
                function(){
                    alert("File uploaded successfully!");
                }, function(err){

 

                    if(err.data.error_message)
                        alert(err.data.error_message);
                    else
                        alert("Unable to upload file!");
                    console.log(err);
                });
    };
        $scope.uploadClaimImages=function(data){
            ajaxService.sendClaimCaseImage(data, "Uploading file. Please wait!").then(
                function(){
                    alert("File uploaded successfully!");
                }, function(err){

 

                    if(err.data.error_message)
                        alert(err.data.error_message);
                    else
                        alert("Unable to upload file!");
                    console.log(err);
                });
        }
        
        $scope.addPhoto = function(){
            var file = document.getElementById('uploadFileIdentifier').files[0];
            
            if(!file){
                alert("Please choose a file!");
                return false;
            }
            
            var formData = new FormData();
            formData.append('case_file', file);
            formData.append('case_id', $scope.caseId);
            formData.append('comment', $scope.uploadFileComment);
            formData.append('photo_type', $scope.uploadFileType);
    
     
    
            ajaxService.addPhoto(formData, "Uploading file. Please wait!").then(
                    function(){
                        alert("File uploaded successfully!");
                    }, function(err){
    
     
    
                        if(err.data.error_message)
                            alert(err.data.error_message);
                        else
                            alert("Unable to upload file!");
                        console.log(err);
                    });
        };


        $scope.addRecordToAssessment = function () {
        	// debugger;
        	
        	var data=$scope.claimCase.lossAssessments;
        	var flag= true;
        	for (var i = 0; i < data.length; i++) {
        		 var innerdata=data[i].vehiclePartParticularType.partParticularType;
        		 if($scope.vehiclePartParticularType.id==innerdata){
        			 flag=false;
        			 break;
        		 }
        		
        	}

           if(flag){
            $scope.claimCase.lossAssessments.push(getTemplateForAssesment($scope.getVehiclePartParticularTypeById($scope.vehiclePartParticularType.id)));
            console.log($scope.claimCase.lossAssessments);
           }else{
        	   console.log('hello');
           }
        };

        $scope.removeRecordToAssessment = function () {
            
            console.log($scope.claimCase.lossAssessments.length);
        };
        ///////////
        var app = angular.module('plunker', ['ui.bootstrap']);

        app.controller('ModalCtrl', function($scope, $uibModal) {
        
          $scope.open = function() {
            var modalInstance =  $uibModal.open({
              templateUrl: "modalContent.html",
              controller: "ModalContentCtrl",
              size: '',
            });
            
            modalInstance.result.then(function(response){
                $scope.result = `${response} button hitted`;
            });
            
          };
        })
        ////////////////
        $scope.updateRepairList = function (data) {
            if (data.damageTypeId === "Repair") {
                data.repairList = $scope.claimsConfig.vehicleRepairType;
                for (var i = 0; i < data.repairList.length; i++) {
                    data.repairList[i].value = data.repairList[i].repairType;
                }
            } else {
                data.repairList = $scope.claimsConfig.replacementProduct;
                for (var i = 0; i < data.repairList.length; i++) {
                    data.repairList[i].value = data.repairList[i].productName;
                }
            }
        };
        
        $scope.getcostconfigurationdata = function(imakeModelVariantId,ipartParticularType,irepairType,idamageType){
        	 $scope.lodaLehsun(imakeModelVariantId,ipartParticularType,irepairType,idamageType);
        };

        $scope.lodaLehsun = function (imakeModelVariantId,ipartParticularType,irepairType,idamageType) {
           

            ajaxService.getCostConfigurationRecord({
                makeModelVariantId: imakeModelVariantId,
                partParticularType: ipartParticularType,
                repairType: irepairType,
                damageType: idamageType
            }, "Fetching data. Please wait!")
                    .then(function (res) {
                        var result = res.data;
                        console.log(result);
                        var data =$scope.claimCase.lossAssessments;
                        
                       for(var p=0;p<data.length;p++){
                               	if(data[p].vehiclePartParticularType.id==ipartParticularType){
                               		data[p].gstRateForPart = result.partGst;
                               		data[p].partCost = result.partCost;
                               		data[p].gstRateForLabour = result.labourGst;
                               		data[p].labourCost = result.labourCost;
                               		data[p].gstRateForPaint = result.paintGst;
                               		data[p].paintCost = result.paintCost;
                               		data[p].depRateForPart = result.rateOfDepForPart;
                               		data[p].depRateForPaint = result.rateOfDepForPaint;
                               		data[p].depRateForLabour = result.rateOfDepForLabour;
                               		break;
                               	}
                        }
                         

                        
                    });
        };

        

        var getTemplateForAssesment = function (vehiclePartParticularType) {
            return {
                vehiclePartParticularType: vehiclePartParticularType,
                partCost: null,
                labourCost: null,
                paintCost: null,
                depRateForPart: null,
                gstRateForPart: null,
                depRateForLabour: null,
                gstRateForLabour: null,
                depRateForPaint: null,
                gstRateForPaint: null
            };
        };

        $scope.calculateFinalTotal = function (cost, depRate, gstRate) {
            return (cost - (cost * depRate / 100)) * (1 + (gstRate / 100));
        };

        $scope.toggleEditMode = function () {
            $scope.editMode = !$scope.editMode;
        };
        $scope.openModalValue = function() {
            $("#qc-check-lossAssessment").modal('show');
        $("#qc-check-lossAssessment .modal-body")
                .html($compile("<h1>hsuhsuhu</h1>")($scope));
          };
        

        $scope.update = function () {
            if (!$scope.claimCase.assessmentSummary)
                $scope.claimCase.assessmentSummary = {};

            console.log($scope.claimCase);

            ajaxService.updateClaimsData($scope.claimCase, "Updating claims data.. Please wait!").then(function (result) {
                console.log(result);
                $scope.claimCase = result.data;
                $scope.claimCaseMasterData = angular.copy($scope.claimCase);
                $scope.editMode = false;
                $scope.showModal=false;
            }, function (error) {
                console.log(error);
            });
        };

        angular.element(document).ready(function () {

            $q.all([
                ajaxService.getClaimsCaseDetails({case_id: $routeParams.id}, "Fetching data. Please wait!"),
                ajaxService.getClaimsConfigDetails(null, "Fetching data. Please wait!")
            ]).then(function (result) {
                console.log(result)
                $scope.claimCase = result[0].data;
                $scope.claimCaseMasterData = angular.copy($scope.claimCase);
                $scope.claimsConfig = result[1].data;
            }).catch(function (error) {
                console.log("caseDetails", error);
            });

// var result =
// JSON.parse('[{"data":{"id":39,"vehicleNumber":"GOVINDA","claimNumber":null,"vehicleDetails":{"id":9,"registeredOwnerName":"Test
// Kumar","chassisNumber":"Chassis Kumar","engineNumber":"Engine
// Kumar","permitNumber":"Permit
// Kumar","makeModelVariant":{"id":2,"type":"4-wheeler","make":"Make
// 2","model":"Model
// 2","createdDate":1560261454000,"updatedDate":null,"variant":"Variant
// 2","subType":"Sub Type
// 2","enabled":true,"createdBy":"admin","updatedBy":null},"vehiclePermitType":null,"vehicleBodyType":null,"fitnessCertificateNumber":"Fitness
// Kumar","passengerCarryingCapacity":"2","dateOfRegistration":null,"validUpto":null,"taxPaidUpto":null,"remarks":"Remarks
// Kumar","vehicleColor":"Blue","vehicleYOM":0,"odometerReading":"0"},"accidentDetails":{"id":10,"voiceSummary":"1582116521756.mp3","accidentLocation":"Kondapur,
// Hyderabad, Telangana,
// India","dateOfAccident":1580039306000,"dateOfSurvey":1582116537000,"dateOfIntimation":null,"licenseNumber":"License
// Kumar","locationOfSurvey":"Plot no.413, Naga Teju Residency, Raja Rajeshwara
// Nagar, Kondapur, Hyderabad, Telangana 500084,
// India","thirdPartyDamage":false,"thirdPartyDamageType":null,"reportedToPolice":false,"policeStationName":"Police
// Kumar","stationDiaryEntryNumber":"Station
// Kumar","punchnamaCarriedOut":null,"accidentRemarks":"Remarks
// Kumar","thirdPartyDamageRemarks":"Third Kumar","otherRemarks":"Other
// Kumar"},"driverDetails":{"id":11,"driverName":"Ravinded","driverRelation":"Self","issueDate":1580647668000,"expiryDate":1582980468000,"issuingAuthority":"Authority
// Kumar","badgeNumber":"Badge Kumar","licenseEndorsement":"License Endorsement
// Kumar","licenseNumber":"FVGBHNJMK","driverLicenseType":null,"remarks":"Remarks
// Kumar"},"insuranceDetails":{"id":21,"insurer":{"id":"IFFCO Tokio General
// Insurance Co. Ltd.","name":"IFFCO Tokio General Insurance Co.
// Ltd.","logoUrl":"iffco.png","type":"Private"},"financer":"test","policyNumber":"TFVGHNKBV","startDate":1580581800000,"endDate":1582914600000,"insuredName":"Govinda","insuredAddress":"Kondapur,
// Near Hyundai service
// center","insuredMobileNumber":"8297395048","idv":null,"sumInsured":null,"remarks":null},"vehicleFuelType":null,"requestor":null,"qc":null,"purposeOfSurvey":null,"customerName":null,"customerPhoneNumber":null,"customerEmail":null,"currentStage":4,"inspectionAddress":0,"inspectionLatitude":null,"inspectionLongitude":null,"inspectionTime":null,"companyBranchDivision":null,"creationTime":null,"reopenTime":null,"reopenReason":null,"qcReopenTime":null,"qcReopenReason":null,"photosUploaded":false,"videosUploaded":false,"recommendation":null,"remark":null,"comment":null,"inspectionSubmitTime":null,"inspectionStartTime":null,"closeTime":null,"qcTime":null,"closeReason":null,"encryptionKey":null,"fileAvailable":false,"fileAvailableTime":null,"downloadKey":null,"inspectionType":null,"assessmentSummary":{"id":8,"totalLabourCharges":1,"supplementryLabourCharges":2,"totalPartCharges":3,"supplementryPartCharges":4,"totalPaintCost":5,"supplementaryPaintCost":6,"lessExcess":7,"lessSalavage":8,"total":9,"remarks":"ss","createdDate":null,"updatedDate":null},"claimCasePhotos":[{"id":577,"snapTime":1582116503000,"latitude":17.4680588,"longitude":78.3529758,"fileName":"1582116498780.png","photoType":"driver_details","$$hashKey":"object:72"},{"id":574,"snapTime":1582116537000,"latitude":17.4680089,"longitude":78.3530496,"fileName":"1582116532701.png","photoType":"accident_details","$$hashKey":"object:73"},{"id":576,"snapTime":1582116503000,"latitude":17.4680588,"longitude":78.3529758,"fileName":"1582116485611.png","photoType":"driver_details","$$hashKey":"object:74"},{"id":575,"snapTime":1582116760000,"latitude":17.4680618,"longitude":78.3529763,"fileName":"1582116760759.jpeg","photoType":"vehicle_details","$$hashKey":"object:75"},{"id":573,"snapTime":1582116466000,"latitude":17.4679295,"longitude":78.3530168,"fileName":"1582116459812.png","photoType":"insurance_details","$$hashKey":"object:76"}],"lossAssessments":[{"id":114,"vehiclePartParticularType":{"id":"Instrument
// Panel","partParticularType":"Instrument
// Panel","partTypeId":"5","partSideId":"Other","createdDate":1560259847000,"updatedDate":1560259847000},"vehicleDamageType":{"id":"Repair","damageType":"Repair","createdDate":1560259847000,"updatedDate":1560259847000},"replacementProduct":null,"vehicleRepairType":{"id":"Major","repairType":"Major","createdDate":1560259847000,"updatedDate":1560259847000},"partCost":1000,"labourCost":900,"paintCost":700,"depRateForPart":1,"gstRateForPart":1,"depRateForLabour":5,"gstRateForLabour":3,"depRateForPaint":44,"gstRateForPaint":55,"createdDate":null,"updatedDate":null,"lossAssessmentPhotos":[]},{"id":108,"vehiclePartParticularType":{"id":"Front
// Bumper","partParticularType":"Front
// Bumper","partTypeId":"Engine","partSideId":"Front","createdDate":1560259847000,"updatedDate":1560259847000},"vehicleDamageType":{"id":"Replacement","damageType":"Replacement","createdDate":1560259847000,"updatedDate":1560259847000},"replacementProduct":{"id":"Product
// 1","productName":"Product
// 1","createdDate":1563367609000,"updatedDate":1563367609000},"vehicleRepairType":{"id":"Major","repairType":"Major","createdDate":1560259847000,"updatedDate":1560259847000},"partCost":116,"labourCost":105,"paintCost":0,"depRateForPart":0,"gstRateForPart":0,"depRateForLabour":0,"gstRateForLabour":0,"depRateForPaint":0,"gstRateForPaint":0,"createdDate":null,"updatedDate":null,"lossAssessmentPhotos":[{"id":102,"snapTime":1582116729000,"latitude":17.4679441,"longitude":78.3529925,"fileName":"1582116722564.png","photoType":"loss_assessment"}]},{"id":109,"vehiclePartParticularType":{"id":"Dicky
// Glass","partParticularType":"Dicky
// Glass","partTypeId":"Glass","partSideId":"Back","createdDate":1560259846000,"updatedDate":1560259846000},"vehicleDamageType":{"id":"Replacement","damageType":"Replacement","createdDate":1560259847000,"updatedDate":1560259847000},"replacementProduct":{"id":"Product
// 1","productName":"Product
// 1","createdDate":1563367609000,"updatedDate":1563367609000},"vehicleRepairType":null,"partCost":179,"labourCost":141,"paintCost":0,"depRateForPart":0,"gstRateForPart":0,"depRateForLabour":0,"gstRateForLabour":0,"depRateForPaint":0,"gstRateForPaint":0,"createdDate":null,"updatedDate":null,"lossAssessmentPhotos":[{"id":103,"snapTime":1582116747000,"latitude":17.4679603,"longitude":78.3530388,"fileName":"1582116740969.png","photoType":"loss_assessment"}]},{"id":113,"vehiclePartParticularType":{"id":"Bulkhead","partParticularType":"Bulkhead","partTypeId":"Body","partSideId":"Front","createdDate":1560259847000,"updatedDate":1560259847000},"vehicleDamageType":{"id":"Repair","damageType":"Repair","createdDate":1560259847000,"updatedDate":1560259847000},"replacementProduct":null,"vehicleRepairType":{"id":"Minor","repairType":"Minor","createdDate":1560259847000,"updatedDate":1560259847000},"partCost":115,"labourCost":100,"paintCost":90,"depRateForPart":5,"gstRateForPart":5,"depRateForLabour":7,"gstRateForLabour":6,"depRateForPaint":2,"gstRateForPaint":2,"createdDate":null,"updatedDate":null,"lossAssessmentPhotos":[]},{"id":115,"vehiclePartParticularType":{"id":"Intercooler","partParticularType":"Intercooler","partTypeId":"Body","partSideId":"Front","createdDate":1560259846000,"updatedDate":1560259846000},"vehicleDamageType":{"id":"Repair","damageType":"Repair","createdDate":1560259847000,"updatedDate":1560259847000},"replacementProduct":{"id":"Product
// 2","productName":"Product
// 2","createdDate":1563367611000,"updatedDate":1563367611000},"vehicleRepairType":{"id":"Minor","repairType":"Minor","createdDate":1560259847000,"updatedDate":1560259847000},"partCost":1111,"labourCost":2,"paintCost":333,"depRateForPart":2,"gstRateForPart":1,"depRateForLabour":2,"gstRateForLabour":22,"depRateForPaint":33,"gstRateForPaint":33,"createdDate":null,"updatedDate":null,"lossAssessmentPhotos":[]}]},"status":200,"config":{"method":"GET","transformRequest":[null],"transformResponse":[null],"url":"qc/survey/cases/case-details","params":{"case_id":"39"},"spinnerText":"Fetching
// data. Please wait!","headers":{"Accept":"application/json, text/plain,
// */*","Authorization":"Bearer
// 8bca6b5b-58c6-489f-8887-2b65c7622c40"}},"statusText":""},{"data":{"thirdPartyDamageItems":[{"id":1,"selector":"Yes","createdDate":1560259917000,"updatedDate":1560259917000},{"id":2,"selector":"No","createdDate":1560259917000,"updatedDate":1560259917000}],"vehiclePartCost":[{"id":1,"partCost":300,"createdDate":1560259847000,"updatedDate":1560259847000},{"id":2,"partCost":450,"createdDate":1560259847000,"updatedDate":1560259847000},{"id":3,"partCost":1000,"createdDate":1560259847000,"updatedDate":1560259847000},{"id":4,"partCost":1100,"createdDate":1560259847000,"updatedDate":1560259847000}],"driverLicenseTypes":[{"id":1,"licenseType":"Learner","createdDate":1560259917000,"updatedDate":1560259917000},{"id":2,"licenseType":"Permanent","createdDate":1560259917000,"updatedDate":1560259917000}],"vehiclePartSide":[{"id":"Back","partSide":"Back","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Front","partSide":"Front","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Left","partSide":"Left","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Other","partSide":"Other","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Right","partSide":"Right","createdDate":1560259846000,"updatedDate":1560259846000}],"vehicleLabourCost":[{"id":1,"labourCost":100,"createdDate":1560259847000,"updatedDate":1560259847000},{"id":2,"labourCost":200,"createdDate":1560259847000,"updatedDate":1560259847000},{"id":3,"labourCost":500,"createdDate":1560259847000,"updatedDate":1560259847000}],"replacementProduct":[{"id":"Product
// 1","productName":"Product
// 1","createdDate":1563367609000,"updatedDate":1563367609000},{"id":"Product
// 2","productName":"Product
// 2","createdDate":1563367611000,"updatedDate":1563367611000}],"vehicleDamageType":[{"id":"Repair","damageType":"Repair","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"Replacement","damageType":"Replacement","createdDate":1560259847000,"updatedDate":1560259847000}],"reportToPoliceItems":[{"id":1,"selector":"Yes","createdDate":1560259917000,"updatedDate":1560259917000},{"id":2,"selector":"No","createdDate":1560259917000,"updatedDate":1560259917000}],"vehiclePartType":[{"id":"Body","partType":"Body","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Door","partType":"Door","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Engine","partType":"Engine","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Glass","partType":"Glass","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Interior","partType":"Interior","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Lamp","partType":"Lamp","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Wheel","partType":"Wheel","createdDate":1560259846000,"updatedDate":1560259846000}],"vehiclePartParticularType":[{"id":"Bonnet","partParticularType":"Bonnet","partTypeId":"Engine","partSideId":"Front","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"Bulkhead","partParticularType":"Bulkhead","partTypeId":"Body","partSideId":"Front","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"Centre
// Member","partParticularType":"Centre
// Member","partTypeId":"Body","partSideId":"Front","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"Dicky","partParticularType":"Dicky","partTypeId":"Engine","partSideId":"Back","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"Dicky
// Glass","partParticularType":"Dicky
// Glass","partTypeId":"Glass","partSideId":"Back","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Front
// Bumper","partParticularType":"Front
// Bumper","partTypeId":"Engine","partSideId":"Front","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"Front
// W/s Glass","partParticularType":"Front W/s
// Glass","partTypeId":"Glass","partSideId":"Front","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Instrument
// Panel","partParticularType":"Instrument
// Panel","partTypeId":"5","partSideId":"Other","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"Intercooler","partParticularType":"Intercooler","partTypeId":"Body","partSideId":"Front","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"LH
// Apron","partParticularType":"LH
// Apron","partTypeId":"Engine","partSideId":"Left","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"LH
// Fender","partParticularType":"LH
// Fender","partTypeId":"Engine","partSideId":"Left","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"LH
// Fender Lining","partParticularType":"LH Fender
// Lining","partTypeId":"Engine","partSideId":"Left","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"LH
// Fog lamp","partParticularType":"LH Fog
// lamp","partTypeId":"Lamp","partSideId":"Left","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"LH
// Front door","partParticularType":"LH Front
// door","partTypeId":"Door","partSideId":"Left","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"LH
// Front Door Glass","partParticularType":"LH Front Door
// Glass","partTypeId":"Glass","partSideId":"Left","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"LH
// Front Door Qtr Glass","partParticularType":"LH Front Door Qtr
// Glass","partTypeId":"Glass","partSideId":"Left","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"LH
// Front Pillar A","partParticularType":"LH Front Pillar
// A","partTypeId":"Engine","partSideId":"Left","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"LH
// Front Tyre","partParticularType":"LH Front
// Tyre","partTypeId":"Wheel","partSideId":"Left","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"LH
// Front Wheel Rim","partParticularType":"LH Front Wheel
// Rim","partTypeId":"Wheel","partSideId":"Left","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"LH
// Head Lamp","partParticularType":"LH Head
// Lamp","partTypeId":"Lamp","partSideId":"Left","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"LH
// indicator Lamp","partParticularType":"LH indicator
// Lamp","partTypeId":"Lamp","partSideId":"Left","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"LH
// Quarter Panel","partParticularType":"LH Quarter
// Panel","partTypeId":"Engine","partSideId":"Left","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"LH
// Rear Door","partParticularType":"LH Rear
// Door","partTypeId":"Door","partSideId":"Left","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"LH
// Rear Door Glass","partParticularType":"LH Rear Door
// Glass","partTypeId":"Glass","partSideId":"Left","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"LH
// Rear Door Qtr Glass","partParticularType":"LH Rear Door Qtr
// Glass","partTypeId":"Glass","partSideId":"Left","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"LH
// Rear Tyre","partParticularType":"LH Rear
// Tyre","partTypeId":"Wheel","partSideId":"Left","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"LH
// Rear View Mirror","partParticularType":"LH Rear View
// Mirror","partTypeId":"Glass","partSideId":"Left","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"LH
// Rear Wheel Rim","partParticularType":"LH Rear Wheel
// Rim","partTypeId":"Wheel","partSideId":"Left","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"LH
// Running Board","partParticularType":"LH Running
// Board","partTypeId":"Engine","partSideId":"Left","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"LH
// Suspension Arm","partParticularType":"LH Suspension
// Arm","partTypeId":"Body","partSideId":"Left","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"LH
// Tail Lamp","partParticularType":"LH Tail
// Lamp","partTypeId":"Lamp","partSideId":"Left","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Lower
// Member","partParticularType":"Lower
// Member","partTypeId":"Body","partSideId":"Front","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"Moulding","partParticularType":"Moulding","partTypeId":"Body","partSideId":"Front","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"Radiator","partParticularType":"Radiator","partTypeId":"Body","partSideId":"Front","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Rear
// Bumper","partParticularType":"Rear
// Bumper","partTypeId":"Engine","partSideId":"Back","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"RH
// Apron","partParticularType":"RH
// Apron","partTypeId":"Engine","partSideId":"Right","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"RH
// Fender","partParticularType":"RH
// Fender","partTypeId":"Engine","partSideId":"Right","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"RH
// Fender Lining","partParticularType":"RH Fender
// Lining","partTypeId":"Engine","partSideId":"Right","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"RH
// Fog lamp","partParticularType":"RH Fog
// lamp","partTypeId":"Lamp","partSideId":"Right","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"RH
// Front door","partParticularType":"RH Front
// door","partTypeId":"Door","partSideId":"Right","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"RH
// Front door Glass","partParticularType":"RH Front door
// Glass","partTypeId":"Glass","partSideId":"Right","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"RH
// Front door Qtr Glass","partParticularType":"RH Front door Qtr
// Glass","partTypeId":"Glass","partSideId":"Right","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"RH
// Front Pillar A","partParticularType":"RH Front Pillar
// A","partTypeId":"Engine","partSideId":"Right","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"RH
// Front Tyre","partParticularType":"RH Front
// Tyre","partTypeId":"Wheel","partSideId":"Right","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"RH
// Front Wheel Rim","partParticularType":"RH Front Wheel
// Rim","partTypeId":"Wheel","partSideId":"Right","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"RH
// Head Lamp","partParticularType":"RH Head
// Lamp","partTypeId":"Lamp","partSideId":"Right","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"RH
// Indicator Lamp","partParticularType":"RH Indicator
// Lamp","partTypeId":"Lamp","partSideId":"Right","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"RH
// Quarter Panel","partParticularType":"RH Quarter
// Panel","partTypeId":"Engine","partSideId":"Right","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"RH
// Rear Door","partParticularType":"RH Rear
// Door","partTypeId":"Door","partSideId":"Right","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"RH
// Rear Door Glass","partParticularType":"RH Rear Door
// Glass","partTypeId":"Glass","partSideId":"Right","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"RH
// Rear door Qtr Glass","partParticularType":"RH Rear door Qtr
// Glass","partTypeId":"Glass","partSideId":"Right","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"RH
// Rear Tyre","partParticularType":"RH Rear
// Tyre","partTypeId":"Wheel","partSideId":"Right","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"RH
// Rear View Mirror","partParticularType":"RH Rear View
// Mirror","partTypeId":"Glass","partSideId":"Right","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"RH
// Rear Wheel Rim","partParticularType":"RH Rear Wheel
// Rim","partTypeId":"Wheel","partSideId":"Right","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"RH
// Running Board","partParticularType":"RH Running
// Board","partTypeId":"Engine","partSideId":"Right","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"RH
// Suspension arm","partParticularType":"RH Suspension
// arm","partTypeId":"Body","partSideId":"Right","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"RH
// Tail Lamp","partParticularType":"RH Tail
// Lamp","partTypeId":"Lamp","partSideId":"Right","createdDate":1560259846000,"updatedDate":1560259846000},{"id":"Roof","partParticularType":"Roof","partTypeId":"Engine","partSideId":"Other","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"Sealant
// Kit","partParticularType":"Sealant
// Kit","partTypeId":"Body","partSideId":"Front","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"Tie
// Member","partParticularType":"Tie
// Member","partTypeId":"Body","partSideId":"Front","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"Upper
// Member","partParticularType":"Upper
// Member","partTypeId":"Body","partSideId":"Front","createdDate":1560259847000,"updatedDate":1560259847000}],"vehicleClassTypes":[{"id":1,"vehicleType":"2-Wheeler","createdDate":1560259917000,"updatedDate":1560259917000},{"id":2,"vehicleType":"4-Wheeler","createdDate":1560259917000,"updatedDate":1560259917000}],"vehiclePermitTypes":[{"id":1,"permitType":"National
// Permit","createdDate":1560259917000,"updatedDate":1560259917000},{"id":2,"permitType":"State
// Permit","createdDate":1560259917000,"updatedDate":1560259917000}],"thirdPartyDamageTypes":[{"id":1,"damageType":"Life","createdDate":1560259917000,"updatedDate":1560259917000}],"punchnamaCarriedOutItems":[{"id":1,"selector":"Yes","createdDate":1560259917000,"updatedDate":1560259917000},{"id":2,"selector":"No","createdDate":1560259917000,"updatedDate":1560259917000}],"vehicleBodyTypes":[{"id":1,"bodyType":"Silver","createdDate":1560259917000,"updatedDate":1560259917000},{"id":2,"bodyType":"Metal","createdDate":1560259917000,"updatedDate":1560259917000},{"id":3,"bodyType":"Steel","createdDate":1560259917000,"updatedDate":1560259917000}],"vehicleType":[{"id":"2-wheeler","name":"2
// Wheeler","vehicleSubTypes":[{"id":"scooter","name":"Scooter"},{"id":"bike","name":"Bike"}]},{"id":"4-wheeler","name":"4
// Wheeler","vehicleSubTypes":[{"id":"hatchback","name":"HatchBack"},{"id":"luxury","name":"Luxury"},{"id":"sedan","name":"Sedan"},{"id":"suv","name":"SUV"}]},{"id":"commercial","name":"Commercial","vehicleSubTypes":[{"id":"tractor","name":"Tractor"},{"id":"mini-truck","name":"Mini
// Truck"},{"id":"pickup","name":"Pickup"},{"id":"truck","name":"Truck"},{"id":"tipper","name":"Tipper"}]}],"vehicleRepairType":[{"id":"Major","repairType":"Major","createdDate":1560259847000,"updatedDate":1560259847000},{"id":"Minor","repairType":"Minor","createdDate":1560259847000,"updatedDate":1560259847000}]},"status":200,"config":{"method":"GET","transformRequest":[null],"transformResponse":[null],"url":"claims/config-details","params":null,"spinnerText":"Fetching
// data. Please wait!","headers":{"Accept":"application/json, text/plain,
// */*","Authorization":"Bearer
// 962f19d0-6133-44d8-8cf1-e80659013a73"}},"statusText":""}]')
//
// $scope.claimCase = result[0].data;
// $scope.claimCaseMasterData = angular.copy($scope.claimCase);
// $scope.claimsConfig = result[1].data;
//
// console.log($scope.claimCase);
        });
        
        $scope.checkassessmentphotocriteria = function (data,criteria) {
           return  data.includes(criteria);
        };


    }]);
