/* global angular, jaduApp */

jaduApp.controller('AllCasesForAgentController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', '$q', '$window', 'NgTableParams', function($scope, $timeout, $location, $compile, ajaxService, $q, $window, NgTableParams) {

	  $scope.selected = [];
	  $scope.limitOptions = [5, 10, 15, 30];
	  
	  $scope.options = {
	    rowSelection: false,
	    multiSelect: false,
	    autoSelect: false,
	    decapitate: false,
	    largeEditDialog: false,
	    boundaryLinks: false,
	    limitSelect: true,
	    pageSelect: true
	  };
	  
	  $scope.query = {
	    order: 'name',
	    limit: 30,
	    page: 1
	  };
	  
	  $scope.toggleLimitOptions = function () {
		    $scope.limitOptions = $scope.limitOptions ? undefined : [15, 20, 30];
	  };
	  $scope.logItem = function (item) {
		    console.log(item.name, 'was selected');
	  };
	  
	  $scope.logOrder = function (order) {
	    console.log('order: ', order);
	  };
	  
	  $scope.logPagination = function (page, limit) {
	    console.log('page: ', page);
	    console.log('limit: ', limit);
	  }
	  
    $scope.showInspectionReport = function(id){
        $window.open('#/report/' + id, '_blank');
    };

    $scope.getCaseDetailsModel = {
        case_id :  "",
        vehicle_id : "",
        customer_phone : "",
        requester_phone : ""
      };
    
    $scope.clearValues = function(){
    	$scope.getCaseDetailsModel = {
    	        case_id :  "",
    	        vehicle_id : "",
    	        customer_phone : "",
    	        requester_phone : ""
    	      };
    }

  /* To fetch all the cases form DB */
  ajaxService.getAllCasesAgent({},'Loading Agents Data....')
  .then(function(res){
        $scope.allCases = res.data;
  })
  .catch(function(e){
    alert("Unable to fetch agent data!");
    console.error('All agent fetch error', e);
  })
	 

    $scope.getCaseDetails = function(){
        var fieldName = '';
        var fieldValue = "";
        //To get form value and name
        if($scope.getCaseDetailsModel.case_id!="")
        {
            fieldName = "case_id";
            fieldValue =  $scope.getCaseDetailsModel.case_id;
        }
        else if($scope.getCaseDetailsModel.vehicle_id!=""){
            fieldName = "vehicle_id";
            fieldValue =  $scope.getCaseDetailsModel.vehicle_id;
        }
        else if($scope.getCaseDetailsModel.customer_phone!=""){
            fieldName = "customer_phone";
            fieldValue =  $scope.getCaseDetailsModel.customer_phone;
        }
        else{
            fieldName = "requester_phone";
            fieldValue =  $scope.getCaseDetailsModel.requester_phone;
        }
        ajaxService.getRequestedCases({
            field: fieldName,
            value : fieldValue
        }, "Fetching details. Please wait!").then(function(res){
            $scope.data = res.data;
            $scope.clearValues();
        }, function(err){
        	alert("No results found");
        	$scope.clearValues();
        });
    }
  
    $scope.goToCreateCaseAgent = function(){
    	$location.url('/create-case-agent');
    }
    
    
    $scope.refreshDeamonActive = false;

    $scope.refreshDeamon = function(){
      $timeout(function(){
        $scope.fetchAllCases('refresh');
        $scope.fetchCompletedCases('refresh');
        $scope.fetchClosedCases('refresh');
      },60000);
    };

//    $scope.fetchAllCases("Fetching data. Please wait!");
    
    $scope.reopenCaseForQC = function(qcCase){
        $scope.currentCase = qcCase;
        
        $("#manual-qc-container").modal('show');
        $("#manual-qc-container .modal-body")
                .html($compile("<reopen-case-qc inspection-case='currentCase' callback='reopenCaseCallback()'></reopen-case-qc>")($scope));
        
    }; 
    
    $scope.reopenCaseCallback = function(){
        $("#manual-qc-container").modal('hide');
        $scope.fetchCompletedCases("Fetching data. Please wait!");
    };
    
    $scope.allRemarks = [
        { id: "recommended", title: "Recommended"}, 
        { id: "not-recommended", title: "Not Recommended"},
        { id: "underwriter", title: "Underwriter"}
    ];
    
    $scope.showComments = function(data){        
      
       $scope.selectedCaseForComment = data;
      
       $("#manual-qc-container3").modal('show');
       $("#manual-qc-container3 .modal-body").html($compile("<case-comments inspection-case='selectedCaseForComment'></case-comments>")($scope));
    };
    
    $scope.reopenCase = function(qcCase){
        $scope.currentCase = qcCase;
        
        $("#reopen-case-container").modal('show');
        $("#reopen-case-container .modal-body").html($compile("<reopen-case inspection-case='currentCase' callback='reopenCaseCallback()'></reopen-case>")($scope));
    }
    
    $scope.sendReport = function(qcCase){
        ajaxService.sendReport({
            case_id : qcCase
        }, "Mailing report. Please wait!").then(function(){
            alert("Report sent successfully!");
        }, function(){
            alert("Unable to send report. Please try again later!");
        });
    }
}]);
