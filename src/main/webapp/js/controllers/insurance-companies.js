jaduApp.controller('InsuranceCompaniesController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', '$q',  function($scope, $timeout, $location, $compile, ajaxService, $q) {
    angular.element(document).ready(function () {
        $q.all([
            ajaxService.getInsuranceCompanies("Fetching insurance companies details. Please wait!")
        ]).then(function(data){
            $scope.insuranceCompanies = data[0];
        });
    });
}]);
