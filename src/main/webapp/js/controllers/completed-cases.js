jaduApp.controller('CompletedCasesController',['$scope', '$timeout', '$location', '$compile', 'ajaxService', '$q', '$window',  function($scope, $timeout, $location, $compile, ajaxService, $q, $window) {

    $scope.showInspectionReport = function(id){
        $window.open('#/report/' + id, '_blank');
    };

    angular.element(document).ready(function () {
        $q.all([
            ajaxService.getCompletedCases(null, "Fetching data. Please wait!")
        ]).then(function(res){
            $scope.qcCases = res[0].data;
        });
    });
}]);
