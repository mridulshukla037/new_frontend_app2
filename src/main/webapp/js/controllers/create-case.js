jaduApp.controller('CreateCaseController', ['$scope', '$rootScope', '$timeout', '$location', '$compile', 'ajaxService', '$q', 'GoogleMaps', '$log', function ($scope, $rootScope, $timeout, $location, $compile, ajaxService, $q, GoogleMaps, $log) {

    $scope.vehicleNumberRegex = '/^(([A-Z]){2,3}(?:[0-9]){1,2}(?:[A-Z]){1,2}([0-9]){1,4})$/';// not
    // using

    $scope.createCaseModel = {
        company_name: undefined,
        purpose_of_survey: undefined,
        customer_name: undefined,
        customer_phone_number: undefined,
        self_inspect: true,
        inspection_type: undefined,
        vehicle_number: undefined,
        claim_number: undefined,
        agent_id: undefined,
        agent_phone_number: undefined,
        inspection_time: undefined
    };

    $scope.resetCase = function () {
        $scope.createCaseModel = {
            company_name: undefined,
            purpose_of_survey: undefined,
            customer_name: undefined,
            customer_phone_number: undefined,
            self_inspect: true,
            inspection_type: undefined,
            vehicle_number: undefined,
            claim_number: undefined,
            agent_id: undefined,
            agent_phone_number: undefined,
            inspection_time: undefined
        };
        $scope.setInspectionType();

    }

    $scope.setInspectionType = function () {
        $scope.inspectionTypes
                .forEach(function (inspectionType, index) {
                    if (inspectionType.isDefault) {
                        $scope.createCaseModel.inspection_type = inspectionType.id;
                    }
                });
    }

    $scope.getAgentUsername = function (phoneNumber) {
        for (var i = 0; i < $scope.allAgents.length; i++){
            if($scope.allAgents[i].phoneNumber== phoneNumber)
                return $scope.allAgents[i].username;
        }
    }

    $scope.createCase = function () {
        var latlng = GoogleMaps.getLatLong();
        $scope.createCaseModel.latitude = typeof latlng.lat === "function" ? latlng
                .lat()
                : latlng.lat;
        $scope.createCaseModel.longitude = typeof latlng.lng === "function" ? latlng
                .lng()
                : latlng.lng;

        $scope.createCaseModel.agent_id = $scope.getAgentUsername($scope.createCaseModel.agent_phone_number);
        $scope.createCaseModel.insurance_company = $scope.createCaseModel.company_name
        console.log($scope.createCaseModel);
        
        ajaxService
                .createCase(
                        $scope.createCaseModel,
                        "Creating case. Please wait!")
                .then(
                        function (res) {
                            alert("Case has been created successfully!");
                            if ($scope.createCaseModel.inspection_type == 'ASSIGN_TO_INSPECTOR')
                                $location
                                        .url('/address-pending');
                            else
                                $location
                                        .url('/scheduled-cases');

                            $scope.resetCase();

                        },
                        function (err) {
                            alert( "Unable to create case!",err);
                        });
    };

    angular
            .element(document)
            .ready(
                    function () {

                        $q
                                .all(
                                        [
                                            ajaxService
                                                    .getPurposeOfSurvey(
                                                            null,
                                                            "Fetching data. Please wait!"),
                                            ajaxService
                                                    .getInspectionTypes(),
                                            ajaxService.getInsuranceCompanies({}, "Fetching data. Please wait!")])
                                .then(
                                        function (res) {
                                            $scope.poi = res[0].data;
                                            /*$scope.allAgents = res[1].data;*/
//																	console
//																			.log($scope.allAgents)
                                            $scope.inspectionTypes = res[1].data;
                                            $scope.companies = res[2].data;

                                            $scope
                                                    .setInspectionType();
                                            /*$scope
                                             .initializeAgentAutoComplete();*/
                                        });
                    });
    $scope.getAgents = function (id) {
        ajaxService.getMappedAgents(
                {
                    company: $scope.createCaseModel.company_name},
        "Fetching agents. Please wait!").then(function (res) {
            console.log(res);
            if (res.data) {
                $scope.allAgents = res.data;
            }
            $scope.initializeAgentAutoComplete();
        }).catch(function (error) {
            console.log(error)
        })
    }

    $scope.verifyAgentContactActive = false;
    $scope.verifyAgentContactStatus = false;
    $scope.verifyAgentContactActiveClass = '';
    $scope.verifyFlag = 0;

    $scope.verifyAgentContact = function () {
        if (!$scope.verifyAgentContactActive
                && $scope.createCaseModel.agent_phone_number.length == 10
                && !$scope.verifyAgentContactStatus) {
            $scope.verifyAgentContactActive = true;
            ajaxService
                    .getAgentByPhoneNumber(
                            $scope.createCaseModel.agent_phone_number,
                            "Verifyring Agent Contact. Please wait!")
                    .then(
                            function (data) {
                                $scope.verifyFlag++;
                                $scope.createCaseModel.agent_id = data.username;
                                $scope.verifyAgentContactActive = false;
                                $scope.verifyAgentContactStatus = true;
                            },
                            function () {
                                $scope.verifyFlag++;
                                $scope.verifyAgentContactActive = false;
                                $scope.verifyAgentContactStatus = false;
                            })
        }
    }

    $scope.checkAgentContact = function () {
        if ($scope.createCaseModel.agent_phone_number != undefined) {
            if ($scope.createCaseModel.agent_phone_number.length == 10
                    && !$scope.verifyAgentContactActive) {
                $scope.verifyAgentContact();
            } else {
                $scope.verifyFlag = 0;
                $scope.verifyAgentContactStatus = false;
            }
        }
    };

    // auto-complete below
    var self = $scope;
    self.agents = "";

    $scope.initializeAgentAutoComplete = function () {
        self.agents = loadAll();
        self.simulateQuery = false;
        self.isDisabled = false;

        self.querySearch = querySearch;
        self.selectedItemChange = selectedItemChange;
        self.searchTextChange = searchTextChange;

        self.newState = newState;
    }

    function newState(state) {
        alert("Sorry! No data present for entered query!");
    }

    function querySearch(query) {
        var results = query ? self.agents
                .filter(createFilterFor(query))
                : self.agents, deferred;
        if (self.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () {
                deferred.resolve(results);
            }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        // $log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        if (item != undefined)
            $scope.createCaseModel.agent_phone_number = item.value;
        // $log.info('Item changed to ' +
        // JSON.stringify(item));
    }

    function loadAll() {
        var agents = [];
        $scope.allAgents
                .forEach(function (agent, index) {
                    agents.push(agent.phoneNumber);
                });

        return agents.map(function (agent) {
            return {
                value: agent,
                display: agent
            };
        });
    }

    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(agent) {
            return (agent.value.indexOf(lowercaseQuery) === 0);
        };
    }

}]);
