jaduApp.controller('AgentHomeController',['$scope', '$rootScope', '$timeout', '$location', '$compile', 'ajaxService', '$q', 'GoogleMaps', '$log',   function($scope, $rootScope, $timeout, $location, $compile, ajaxService, $q, GoogleMaps, $log) {
    
    $scope.goToCreateCase = function(){
        $location.url('/create-case-agent');
    };
    
    $scope.goToScheduleCase = function(){
        $location.url('/scheduled-cases-agent');
    };
    
    $scope.goToMyCases = function(){
        $location.url('/all-cases-agent');
    };
}]);
