jaduApp.controller('AllAgentsController',['$scope', '$timeout', '$location', '$compile' , '$auth', '$q','ajaxService', function($scope, $timeout, $location, $compile, $auth, $q, ajaxService) {

  $scope.agentList = [];
  $scope.branchList = [];
  
    ajaxService.getTopAgents({},'Loading Agents Data....')
  .then(function(res){
        $scope.agentList = res.data;
  })
  .catch(function(e){
        alert("Unable to fetch agent data!");
        console.error('All agent fetch error', e);
  })
  
  $scope.getAllAgents = function(){
	  ajaxService.getAllAgentList({},'Loading Agents Data....')
	  .then(function(res){
	        $scope.agentList = res.data;
	        if(res.data.lenght<=0)
	        	$scope.agentlist = [{
	        		agentCode: "123456",
	        			agentCompany: "Demo Insurer",
	        			authority: null,
	        			branchName: "Vikarabad",
	        			canUpdate: false,
	        			deviceId: null,
	        			deviceLocked: false,
	        			email: "pankaj@wimwisure.com",
	        			emailVerified: true,
	        			enabled: true,
	        			firstName: "Pankaj",
	        			lastName: "Kumar",
	        			passwordChangeRequired: false,
	        			phoneNumber: "7015889837",
	        			phoneNumberVerified: true,
	        			profilePhotoRelativeUrl: "/util/profile-image/1515311510819pOl3W.jpg",
	        			profilePhotoUrl: "1515311510819pOl3W.jpg",
	        			profilePhotoUrlThumb: "1515311510819pOl3W.jpg",
	        			username: "pankaj@wimwisure.com"
	        	}]
	        
	  })
	  .catch(function(e){
	        alert("Unable to fetch agent data!");
	        console.error('All agent fetch error', e);
	  })
  }
  
  $scope.selected = [];
  $scope.limitOptions = [5, 10, 15, 30];
  
  $scope.options = {
    rowSelection: false,
    multiSelect: false,
    autoSelect: false,
    decapitate: false,
    largeEditDialog: false,
    boundaryLinks: false,
    limitSelect: true,
    pageSelect: true
  };
  
  $scope.query = {
    order: 'name',
    limit: 30,
    page: 1
  };
  
  $scope.toggleLimitOptions = function () {
	    $scope.limitOptions = $scope.limitOptions ? undefined : [15, 20, 30];
  };
  $scope.logItem = function (item) {
	    console.log(item.name, 'was selected');
  };
  
  $scope.logOrder = function (order) {
    console.log('order: ', order);
  };
  
  $scope.logPagination = function (page, limit) {
    console.log('page: ', page);
    console.log('limit: ', limit);
  }

  $scope.showCases = function(data){        
      
      $scope.selectedCaseForComment = data;
     
      $("#manual-qc-container3").modal('show');
      $("#manual-qc-container3 .modal-body").html($compile("<case-comments inspection-case='selectedCaseForComment'></case-comments>")($scope));
   };

  
  

//  
//  ajaxService.getAllAgents({},'Loading Agents Data....')
//  .then(function(res){
//        $scope.agentList = res.data;
//  })
//  .catch(function(e){
//        alert("Unable to fetch agent data!");
//        console.error('All agent fetch error', e);
//  })
  
  $scope.getAgentDetailsModel = {
	contact :  "",
	emailId : "",
	agentId : "",
	branch : ""
  };

  $scope.clearValues = function(){
	$scope.getAgentDetailsModel = {
			contact :  "",
			emailId : "",
			agentId : "",
			branch : ""
	};
  }
  
  $scope.getAgentDetails = function(){
      var fieldName = '';
      var fieldValue = "";
      //To get form value and name
      if($scope.getAgentDetailsModel.contact!="")
      {
          fieldName = "contact";
          fieldValue =  $scope.getAgentDetailsModel.contact;
      }
      else if($scope.getAgentDetailsModel.emailId!=""){
          fieldName = "emailId";
          fieldValue =  $scope.getAgentDetailsModel.emailId;
      }
      else if($scope.getAgentDetailsModel.agentId!=""){
          fieldName = "agentId";
          fieldValue =  $scope.getAgentDetailsModel.agentId;
      }
      else{
          fieldName = "branch";
          fieldValue =  $scope.getAgentDetailsModel.branch;
      }
      ajaxService.getReqAgentsDetails({
          field: fieldName,
          value : fieldValue
      }, "Fetching details. Please wait!").then(function(res){
          $scope.agentList = res.data;
          $scope.clearValues();
      }, function(err){
          alert("No results found");
          $scope.clearValues();
      });
  }
  
	ajaxService.getAllBranches({},"fetching all branches")
	.then(function(res){
	      $scope.branchList = res.data;
	      $scope.branchList.sort();
	})
	.catch(function(e){
	      alert("Unable to fetch branches data!");
	      console.error('All agent fetch error', e);
	})

  $scope.showAgentActionsModel = function(agent){
      $scope.currentAgent = agent;
      $('#agent-actions').modal('show');
      $('#agent-actions .modal-body').html($compile("<user-actions  user='currentAgent' callback='updateCallback()'></user-actions>")($scope));
  };

  $scope.updateCallback = function(){
    $('#agent-actions').modal('hide');
  };

  $scope.goToCreateAgent = function(){
    $location.url('/create-agent');
  };

}]);
