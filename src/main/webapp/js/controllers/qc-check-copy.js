/* global angular */

jaduApp.controller('QCCheckController', ['$scope', '$timeout', '$location', '$compile', '$auth', '$q', 'ajaxService', 'NgTableParams', '$routeParams', 'Lightbox', '$filter', function ($scope, $timeout, $location, $compile, $auth, $q, ajaxService, NgTableParams, $routeParams, Lightbox, $filter) {

    $scope.caseId = $routeParams.id;

    $scope.activeGroup = "Insurance-details";

    $scope.groups = [
        {id: "Insurance-details", value: "Insurance Details", tags: []},
        {id: "vehicle-details", value: "Vehicle Details", tags: []},
        {id: "Accident-details", value: "Accident Details", tags: []},
        {id: "Driver-details", value: "Driver Details", tags: []},
        {id: "Assessment", value: "Assessment", tags: []},
        {id: "Assessment-Front", value: "Assessment Front", tags: [], hide: true},
        {id: "Assessment-Left", value: "Assessment Left", tags: [], hide: true},
        {id: "Assessment-Back", value: "Assessment Back", tags: [], hide: true},
        {id: "Assessment-right", value: "Assessment Right", tags: [], hide: true},
        {id: "Summary", value: "Summary", tags: []},
        {id: "coordinates", value: "Coordinates", tags: []},
        {id: "video", value: "Video", tags: ["video"], showCount: false}
    ];

    $scope.recommendation = "";

    $scope.recommendations = [
        {id: "hold", value: "Hold"},
        {id: "recommended", value: "Recommended"},
        {id: "not-recommended", value: "Not Recommended"},
        {id: "underwriter", value: "Reffered to underwriter"}
    ];

    $scope.jkc = [];// ud
    
    
    // Vehicle Edit
    $scope.editVehicleData = false;
    $scope.vehicleNumberEditEnable = false;
    $scope.vehicleNumberVerifyDisabled = true;
    $scope.verifyVahanMessage = "Please verify vehicle number!";
    $scope.totalPartCost = 0;
    $scope.totalLabourCost = 0;
    $scope.selected = {};
    $scope.qwe = Lightbox.setImage;
    $scope.qwe2 = Lightbox.openModal;
    // Static GST and DEP
    $scope.gst = [
        {id: 1, value: 5},
        {id: 2, value: 12},
        {id: 3, value: 18},
        {id: 4, value: 28}
    ];
    $scope.dep = [
        {id: 1, value: 5},
        {id: 2, value: 12},
        {id: 3, value: 18},
        {id: 4, value: 28}
    ];
    // Insurance edit
    $scope.editInsuranceData = false;
    //$scope.insuranceDetailsPhoto = false;
    
    
    // Driver edit
    $scope.editDriverData = false;
    // Accident edit
    $scope.editAccidentData = false;
    
    // summary edit
    $scope.editSummaryData = false;
    // End of summary assessment

    // Loss Assessment edit
    $scope.editLossAssessmentData = false;

    $scope.changeActive = function (id) {
        if (id === 'Assessment') {
            for (var i = 0; i < $scope.groups.length; i++) {
                if ("Assessment-Front" === $scope.groups[i].id)
                    $scope.groups[i].hide = false;
            }
        }
        $scope.activeGroup = id;
        console.log('SETTING GROUP-->', $scope.activeGroup);
    };

    $scope.checkInActiveGroup = function (index) {
        return $scope.activeGroup === $scope.casePhotos[index].photoType;
    };


    $scope.getActiveGroupById = function (id) {
        for (var i = 0; i < $scope.groups.length; i++) {
            if ($scope.groups[i].id === id)
                return $scope.groups[i];
        }

        return null;
    };

    $scope.ifTagInActiveGroup = function (tag, groupId) {
        var group = $scope.getActiveGroupById(groupId);
        if (!group)
            return false;
        if (group.tags.indexOf(tag) > -1)
            return true;
        return false;
    };

    $scope.getTotalAnswerCount = function () {
        var count = 0;
        if ($scope.caseQuestions) {
            for (var j = 0; j < $scope.caseQuestions.length; j++) {
                var question = $scope.caseQuestions[j];
                if (question.answer)
                    count++;
            }
        }

        return count;
    };

    $scope.getAnswersCount = function (groupId) {
        var group = $scope.getActiveGroupById(groupId);
        var count = 0;
        var totalCount = 0;
        if (group.showCount && $scope.caseQuestions) {
            for (var j = 0; j < $scope.caseQuestions.length; j++) {
                var question = $scope.caseQuestions[j];
                if (group.tags.indexOf(question.photoTag) > -1) {
                    totalCount++;
                    if (question.answer)
                        count++;
                }
            }

            return "(" + count + "/" + totalCount + ")";
        } else {
            return "";
        }


    };

    $scope.submitReport = function () {
        if ($scope.remarks && $scope.recommendation.length > 0) {
            var items = [];
            for (var i = 0; i < $scope.caseQuestions.length; i++) {
                items.push({
                    id: $scope.caseQuestions[i].id,
                    answer: $scope.caseQuestions[i].answer
                });
            }

            var data = {
                answers: JSON.stringify(items),
                recommendation: $scope.recommendation,
                case_id: $routeParams.id,
                remarks: $scope.remarks
            };

            ajaxService.sendInspectionReport(data, "Submitting inspection report. Please wait!").then(function () {
                alert("Inspection report submitted");
                $location.url('/qc');
            }, function (error) {
                console.log(error);
                if (_.isObject(error.data))
                    alert(error.data.error_message);
            });
        }
        else {
            if ($scope.recommendation.length === 0) {
                alert('Please select recommendation');
            }
            else {
                alert('Please check remarks.[maximum 50 characters]');
            }
        }


    };

    function getPhotoTagsByPhotoType(photoType) {

        for (var i = 0; i < $scope.photoTypes.length; i++) {
            if ($scope.photoTypes[i].id === photoType) {
                var result = [];
                for (var j = 0; j < $scope.photoTypes[i].photoTags.length; j++) {
                    result.push($scope.photoTypes[i].photoTags[j].id);
                }

                return result;
            }
        }

    }

    function getGroupsByPhotoTags(photoTags) {
        var result = [];

        if (!photoTags)
            return result;

        for (var i = 0; i < photoTags.length; i++) {
            for (var j = 0; j < $scope.groups.length; j++) {
                if ($scope.groups[j].tags.indexOf(photoTags[i]) > -1)
                    result.push($scope.groups[j].id);
            }
        }

        return result;
    }

    $scope.getGroupNameById = function (id) {
        for (k = 0; k < $scope.groups.length; k++) {
            if ($scope.groups[k].id === id) {
                return $scope.groups[k].value;
            }
        }
        return '';
    };
    
    Lightbox.setImage = function (a) {
        $scope.activeGroup = $scope.jkc[a].groups[0];
        $scope.qwe(a);
        $timeout(function () {
            $('#q-data').html("Working");
            $('#q-data').html($compile('<table class=" table"> <tbody class="panel panel-primary">\n\
            <tr class="panel-heading clearfix"><th colspan=2><h2><b>Group: {{getGroupNameById(activeGroup)}}</b></h2></th></tr>\n\
            <tr ng-repeat="question in caseQuestions" ng-show="ifTagInActiveGroup(question.photoTag, activeGroup)"> \n\
                <td> \n\
                    <h5> \n\
                        <b>{{question.value}}</b>\n\
                    </h5> \n\
                </td> \n\
                <td> \n\
                    <select ng-model="question.answer"  class="form-control">\n\
                        <option value="" disabled selected>Please select an option</option>\n\
                        <option ng-show="option.group === question.group" ng-repeat="option in caseOptions" value={{option.id}} ng-selected="question.answer === option.id">{{option.value}}</option>\n\
                    </select>\n\
                </td>\n\
            </tr></tbody>\n\
        </table>')($scope));
            $timeout(function () {
                $('#q-data').height($('.modal-content').height() - 8);
                console.log($('.modal-content').height() - 8);
            }, 10);

        }, 10);

    };

    $scope.getCasePhotos = function () {
        ajaxService.getCasePhotos({case_id: $routeParams.id}, "Fetching photos... Please wait!").then(function (result) {
            $scope.casePhotos = result.data;
            $scope.casePhotos = $scope.casePhotos.reverse();
            console.log('reversed case photos==> ', $scope.casePhotos);
            var tmpLast = parseInt(($scope.casePhotos[0].fileName).split('.')[0]);
            $scope.casePhotos.forEach(function (item, index) {
                var tags = getPhotoTagsByPhotoType(item.photoType);
                item.tags = tags;
                item.groups = getGroupsByPhotoTags(item.tags);
                item.zoom = "util/case-image/" + $scope.caseId + "/" + item.fileName;
                if (item.photoType === "chachis-number") {
                    $scope.vehicleChachis = item.qcAnswer;
                    $scope.vehicleChachisPhoto = "util/case-image/" + $scope.caseId + "/" + item.fileName;
                }
                if (item.photoType === "vin-plate-number") {
                    $scope.vehicleVin = item.qcAnswer;
                    $scope.vehicleVinPhoto = "util/case-image/" + $scope.caseId + "/" + item.fileName;
                }

                if (item.photoType === "odometer-rpm") {
                    $scope.odometerReading = item.qcAnswer;
                }
            });
            $scope.casePhotos.lastCasePhotoUploadTime = (_.maxBy($scope.casePhotos, 'snapTime')).snapTime;
            console.log('$scope.casePhotos--->', $scope.casePhotos);
            console.log("getCasePhotos", result);
        }).catch(function (error) {
            console.log("getCasePhotos", error);
        });
    };

    $scope.getAssessmentSummary = function () {
        ajaxService.getAssessmentSummary($routeParams.id, 'Please Wait!...').then(function (result) {
            $scope.assessmentSummary = result.data;
            if ($scope.assessmentSummary === "") {
                $scope.assessmentSummary = {};
                $scope.assessmentSummary.totalLabourCharges = 142.567;
                $scope.assessmentSummary.supplementryLabourCharges = 122.667;
                $scope.assessmentSummary.totalPartCharges = 242.567;
                $scope.assessmentSummary.supplementryPartCharges = 182.667;
            }
            $scope.assessmentSummary.totalLabourCharges = $scope.totalLabourCost;
            $scope.assessmentSummary.totalPartCharges = $scope.totalPartCost;
            $scope.assessmentSummary.total_cost = $scope.assessmentSummary.totalLabourCharges + $scope.assessmentSummary.supplementryLabourCharges;
            $scope.assessmentSummary.supplementry_cost = $scope.assessmentSummary.totalPartCharges + $scope.assessmentSummary.supplementryPartCharges;
            $scope.assessmentSummary.total = $scope.assessmentSummary.total_cost + $scope.assessmentSummary.supplementry_cost;
            console.log("assessmentSummary", result);
        }).catch(function (error) {
            console.log("assessmentSummary", error);
        });
    };
    // Assessment front edit
    // Approcah :1
    $scope.depRateChange = function (data, id) {
        if (parseInt(data.depRate) > 0) {
            var finalCostgst = parseInt(data.Cost) * (parseInt(data.gstRate) / 100);
            var finalCostdep = finalCostgst * (parseInt(data.depRate) / 100);
            data.finalTotal = parseInt(data.Cost) + (finalCostgst - finalCostdep);
        }
    };

    var getTemplateForAssesment = function (identifier) {
        return {
            "type": identifier,
            "damageType": "",
            "partParticularType": "",
            "repairType": "",
            "gstRate": 0,
            "depRate": 0,
            "Cost": 0
        };
    };

    $scope.updateAssessmentRow = function (data, id) {
        // $scope.user not updated yet
        console.log("saveLossAssessment", data);
        angular.extend(data, {id: id});
        alert("saved data locally");
        $scope.selected = {};
        $scope.getTemplate(data);
    };

    $scope.editAssessmentRow = function (data) {
        $scope.selected = angular.copy(data);
    };

    $scope.addRecordToAssessment = function () {
        $scope.HandlingLossAssement.push(getTemplateForAssesment('Spare'));
        $scope.HandlingLossAssement.push(getTemplateForAssesment('Labour'));
        $scope.HandlingLossAssement.push(getTemplateForAssesment('Paint'));
        console.log($scope.HandlingLossAssement);
    };

    $scope.reset = function () {
        $scope.selected = {};
    };
    $scope.getTemplate = function (data) {
        if (data.id === $scope.selected.id) {
            return 'edit';
        }
        else
            return 'display';
    };

    $scope.submitLossAssessement = function () {
        var lossassement_details = [];
        for (var i = 0; i < $scope.HandlingLossAssement.length; i += 2) {
            var data = {
                "type": 'Spare',
                "claim_number": $scope.claimCase.claimNumber,
                "damage_type": $scope.HandlingLossAssement[i].damageType,
                "labour_cost": $scope.HandlingLossAssement[i + 1].Cost,
                "gstRateForLabour": parseInt($scope.HandlingLossAssement[i + 1].gstRate),
                "depRateForLabour": parseInt($scope.HandlingLossAssement[i + 1].depRate),
                "part_cost": $scope.HandlingLossAssement[i].Cost,
                "gstRateForPart": parseInt($scope.HandlingLossAssement[i].gstRate),
                "depRateForPart": parseInt($scope.HandlingLossAssement[i].depRate),
                "part_particular_type": $scope.HandlingLossAssement[i].partParticularType,
                "part_side": "Right",
                "part_type": $scope.HandlingLossAssement[i].partParticularType,
                "repair_type": $scope.HandlingLossAssement[i].repairType
            };
            lossassement_details.push(data);
            console.log(lossassement_details);
        }
//			var lossassementDetails = {
//	        		lossassement_details
//	        };

        console.log('data=================================*******', lossassementDetails);

        ajaxService.updateClaimsLossAssessmentDetails($routeParams.id, lossassementDetails, 'Updating Details. PleaseWait!...')
                .then(function (result) {
                    alert('Loss Assessment Data Updated!');
                })
                .catch(function (e) {
                    alert('Error updating Insurance Data.');
                    console.log('Error updating Insurance Data.', e);
                });
    };

    $scope.showParticularType = function (user) {
        var selectedType = [];
        if (user.partParticularType) {
            selectedType = $filter('filter')($scope.getVehicleParticularTypesNew, {partParticularType: user.partParticularType});
        }
        return selectedType.length ? selectedType[0].partParticularType : 'Not set';
    };

    $scope.showDamageType = function (user) {
        var selectedType = [];
        if (user.damageType) {
            selectedType = $filter('filter')($scope.getVehicleDamageTypes, {damageType: user.damageType});
        }
        return selectedType.length ? selectedType[0].damageType : 'Not set';
    };

    $scope.showRepairType = function (user) {
        var selectedType = [];
        if (user.repairType) {
            selectedType = $filter('filter')($scope.vehicleRepairTypes, {repairType: user.repairType});
        }
        return selectedType.length ? selectedType[0].repairType : 'Not set';
    };

    Lightbox.openModal = function (a, b, c) {
        $scope.qwe2(a, b, c);
        $timeout(function () {
            $('#q-data').html("Working");
            $('#q-data').html($compile('<table class=" table"> <tbody class="panel panel-primary">\n\
            <tr class="panel-heading clearfix"><th colspan=2><h2><b>Group: {{getGroupNameById(activeGroup)}}</b></h2></th></tr>\n\
            <tr ng-repeat="question in caseQuestions" ng-show="ifTagInActiveGroup(question.photoTag, activeGroup)"> \n\
                <td> \n\
                    <h5> \n\
                        <b>{{question.value}}</b>\n\
                    </h5> \n\
                </td> \n\
                <td> \n\
                    <select ng-model="question.answer"  class="form-control">\n\
                        <option value="" disabled selected>Please select an option</option>\n\
                        <option ng-show="option.group === question.group" ng-repeat="option in caseOptions" value={{option.id}} ng-selected="question.answer === option.id">{{option.value}}</option>\n\
                    </select>\n\
                </td>\n\
            </tr></tbody>\n\
        </table>')($scope));
            $timeout(function () {
                $('#q-data').height($('.modal-content').height() - 8);
                console.log($('.modal-content').height() - 8);
            }, 100);

        }, 100);
    };

    $scope.editInsuranceDataEnable = function () {
        $scope.editInsuranceData = !$scope.editInsuranceData;
        //$scope.insuranceDetailsPhoto = !$scope.insuranceDetailsPhoto;

        $scope.setNewInsuranceData();
    };

    $scope.setUpadtedInsuranceData = function (result) {
        $scope.editInsuranceData = !$scope.editInsuranceData;
        $scope.claimCase.insuranceDetails = result.data;
        $scope.setNewInsuranceData();
    };

    $scope.setNewInsuranceData = function () {
        if ($scope.claimCase.insuranceDetails === null) {
            $scope.claimCase.insuranceDetails = {};
            $scope.claimCase.insuranceDetails.insurer = "";
            $scope.claimCase.insuranceDetails.financer = "";
            $scope.claimCase.insuranceDetails.policyNumber = "";
            $scope.claimCase.insuranceDetails.insuredName = "";
            $scope.claimCase.insuranceDetails.sumInsured = "";
            $scope.claimCase.insuranceDetails.idv = "";
            $scope.claimCase.insuranceDetails.insuredAddress = "";
            $scope.claimCase.insuranceDetails.insuredMobileNumber = "";
            $scope.claimCase.insuranceDetails.startDate = new Date().getTime();
            $scope.claimCase.insuranceDetails.endDate = new Date().getTime();
        }
        $scope.newInsuranceData = {
            insurer: $scope.claimCase.insuranceDetails.insurer,
            financer: $scope.claimCase.insuranceDetails.financer,
            policyNumber: $scope.claimCase.insuranceDetails.policyNumber,
            insuredName: $scope.claimCase.insuranceDetails.insuredName,
            sumInsured: $scope.claimCase.insuranceDetails.sumInsured,
            idv: $scope.claimCase.insuranceDetails.idv,
            insuredAddress: $scope.claimCase.insuranceDetails.insuredAddress,
            insuredMobileNumber: $scope.claimCase.insuranceDetails.insuredMobileNumber,
            startDate: new Date($scope.claimCase.insuranceDetails.startDate),
            endDate: new Date($scope.claimCase.insuranceDetails.endDate),
            ncbStatus: $scope.claimCase.customerEmail,
            customerName: $scope.claimCase.customerName,
            customerPhoneNumber: $scope.claimCase.customerPhoneNumber
        };
    };

    $scope.submitNewInsuranceData = function () {
        var data = {
            "caseId": $routeParams.id,
            "claim_number": $scope.claimCase.claimNumber,
            "financier": $scope.newInsuranceData.financer,
            "end_date": new Date($scope.newInsuranceData.endDate).getTime(),
            "insured_name": $scope.newInsuranceData.insuredName,
            "insurer": $scope.newInsuranceData.insurer,
            "sumInsured": $scope.newInsuranceData.sumInsured,
            "idv": $scope.newInsuranceData.idv,
            "insuredAddress": $scope.newInsuranceData.insuredAddress,
            "insuredMobileNumber": $scope.newInsuranceData.insuredMobileNumber,
            "remarks": $scope.newInsuranceData.remarks,
            "policy_number": $scope.newInsuranceData.policyNumber,
            "start_date": new Date($scope.newInsuranceData.startDate).getTime()
        };

        var insuranceDetails = {
            "insurance_details": [
                data
            ]
        };

        ajaxService.updateClaimsInsuranceDetails($routeParams.id, insuranceDetails, 'Update Details. PleaseWait!...')
                .then(function (result) {
                    alert('Insurance Data Updated!');
                    $scope.setUpadtedInsuranceData(result);
                })
                .catch(function (e) {
                    alert('Error updating Insurance Data.');
                    console.log('Error updating Insurance Data.', e);
                });
    };

    $scope.editDriverDataEnable = function () {
        $scope.editDriverData = !$scope.editDriverData;
        $scope.setNewDriverData();
    };

    $scope.setUpdatedDriverData = function (result) {
        $scope.editDriverData = !$scope.editDriverData;
        $scope.claimCase.driverDetails = result.data;
        $scope.setNewDriverData();
    };

    $scope.setNewDriverData = function () {
        if ($scope.claimCase.driverDetails === null) {
            $scope.claimCase.driverDetails = {};
            $scope.claimCase.driverDetails.driverName = "";
            $scope.claimCase.driverDetails.licenseType = "";
            $scope.claimCase.driverDetails.relationWithClaimant = "";
            $scope.claimCase.driverDetails.licenseNumber = "";

            $scope.claimCase.driverDetails.expiryDate = new Date().getTime();
            $scope.claimCase.driverDetails.badgeNumber = "";
            $scope.claimCase.driverDetails.licenseEndorsement = "";
            $scope.claimCase.driverDetails.remarks = "";
        }
        if ($scope.claimCase.driverDetails.issueDate === null) {
            $scope.claimCase.driverDetails.issueDate = new Date().getTime();
        }
        if ($scope.claimCase.driverDetails.expiryDate === null) {
            $scope.claimCase.driverDetails.expiryDate = new Date().getTime();
        }



        $scope.newDriverData = {
            driverName: $scope.claimCase.driverDetails.driverName,
            licenseType: $scope.claimCase.driverDetails.licenseType,
            relationWithClaimant: $scope.claimCase.driverDetails.relationWithClaimant,
            licenseNumber: $scope.claimCase.driverDetails.licenseNumber,
            issueDate: new Date($scope.claimCase.driverDetails.issueDate),
            expiryDate: new Date($scope.claimCase.driverDetails.expiryDate),
            badgeNumber: $scope.claimCase.driverDetails.badgeNumber,
            licenseEndorsement: $scope.claimCase.driverDetails.licenseEndorsement,
            remarks: $scope.claimCase.driverDetails.licenseEndorsement
        };
        for (var i = 0; i < $scope.licenseTypes.length; i++) {
            if ($scope.licenseTypes[i].licenseType === $scope.claimCase.driverDetails.licenseType) {
                $scope.newDriverData.licenseType = $scope.licenseTypes[i].licenseType;
            }
        }
    };

    $scope.setSelectedLicenseType = function (item) {
        $scope.newDriverData.licenseType = item;
    };

    $scope.checkLicenseType = function (item, index) {
        if (item.licenseType === $scope.claimCase.driverDetails.licenseType) {
            return true;
        }
        else {
            return false;
        }
    };

    $scope.submitNewDriverData = function () {
        var data = {
            claim_number: $scope.claimCase.claimNumber,
            driver_name: $scope.newDriverData.driverName,
            license_type: $scope.newDriverData.licenseType,
            issuingAuthority: $scope.newDriverData.issuingAuthority,
            license_number: $scope.newDriverData.licenseNumber,
            issue_date: new Date($scope.newDriverData.issueDate).getTime(),
            expiry_date: new Date($scope.newDriverData.expiryDate).getTime(),
            badgeNumber: $scope.newDriverData.badgeNumber,
            licenseEndorsement: $scope.newDriverData.licenseEndorsement,
            remarks: $scope.newDriverData.remarks
        };

        var driverDetails = {
            "driver_details": [
                data
            ]
        };

        console.log('data=================================*******', data);

        ajaxService.updateClaimsDriverDetails($routeParams.id, driverDetails, 'Update Details. PleaseWait!...')
                .then(function (result) {
                    alert('Driver Data Updated!');
                    $scope.setUpdatedDriverData(result);
                })
                .catch(function (e) {
                    alert('Error updating Driver Data.');
                    console.error('Error updating Driver Data.', e);
                });
    };

    $scope.editAccidentDataEnable = function () {
        $scope.editAccidentData = !$scope.editAccidentData;
        $scope.setNewAccidentData();
    };

    $scope.setSelectedDamage = function (value) {
        $scope.newAccidentData.thirdPartyDamage = value;
    };

    $scope.setSelectedDamageType = function (value) {
        $scope.newAccidentData.thirdPartyDamageType = value;
    };

    $scope.setpunchnamaCarriedOut = function (value) {
        $scope.newAccidentData.punchnamaCarriedOut = value;
    };

    $scope.setReportedtoPolice = function (value) {
        $scope.newAccidentData.reportedToPolice = value;
    };

    $scope.setNewAccidentData = function () {
        if ($scope.claimCase.accidentDetails === null) {
            $scope.claimCase.accidentDetails = {};
            $scope.claimCase.accidentDetails.accidentLocation = "";
            $scope.claimCase.accidentDetails.dateOfAccident = new Date();
            $scope.claimCase.accidentDetails.dateOfSurvey = new Date();
            $scope.claimCase.accidentDetails.dateOfIntimation = new Date();
            $scope.claimCase.accidentDetails.licenseNumber = "";
            $scope.claimCase.accidentDetails.thirdPartyDamage = "";
            $scope.claimCase.accidentDetails.thirdPartyDamageType = "";
            $scope.claimCase.accidentDetails.thirdPartyDamageRemarks = "";
            $scope.claimCase.accidentDetails.stationDiaryEntryNumber = "";
            $scope.claimCase.accidentDetails.punchnamaCarriedOut = "";
            $scope.claimCase.accidentDetails.accidentRemarks = "";
            $scope.claimCase.accidentDetails.otherRemarks = "";
            $scope.claimCase.accidentDetails.locationOfSurvey = "";
            $scope.claimCase.accidentDetails.otherRemarks = "";
            $scope.claimCase.accidentDetails.reportedToPolice = "";
            $scope.claimCase.accidentDetails.policeStationName = "";
        }
        $scope.newAccidentData = {
            accidentLocation: $scope.claimCase.accidentDetails.accidentLocation,
            claimNumber: $scope.claimCase.claimNumber,
            dateOfAccident: new Date($scope.claimCase.accidentDetails.dateOfAccident),
            dateOfSurvey: new Date($scope.claimCase.accidentDetails.dateOfSurvey),
            dateOfIntimation: new Date($scope.claimCase.accidentDetails.dateOfIntimation),
            licenseNumber: $scope.claimCase.accidentDetails.licenseNumber,
            thirdPartyDamage: $scope.claimCase.accidentDetails.thirdPartyDamage,
            thirdPartyDamageType: $scope.claimCase.accidentDetails.thirdPartyDamageType,
            thirdPartyDamageRemarks: $scope.claimCase.accidentDetails.thirdPartyDamageRemarks,
            stationDiaryEntryNumber: $scope.claimCase.accidentDetails.stationDiaryEntryNumber,
            punchnamaCarriedOut: $scope.claimCase.accidentDetails.punchnamaCarriedOut,
            accidentRemarks: $scope.claimCase.accidentDetails.accidentRemarks,
            otherRemarks: $scope.claimCase.accidentDetails.otherRemarks,
            locationOfSurvey: $scope.claimCase.accidentDetails.locationOfSurvey,
            thridPartLoss: $scope.claimCase.accidentDetails.thridPartLoss,
            reportedToPolice: $scope.claimCase.accidentDetails.reportedToPolice,
            policeStationName: $scope.claimCase.accidentDetails.policeStationName
        };


        for (var i = 0; i < $scope.reportPoliceItems.length; i++) {
            if ($scope.reportPoliceItems[i].selector === $scope.claimCase.accidentDetails.reportedToPolice) {
                $scope.newAccidentData.reportedToPolice = $scope.reportPoliceItems[i].selector;
            }
        }

        for (var i = 0; i < $scope.punchnamCarriedItemsNew.length; i++) {
            if ($scope.punchnamCarriedItemsNew[i].selector === $scope.claimCase.accidentDetails.punchnamaCarriedOut) {
                $scope.newAccidentData.punchnamaCarriedOut = $scope.punchnamCarriedItemsNew[i].selector;
            }
        }

        for (var i = 0; i < $scope.thridPartyDamageTypesNew.length; i++) {
            if ($scope.thridPartyDamageTypesNew[i].damageType === $scope.claimCase.accidentDetails.thirdPartyDamageType) {
                $scope.newAccidentData.thirdPartyDamageType = $scope.thridPartyDamageTypesNew[i].damageType;
            }

        }
        for (var i = 0; i < $scope.thridPartyDamageItems.length; i++) {
            if ($scope.thridPartyDamageItems[i].selector === $scope.claimCase.accidentDetails.thirdPartyDamage) {
                $scope.newAccidentData.thirdPartyDamage = $scope.thridPartyDamageItems[i].selector;
            }
        }
    };

    $scope.setUpdatedAccidentData = function (result) {
        $scope.editAccidentData = !$scope.editAccidentData;
        $scope.claimCase.accidentDetails = result.data;
        $scope.setNewAccidentData();
    };

    $scope.submitNewAccidentData = function () {
        var data = {
            "accident_location": $scope.newAccidentData.accidentLocation,
            "claim_number": $scope.newAccidentData.claimNumber,
            "date_of_accident": new Date($scope.newAccidentData.dateOfAccident).getTime(),
            "date_of_survey": new Date($scope.newAccidentData.dateOfSurvey).getTime(),
            "dateOfIntimation": new Date($scope.newAccidentData.dateOfIntimation).getTime(),
            "license_number": $scope.newAccidentData.licenseNumber,
            "thirdPartyDamage": $scope.newAccidentData.thirdPartyDamage,
            "thirdPartyDamageType": $scope.newAccidentData.thirdPartyDamageType,
            "thirdPartyDamageRemarks": $scope.newAccidentData.thirdPartyDamageRemarks,
            "stationDiaryEntryNumber": $scope.newAccidentData.stationDiaryEntryNumber,
            "punchnamaCarriedOut": $scope.newAccidentData.punchnamaCarriedOut,
            "accidentRemarks": $scope.newAccidentData.accidentRemarks,
            "otherRemarks": $scope.newAccidentData.otherRemarks,
            "locationOfSurvey": $scope.newAccidentData.locationOfSurvey,
            "location_of_survey": $scope.newAccidentData.locationOfSurvey,
            "third_party_loss": $scope.newAccidentData.thridPartLoss,
            "reportedToPolice": $scope.newAccidentData.reportedToPolice,
            "policeStationName": $scope.newAccidentData.policeStationName
        };

        var accidentDetails = {
            "accident_details": [
                data
            ]
        };

        console.log('data=================================*******', data);

        ajaxService.updateClaimsAccidentDetails($routeParams.id, accidentDetails, 'Update Details. PleaseWait!...')
                .then(function (result) {
                    alert('Accident Data Updated!');
                    $scope.setUpdatedAccidentData(result);
                })
                .catch(function (e) {
                    alert('Error updating Accident Data.');
                    console.error('Error updating Driver Data.', e);
                });
    };

    $scope.editSummaryDataEnable = function () {
        $scope.editSummaryData = !$scope.editSummaryData;
        $scope.setNewSummaryData();
    };

    $scope.updateTotalCost = function () {
        $scope.newSummaryData.total_cost = parseInt($scope.newSummaryData.totalLabourCharges) + parseInt($scope.newSummaryData.supplementryLabourCharges);
    };

    $scope.updatePartCost = function () {
        $scope.newSummaryData.supplementry_cost = parseInt($scope.newSummaryData.totalPartCharges) + parseInt($scope.newSummaryData.supplementryPartCharges);
    };

    $scope.setUpdatedSummaryData = function (result) {
        $scope.editSummaryData = !$scope.editSummaryData;
        $scope.assessmentSummary = result.data;
        $scope.assessmentSummary.total_cost = parseInt($scope.assessmentSummary.totalLabourCharges) + parseInt($scope.assessmentSummary.supplementryLabourCharges);
        $scope.assessmentSummary.supplementry_cost = parseInt($scope.assessmentSummary.totalPartCharges) + parseInt($scope.assessmentSummary.supplementryPartCharges);
        $scope.setNewSummaryData();
    };

    $scope.lossAssessmentPhotos = function (data) {
        $scope.losssPhotoDetails = $scope.caseDetails.lossAssessmentPhotos;
        $("#qc-check-lossAssessment").modal('show');
        $("#qc-check-lossAssessment .modal-body")
                .html($compile("<loss-photos  lossAssessmentPhotos='losssPhotoDetails' case-id='caseId' photo-types='losssPhotoDetails'></loss-photos>")($scope));
    };
    $scope.lossAssessmentPhoto = function(data){
        $scope.losssPhotoDetails = $scope.caseDetails.lossAssessmentPhotos;
        $("#qc-check-lossAssessment").modal('show');
        $("#qc-check-lossAssessment .modal-body")
                .html($compile("<loss-photos  lossAssessmentPhotos='losssPhotoDetails' case-id='caseId' photo-types='losssPhotoDetails'></loss-photos>")($scope));
    }
    // $scope.lossAssessmentPhoto = function (data) {
    //     $scope.losssPhotoDetails = $scope.caseDetails.lossAssessmentPhotos;
    //     $("#qc-check-lossAssessment").modal('show');
    //     $("#qc-check-lossAssessment .modal-body")
    //             .html($compile("<loss-photos  lossAssessmentPhotos='losssPhotoDetails' case-id='caseId' photo-types='losssPhotoDetails'></loss-photos>")($scope));
    // };
    $scope.setNewSummaryData = function () {
        $scope.newSummaryData = {
            claimNumber: $scope.assessmentSummary.claimNumber,
            totalLabourCharges: $scope.assessmentSummary.totalLabourCharges,
            supplementryLabourCharges: $scope.assessmentSummary.supplementryLabourCharges,
            totalPartCharges: $scope.assessmentSummary.totalPartCharges,
            supplementryPartCharges: $scope.assessmentSummary.supplementryPartCharges,
            lessExcess: $scope.assessmentSummary.lessExcess,
            lessSalavage: $scope.assessmentSummary.lessSalavage,
            remarks: $scope.assessmentSummary.remarks,
            supplementry_cost: $scope.assessmentSummary.supplementry_cost,
            total_cost: $scope.assessmentSummary.total

        };
    };

    $scope.submitNewSummaryData = function () {
        var data = {
            claimNumber: $scope.newSummaryData.claimNumber,
            total_labour_charges: parseInt($scope.newSummaryData.totalLabourCharges),
            supplementry_labour_charges: parseInt($scope.newSummaryData.supplementryLabourCharges),
            total_part_costs: parseInt($scope.newSummaryData.totalPartCharges),
            supplementry_part_costs: parseInt($scope.newSummaryData.supplementryPartCharges),
            less_excess: parseInt($scope.newSummaryData.lessExcess),
            less_salavage: parseInt($scope.newSummaryData.lessSalavage),
            remarks: $scope.newSummaryData.remarks
        };

        var assessmentSummary = {
            "assessment-summary": [
                data
            ]
        };


        console.log('data=================================*******', data);

        ajaxService.updateAssessmentSummary($routeParams.id, assessmentSummary, 'Update Details. PleaseWait!...')
                .then(function (result) {
                    alert('Summary Data Updated!');
                    $scope.setUpdatedSummaryData(result);
                })
                .catch(function (e) {
                    alert('Error updating Driver Data.');
                    console.error('Error updating Driver Data.', e);
                });
    };

    

    $scope.editLossAssessmentEnable = function () {
        $scope.editLossAssessmentData = !$scope.editLossAssessmentData;
        $scope.setNewLossAssessmentData();
    };

    $scope.setNewLossAssessmentData = function () {
    };

    $scope.submitNewLossAssessmentData = function () {

        var data = {
            "damage_type": $scope.newLossAssessmentData.damageType,
            "labour_cost": parseInt($scope.newLossAssessmentData.labourCost),
            "part_cost": parseInt($scope.newLossAssessmentData.partCost),
            "part_particular_type": $scope.newLossAssessmentData.partParticularType,
            "part_side": $scope.newLossAssessmentData.partSide,
            "part_type": $scope.newLossAssessmentData.partType,
            "repair_type": $scope.newLossAssessmentData.repairType
        };

        var lossAssessmentData = [];
        lossAssessmentData.push(data);

        var lossAssessmentDetails = {
            "lossassement_details": lossAssessmentData
        };

        console.log('data=================================*******', data);

        ajaxService.updateClaimsLossAssessmentDetails($routeParams.id, lossAssessmentDetails, 'Update Details. PleaseWait!...')
                .then(function () {
                    alert('LossAssessment Data Updated!');
                    $scope.getReportDetails();
                    $scope.editLossAssessmentEnable();
                })
                .catch(function (e) {
                    alert('Error updating Accident Data.');
                    console.error('Error updating Driver Data.', e);
                });
    };


    $scope.enableVehicleNumberEdit = function () {
        $scope.vehicleNumberEditEnable = true;
        $scope.vahanVerified = true;
    };

    $scope.editVehicleDataEnable = function () {
        $scope.editVehicleData = !$scope.editVehicleData;
        $scope.setNewVehicleData();
    };

    $scope.setUpadtedVehicleData = function (result) {
        $scope.editVehicleData = !$scope.editVehicleData;
        $scope.claimCase.vehicleDetails = result.data;
        $scope.claimCase.vehicleType = result.data.vehicleType;
        $scope.claimCase.engineNumber = result.data.vehicleType;
        $scope.claimCase.vehicleNumber = result.data.vehicleNumber;
        $scope.setNewVehicleData();
    };

    $scope.setNewVehicleData = function () {
        if ($scope.claimCase.vehicleDetails === null) {
            $scope.claimCase.vehicleDetails = {};
            $scope.claimCase.vehicleDetails.vehicleType = "";
            $scope.claimCase.vehicleDetails.make = "";
            $scope.claimCase.vehicleDetails.validUpto = new Date();
            $scope.claimCase.vehicleDetails.taxPaidUpto = new Date();
            $scope.claimCase.vehicleDetails.vehicleYOM = new Date();
            $scope.claimCase.vehicleDetails.model = "";
            $scope.claimCase.vehicleDetails.vehicleColor = "";
            $scope.claimCase.vehicleDetails.vehicleFuelType = "";
            $scope.claimCase.vehicleDetails.chassisNumber = "";
            $scope.claimCase.vehicleDetails.engineNumber = "";
            $scope.claimCase.vehicleDetails.vehicleNumber = "";
            $scope.claimCase.vehicleDetails.odometerReading = "";
            $scope.claimCase.vehicleDetails.remark = "";
            $scope.claimCase.vehicleDetails.permitNumber = "";
            $scope.claimCase.vehicleDetails.vehiclePermitType = "";
            $scope.claimCase.vehicleDetails.bodyType = "";
            $scope.claimCase.vehicleDetails.passengerCarryingCapacity = "";
            $scope.claimCase.vehicleDetails.fitnessCertificateNumber = "";
            $scope.claimCase.vehicleDetails.variant = "";
        }
        $scope.newVehicleData = {
            type: $scope.claimCase.vehicleType,
            make: $scope.claimCase.vehicleDetails.vehicleMake,
            model: $scope.claimCase.vehicleDetails.vehicleModel,
            vehicleNumber: $scope.claimCase.vehicleDetails.vehicleNumber,
            yom: new Date($scope.claimCase.vehicleYOM),
            chassisNumber: $scope.claimCase.vehicleDetails.chassisNumber,
            engineNumber: $scope.claimCase.vehicleDetails.engineNumber,
            vinPlateNumber: $scope.claimCase.vehicleDetails.vehicleNumber,
            odometerReading: $scope.claimCase.vehicleDetails.odometerReading,
            remark: $scope.claimCase.vehicleDetails.remark,
            permitNumber: $scope.claimCase.vehicleDetails.permitNumber,
            permitType: $scope.claimCase.vehicleDetails.permitType,
            bodyType: $scope.claimCase.vehicleDetails.bodyType,
            passengerCarryingCapacity: $scope.claimCase.vehicleDetails.passengerCarryingCapacity,
            fitnessCertificateNumber: $scope.claimCase.vehicleDetails.fitnessCertificateNumber,
            vehicleVaraint: $scope.claimCase.vehicleDetails.variant,
            validUpto: new Date($scope.claimCase.vehicleDetails.validUpto),
            taxPaidUpto: new Date($scope.claimCase.vehicleDetails.taxPaidUpto)
        };

        for (i = 0; i < $scope.vehicleTypesNew.length; i++) {
            if ($scope.vehicleTypesNew[i].name === $scope.claimCase.vehicleType) {
                $scope.newVehicleData.type = $scope.vehicleTypesNew[i].id;
            }
        }
        for (i = 0; i < $scope.vehicleFuelTypesNew.length; i++) {
            if ($scope.vehicleFuelTypesNew[i].name === $scope.claimCase.vehicleFuelType) {
                $scope.newVehicleData.vehicle_fuel_type = $scope.vehicleFuelTypesNew[i].id;
            }
        }
        for (i = 0; i < $scope.vehicleMakeModelsUniqNew.length; i++) {
            if ($scope.vehicleMakeModelsUniqNew[i].id === $scope.claimCase.vehicleDetails.vehicleMake) {
                $scope.newVehicleData.make = $scope.vehicleMakeModelsUniqNew[i].make;
            }
        }
        for (i = 0; i < $scope.vehicleModelsUniqNew.length; i++) {
            if ($scope.vehicleModelsUniqNew[i].model === $scope.claimCase.vehicleDetails.vehicleModel) {
                $scope.newVehicleData.model = $scope.vehicleModelsUniqNew[i].model;
            }
        }
        for (i = 0; i < $scope.vehicleBodyTypesNew.length; i++) {
            if ($scope.vehicleBodyTypesNew[i].bodyType === $scope.claimCase.bodyType) {
                $scope.newVehicleData.bodyType = $scope.vehicleBodyTypesNew[i].bodyType;
            }
        }

        // vehiclePermitTypes
        for (var i = 0; i < $scope.vehiclePermitTypesNew.length; i++) {
            if ($scope.vehiclePermitTypesNew[i].permitType === $scope.claimCase.vehiclePermitType) {
                $scope.newVehicleData.vehiclePermitType = $scope.vehiclePermitTypesNew[i].permitType;
            }
        }

        // makeModelVariants
        for (var i = 0; i < $scope.makeModelVariantsNew.length; i++) {
            if ($scope.makeModelVariantsNew[i].variant === $scope.claimCase.variant) {
                $scope.newVehicleData.variant = $scope.makeModelVariantsNew[i].variant;
            }
        }

        // vehicleTypes
        for (var i = 0; i < $scope.vehicleTypesNew.length; i++) {
            if ($scope.vehicleTypesNew[i].name === $scope.claimCase.vehicleType) {
                $scope.newVehicleData.type = $scope.vehicleTypesNew[i].id;
            }
        }

    };

    $scope.submitNewVehicleData = function () {

        ajaxService.updateCaseVehicleDetails($routeParams.id, $scope.newVehicleData, 'Update Details. PleaseWait!...')
                .then(function (result) {
                    alert('Vehicle Data Updated!');
                    $scope.setUpadtedVehicleData(result);
                })
                .catch(function (e) {
                    alert('Error updating Vehicle Data.');
                    console.error('Error updating Vehicle Data.', e);
                });
    };

    $scope.unsetMakeModels = function () {
        $scope.newVehicleData.make = "";
        $scope.newVehicleData.makeModel = "";
    };

    $scope.setSelectedMakeModels = function (data) {
        $scope.vehicleMakeSelectedModels = [];

        for (i = 0; i < $scope.vehicleMakeModelsNew.length; i++) {
            if ($scope.vehicleMakeModelsNew[i].make === $scope.newVehicleData.make) {
                $scope.vehicleMakeSelectedModels.push($scope.vehicleMakeModelsNew[i]);
            }
        }

        for (i = 0; i < $scope.vehicleMakeSelectedModels.length; i++) {
            if ($scope.vehicleMakeSelectedModels[i].model === $scope.claimCase.model) {
                $scope.newVehicleData.model = $scope.vehicleMakeSelectedModels[i].id + "";
            }
        }
    };

    $scope.checkType = function (item, index) {
        return item.make === $scope.claimCase.vehicleMake;
    };

    $scope.enableVerify = function () {
        if ($scope.caseDetails.vehicleNumber.trim() !== $scope.newVehicleData.vehicle_number.trim())
            $scope.vehicleNumberVerifyDisabled = false;
        else
            $scope.vehicleNumberVerifyDisabled = true;
    };

    $scope.vahanVerify = function () {
        $scope.getVahanData();
    };

    $scope.zoomIn = function (id) {
        $("#" + id).elevateZoom({
            zoomWindowPosition: 10,
            zoomWindowOffetx: -15,
            scrollZoom: true,
            zoomWindowWidth: 500,
            zoomWindowHeight: 500
        });
    };

    $scope.zoomOut = function () {
    };
    
    function disableNonQuestionGroups(){
        var questionTags = [];

        for(var i=0 ; i < $scope.caseQuestions.length; i++){
            if(questionTags.indexOf($scope.caseQuestions[i].photoTag) === -1){
                questionTags.push($scope.caseQuestions[i].photoTag)
            }
        }

        for(var i=0; i < $scope.groups.length; i++){
            var item = $scope.groups[i];

            if(item.questionTag && questionTags.indexOf(item.questionTag) === -1)
                item.hide = true;
        }
    }

    angular.element(document).ready(function () {
            
            ajaxService.getVehicleMakeModels(null, 'Please Wait!...').then(function (result) {
                $scope.vehicleMakeModelsNew = result.data;
                $scope.vehicleMakeModelsUniqNew = [];
                $scope.vehicleModelsUniqNew = [];
                _.map(_.uniqBy($scope.vehicleMakeModelsNew, 'make'), function (item) {
                    $scope.vehicleMakeModelsUniqNew.push(item);
                });
                _.map(_.uniqBy($scope.vehicleMakeModelsNew, 'model'), function (item) {
                    $scope.vehicleModelsUniqNew.push(item);
                });

                console.log("getVehicleMakeModels", result);
            }).catch(function (error) {
                console.log("getVehicleMakeModels", error);
            });

            // ajaxService.getCostConfigurationRecord().then(function (result){
            //     $scope.costConfig = result.data;                        
            // }).catch(function(error){
            //     console.log("error",error);
            // })

            ajaxService.getClaimsCaseDetails({case_id: $routeParams.id}, "Fetching data. Please wait!").then(function (result) {
                $scope.caseDetails = result.data;
                $scope.claimCase = result.data.claimCase;
                
                function getPhotosArray(arr){
                    var result = [];
                    for (var i = 0; i < arr.length; i++) {
                        result.push("util/case-image/" + $scope.caseId + "/" + arr[i].fileName);
                    }
                }
               

                $scope.insuranceDetailsPhotos = getPhotosArray($scope.caseDetails.insuranceDetailsPhotos);
                $scope.vehicleDetailsPhotos = getPhotosArray($scope.caseDetails.vehicleDetailsPhotos);
                $scope.driverDetailsPhotos = getPhotosArray($scope.caseDetails.driverDetailsPhotos);
                $scope.accidentDetailsPhotos = getPhotosArray($scope.caseDetails.accidentDetailsPhotos);

                $scope.claimCase.lossAssessments = $scope.caseDetails.lossAssessments;
                if ($scope.claimCase.lossAssessments === null) {
                    $scope.claimCase.lossAssessments = [];
                    $scope.HandlingLossAssement = [];
                }
                else {
                    // Calcaulate total value
                    for (var i = 0; i < $scope.claimCase.lossAssessments.length; i++) {
                        var calPartCost = $scope.claimCase.lossAssessments[i].partCost + ($scope.claimCase.lossAssessments[i].gstRateForPart / 100) * $scope.claimCase.lossAssessments[i].partCost;
                        calPartCost = calPartCost - (($scope.claimCase.lossAssessments[i].depRateForPart / 100) * calPartCost);
                        $scope.totalPartCost = $scope.totalPartCost + calPartCost;
                    }
                    for (var i = 0; i < $scope.claimCase.lossAssessments.length; i++) {
                        var calLabourCost = $scope.claimCase.lossAssessments[i].labourCost + ($scope.claimCase.lossAssessments[i].gstRateForLabour / 100) * $scope.claimCase.lossAssessments[i].labourCost;
                        calLabourCost = calLabourCost - (($scope.claimCase.lossAssessments[i].depRateForLabour / 100) * calPartCost);
                        $scope.totalLabourCost = $scope.totalLabourCost + calLabourCost;
                    }


                    // Handling two rows
                    $scope.HandlingLossAssement = [];
                    for (var i = 0; i < $scope.claimCase.lossAssessments.length; i++) {
                        var cost = "";
                        if (i % 2 === 0) {
                            cost = $scope.claimCase.lossAssessments[i].partCost;
                        }
                        else {
                            cost = $scope.claimCase.lossAssessments[i].labourCost;
                        }
                        var finalGSTCost = $scope.claimCase.lossAssessments[i].partCost * (parseInt($scope.claimCase.lossAssessments[i].gstRateForPart) / 100);
                        var finalDepCost = finalGSTCost * (parseInt($scope.claimCase.lossAssessments[i].depRateForPart) / 100);
                        $scope.claimCase.lossAssessments[i].finalTotal = cost + finalGSTCost - finalDepCost;
                    }
                    for (var i = 0; i < $scope.claimCase.lossAssessments.length; i++) {
                        var data = {
                            "id": $scope.HandlingLossAssement.length + 1,
                            "type": 'Spare',
                            "damageType": $scope.claimCase.lossAssessments[i].damageType,
                            "partParticularType": $scope.claimCase.lossAssessments[i].partParticularType,
                            "repairType": $scope.claimCase.lossAssessments[i].repairType,
                            "gstRate": parseInt($scope.claimCase.lossAssessments[i].gstRateForPart),
                            "depRate": parseInt($scope.claimCase.lossAssessments[i].depRateForPart),
                            "Cost": $scope.claimCase.lossAssessments[i].partCost,
                            "finalTotal": $scope.claimCase.lossAssessments[i].finalTotal
                        };
                        $scope.HandlingLossAssement.push(data);
                        var dataNew = {
                            "id": $scope.HandlingLossAssement.length + 1,
                            "type": 'Labour',
                            "damageType": $scope.claimCase.lossAssessments[i].damageType,
                            "partParticularType": $scope.claimCase.lossAssessments[i].partParticularType,
                            "repairType": $scope.claimCase.lossAssessments[i].repairType,
                            "gstRate": parseInt($scope.claimCase.lossAssessments[i].gstRateForLabour),
                            "depRate": parseInt($scope.claimCase.lossAssessments[i].depRateForLabour),
                            "Cost": $scope.claimCase.lossAssessments[i].labourCost,
                            "finalTotal": $scope.claimCase.lossAssessments[i].finalTotal
                        };
                        $scope.HandlingLossAssement.push(dataNew);
                    }
                    console.log("HandlingLossAssement", $scope.HandlingLossAssement);
                }

            }).catch(function (error) {
                console.log("caseDetails", error);
            });
            
            
            
            $q.all([
                ajaxService.getCasePhotos({case_id: $routeParams.id}, "Fetching photos... Please wait!"),
                // ajaxService.getClaimsCaseQuestions({case_id: $routeParams.id}, "Fetching data. Please wait!"),
                ajaxService.getClaimsCaseDetails({case_id: $routeParams.id}, "Fetching data. Please wait!"),
                //ajaxService.getQuestionOptions(null, "Fetching data. Please wait!"),
                ajaxService.getPhotoTypes(null, "Fetching data. Please wait!"),
                ajaxService.getVehicleTypes(null, 'Please Wait!...'),
                ajaxService.getVehicleMakeModels(null, 'Please Wait!...'),
                // Claims DropDownData
                ajaxService.getClaimsConfigDetails(null, "Fetching data. Please wait!")
            ]).then(function (res) {
                console.log('res==================>*', res);
                // $scope.casePhotos = res[0].data;
                    res[0].data.sort(function(a,b) {
                    if (a.id < b.id)
                      return -1;
                    if (a.id > b.id)
                      return 1;
                    return 0;
                });
                $scope.casePhotos = res[0].data;
                $scope.caseDetails = res[1].data;
                //$scope.caseOptions = res[3].data;
                $scope.photoTypes = res[2].data;
                console.log("$scope.caseDetails", $scope.caseDetails);
                console.log("$scope.caseDetails", $scope.caseDetails.customerEmail);
                $scope.newCustName = $scope.caseDetails.customerName;
                disableNonQuestionGroups();
                //$scope.vehicleTypes = res[5].data;
//                $scope.vehicleMakeModels = res[6].data;
//                $scope.vehicleMakeModelsUniq = [];
//                $scope.makeModelVariants = res[7].data;
//                $scope.vehicleBodyTypes = res[8].data;
//                $scope.getVehicleTypes = res[9].data;
//                $scope.getVehiclePermitTypes = res[10].data;
//                $scope.getMakeModelVariants = res[11].data;
//                $scope.getDriverLicenseTypes = res[12].data;
//                $scope.getThirdPartyDamageTypes = res[13].data;
//                $scope.getVehicleLabourCost = res[14].data;
//                $scope.getVehiclePartCost = res[15].data;
//                $scope.getVehicleRepairType = res[16].data;
//                $scope.getDriverLicenseTypes = res[17].data;
//                $scope.getVehicleDamageType = res[18].data;
//                $scope.getVehiclePartType = res[19].data;
//                $scope.getVehiclePartSide = res[20].data;
//                $scope.getVehicleParticularType = res[21].data;
//                $scope.getThirdPartyDamageItems = res[22].data;
//                $scope.getReportPoliceItems = res[23].data;
//                $scope.getPunchnamaCarriedOutItems = res[24].data;
                if ($scope.caseDetails.remark)
                    $scope.recommendation = $scope.caseDetails.remark;

                _.map(_.uniqBy(res[5].data, 'id'), function (item) {
                    $scope.vehicleTypes.push(item);
                });

                _.map(_.uniqBy(res[7].data, 'make'), function (item) {
                    $scope.vehicleMakeModelsUniq.push(item);
                });

                $scope.caseQuestions.forEach(function (caseQuestion, index) {
                    var item = getDefaultGroupOption(caseQuestion.group);
                    if (item !== null)
                        caseQuestion.answer = "" + item.id;
                });

                $scope.caseDetails.inspectionTime = new Date($scope.caseDetails.inspectionTime);
                // $scope.casePhotos.sort(compare);
                var item = (_.sortBy($scope.casePhotos, function (o) {
                    return o.id;
                }));

                $scope.casePhotos = item.reverse();
                console.log('reversed case photos==> ', $scope.casePhotos);

                var tmpLast = parseInt(($scope.casePhotos[0].fileName).split('.')[0]);
                $scope.casePhotos.lastCasePhotoUploadTime = tmpLast;
                $scope.casePhotos.forEach(function (item, index) {
                    var tags = getPhotoTagsByPhotoType(item.photoType);
                    item.tags = tags;
                    item.groups = getGroupsByPhotoTags(item.tags);
                    item.zoom = "util/case-image/" + $scope.caseId + "/" + item.fileName;
                    if (item.photoType == "chachis-number") {
                        $scope.vehicleChachis = item.qcAnswer;
                        $scope.vehicleChachisPhoto = "util/case-image/" + $scope.caseId + "/" + item.fileName;
                    }

                    if (item.photoType == "vin-plate-number") {
                        $scope.vehicleVin = item.qcAnswer;
                        $scope.vehicleVinPhoto = "util/case-image/" + $scope.caseId + "/" + item.fileName;
                    }

                    if (item.photoType == "odometer-rpm") {
                        $scope.odometerReading = item.qcAnswer;
                    }



                    // if(parseInt((item.fileName).split('.')[0]) > tmpLast){
                    // $scope.casePhotos.lastCasePhotoUploadTime =
                    // parseInt((item.fileName).split('.')[0]);
                    // }
                });

                $scope.casePhotos.lastCasePhotoUploadTime = (_.maxBy($scope.casePhotos, 'snapTime')).snapTime;

                console.log('$scope.casePhotos--->', $scope.casePhotos);
                // $scope.buildCarousel($scope.casePhotos);

                var t1 = moment($scope.caseDetails.creationTime);
                var t2 = moment($scope.caseDetails.inspectionTime);
                $scope.caseDetails.ciTimeDiff = t2.diff(t1, 'minutes');

                var t2 = moment($scope.casePhotos.lastCasePhotoUploadTime);
                console.log('t2===============<', t2);
                $scope.caseDetails.cpTimeDiff = t2.diff(t1, 'minutes');

                $scope.setNewVehicleData();

            });
            $scope.caseVideo = undefined;
            $scope.caseVideoRequest = true;

            var vidReq = new XMLHttpRequest();
            vidReq.open('GET', 'case/inspection-video/' + $scope.caseId, true);
            vidReq.responseType = 'blob';

            vidReq.onload = function () {
                // Onload is triggered even on 404
                // so we need to check the status code
                if (this.status === 200) {
                    var videoBlob = this.response;
                    var vid = URL.createObjectURL(videoBlob); // IE10+
                    // Video is now downloaded
                    console.log('Video is now downloaded', vid);
                    // and we can set it as source on the video element
                    $scope.$apply(function () {
                        $scope.caseVideo = vid;
                        $scope.caseVideoRequest = false;
                    });
                    // $scope.caseVideo = vid;
                    // $scope.caseVideoRequest = false;

                }
            }
            vidReq.onerror = function () {
                console.log('Video download Error');
            }

            vidReq.send();
        });

    $scope.isUploadMultiple = function () {

        if (!$scope.photoTypes)
            return false;

        for (var i = 0; i < $scope.photoTypes.length; i++)
            if ($scope.photoTypes[i].id === $scope.uploadFileType && $scope.photoTypes[i].multiple)
                return true;
        return false;
    };

    $scope.replacePhoto = function () {
        var file = document.getElementById('uploadFileIdentifier').files[0];

        if (!file) {
            alert("Please choose a file!");
            return false;
        }

        var formData = new FormData();
        formData.append('case_file', file);
        formData.append('case_id', $scope.caseId);
        formData.append('comment', $scope.uploadFileComment);
        formData.append('photo_type', $scope.uploadFileType);

        ajaxService.replacePhoto(formData, "Uploading file. Please wait!").then(
                function () {
                    alert("File uploaded successfully!");
                }, function (err) {

            if (err.data.error_message)
                alert(err.data.error_message);
            else
                alert("Unable to upload file!");
            console.log(err);
        });
    };

    $scope.addPhoto = function () {
        var file = document.getElementById('uploadFileIdentifier').files[0];

        if (!file) {
            alert("Please choose a file!");
            return false;
        }

        var formData = new FormData();
        formData.append('case_file', file);
        formData.append('case_id', $scope.caseId);
        formData.append('comment', $scope.uploadFileComment);
        formData.append('photo_type', $scope.uploadFileType);

        ajaxService.addPhoto(formData, "Uploading file. Please wait!").then(
                function () {
                    alert("File uploaded successfully!");
                }, function (err) {

            if (err.data.error_message)
                alert(err.data.error_message);
            else
                alert("Unable to upload file!");
            console.log(err);
        });
    };

    $scope.retagPhotos = function (data) {


        $("#qc-check-retag").modal('show');
        $("#qc-check-retag .modal-body")
                .html($compile("<retag-photos callback='retagPhotosCallback()' case-photos='casePhotos' case-id='caseId' photo-types='photoTypes'></retag-photos>")($scope));
    };

    $scope.retagPhotosCallback = function () {
    };

    $scope.bulkUploadFiles = function () {
        var file = document.getElementById('bulkUploadFileIdentifier').files;

        if (!file) {
            alert("Please choose a file!");
            return false;
        }

        var formData = new FormData();

        $.each($("input[type='file']")[0].files, function (i, file) {
            formData.append('photos', file);
        });

        formData.append('case_id', $scope.caseId);

        ajaxService.claimBulkUploadPhoto(formData, "Uploading file. Please wait!").then(
                function () {
                    alert("File uploaded successfully!");
                }, function (err) {

            if (err.data.error_message)
                alert(err.data.error_message);
            else
                alert("Unable to upload file!");
            console.log(err);
        });
    };

    $scope.deletePhoto = function (currentPhoto) {
        ajaxService.deletePhoto({
            photo_id: currentPhoto.id
        }, "Deleting photo. Please wait!").then(function () {
            alert("Photo deleted successfully!");
            var index = $scope.casePhotos.indexOf(currentPhoto);
            $scope.casePhotos.splice(index, 1);
        }, function () {
            alert("Unable to delete photo!");
        });
    };

    $scope.updateCustomerName = function () {
        ajaxService.updateCustomerName({
            case_id: $scope.caseId,
            customer_name: $scope.newCustName
        }, "Updating customer name. Please wait!").then(function () {
            alert("Updated successfully!");
            $scope.caseDetails.customerName = $scope.newCustName;
        }, function () {
            alert("Unable to update name!");
        });
    };

    $scope.getReportDetails = function () {
        ajaxService.fetchReportDetails({
            case_id: $scope.caseId
        }, "fetching report data.Please Wait")
                .then(function (data) {
                    $scope.reportDetails = data;

                    $scope.reportDetails.data.insuranceDetails = $scope.claimCase.insuranceDetails;
                    $scope.reportDetails.data.accidentDetails = $scope.claimCase.accidentDetails;
                    $scope.reportDetails.data.vehicleDetails = $scope.claimCase.vehicleDetails;
                    $scope.reportDetails.data.insuranceDetails = $scope.claimCase.insuranceDetails;

                    //To replace null with N/A in pdf report
                    Object.keys($scope.reportDetails.data.insuranceDetails).forEach(function (key) {
                        if ($scope.reportDetails.data.insuranceDetails[key] === null) {
                            $scope.reportDetails.data.insuranceDetails[key] = 'N/A';
                        }
                    });
                    Object.keys($scope.reportDetails.data.accidentDetails).forEach(function (key) {
                        if ($scope.reportDetails.data.accidentDetails[key] === null) {
                            $scope.reportDetails.data.accidentDetails[key] = 'N/A';
                        }
                    });
                    Object.keys($scope.reportDetails.data.vehicleDetails).forEach(function (key) {
                        if ($scope.reportDetails.data.vehicleDetails[key] === null) {
                            $scope.reportDetails.data.vehicleDetails[key] = 'N/A';
                        }
                    });
                    Object.keys($scope.reportDetails.data.insuranceDetails).forEach(function (key) {
                        if ($scope.reportDetails.data.insuranceDetails[key] === null) {
                            $scope.reportDetails.data.insuranceDetails[key] = 'N/A';
                        }
                    });
                    $scope.downloadPDF();
                    //console.log($scope.reportDetails);
                }, function () {
                    alert("failed fetching RD");
                });
    };

    $scope.downloadPDFNew = function () {
        $scope.getReportDetails();
    };

    $scope.downloadImagePDF = function () {
        downloadImagePDF($scope);
    };

    $scope.downloadPDF = function () {
        downloadPDF($scope);
    };

}]);
