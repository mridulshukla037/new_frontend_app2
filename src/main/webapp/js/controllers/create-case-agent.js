jaduApp.controller('CreateCaseAgentController',['$scope', '$rootScope', '$timeout', '$location', '$compile', 'ajaxService', '$q', 'GoogleMaps', '$log',   function($scope, $rootScope, $timeout, $location, $compile, ajaxService, $q, GoogleMaps, $log) {

    $scope.vehicleNumberRegex = '/^(([A-Z]){2,3}(?:[0-9]){1,2}(?:[A-Z]){1,2}([0-9]){1,4})$/';//not using

    $scope.createCaseModel = {
      purpose_of_survey :  undefined,
      customer_name : undefined,
      customer_phone_number : undefined,
      self_inspect : true,
      vehicle_number : undefined,
      inspection_time : undefined
    };

    $scope.resetCase = function(){
      $scope.createCaseModel = {
        purpose_of_survey :  undefined,
        customer_name : undefined,
        customer_phone_number : undefined,
        self_inspect : true,
        vehicle_number : undefined,
        inspection_time : undefined
      };
      $scope.setInspectionType();

    }

    $scope.setInspectionType = function(){
      $scope.inspectionTypes.forEach(function(inspectionType, index){
        if(inspectionType.isDefault){
          $scope.createCaseModel.inspection_type = inspectionType.id;
        }
      });
    };

    $scope.createCase = function(){
      var latlng = GoogleMaps.getLatLong();
      $scope.createCaseModel.latitude = typeof latlng.lat  === "function" ? latlng.lat() :latlng.lat;
      $scope.createCaseModel.longitude = typeof latlng.lng  === "function" ? latlng.lng() : latlng.lng;
      
      if($scope.createCaseModel.inspection_type === "ASSIGN_TO_CUSTOMER")
          $scope.createCaseModel.self_inspect = false;
      else
          $scope.createCaseModel.self_inspect = true;

      console.log($scope.createCaseModel)
      ajaxService.createCaseAgent($scope.createCaseModel, "Creating case. Please wait!")
        .then(function(res){
            alert("Case has been created successfully!");
            $location.url('/scheduled-cases-agent');

        }, function(err){
            alert("Unable to create case!",err);
        });
    };

    angular.element(document).ready(function () {

      $q.all([
          ajaxService.getPurposeOfSurvey(null, "Fetching data. Please wait!"),
          ajaxService.getInspectionTypes()
      ]).then(function(res){
            $scope.poi = res[0].data;
            $scope.inspectionTypes = res[1].data;

            $scope.setInspectionType();
            GoogleMaps.getCurrentLocation(function(){
              GoogleMaps.addMap(null, null, "create-case-map");
              GoogleMaps.addInputField(document.getElementById('pac-input-create-case'));

            },
            function(){
              GoogleMaps.addMap(null, null, "create-case-map");
              GoogleMaps.addInputField(document.getElementById('pac-input-create-case'), function(){
            });

          });
      });
    });
    
    
    $scope.goToCreateCase = function(){
        $location.url('/create-case-agent');
    };
    
    $scope.goToScheduleCase = function(){
        $location.url('/scheduled-cases-agent');
    };
    
    $scope.goToMyCases = function(){
        $location.url('/create-case');
    };
}]);
