jaduApp.run(['$rootScope','$location','$q','ajaxService','$window',function($rootScope, $location, $q, ajaxService, $window) {
  $rootScope.$on('$locationChangeStart', function(event, newUrl, oldUrl){

    var redirect = function(){
      if($rootScope.userProfile.authority.authority === 'ROLE_ADMIN' || $rootScope.userProfile.authority.authority === 'ROLE_COMPANY_USER'){
        $location.path('/dashboard');
      }
      else if($rootScope.userProfile.authority.authority === 'ROLE_QC'){
        $location.url('/create-case')
      }else if($rootScope.userProfile.authority.authority === 'ROLE_AGENT'){
        $location.url('/agent-home')
      }
    }

    var fetchUserProfile = function(){
      $q.all([
        ajaxService.getProfileDetails({},'Loading...'),
        ajaxService.getConfigurations({},'Loading...')
      ])
      .then(function(res){
        $rootScope.userProfile = res[0].data;
        $rootScope.userConfigurations = res[1].data;
        $rootScope.enable_inspector = res[1].data['ENABLE_INSPECTOR'];

        while($rootScope.userProfile.profilePhotoRelativeUrl.charAt(0) === '/')
        {
         $rootScope.userProfile.profilePhotoRelativeUrl = $rootScope.userProfile.profilePhotoRelativeUrl.substr(1);
        }

        window.localStorage.setItem('userProfile', JSON.stringify(res[0].data));
        window.localStorage.setItem('userConfigurations', JSON.stringify(res[1].data));

        redirect();//redirect according to role
      })
      .catch(function(e){
        console.error('$locationChangeStart profile fetch err',e);
      })
    };

    var userProfile = JSON.parse(localStorage.getItem('userProfile'));
    var userConfigurations = JSON.parse(localStorage.getItem('userConfigurations'));
    if(userProfile == undefined || userConfigurations == undefined){
      fetchUserProfile();
    }
    else{ //userProfile exists
      $rootScope.userProfile = userProfile;
      $rootScope.userConfigurations = userConfigurations;
      $rootScope.enable_inspector = userConfigurations['ENABLE_INSPECTOR'];
      if(oldUrl.indexOf('#') === -1 || $location.url() === "/"){ //if coming out of SPA, from login or register
        console.log('COMING FROM LOGIN fetch profile called');
        fetchUserProfile(); //overwrite profile
      }
    }
  });

}]);
