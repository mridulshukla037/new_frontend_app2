<%
  String userAgent = request.getHeader("user-agent");
  if (userAgent.matches(".*Android.*")){
    response.sendRedirect("https://play.google.com/store/apps/details?id=com.proj.wimwicustomer");
  }
  else{
    //out.print("Detected OS is not Android: " + request.getAttribute("caseId")); // iOS
    response.sendRedirect("https://radiant-refuge-79509.herokuapp.com/?case_id=" + request.getAttribute("caseId"));
  }
%>