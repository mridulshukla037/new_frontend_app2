<%-- 
    Document   : images
    Created on : 4 Jan, 2019, 12:06:57 AM
    Author     : gautam
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inspection photos</title>
        <!-- Bootstrap stylesheet min version -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<!-- Mouldifi core stylesheet -->
<link href="../css/mouldifi-core.css" rel="stylesheet">
<!-- /mouldifi core stylesheet -->

<link href="../css/mouldifi-forms.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped">
                        <tr>
                            <th>Inspection case</th><td>${ic.getId()}</td>
                        </tr><tr>
                            <th>Vehicle Number:</th><td>${ic.getVehicleNumber()}</td>
                        </tr>

                    </table>
                </div>
                <div class="col-xs-12">
                    <c:forEach items="${images}" var="image">
                        <div class="col-xs-6">
                             <img src="../util/case-image/${ic.getId()}/${image}"/>
                        </div>
                       
                    </c:forEach>
                </div>
            </div>
        </div>
                    
    </body>
</html>
