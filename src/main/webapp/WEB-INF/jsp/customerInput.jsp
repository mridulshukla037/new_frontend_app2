<html ng-app="customerApp">
  <head>
    <%-- <base href="/" /> --%>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="../css/customer.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.16/jquery.datetimepicker.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/angular-material/1.1.5/angular-material.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/angular-moment-picker/0.10.2/angular-moment-picker.css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfKsJKmaMO0xxO2zdcTHAFmNzarQkUSSM&libraries=places"
    async defer></script>

  </head>
  <body ng-controller="CustomerAddressController" ng-cloak>
    <md-toolbar class="md-hue-2">
      <div class="md-toolbar-tools">
        <h2 flex md-truncate>Welcome</h2>
        <md-button aria-label="Go Back" ng-show="screen=='datetime'" ng-click="screenToAddress()">
          <i class="material-icons">
            keyboard_backspace
          </i>
        </md-button>
      </div>
    </md-toolbar>
    <div ng-if="screen=='homeScreen'" ng-cloak>
      <div class="logo-container" layout="row" layout-align="center center">
        <div flex-gt-sm="50" flex="70">
          <img class="logo" src="../images/logo.png" />
        </div>
      </div>
      <div style="margin-top:100px" layout="row" layout-align="center center">
        <span class="md-headline">Customer Address App</span>
      </div>
      <div style="margin-top:50px" layout="row" layout-align="center center">
        <md-button class="md-raised md-primary" ng-click="enterApp()">Enter</md-button>
      </div>
    </div>
    <div ng-show="screen=='address'" ng-cloak>
      <div layout="row" layout-align="center center">
        <div flex="100">
          <md-input-container style="width:100%" class="no-md-errors-spacer no-margin-bottom">
            <label>Enter Inspection Address</label>
            <input type="text" id="customer-address-input" ng-model="customer.address"  placeholder="">
          </md-input-container>
        </div>
      </div>
      <div layout="row" layout-align="center center">
        <div flex="100">
          <div id="customer-address-map" style="min-height:75%;" ></div>
        </div>
      </div>
      <div layout="row" layout-align="center center">
        <md-button class="md-raised md-primary" ng-click="next()">Next</md-button>
      </div>
    </div>

    <div ng-show="screen=='datetime'" ng-cloak>
      <form name="datetimeForm" ng-submit="submit()" >
        <div layout="row" layout-align="center center" style="margin-top:10px">
          <div flex="70">
            <md-input-container style="width:100%" class="no-md-errors-spacer no-margin-bottom">
              <label>Enter Inspection Time</label>
              <input ng-model-options="{ updateOn: 'blur' }" moment-picker="customer.datetime" name="datetime" ng-model="ctrl.input" required>
              <div ng-messages="datetimeForm.datetime.$error">
                <div ng-message="required">This is required.</div>
              </div>
            </md-input-container>
          </div>
          </div>
          <div layout="row" layout-align="center center">
            <md-button class="md-raised md-primary" type="submit" >Submit</md-button>
          </div>
      </form>
    </div>

    <div ng-show="screen=='thankyou'" ng-cloak>
      <div layout="row" layout-align="center center" style="margin-top:10px">
        <div flex="70">
          <div class="md-title">
            Thank you
          </div>
        </div>
      </div>
      <div layout="row" layout-align="center center">
        <div flex="70">
          <div class="md-subhead">
            We will get back to you soon.
          </div>
        </div>
      </div>
    </div>

    <div ng-show="loading" style="position:absolute;top:0;right:0;left:0;bottom:0;width:100%;height:100%;background:rgba(0,0,0,0.9);">
      <div layout="row" layout-sm="column" layout-align="space-around" style="margin-top:60%">
        <md-progress-circular md-mode="indeterminate"></md-progress-circular>
      </div>
    </div>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.8/angular.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.8/angular-animate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.8/angular-route.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.8/angular-cookies.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.8/angular-resource.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.8/angular-aria.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-messages/1.4.8/angular-messages.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-moment-picker/0.10.2/angular-moment-picker.js"></script>

    <%-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfKsJKmaMO0xxO2zdcTHAFmNzarQkUSSM&callback=initMap&libraries=places"
    async defer></script> --%>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.16/jquery.datetimepicker.full.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-material/1.1.5/angular-material.js" integrity="sha256-T0Q5uw9VcIi0sXhV9kBtu/poum1MS8JZCqEzjqgWRng=" crossorigin="anonymous"></script>
    <script src="../js/customer.js"></script>
  </body>
</html>
