package com.jadu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.jadu.dao.CaseDAO;
import com.jadu.dao.CaseLogDAO;
import com.jadu.dao.CasePhotoDAO;
import com.jadu.dao.PhotoTypeDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dao.VehicleDAO;
import com.jadu.dao.VehicleFuelTypeDAO;
import com.jadu.dao.VehicleTypeDAO;
import com.jadu.push.notification.AndroidPushNotificationsService;

@Service
public class PruneCaseService {

	@Autowired
	S3DirectUploadService s3DirectUploadService;

	@Autowired
	S3Service s3Service;

	@Autowired
	CaseDAO caseDAO;

	@Autowired
	Environment env;

	@Autowired
	VehicleDAO vehicleDAO;

	@Autowired
	PhotoTypeDAO photoTypeDAO;

	@Autowired
	VehicleFuelTypeDAO vehicleFuelTypeDAO;

	@Autowired
	VehicleTypeDAO vehicleTypeDAO;

	@Autowired
	AndroidPushNotificationsService androidPushNotificationsService;

	@Autowired
	CaseLogDAO caseLogDAO;

	@Autowired
	UserDAOImpl userDAO;

	@Autowired
	CasePhotoDAO casePhotoDAO;

	@Autowired
	SmsService smsService;

	@Autowired
	URLShortnerService urlShortnerService;

	@Autowired
	private AppConfigService appConfigService;

	public void persistInInspectionCasesHistory() {
		// TODO Auto-generated method stub

	}
}
