package com.jadu.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.CaseDAO;
import com.jadu.dto.AllCases;
import com.jadu.dto.ScheduledCasesDTO;
import com.jadu.model.CompanyBranchDivisionUser;
import com.jadu.model.User;

@Service
public class RedisReconcileService {

	@Autowired
	private CaseDAO caseDAO;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private RedisService redisService;

	@Autowired
	private UtilService utilService;

	@Autowired
	private AgentDAOImpl agentDAO;

	private static final Logger LOGGER = LoggerFactory.getLogger(RedisReconcileService.class);

	public void reconcileCasesForByStageAndByUser(User currentUser, String caseStage) {
		if (currentUser != null) {
			if (caseStage == null || caseStage.isEmpty()) {
				LOGGER.info("Reconcile all cases for user={} started at={}", currentUser.getPhoneNumber(), new Date());
				reconcileAllCasesForUser(currentUser);
				LOGGER.info("Reconcile all cases for user={} finished at={}", currentUser.getPhoneNumber(), new Date());

			} else if (caseStage != null && caseStage.equalsIgnoreCase("all")) {
				LOGGER.info("Reconcile cases for user={} started at={}", currentUser.getPhoneNumber(), new Date());
				reconcileAllCasesForUser(currentUser);
				reconcileClosedCasesForUser(currentUser);
				reconcileCompleteCasesForUser(currentUser);
				reconcileScheduledCasesForUser(currentUser);
				LOGGER.info("Reconcile cases for user={} finished at={}", currentUser.getPhoneNumber(), new Date());

			} else if (caseStage.equals("-1")) {
				LOGGER.info("Reconcile closed cases for user={} started at={}", currentUser.getPhoneNumber(),
						new Date());
				reconcileClosedCasesForUser(currentUser);
				LOGGER.info("Reconcile closed cases for user={} finished at={}", currentUser.getPhoneNumber(),
						new Date());

			} else if (caseStage.equals("5")) {
				LOGGER.info("Reconcile complete cases for user={} started at={}", currentUser.getPhoneNumber(),
						new Date());
				reconcileCompleteCasesForUser(currentUser);
				LOGGER.info("Reconcile complete cases for user={} finished at={}", currentUser.getPhoneNumber(),
						new Date());

			} else if (caseStage.equals("3")) {
				LOGGER.info("Reconcile scheduled cases for user={} started at={}", currentUser.getPhoneNumber(),
						new Date());
				reconcileScheduledCasesForUser(currentUser);
				LOGGER.info("Reconcile scheduled cases for user={} finished at={}", currentUser.getPhoneNumber(),
						new Date());

			}
		}
	}

	public void reconcileScheduledCasesForUser(User currentUser) {
		List<ScheduledCasesDTO> scheduledCases = caseDAO
				.getScheduledCasesDTO(utilService.getCompanyBranchDivisionUser(currentUser));
		LOGGER.info("all scheduled cases of size={} are being cached for user={}",
				scheduledCases != null ? scheduledCases.size() : 0, currentUser.getPhoneNumber());
		if (scheduledCases != null && !scheduledCases.isEmpty())
			redisService.cacheScheduledCases("SCHEDULED_CASES_FOR_USER_" + currentUser.getUsername(), scheduledCases);
	}

	public void reconcileCompleteCasesForUser(User currentUser) {
		if (appConfigService.getBooleanProperty("IS_TO_RECONCILE_COMPLETED_CASES", false)) {
			List<AllCases> completedCases = caseDAO.getAllCasesByStageDTO(5,
					utilService.getCompanyBranchDivisionUser(currentUser));
			LOGGER.info("all complete cases of size={} are being cached for user={}",
					completedCases != null ? completedCases.size() : 0, currentUser.getPhoneNumber());
			if (completedCases != null && !completedCases.isEmpty())
				redisService.cacheAllCases("COMPLETE_CASES_FOR_USER_" + currentUser.getUsername(), completedCases);
		}
	}

	private void reconcileClosedCasesForUser(User currentUser) {
		if (appConfigService.getBooleanProperty("IS_TO_RECONCILE_CLOSED_CASES", false)) {
			List<AllCases> closedCases = caseDAO.getAllCasesByStageDTO(-1,
					utilService.getCompanyBranchDivisionUser(currentUser));
			LOGGER.info("all closed cases of size={} are being cached for user={}",
					closedCases != null ? closedCases.size() : 0, currentUser.getPhoneNumber());
			if (closedCases != null && !closedCases.isEmpty())
				redisService.cacheAllCases("CLOSED_CASES_FOR_USER_" + currentUser.getUsername(), closedCases);
		}
	}

	public void reconcileAllCasesForUser(User currentUser) {
		if (appConfigService.getBooleanProperty("IS_TO_RECONCILE_ALL_CASES", false)) {
			Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = utilService
					.getCompanyBranchDivisionUser(currentUser);
			List<AllCases> allCases = caseDAO.getAllDTO(companyBranchDivisionUsers);
			LOGGER.info("all cases of size={} are being cached for user={}", allCases != null ? allCases.size() : 0,
					currentUser.getPhoneNumber());
			if (allCases != null && !allCases.isEmpty())
				redisService.cacheAllCases("ALL_CASES_FOR_USER_" + currentUser.getUsername(), allCases);
		}
	}

	public List<Object> reconcileAllAgentsForUser(User user, boolean all) {
		List<String> agentPhoneNumbers = agentDAO.getAllAgents(utilService.getCompanyBranchDivisionUser(user));
		return getAgentDetails(user, agentPhoneNumbers, all);

	}

	public List<Object> getAgentDetails(User user, List<String> agentPhoneNumbers, boolean all) {
		List<Object> allAgents = new ArrayList<Object>();
		for (String phoneNumber : agentPhoneNumbers) {
			try {
				allAgents.add(utilService.getAgentByPhoneNumber(phoneNumber, all));
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error("Something went wrong while fetching data for user={}, error={}", user.getPhoneNumber(),
						e.getMessage());
			}
		}
		return allAgents;

	}
}
