package com.jadu.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jadu.dao.LossAssessmentDAOImpl;
import com.jadu.dto.AssessmentSummary;
import com.jadu.dto.ClaimsFinalSubmissionDTO;
import com.jadu.dto.LossAssessmentDTO;
import com.jadu.dto.ReportHeaderInfo;
import com.jadu.model.AccidentDetails;
import com.jadu.model.Agent;
import com.jadu.model.ClaimCase;
import com.jadu.model.DriverDetails;
import com.jadu.model.InsuranceDetails;
import com.jadu.model.LossAssessment;
import com.jadu.model.User;
import com.jadu.model.VehicleDetails;

@Service
public class ClaimsReportDataGeneratorFactory implements Serializable {

	@Autowired
	AppConfigService appConfigService;

	@Autowired
	private LossAssessmentDAOImpl lossAssessmentDAO;

	@Autowired
	private UtilService utilService;

	private static final Logger LOGGER = LoggerFactory.getLogger(ClaimsReportDataGeneratorFactory.class);

	public ClaimsFinalSubmissionDTO prepareData(User user, ClaimCase ic, List<Agent> refAgent) {
		ClaimsFinalSubmissionDTO claimsFinalSubmissionDTO = new ClaimsFinalSubmissionDTO();
		claimsFinalSubmissionDTO.setReportHeaderInfo(prepareReportHeaderInfo(ic));
		//claimsFinalSubmissionDTO.setInsuranceDetails(prepareInsuranceDetails(ic));
//		claimsFinalSubmissionDTO.setVehicleDetails(prepareVehicleDetails(ic));
//		claimsFinalSubmissionDTO.setDriverDetails(prepareDriverDetails(ic));
//		claimsFinalSubmissionDTO.setAccidentDetails(prepareAccidentDetails(ic));
		claimsFinalSubmissionDTO.setLossAssessmentDTO(prepareLossAssessmentDTO(ic));
		claimsFinalSubmissionDTO.setAssessmentSummary(prepareAssessmentSummary(claimsFinalSubmissionDTO, ic));
		claimsFinalSubmissionDTO.setEnclosed(prepareEnclosed(ic));
		return claimsFinalSubmissionDTO;
	}

	private List<String> prepareEnclosed(ClaimCase ic) {
		List<String> enclosed = new ArrayList<String>();
		enclosed.add(appConfigService.getProperty("CLAIM_REPORT_ENCLOSED",
				"1. Digital Photographs 						\n 2. Loss Assessment Fee bill in duplicate.						\n 3. Claim form & Estimate.						\n 4. Copies of the vehicular documents duly verified from original as produced.						\n 5. Repair Invoice.						"));
		return enclosed;
	}

	private ReportHeaderInfo prepareReportHeaderInfo(ClaimCase ic) {
		ReportHeaderInfo reportHeaderInfo = new ReportHeaderInfo();
		reportHeaderInfo.setCompanyHeader(
				appConfigService.getProperty("CLAIM_REPORT_COMPANY_NAME", "Hedgerow Technologies Pvt. Ltd."));
		reportHeaderInfo.setReferenceId(ic.getId());
		reportHeaderInfo.setSubHeaders(prepareSubHeaders(ic));
		return reportHeaderInfo;
	}

	private List<String> prepareSubHeaders(ClaimCase ic) {
		List<String> subHeaders = new ArrayList<String>();
		subHeaders.add(appConfigService.getProperty("CLAIM_REPORT_TITLE", "Claims Services With Value Addition"));
		subHeaders.add(appConfigService.getProperty("CLAIM_REPORT_COMPANY_ADDRESS",
				"WZ 33A, Dayal Sir Road, Uttam Nagar West, New Delhi 110 059"));
		subHeaders.add(appConfigService.getProperty("CLAIM_REPORT_COMPANY_LICENSE_INFO",
				"PAN No. :- AAECH3682G       GSTIN:- 07AAECH3682G1ZJ"));
		subHeaders.add(appConfigService.getProperty("CLAIM_REPORT_COMPANY_CONTACT_INFO",
				"Phone No - 7290049100, Email : info@wimwisure.com"));

		return subHeaders;
	}

	private LossAssessmentDTO prepareLossAssessmentDTO(ClaimCase ic) {
		LossAssessmentDTO lossAssessmentDTO = new LossAssessmentDTO();
		if (ic != null) {
			lossAssessmentDTO.setLeftNotes("1. Assessed for the SPARE PARTS of accident vehicle bearing Regd. No.");
			lossAssessmentDTO.setRightNotes(ic.getVehicleNumber());
			lossAssessmentDTO.setLossAssessment(lossAssessmentDAO.getCaseById(ic.getId()));
			lossAssessmentDTO.setTotalLabourCharge(prepareLabourCharge(ic, lossAssessmentDTO));
			lossAssessmentDTO.setTotalPartsCharges(preparePartCharges(ic, lossAssessmentDTO));
			return lossAssessmentDTO;
		}
		return lossAssessmentDTO;
	}

	private Float preparePartCharges(ClaimCase ic, LossAssessmentDTO lossAssessmentDTO) {
		float totalPartCharges = 0.0f;
		if (lossAssessmentDTO != null) {
			List<LossAssessment> lossAssessments = lossAssessmentDTO.getLossAssessment();
			for (LossAssessment lossAssessment : lossAssessments) {
				totalPartCharges += lossAssessment.getPartCost();
				if (lossAssessment.getGstRateForPart() != 0.0) {
					if (appConfigService.getBooleanProperty("IS_TO_ADD_GST_IN_FINAL_PART_COST", true)) {
						double gstCost = lossAssessment.getPartCost() * lossAssessment.getGstRateForPart();
						totalPartCharges += gstCost;
					}
				}
				if (lossAssessment.getDepRateForPart() != 0.0) {
					if (appConfigService.getBooleanProperty("IS_TO_ADD_DEP_IN_FINAL_PART_COST", true)) {
						double depCost = lossAssessment.getPartCost() * lossAssessment.getDepRateForPart();
						totalPartCharges += depCost;
					}
				}
			}
		}
		LOGGER.info("Total part cost for claim case={} is ={}", ic.getId(), totalPartCharges);
		return totalPartCharges;
	}

	private Float prepareLabourCharge(ClaimCase ic, LossAssessmentDTO lossAssessmentDTO) {
		float totalLabourCharges = 0.0f;
		if (lossAssessmentDTO != null) {
			List<LossAssessment> lossAssessments = lossAssessmentDTO.getLossAssessment();
			for (LossAssessment lossAssessment : lossAssessments) {
				totalLabourCharges += lossAssessment.getLabourCost();
				if (lossAssessment.getGstRateForLabour() != 0.0) {
					if (appConfigService.getBooleanProperty("IS_TO_ADD_GST_IN_FINAL_LABOUR_COST", true)) {
						double gstCost = lossAssessment.getLabourCost() * lossAssessment.getGstRateForLabour();
						totalLabourCharges += gstCost;
					}
				}
				if (lossAssessment.getDepRateForLabour() != 0.0) {
					if (appConfigService.getBooleanProperty("IS_TO_ADD_DEP_IN_FINAL_LABOUR_COST", true)) {
						double depCost = lossAssessment.getLabourCost() * lossAssessment.getDepRateForLabour();
						totalLabourCharges += depCost;
					}
				}
			}

		}
		LOGGER.info("Total labour cost for claim case={} is ={}", ic.getId(), totalLabourCharges);
		return totalLabourCharges;

	}

	private AssessmentSummary prepareAssessmentSummary(ClaimsFinalSubmissionDTO claimsFinalSubmissionDTO,
			ClaimCase ic) {
		if (claimsFinalSubmissionDTO != null && ic != null) {
			AssessmentSummary assessmentSummary = new AssessmentSummary();
			assessmentSummary.setEstimates(prepareEstimates(ic, claimsFinalSubmissionDTO));
			assessmentSummary.setAssessedFor(prepareAssessedFor(ic, claimsFinalSubmissionDTO));
			float totalLabourCharges = claimsFinalSubmissionDTO.getLossAssessmentDTO().getTotalLabourCharge();
			float totalPartCharges = claimsFinalSubmissionDTO.getLossAssessmentDTO().getTotalPartsCharges();
			int totalCost = Math.round(totalLabourCharges + totalPartCharges);
			assessmentSummary.setSummaryNotes("Hence, the Net Loss assessment comes to = INR" + totalCost + " (Rupees "
					+ utilService.convertNumberToWords(totalCost) + ") only.");
			assessmentSummary.setSummaryRemarks(prepareRemarks());
			return assessmentSummary;
		}
		return null;

	}

	private Map<String, Float> prepareAssessedFor(ClaimCase ic, ClaimsFinalSubmissionDTO claimsFinalSubmissionDTO) {
		Map<String, Float> assessedFor = new HashMap<String, Float>();
		assessedFor.put("totalLabourCharges", claimsFinalSubmissionDTO.getLossAssessmentDTO().getTotalLabourCharge());
		assessedFor.put("totalCostOfSpareParts",
				claimsFinalSubmissionDTO.getLossAssessmentDTO().getTotalPartsCharges());
		assessedFor.put("lessExcess", appConfigService.getFloatProperty("LESS_EXCEESS_IN_ASSESSED_FOR", 0.00f));
		assessedFor.put("lessSalvageCost", appConfigService.getFloatProperty("LESS_SALAVAGE_IN_ASSESSED_FOR", 0.00f));
		return assessedFor;
	}

	private Map<String, Float> prepareEstimates(ClaimCase ic, ClaimsFinalSubmissionDTO claimsFinalSubmissionDTO) {
		Map<String, Float> estimates = new HashMap<String, Float>();
		estimates.put("totalLabourCharges", claimsFinalSubmissionDTO.getLossAssessmentDTO().getTotalLabourCharge());
		estimates.put("totalCostOfParts", claimsFinalSubmissionDTO.getLossAssessmentDTO().getTotalPartsCharges());
		estimates.put("supplementaryParts",
				appConfigService.getFloatProperty("SUPPLEMENTRY_PARTS_COST_IN_ESTIMATES", 0.00f));
		estimates.put("supplementaryLabour",
				appConfigService.getFloatProperty("SUPPLEMENTRY_LABOUR_COST_IN_ESTIMATES", 0.00f));
		return estimates;
	}

	private List<String> prepareRemarks() {
		List<String> remarks = new ArrayList<String>();
		remarks.add(
				"The damages as observed were found to be fresh and consistent with the nature of accident as reported.The loss was proximately caused by an insured peril and none of the exclusions under the policy had operated to bring about the loss");
		remarks.add("The Repair Bill/Invoice is attached. This report is issued without  bias and prejudice.");
		return remarks;
	}

//	private AccidentDetails prepareAccidentDetails(ClaimCase ic) {
//		if (ic != null) {
//			return ic.getAccidentDetails();
//		}
//		return null;
//	}
//
//	private DriverDetails prepareDriverDetails(ClaimCase ic) {
//		if (ic != null) {
//			return ic.getDriverDetails();
//		}
//		return null;
//	}
//
//	private VehicleDetails prepareVehicleDetails(ClaimCase ic) {
//		if (ic != null) {
//			return ic.getVehicleDetails();
//		}
//		return null;
//	}

//	private InsuranceDetails prepareInsuranceDetails(ClaimCase ic) {
//		if (ic != null) {
//			return ic.getInsuranceDetails();
//		}
//		return null;
//	}

}
