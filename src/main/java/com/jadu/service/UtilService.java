package com.jadu.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.jadu.dao.CompanyBranchDivisionDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dto.AllUserDTO;
import com.jadu.dto.FilterDTO;
import com.jadu.front.controller.UtilController;
import com.jadu.helpers.FileHelper;
import com.jadu.model.Admin;
import com.jadu.model.Agent;
import com.jadu.model.CompanyBranchDivision;
import com.jadu.model.CompanyBranchDivisionUser;
import com.jadu.model.CompanyUser;
import com.jadu.model.QC;
import com.jadu.model.User;

@Service
public class UtilService {

	@Autowired
	private S3Service s3Service;

	@Autowired
	private RedisService redisService;

	@Autowired
	private UserDAOImpl userDAO;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private CompanyBranchDivisionDAO companyBranchDivisionDAO;

	private static final Logger LOGGER = LoggerFactory.getLogger(UtilService.class);

	public ResponseEntity<byte[]> getCompanyImage(String fileName) throws IOException {
		return getImage("companies/" + fileName, fileName);
	}

	public ResponseEntity<byte[]> getProfileImage(String fileName) throws IOException {
		return getImage("users/profile_photos/" + fileName, fileName);
	}

	public ResponseEntity<byte[]> getCaseImage(String id, long caseId) throws IOException {
		return getImage("cases/" + caseId + "/" + id, id);
	}

	public byte[] getCompanyImageBytes(String fileName) throws IOException {
		return s3Service.readFile("companies/" + fileName);
	}

	public byte[] getProfileImageBytes(String fileName) throws IOException {
		return s3Service.readFile("users/profile_photos/" + fileName);
	}

	public byte[] getCaseImageBytes(String id, long caseId) throws IOException {
		return s3Service.readFile("cases/" + caseId + "/" + id);
	}

	private ResponseEntity<byte[]> getImage(String path, String fileName) throws IOException {
		byte[] media = s3Service.readFile(path);

		if (media == null) {
			LOGGER.info("Image={} at path={} not found!", fileName, path);
			return new ResponseEntity<>("Image not found!".getBytes(), HttpStatus.NOT_FOUND);

		}

		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(FileHelper.getMediaTypeByFileExtension(FilenameUtils.getExtension(fileName)));

		return new ResponseEntity<>(media, headers, HttpStatus.OK);
	}

	public FilterDTO getQuery(String prefix, Set<CompanyBranchDivisionUser> companyBranchDivisionUser) {

		if (companyBranchDivisionUser == null || companyBranchDivisionUser.isEmpty())
			return null;

		List<String> result = new ArrayList<>();
		HashMap<String, Object> map = new HashMap();

		int i = 1;
		for (CompanyBranchDivisionUser item : companyBranchDivisionUser) {
			List<String> conditions = new ArrayList<>();
			if (item.getCompany() != null) {
				String[] companies = item.getCompany().split("\\,");
				if (companies.length == 1) {
					conditions.add(prefix + ".company = :company" + i);
					map.put("company" + i, item.getCompany());
				} else {
					String companyCSV = null;
					for (String company : companies) {
						if (companyCSV == null)
							companyCSV = prefix + ".company = '" + company.trim() + "'".trim();
						else
							companyCSV += " OR " + prefix + ".branch = '" + company.trim() + "'".trim();
					}
					conditions.add("( " + companyCSV + " )");

				}
			}

			if (item.getState() != null) {
				conditions.add(prefix + ".state = :state" + i);
				map.put("state" + i, item.getState());
			}

			if (item.getDivision() != null) {
				conditions.add(prefix + ".division = :division" + i);
				map.put("division" + i, item.getDivision());
			}

			if (item.getBranch() != null) {
				String[] branches = item.getBranch().split("\\,");
				if (branches.length == 1) {
					conditions.add(prefix + ".branch = :branch" + i);
					map.put("branch" + i, item.getBranch());
				} else {
					String branchCSV = null;
					for (String branch : branches) {
						if (branchCSV == null)
							branchCSV = prefix + ".branch = '" + branch.trim() + "'".trim();
						else
							branchCSV += " OR " + prefix + ".branch = '" + branch.trim() + "'".trim();
					}
					conditions.add("( " + branchCSV + " )");

				}
			}

			i++;

			if (!conditions.isEmpty())
				result.add(" ( " + String.join(" AND ", conditions) + " ) ");
		}

		if (result.isEmpty())
			return null;

		FilterDTO filterDTO = new FilterDTO();
		filterDTO.setMap(map);
		filterDTO.setWhereClause(" ( " + String.join(" OR ", result) + " ) ");
		System.out.println(filterDTO.getWhereClause());
		return filterDTO;
	}

	public Query createQuery(Query createQuery, FilterDTO filterDTO) {
		if (filterDTO != null) {
			for (Map.Entry<String, Object> entry : filterDTO.getMap().entrySet()) {
				String[] csv = String.valueOf(entry.getValue()).split("\\,");
				if (csv.length > 1)
					createQuery.setParameterList(entry.getKey(), Arrays.asList(csv));
				else
					createQuery.setParameter(entry.getKey(), entry.getValue());

			}
		}

		return createQuery;
	}

	public Object getAgentByPhoneNumber(String phoneNumber, boolean isfresh) throws Exception {
		List<AllUserDTO> agentDetails = new ArrayList<AllUserDTO>();
		AllUserDTO allUserDetails = redisService.findAndCacheAllUserDetails(phoneNumber);
                agentDetails = new ArrayList<>();
                agentDetails.add(allUserDetails);
		if (agentDetails.isEmpty())
			throw new Exception("No agent exists for the given phone number!");

		if (agentDetails.size() > 1)
			throw new Exception("Multiple agents exists for given phone number!");

		return agentDetails.get(0);
	}

	public Set<CompanyBranchDivisionUser> getCompanyBranchDivisionUser(com.jadu.model.User currentUser) {
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = new HashSet<CompanyBranchDivisionUser>();
		if (currentUser != null) {
			if (currentUser instanceof Admin) {
				companyBranchDivisionUsers = ((Admin) currentUser).getCompanyBranchDivisionUser();
			}

			if (currentUser instanceof CompanyUser) {
				companyBranchDivisionUsers = ((CompanyUser) currentUser).getCompanyBranchDivisionUser();
			}
			if (currentUser instanceof QC) {
				companyBranchDivisionUsers = ((QC) currentUser).getCompanyBranchDivisionUser();
			}
		}
		return companyBranchDivisionUsers;
	}

	public AllUserDTO prepareAllUserDTOAndCache(Agent agent, String phoneNumber) {
		if (agent != null) {
			AllUserDTO allUserDTO = new AllUserDTO();
			allUserDTO.setUsername(agent.getUsername());
			allUserDTO.setCanUpdate(agent.isCanUpdate());
			allUserDTO.setDeviceId(agent.getDeviceId());
			allUserDTO.setDeviceLocked(agent.isDeviceLocked());
			allUserDTO.setEmail(agent.getEmail());
			allUserDTO.setEmailVerified(agent.isEmailVerified());
			allUserDTO.setEnabled(agent.isEnabled());
			allUserDTO.setFirstName(agent.getFirstName());
			allUserDTO.setLastName(agent.getLastName());
			allUserDTO.setPasswordChangeRequired(agent.isPasswordChangeRequired());
			allUserDTO.setPhoneNumber(agent.getPhoneNumber());
			allUserDTO.setPhoneNumberVerified(agent.isPhoneNumberVerified());
			allUserDTO.setProfilePhotoUrl(agent.getProfilePhotoUrl());
			allUserDTO.setProfilePhotoUrlThumb(agent.getProfilePhotoUrlThumb());
			allUserDTO.setProfilePhotoRelativeUrl(agent.getProfilePhotoRelativeUrl());
			allUserDTO.setBranchName(agent.getAgentDetails().getCompanyBranchDivision().getBranch());
			allUserDTO.setAgentCode(agent.getAgentDetails().getAgentId());
			allUserDTO.setAgentCompany(agent.getAgentDetails().getCompanyBranchDivision().getCompany());
//			if (appConfigService.getBooleanProperty("IS_TO_PUSH_AGENTS_IN_CACHE", true) && phoneNumber != null
//					&& !phoneNumber.isEmpty()) {
//				redisService.cacheAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + phoneNumber, allUserDTO);
//			}
			return allUserDTO;
		}
		return null;
	}

	public Set<String> fetchBranches(User user) {
		Set<String> branchList = new HashSet<String>();
		if (user != null) {
			List<CompanyBranchDivision> CompanyBranchDivisionList = fetchCompanyBranchDivisionList(user);
			for (CompanyBranchDivision companyBranchDivision : CompanyBranchDivisionList) {
				if (companyBranchDivision != null)
					branchList.add(companyBranchDivision.getBranch());
			}
			LOGGER.info("Total branch size for user={} are ={}", branchList != null ? branchList.size() : 0,
					user.getPhoneNumber());
		}

		return branchList;

	}

	public List<CompanyBranchDivision> fetchCompanyBranchDivisionList(User user) {
		List<CompanyBranchDivision> list = new ArrayList<CompanyBranchDivision>();
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = getCompanyBranchDivisionUser(user);
		for (CompanyBranchDivisionUser companyBranchDivisionUser : companyBranchDivisionUsers) {
			if (companyBranchDivisionUser != null) {
				String company = companyBranchDivisionUser.getCompany();
				if (company != null && !company.isEmpty()) {
					list.addAll(companyBranchDivisionDAO.getCompanyBranchDivisionByCompany(company));
					return list;
				} else {
					list.addAll(companyBranchDivisionDAO.findAll());
				}
			}
		}
		LOGGER.info("Total CompanyBranchDivisionUser size for user={} are ={}", user.getPhoneNumber(),
				list != null ? list.size() : 0);
		return list;

	}

	public User getUser(Authentication authentication) {
		if (authentication != null) {
			return userDAO.getUserByUsername(
					((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		}
		return null;
	}

	public String convertNumberToWords(int number) {

		if (number == 0) {
			return "zero";
		}

		String prefix = "";

		if (number < 0) {
			number = -number;
			prefix = "negative";
		}

		String current = "";
		int place = 0;

		do {
			int n = number % 1000;
			if (n != 0) {
				String s = convertLessThanOneThousand(n);
				current = s + specialNames[place] + current;
			}
			place++;
			number /= 1000;
		} while (number > 0);

		return (prefix + current).trim();

	}

	private static final String[] specialNames = { "", " thousand", " million", " billion", " trillion", " quadrillion",
			" quintillion" };

	private static final String[] tensNames = { "", " ten", " twenty", " thirty", " forty", " fifty", " sixty",
			" seventy", " eighty", " ninety" };

	private static final String[] numNames = { "", " one", " two", " three", " four", " five", " six", " seven",
			" eight", " nine", " ten", " eleven", " twelve", " thirteen", " fourteen", " fifteen", " sixteen",
			" seventeen", " eighteen", " nineteen" };

	private String convertLessThanOneThousand(int number) {
		String current;

		if (number % 100 < 20) {
			current = numNames[number % 100];
			number /= 100;
		} else {
			current = numNames[number % 10];
			number /= 10;

			current = tensNames[number % 10] + current;
			number /= 10;
		}
		if (number == 0)
			return current;
		return numNames[number] + " hundred" + current;
	}

}
