package com.jadu.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.jadu.dao.CaseDAO;
import com.jadu.model.InspectionCase;

@Service
public class ReconcileService {

	@Autowired
	private CaseDAO caseDAO;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	ConnectCustomerToFlow connectCustomerToFlow;

	@Autowired
	private RedisService redisService;

	private static final Logger LOGGER = LoggerFactory.getLogger(ReconcileService.class);

	@Async
	public void remindCaseInspection(boolean lastOneDay, boolean self_inspection) {
		Integer perBatchCallThreshold = appConfigService.getIntProperty("MAX_CALL_PER_BATCH", 5);
		int jobCount = 0;
		List<InspectionCase> scheduledCases = null;
		if (lastOneDay) {
			Date last1Day = getThresholdDate(
					appConfigService.getIntProperty("THRESHOLD_DAYS_FOR_ASSIGN_TO_CUSTOMER_CALL", -1));
			if (self_inspection) {
				scheduledCases = caseDAO.getCaseByTypeAndStageAndCreatedTime(3, "SELF_INSPECT", last1Day);
			} else {
				scheduledCases = caseDAO.getCaseByTypeAndStageAndCreatedTime(3, "ASSIGN_TO_CUSTOMER", last1Day);
			}
		} else if (appConfigService.getBooleanProperty("INITIATE_SYSTEM_CALL_FOR_ALL_SCHEDULED_CASE", false)) {
			if (self_inspection) {
				scheduledCases = caseDAO.getCaseByTypeAndStage(3, "SELF_INSPECT");
			} else {
				scheduledCases = caseDAO.getCaseByTypeAndStage(3, "ASSIGN_TO_CUSTOMER");
			}
		} else {
			Date sinceDays = getThresholdDate(
					appConfigService.getIntProperty("THRESHOLD_DAYS_FOR_SYSTEM_CALL_FOR_SCHEDULED_CASE", -2));
			if (self_inspection) {
				scheduledCases = caseDAO.getCaseByTypeAndStageAndCreatedTime(3, "SELF_INSPECT", sinceDays);
			} else {
				scheduledCases = caseDAO.getCaseByTypeAndStageAndCreatedTime(3, "ASSIGN_TO_CUSTOMER", sinceDays);
			}
		}
		Date thresholdDate = getThresholdDate(
				appConfigService.getIntProperty("THRESHOLD_CASES_DAYS_FOR_AUTOMATED_SYSTEM_CALL", -7));
		for (final InspectionCase scheduledCase : scheduledCases) {
			try {
				if (scheduledCase.getInspectionTime().after(thresholdDate)) {
					if (checkCallCountCriteria(scheduledCase)) {
						if (checkCallDifferenceCriteriaBetweenTwoCall(scheduledCase)) {
							Runnable runnable = new Runnable() {
								public void run() {
									if (appConfigService.getBooleanProperty("IS_TO_RUN_SYSTEM_AUTOMATED_CALL", true))
										connectCustomerToFlow.reminderCallForScheduledCase(
												scheduledCase.getCustomerPhoneNumber(), scheduledCase);
								}
							};

							Thread thread = new Thread(runnable);
							thread.start();
							jobCount++;
							if (!(jobCount <= perBatchCallThreshold)) {
								try {
									// wait for 5 minutes
									thread.sleep(appConfigService.getIntProperty("SYSTEM_CALL_SLEEP_TIME", 300000));
									jobCount = 0;
								} catch (InterruptedException e) {
									e.printStackTrace();
									throw new IllegalStateException(e);
								}
							}
						} else {
							LOGGER.info(
									"case={} has yet not crossed {} hours of time, so automated system call is being skipped",
									scheduledCase,
									appConfigService.getLongProperty("LAST_CALL_TIME_THRESHOLD_IN_HOURS", 24L));
						}
					} else {
						LOGGER.info(
								"case={} has been already followed up required number of time={}, so automated system call is being skipped",
								scheduledCase, appConfigService.getIntProperty("MAX_FOLLOW_UP_FOR_SCHEDULED_CASES", 4));
					}
				} else {
					LOGGER.info("case={} is older than {}, so automated system call is being skipped", scheduledCase,
							Math.abs(
									appConfigService.getIntProperty("THRESHOLD_DAYS_FOR_ASSIGN_TO_CUSTOMER_CALL", -7)));
				}

			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error("Exception raised while system call for case={}, error={}", scheduledCase.getId(),
						e.getMessage());
			}
		}
		System.gc();
	}

	private boolean checkCallDifferenceCriteriaBetweenTwoCall(InspectionCase scheduledCase) {
		String lastCallTime = redisService.get("LAST_CALL_TIME_FOR_SCHEDULED_CASE_" + scheduledCase.getId());
		Date lastCallDate = lastCallTime != null ? new Date(Long.valueOf(lastCallTime)) : null;
		return lastCallDate == null || getLastCallTimeDifference(lastCallDate) >= appConfigService
				.getLongProperty("LAST_CALL_TIME_THRESHOLD_IN_HOURS", 24L);
	}

	private Long getLastCallTimeDifference(Date lastCallDate) {
		long timediff = lastCallDate != null ? ((new Date().getTime() - lastCallDate.getTime()) / (1000 * 60 * 60))
				: 24;
		return timediff;

	}

	private boolean checkCallCountCriteria(InspectionCase scheduledCase) {
		int count = getCallCount(scheduledCase);
		return count <= appConfigService.getIntProperty("MAX_FOLLOW_UP_FOR_SCHEDULED_CASES", 4);
	}

	private int getCallCount(InspectionCase scheduledCase) {
		String followUpCount = redisService.get("FOLLOW_UP_COUNTER_FOR_SCHEDULED_CASE_" + scheduledCase.getId());
		return followUpCount != null ? Integer.parseInt(followUpCount) : 0;
	}

	private Date getThresholdDate(int days) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, days);
		Date thresholdDate = cal.getTime();
		return thresholdDate;
	}
}
