package com.jadu.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.jadu.dao.OTPDAO;
import com.jadu.helpers.TimeHelper;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.OTP;
import com.jadu.model.User;

@Service
public class SmsService {

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	OTPDAO otpDAO;

	@Async
	public void sendSMSAsync(String mobileNumber, String message) {
		try {
			sendSMS(mobileNumber, message);
		} catch (IOException ex) {
			Logger.getLogger(SmsService.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void sendSMS(String mobileNumber, String message) throws MalformedURLException, IOException {
		String url = appConfigService.getProperty("SMS_SERVICE_URL");
		url += "?APIKEY=" + java.net.URLEncoder.encode(appConfigService.getProperty("SMS_SERVICE_API_KEY"), "UTF-8");
		url += "&ServiceName=TEMPLATE_BASED";
		url += "&SenderID=" + java.net.URLEncoder.encode(appConfigService.getProperty("SMS_SERVICE_ID"), "UTF-8");
		url += "&MobileNo=" + java.net.URLEncoder.encode(mobileNumber, "UTF-8");
		url += "&Message=" + java.net.URLEncoder.encode(message, "UTF-8");

		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		int responseCode = con.getResponseCode();

		if (responseCode != 200) {
			StringBuilder response;
			try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
				String inputLine;
				response = new StringBuilder();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
			}

			throw new IOException("Unable to send sms!");
		}
	}

	@Async
	public void sendRegistrationOTP(User user) {
		int validity = 300;
		OTP otp = otpDAO.generatePhoneOtp(user.getUsername(), UtilHelper.getDateAddMinutes(validity), "REGISTER");

		try {

			this.sendSMS(user.getPhoneNumber(), "Your WIMWISure OTP - " + otp.getOtp() + ". Valid for "
					+ TimeHelper.getTimeString(validity) + ".");
		} catch (Exception ex) {
			Logger.getLogger(SmsService.class.getName()).log(Level.SEVERE, null, ex);
			otp.setSuccess(false);
			otp.setErrorLog(ex.getMessage());
			otpDAO.save(otp);
		}
	}

	@Async
	public void sendRegistrationPassword(User user, String password) {
		try {
			this.sendSMS(user.getPhoneNumber(),
					"Your WIMWISure username is " + user.getPhoneNumber() + " and password is " + password);
		} catch (IOException ex) {
			Logger.getLogger(SmsService.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public boolean validateRegistrationOTP(User user, String otpVal) throws Exception {
		return validateRegistrationOTP(user, otpVal, 10);
	}

	public boolean validateRegistrationOTP(User user, String otpVal, int minutes) throws Exception {
		OTP otp = (OTP) otpDAO.getPhoneOtp(user.getUsername(), otpVal);

		if (otp == null || !otp.getOtp().equals(otpVal)) {
			throw new Exception("Incorrect OTP");
		}

		if (UtilHelper.getDate().getTime() - otp.getCreationTime().getTime() >= minutes * 60 * 1000) {
			throw new Exception("OTP expired!");
		}

		return true;
	}
}