/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.service;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.jadu.helpers.FileHelper;

@Service
public class S3DirectUploadService {
	@Autowired
	private AppConfigService appConfigService;

	private final static String keyId = "S3_DIRECT_UPLOAD_ACCESS_KEY";
	private final static String secretKeyId = "S3_DIRECT_UPLOAD_SECRET_KEY";
	private final static String bucketId = "S3_DIRECT_UPLOAD_BUCKET";

	public void uploadMultipartImage(MultipartFile multipartFile, String uploadFullPath, /*
																							 * Directory concatenated
																							 * with file name
																							 */
			String uploadThumbFullPath) throws Exception {
		File serverFile = FileHelper.convert(multipartFile);

		BufferedImage resizeMe = ImageIO.read(serverFile);

		String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());

		// resizeMe.getWidth();
		Dimension newMaxSize = new Dimension(255, 255);
		BufferedImage resizedImg = Scalr.resize(resizeMe, Scalr.Method.QUALITY, newMaxSize.width, newMaxSize.height);
		File file = File.createTempFile(Long.toString(System.currentTimeMillis()), "." + extension);
		ImageIO.write(resizedImg, extension, file);
		try {
			this.uploadFile(serverFile, uploadFullPath);
			this.uploadFile(file, uploadThumbFullPath);
		} finally {
			if (serverFile != null)
				serverFile.delete();
		}

	}

	public void uploadMultipartImage(MultipartFile multipartFile, String uploadFullPath) throws Exception {
		File serverFile = FileHelper.convert(multipartFile);

		try {
			this.uploadFile(serverFile, uploadFullPath);
		} finally {
			if (serverFile != null)
				serverFile.delete();
		}

	}

	public void uploadMultipartFile(MultipartFile multipartFile,
			String uploadFullPath /* Directory concatenated with file name */
	) throws Exception {
		File serverFile = FileHelper.convert(multipartFile);

		try {
			this.uploadFile(serverFile, uploadFullPath);
		} finally {
			if (serverFile != null)
				serverFile.delete();
		}

	}

	public void uploadFile(File file, String uploadFullPath /* Directory concatenated with file name */
	) throws Exception {

		AmazonS3 s3client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
				appConfigService.getProperty(secretKeyId)));
		try {
			PutObjectRequest putObjectRequest = new PutObjectRequest(appConfigService.getProperty(bucketId),
					uploadFullPath, file);
			putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead); // public for all
			s3client.putObject(putObjectRequest); // upload file

		} catch (AmazonServiceException ase) {
			/*
			 * Caught an AmazonServiceException, which means your request made it to Amazon
			 * S3, but was rejected with an error response for some reason.
			 */

			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());

			throw new Exception("Unable to upload file!");
		} catch (AmazonClientException ace) {
			/*
			 * Caught an AmazonClientException, which means the client encountered an
			 * internal error while trying to communicate with S3, such as not being able to
			 * access the network.
			 */

			System.out.println("Error Message: " + ace.getMessage());

			throw new Exception("Unable to upload file!");
		}
	}

	public byte[] readFile(String fileFullPath) {
		try {
			byte[] result;
			try (InputStream objectData = readFileStream(fileFullPath)) {
				result = IOUtils.toByteArray(objectData);
			}

			return result;
		} catch (IOException ex) {
			Logger.getLogger(S3Service.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
	}

	public InputStream readFileStream(String fileFullPath) {
		AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
				appConfigService.getProperty(secretKeyId)));

		S3Object object = s3Client
				.getObject(new GetObjectRequest(appConfigService.getProperty(bucketId), fileFullPath));

		return object.getObjectContent();
	}

	public List<String> getObjectslistFromFolder() {

		AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
				appConfigService.getProperty(secretKeyId)));

		ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
				.withBucketName(appConfigService.getProperty(bucketId));

		List<String> keys = new ArrayList<>();

		ObjectListing objects = s3Client.listObjects(listObjectsRequest);

		for (S3ObjectSummary summary : objects.getObjectSummaries()) {
			keys.add(summary.getKey());
		}

		return keys;
	}

	public boolean exists(String fileFullPath) {
		AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(appConfigService.getProperty(keyId),
				appConfigService.getProperty(secretKeyId)));

		try {
			S3Object object = s3Client
					.getObject(new GetObjectRequest(appConfigService.getProperty(bucketId), fileFullPath));
			object.getObjectMetadata();
		} catch (AmazonServiceException e) {
			return false;
		}
		return true;
	}

}
