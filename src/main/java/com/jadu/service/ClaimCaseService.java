package com.jadu.service;

import com.amazonaws.services.s3.model.ObjectMetadata;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jadu.dao.AccidentDetailsDAOImpl;
import com.jadu.dao.AssessmentSummaryDAO;
import com.jadu.dao.CaseLogDAO;
import com.jadu.dao.CasePhotoDAO;
import com.jadu.dao.ClaimCaseDAO;
import com.jadu.dao.ClaimCasePhotoDAO;
import com.jadu.dao.ClaimsPhotoTypeDAO;
import com.jadu.dao.DriverDetailsDAOImpl;
import com.jadu.dao.InsuranceDetailsDAOImpl;
import com.jadu.dao.LossAssessmentDAOImpl;
import com.jadu.dao.LossAssessmentPhotosDAO;
import com.jadu.dao.PhotoTypeDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dao.VehicleDAO;
import com.jadu.dao.VehicleDetailsDAOImpl;
import com.jadu.dao.VehicleFuelTypeDAO;
import com.jadu.dao.VehicleTypeDAO;
import com.jadu.helpers.FileMimeType;
import com.jadu.model.ClaimCase;
import com.jadu.model.ClaimCasePhoto;
import com.jadu.model.LossAssessment;
import com.jadu.model.LossAssessmentPhotos;
import com.jadu.push.notification.AndroidPushNotificationsService;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.FilenameUtils;

@Service
public class ClaimCaseService {

    @Autowired
    S3DirectUploadService s3DirectUploadService;

    @Autowired
    S3Service s3Service;

    @Autowired
    private ClaimCaseDAO claimCaseDAO;

    @Autowired
    Environment env;

    @Autowired
    VehicleDAO vehicleDAO;

    @Autowired
    PhotoTypeDAO photoTypeDAO;

    @Autowired
    VehicleFuelTypeDAO vehicleFuelTypeDAO;

    @Autowired
    VehicleTypeDAO vehicleTypeDAO;

    @Autowired
    AndroidPushNotificationsService androidPushNotificationsService;

    @Autowired
    CaseLogDAO caseLogDAO;

    @Autowired
    UserDAOImpl userDAO;

    @Autowired
    CasePhotoDAO casePhotoDAO;

    @Autowired
    SmsService smsService;

    @Autowired
    URLShortnerService urlShortnerService;

    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    private AccidentDetailsDAOImpl accidentDetailsDAO;

    @Autowired
    private VehicleDetailsDAOImpl vehicleDetailsDAO;

    @Autowired
    private DriverDetailsDAOImpl driverDetailsDAO;

    @Autowired
    private InsuranceDetailsDAOImpl insuranceDetailsDAO;

    @Autowired
    private LossAssessmentDAOImpl lossAssessmentDAO;

    @Autowired
    private ClaimCasePhotoDAO claimCasePhotoDAO;

    @Autowired
    private LossAssessmentPhotosDAO lossAssessmentPhotosDAO;

    @Autowired
    private AssessmentSummaryDAO assessmentSummaryDAO;

    @Autowired
    private ClaimsPhotoTypeDAO claimsPhotoTypeDAO;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ClaimCaseService.class);

    @Async
    public void uploadZipToS3Async(ClaimCase claimCase) {
        try {
            uploadZipToS3(claimCase, "cases/" + claimCase.getId() + "/" + claimCase.getId() + ".zip");
        } catch (Exception ex) {
            Logger.getLogger(ClaimCaseService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Async
    public void loadDetailsFromFile(ClaimCase claimCase){
        try {
            String rootFolder = "cases/" + claimCase.getId() + "/";
            unzipFile(claimCase);
            InputStream item = s3Service.readClaimZipFileStream(rootFolder + claimCase.getId() + ".json");
            StringWriter writer = new StringWriter();
            IOUtils.copy(item, writer);
            ClaimCase claimCaseToPersist = new ObjectMapper().readValue(writer.toString(), ClaimCase.class);
            claimCaseDAO.save(claimCaseToPersist);
        } catch (Exception ex) {
            Logger.getLogger(ClaimCaseService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void unzipFile(ClaimCase c) throws Exception{
        InputStream data = s3Service.readFileStream("cases/" + c.getId() + "/" + c.getId() + ".zip");
        String rootFolder = "cases/" + c.getId() + "/";

        ZipInputStream stream = new ZipInputStream(data);

        byte[] buffer = new byte[1024];
        ZipEntry entry;

        while ((entry = stream.getNextEntry()) != null) {
            String fileName = entry.getName();
            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                String mimeType = FileMimeType.fromExtension(FilenameUtils.getExtension(fileName)).mimeType();

                int len;
                while ((len = stream.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, len);
                }

                try (InputStream is = new ByteArrayInputStream(outputStream.toByteArray())) {
                    ObjectMetadata meta = new ObjectMetadata();
                    meta.setContentLength(outputStream.size());
                    meta.setContentType(mimeType);

                    if (FilenameUtils.getExtension(fileName).equals("mp4"))
                            fileName = "video.mp4";

                    if (FilenameUtils.getExtension(fileName).equals("json"))
                            fileName = c.getId() + ".json";

                    s3Service.uploadClaimFile(rootFolder + fileName, is, meta);
                }
            }
        }
        
        IOUtils.closeQuietly(stream);
    }

    public void uploadZipToS3(ClaimCase c, String zipFile) throws Exception {

        InputStream data = s3Service.readFileStream(zipFile);
        String rootFolder = "cases/" + c.getId() + "/";

//        ZipInputStream stream = new ZipInputStream(data);
//
//        byte[] buffer = new byte[1024];
//        ZipEntry entry;
//
//        while ((entry = stream.getNextEntry()) != null) {
//            String fileName = entry.getName();
//            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
//                String mimeType = FileMimeType.fromExtension(FilenameUtils.getExtension(fileName)).mimeType();
//
//                int len;
//                while ((len = stream.read(buffer)) > 0) {
//                    outputStream.write(buffer, 0, len);
//                }
//
//                try (InputStream is = new ByteArrayInputStream(outputStream.toByteArray())) {
//                    ObjectMetadata meta = new ObjectMetadata();
//                    meta.setContentLength(outputStream.size());
//                    meta.setContentType(mimeType);
//
//                    if (FilenameUtils.getExtension(fileName).equals("mp4"))
//                            fileName = "video.mp4";
//
//                    if (FilenameUtils.getExtension(fileName).equals("json"))
//                            fileName = c.getId() + ".json";
//
//                    s3Service.uploadClaimFile(rootFolder + fileName, is, meta);
//                }
//            }
//        }

        InputStream item = s3Service.readClaimZipFileStream(rootFolder + c.getId() + ".json");
        StringWriter writer = new StringWriter();
        IOUtils.copy(item, writer);
        LOGGER.info("Zip file for case={} before decrypt={}", c,  writer.toString());
        JSONObject json = new JSONObject(writer.toString());
        persistClaimDetails(json, c);
        LOGGER.info("Zip file for case={} having json={}", c, json);
        String vehicleType = json.has("vehicleType") ? json.getString("vehicleType") : null;

        switch (vehicleType) {
            case "2 Wheeler": vehicleType = "2-wheeler"; break;
            case "4 Wheeler": vehicleType = "4-wheeler";break;
            case "Commercial": vehicleType = "commercial";break;
        }

//        submitReport(c, 
//                json.has("makeModel") ? json.getInt("makeModel") : 0, 
//                json.has("fuelType") ? json.getString("fuelType") : null, 
//                json.has("yom") ? json.getInt("yom") : 0, 
//                json.has("vehicleColor") ? json.getString("vehicleColor") : null,
//                json.has("vehicleNumber") ? json.getString("vehicleNumber") : null, 
//                vehicleType, 
//                userDAO.getUserByPhoneOrEmail(c.getRequestorEmail()));
        //  IOUtils.closeQuietly(stream);

    }

    public ClaimCase persistClaimDetails(JSONObject json, ClaimCase c) throws JSONException, IOException {
        JSONArray inspection_details = json.getJSONArray("inspection_details");
        JSONArray lossassement_details = json.getJSONArray("lossassement_details");
        
//        c.setAccidentDetails(persistAccidentDetails(json.getJSONArray("accident_details").getJSONObject(0), c));
//        c.setDriverDetails(persistDriverDetails(json.getJSONArray("driver_details").getJSONObject(0), c));
//        c.setVehicleDetails(persistVehicleDetails(json.getJSONArray("vehical_details").getJSONObject(0), c));
//        c.setInsuranceDetails(persistInsuranceSetails(json.getJSONArray("insurance_details").getJSONObject(0), c));
        
        claimCaseDAO.save(c);

        persistInspectionDetails(inspection_details, c, "inspection_photos");
        persistLossAssessementDetails(lossassement_details, c);

        return c;
    }

//    public VehicleDetails persistVehicleDetails(JSONObject jsonObject, ClaimCase c) throws JSONException, IOException {
//        VehicleDetails vehicleDetails = c.getVehicleDetails();
//        if (vehicleDetails == null)
//            vehicleDetails = new VehicleDetails();
//
//        return new ObjectMapper().readerForUpdating(vehicleDetails).readValue(jsonObject.toString());
//    }

    public void persistLossAssessementDetails(JSONArray lossassement_details, ClaimCase c) throws JSONException, IOException {
        for (int i = 0; i < lossassement_details.length(); i++) {
            JSONObject jsonObject = lossassement_details.getJSONObject(i);
            
            LossAssessment lossAssessment = new ObjectMapper().readValue(jsonObject.toString(), LossAssessment.class);
            persistLossAssessmentPhotos(lossAssessment, jsonObject);
            
            //c.addLossAssessment(lossAssessment); 
        }
    }

//    @Async
//    private void prepareAssessementSummary(List<LossAssessment> lossAssessment, ClaimCase c) {
//        for (LossAssessment assessment : lossAssessment) {
//            double totalPartCost = assessment.getPartCost()
//                            + (assessment.getPartCost() * assessment.getGstRateForPart())
//                            - (assessment.getPartCost() * assessment.getDepRateForPart());
//            double totalLabourCost = assessment.getLabourCost()
//                            + (assessment.getGstRateForLabour() * assessment.getLabourCost())
//                            - (assessment.getLabourCost() * assessment.getDepRateForLabour());
//            AssessmentSummary assessmentSummary = assessmentSummaryDAO.findByClaimNumber(c.getClaimNumber());
//            if (assessmentSummary == null) {
//                    assessmentSummary = new AssessmentSummary();
//                    assessmentSummary.setCreatedDate(new Date());
//            }
//            assessmentSummary.setUpdatedDate(new Date());
//            assessmentSummary.setTotalLabourCharges(totalLabourCost);
//            assessmentSummary.setTotalPartCharges(totalPartCost);
//            assessmentSummary.setTotal(totalLabourCost + totalPartCost);
//            assessmentSummaryDAO.saveOrUpdate(assessmentSummary);
//        }
//    }

//    public InsuranceDetails persistInsuranceSetails(JSONObject jsonObject, ClaimCase c) throws JSONException, IOException {
//        InsuranceDetails insuranceDetails = c.getInsuranceDetails();
//        if (insuranceDetails == null)
//            insuranceDetails = new InsuranceDetails();
//
//        return new ObjectMapper().readerForUpdating(insuranceDetails).readValue(jsonObject.toString());
//    }

    public void persistInspectionDetails(JSONArray inspection_details, ClaimCase c, String photoType) throws JSONException, IOException {
        for (int i = 0; i < inspection_details.length(); i++) {
            JSONObject jsonObject = inspection_details.getJSONObject(i);
            
            for(int j=0; j<jsonObject.getJSONArray("photos").length(); j++){
                JSONObject photos = jsonObject.getJSONArray("photos").getJSONObject(j);
                
                ClaimCasePhoto claimCasePhoto = new ObjectMapper().readValue(photos.toString(), ClaimCasePhoto.class);
                claimCasePhoto.setPhotoType(photoType);
                
                //c.addClaimCasePhoto(claimCasePhoto);
            }     
        }
    }

//    public DriverDetails persistDriverDetails(JSONObject jsonObject, ClaimCase c) throws JSONException, IOException {
//        DriverDetails driverDetails = c.getDriverDetails();
//        if (driverDetails == null)
//            driverDetails = new DriverDetails();
//        return new ObjectMapper().readerForUpdating(driverDetails).readValue(jsonObject.toString());
//    }
//
//    public AccidentDetails persistAccidentDetails(JSONObject jsonObject, ClaimCase c) throws JSONException, IOException {
//        AccidentDetails accidentDetails = c.getAccidentDetails();
//        if (accidentDetails == null)
//            accidentDetails = new AccidentDetails();
//        
//        return new ObjectMapper().readerForUpdating(accidentDetails).readValue(jsonObject.toString());
//    }

    private void persistLossAssessmentPhotos(LossAssessment lossAssessment, JSONObject jsonObject) throws JSONException {
        JSONArray photosDetails;
        photosDetails = jsonObject.has("photos_details") ? jsonObject.getJSONArray("photos_details") : null;
        if (photosDetails != null) {
            for (int i = 0; i < photosDetails.length(); i++) {
                LossAssessmentPhotos claimPhotos = new LossAssessmentPhotos();
                JSONObject photo = photosDetails.getJSONObject(i);
                JSONObject photos = photo.has("photos") ? photo.getJSONArray("photos").getJSONObject(0) : null;
                if (photos != null) {
                    claimPhotos.setFileName(photos.has("fileName") ? photos.getString("fileName") : null);
                    claimPhotos.setLatitude(photos.has("latitude") ? photos.getDouble("latitude") : null);
                    claimPhotos.setLongitude(photos.has("longitude") ? photos.getDouble("longitude") : null);
                    claimPhotos.setSnapTime(photos.has("snap_time") ? new Date(photos.getLong("snap_time")) : null);
                    claimPhotos.setLossAssessment(lossAssessment);
                    lossAssessmentPhotosDAO.saveOrUpdate(claimPhotos);
                }
            }
        }
    }

//    public void submitReport(ClaimCase c, int makeModelId, String fuelType, int YOM, String vehicleColor,
//                    String vehicleNumber, String vehicleType, User user) throws Exception {
//        caseLogDAO.addClaimInspectionComplete(user, c);
//
//        androidPushNotificationsService.sendNotificationToUser(user.getUsername(), "Case created successfully",
//                        "Inspection Completed for Case no :  " + c.getId() + ". Please check History tab to view QC status.",
//                        "");
//    }

//    @Async
//    public void sendCaseCreationNotificationsAsync(User user, InspectionCase ic) {
//        try {
//            smsService.sendSMS(user.getPhoneNumber(), "Your case has been created with Vehicle Number - "
//                                + ic.getVehicleNumber() + " and Case Id - " + ic.getId() + ".");
//        } catch (IOException ex) {
//            Logger.getLogger(ClaimCaseService.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        if (ic.getInspectionType().equals("SELF_INSPECT")) {
//            try {
//                if (appConfigService.getBooleanProperty("IS_TO_SEND_SMS_IN_CREATE_CASE_THROUGH_AGENT_SELF_INSPECT",
//                                false)) {
//                    smsService.sendSMS(ic.getCustomerPhoneNumber(),
//                                    "We have received an inspection request for Vehicle Number - " + ic.getVehicleNumber()
//                                                    + " from Insurance Company. Our agent will contact you shortly.");
//                }
//            } catch (IOException ex) {
//                Logger.getLogger(ClaimCaseService.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            androidPushNotificationsService
//                .sendNotificationToUser(
//                                user.getUsername(), "Case created successfully", "Case no : " + ic.getId() + " created for "
//                                                + ic.getVehicleNumber() + " . Please check scheduled tab to complete inspection.",
//                                "");
//        }
//    }

//    public AssessmentSummary persistAssessmentSummary(JSONObject jsonObject, ClaimCase cc) throws JSONException, IOException {
//        AssessmentSummary assessmentSummary = cc.getAssessmentSummary();
//        if (assessmentSummary == null) {
//            assessmentSummary = new AssessmentSummary();
//            assessmentSummary.setCreatedDate(new Date());
//        }
//        
//        AssessmentSummary updatedAssessment = new ObjectMapper().readerForUpdating(assessmentSummary).readValue(jsonObject.toString());
//        //cc.setAssessmentSummary(updatedAssessment);
//        claimCaseDAO.save(cc);
//        return updatedAssessment;
//    }

//    private double calculateTotal(AssessmentSummary assessmentSummary) {
//        if (assessmentSummary != null) {
//            double finalLabourCharges = assessmentSummary.getTotalLabourCharges()
//                            + assessmentSummary.getSupplementryLabourCharges();
//            double finalPartCost = assessmentSummary.getTotalPartCharges()
//                            + assessmentSummary.getSupplementryPartCharges();
//            double totalSum = finalLabourCharges + finalPartCost
//                            - (assessmentSummary.getLessExcess() - assessmentSummary.getLessSalavage());
//            LOGGER.info("Total and final assessment summary cost is", totalSum);
//            return totalSum;
//        }
//        return 0;
//    }
}
