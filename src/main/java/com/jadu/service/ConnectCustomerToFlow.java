/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.jadu.model.InspectionCase;

@SuppressWarnings("deprecation")
@Service
public class ConnectCustomerToFlow {

	private static final String SID = "wimwisure";
	private static final String TOKEN = "222d4c08066e9a351ffb924b83aec1113242c53a";

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private CaseFollowupService caseFollowupService;

	@Autowired
	private RedisService redisService;

	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ConnectCustomerToFlow.class);

	public void callCustomer(String phoneNumber) throws Exception {

		SSLContext sslContext = SSLContext.getInstance("SSL");

		// set up a TrustManager that trusts everything
		sslContext.init(null, new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				System.out.println("getAcceptedIssuers =============");
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
				System.out.println("checkClientTrusted =============");
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
				System.out.println("checkServerTrusted =============");
			}

			public boolean isClientTrusted(X509Certificate[] arg0) {
				return false;
			}

			public boolean isServerTrusted(X509Certificate[] arg0) {
				return false;
			}
		} }, new SecureRandom());

		SSLSocketFactory sf = new SSLSocketFactory(sslContext, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		Scheme httpsScheme = new Scheme("https", sf, 443);
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(httpsScheme);

		HttpParams params = new BasicHttpParams();
		ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);

		DefaultHttpClient client = new DefaultHttpClient(cm, params);

		// Replace "<Exotel SID>" and "<Exotel Token>" with your SID and Token
		client.getCredentialsProvider().setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
				new UsernamePasswordCredentials(SID, TOKEN));
		// HttpPost post = new
		// HttpPost("https://twilix.exotel.in/v1/Accounts/"+SID+"/Calls/connect");

		HttpPost post = new HttpPost(
				"https://" + SID + ":" + TOKEN + "@api.exotel.com/v1/Accounts/" + SID + "/Calls/connect");
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

		/*
		 * Replace the text enclosed in < > with your desired values The options for
		 * CallType are "trans" for transactional call and "promo" for promotional call
		 */
		nameValuePairs.add(new BasicNameValuePair("From", phoneNumber));
		// nameValuePairs.add(new BasicNameValuePair("To", "8985235216"));
		nameValuePairs.add(
				new BasicNameValuePair("CallerId", appConfigService.getProperty("CALL_CUSTOMER_1", "02248934313")));
		nameValuePairs.add(new BasicNameValuePair("Url", appConfigService.getProperty("EXOTEL_WIMWISURE_CONNECT_URL",
				"http://my.exotel.com/wimwisure/exoml/start_voice/") + 188214));
		// nameValuePairs.add(new BasicNameValuePair("CallType", "trans"));
		/*
		 * Optional params nameValuePairs.add(new BasicNameValuePair("TimeLimit",
		 * "<time-in-seconds>")); nameValuePairs.add(new BasicNameValuePair("TimeOut",
		 * "<time-in-seconds>")); nameValuePairs.add(new
		 * BasicNameValuePair("StatusCallback", "<http//: your company URL>"));
		 */

		post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		String response = client.execute(post, responseHandler);
		System.out.println(response);
	}

	public void callCustomer2(String phoneNumber, String flowId) throws UnsupportedEncodingException, IOException {
		HttpClient httpclient = HttpClients.createDefault();
		HttpPost httppost = new HttpPost(appConfigService.getProperty("EXOTEL_WIMWISURE_ACCOUNT_DETAILS",
				"https://wimwisure:222d4c08066e9a351ffb924b83aec1113242c53a@api.exotel.com/v1/Accounts/wimwisure/Calls/connect"));

		// Request parameters and other properties.
		List<NameValuePair> params = new ArrayList<NameValuePair>(3);
		params.add(new BasicNameValuePair("From", phoneNumber));
		params.add(new BasicNameValuePair("CallerId", appConfigService.getProperty("CALL_CUSTOMER_2", "02248934313")));
		params.add(new BasicNameValuePair("Url", appConfigService.getProperty("EXOTEL_WIMWISURE_CONNECT_URL",
				"http://my.exotel.com/wimwisure/exoml/start_voice/") + flowId));
		httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

		// Execute and get the response.
		HttpResponse response = httpclient.execute(httppost);

	}

	@Async
	public void callCustomerAfterCaseCreate(String phoneNumber) {
		try {
			this.callCustomer2(phoneNumber, appConfigService.getProperty("CREATE_CASE_EXOTEL_CALL_FLOW_ID", "188214"));
		} catch (Exception ex) {
			ex.printStackTrace();
			Logger.getLogger(ConnectCustomerToFlow.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Async
	public HttpResponse reminderCallForScheduledCase(String phoneNumber, InspectionCase scheduledCase) {
		try {
			return this.reminderCall(phoneNumber,
					appConfigService.getProperty("CREATE_CASE_EXOTEL_REMINDER_CALL_FLOW_ID", "188214"), scheduledCase);
		} catch (Exception ex) {
			Logger.getLogger(ConnectCustomerToFlow.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	@Async
	public void callCustomerAfterSignup(String phoneNumber) {
		try {
			this.callCustomer2(phoneNumber, appConfigService.getProperty("USER_SIGNUP_EXOTEL_CALL_FLOW_ID", "188021"));
		} catch (Exception ex) {
			Logger.getLogger(ConnectCustomerToFlow.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public HttpResponse reminderCall(String phoneNumber, String flowId, InspectionCase scheduledCase)
			throws UnsupportedEncodingException, IOException {
		try {
			HttpClient httpclient = HttpClients.createDefault();
			HttpPost httppost = new HttpPost(appConfigService.getProperty("EXOTEL_WIMWISURE_ACCOUNT_DETAILS",
					"https://wimwisure:222d4c08066e9a351ffb924b83aec1113242c53a@api.exotel.com/v1/Accounts/wimwisure/Calls/connect"));
			// Request parameters and other properties.
			List<NameValuePair> params = new ArrayList<NameValuePair>(3);
			params.add(new BasicNameValuePair("From", phoneNumber));
			params.add(
					new BasicNameValuePair("CallerId", appConfigService.getProperty("REMINDER_CALL", "02248934313")));
			params.add(new BasicNameValuePair("Url", appConfigService.getProperty("EXOTEL_WIMWISURE_URL",
					"http://my.exotel.com/wimwisure/exoml/start_voice/") + flowId));
			params.add(new BasicNameValuePair("StatusCallback", appConfigService.getProperty(
					"ATC_CALL_BACK_URL_FOR_EXOTEL", "http://iffco.wimwisure.com/jadu2-develop/callback/status")));
			httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
			// Execute and get the response.
			HttpResponse response = httpclient.execute(httppost);
			if (response.getStatusLine().getStatusCode() == 200) {
				/**
				 * Set call counter so that maximum threshold call can be checked
				 */
				redisService.storeKeyValue("FOLLOW_UP_COUNTER_FOR_SCHEDULED_CASE_" + scheduledCase.getId(),
						String.valueOf(getCallCount(scheduledCase) + 1), appConfigService
								.getIntProperty("SCHEDULED_CASE_AUTOMATED_CALL_COUNTER_EXPIRY_TIME", 7 * 24 * 60 * 60));
				/**
				 * Set last call time so that difference in call time could be maintain
				 */
				redisService.storeKeyValue("LAST_CALL_TIME_FOR_SCHEDULED_CASE_" + scheduledCase.getId(),
						String.valueOf(new Date().getTime()), appConfigService
								.getIntProperty("SCHEDULED_CASE_AUTOMATED_CALL_COUNTER_EXPIRY_TIME", 7 * 24 * 60 * 60));
			} else {
				LOGGER.info(
						"case={} system automated call was not successfull, Exotel api response code={}, System will try attempting call in next iteration",
						scheduledCase, response.getStatusLine().getStatusCode());
			}
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			// LOGGER.debug("Call response for case={} and phonenumber={} is {}",
			// scheduledCase, phoneNumber, responseString);
			try {
				JSONObject jsonObject = XML.toJSONObject(responseString);
				JSONObject callDetails = jsonObject.getJSONObject("TwilioResponse").getJSONObject("Call");
				caseFollowupService.persistCallStatus(callDetails, phoneNumber, flowId, scheduledCase);
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error("Exception raised while outbound call to customer={}, error={}", phoneNumber,
						e.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception while reminder call to customer={}, error={}", phoneNumber, e.getMessage());
		}
		return null;
	}

	private int getCallCount(InspectionCase scheduledCase) {
		String followUpCount = redisService.get("FOLLOW_UP_COUNTER_FOR_SCHEDULED_CASE_" + scheduledCase.getId());
		return followUpCount != null ? Integer.parseInt(followUpCount) : 0;
	}

}