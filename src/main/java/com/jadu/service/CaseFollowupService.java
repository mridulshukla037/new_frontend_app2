package com.jadu.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.transaction.Transactional;

import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jadu.dao.CaseCommentDAO;
import com.jadu.dao.CaseDAO;
import com.jadu.dao.CasesCallStatusDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.model.CallType;
import com.jadu.model.CasesCallStatus;
import com.jadu.model.InspectionCase;

@Service
public class CaseFollowupService {

	@Autowired
	private CasesCallStatusDAO casesCallStatusDAO;

	@Autowired
	private UserDAOImpl userDAO;

	@Autowired
	CaseDAO caseDAO;

	@Autowired
	CaseCommentDAO caseCommentDAO;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private RedisService redisService;

	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CaseFollowupService.class);

	/**
	 * 
	 * @param callDetails
	 * @param phoneNumber
	 * @param flowId
	 */
	public CasesCallStatus persistCallStatus(JSONObject callDetails, String phoneNumber, String flowId,
			InspectionCase scheduledCase) {

		if (callDetails != null) {
			CasesCallStatus caseFollowUp = persistCasesCallStatus(callDetails, phoneNumber, flowId, scheduledCase);
			if (caseFollowUp != null)
				addComments(scheduledCase, appConfigService.getProperty("SYSTEM_COMMENTS_FOR_AUTOMATED_SCHEDULED_CALLS",
						"System automated call initiated") + " with status: " + caseFollowUp.getStatus());
			return caseFollowUp;
		}
		return null;

	}

	private void addComments(InspectionCase scheduledCase, String comments) {
		try {

			com.jadu.model.User user = userDAO
					.getUserByUsername(appConfigService.getProperty("USER_FOR_AUTOMATED_CALLS", "enjoy15rk@gmail.com"));
			caseCommentDAO.add(scheduledCase, comments, user);
			if (appConfigService.getBooleanProperty("IS_TO_UPDATE_COMMENT_IN_CACHE", true))
				redisService.updateCaseComment(scheduledCase, appConfigService.getProperty(
						"SYSTEM_COMMENTS_FOR_AUTOMATED_SCHEDULED_CALLS", "System automated call initiated"), user);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception raised while adding comments to case={} from automated calls, error={}",
					scheduledCase, e.getMessage());
		}
	}

	private CasesCallStatus persistCasesCallStatus(JSONObject callDetails, String phoneNumber, String flowId,
			InspectionCase scheduledCase) {
		CasesCallStatus caseFollowUp = new CasesCallStatus();
		try {
			String datePattern = "yyyy-MM-dd HH:mm:ss";
			caseFollowUp.setFlowId(flowId);
			caseFollowUp.setCaseId(scheduledCase.getId());
			caseFollowUp.setCallType(CallType.values()[0].name());
			caseFollowUp.setAccountSid(callDetails.getString("AccountSid"));
			caseFollowUp.setAnsweredBy(parseData(callDetails.getString("AnsweredBy")));
			caseFollowUp.setCallerName(parseData(callDetails.getString("CallerName")));
			caseFollowUp.setCallSid(callDetails.getString("Sid"));
			caseFollowUp.setDateCreated(new SimpleDateFormat(datePattern).parse(callDetails.getString("DateCreated")));
			caseFollowUp.setDateUpdated(new SimpleDateFormat(datePattern).parse(callDetails.getString("DateUpdated")));
			caseFollowUp.setDirection(callDetails.getString("Direction"));
			caseFollowUp.setDuration(parseData(callDetails.getString("Duration")));
			String timeEnd = callDetails.getString("EndTime").equalsIgnoreCase("{}") ? null
					: callDetails.getString("EndTime");
			try {
				if (timeEnd != null) {
					Date endTime = new SimpleDateFormat(datePattern).parse(timeEnd);
					caseFollowUp.setEndTime(endTime);
				}
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error("Exception while parsing date for exotel case call, caseId={}, error={}", scheduledCase,
						e.getMessage());
			}
			caseFollowUp.setFollowUpTime(new Date());
			caseFollowUp.setForwardedFrom(parseData(callDetails.getString("ForwardedFrom")));
			caseFollowUp.setFrom(String.valueOf(callDetails.getLong("From")));
			caseFollowUp.setParentCallSid(parseData(callDetails.getString("ParentCallSid")));
			caseFollowUp.setPhoneNumberSid(callDetails.getString("PhoneNumberSid"));
			String price = callDetails.getString("Price").equalsIgnoreCase("{}") ? "0" : callDetails.getString("Price");
			caseFollowUp.setPrice(Float.valueOf(price));
			caseFollowUp.setRecordingUrl(parseData(callDetails.getString("RecordingUrl")));
			String timeStart = callDetails.getString("StartTime").equalsIgnoreCase("{}") ? null
					: callDetails.getString("StartTime");
			if (timeStart == null) {
				caseFollowUp.setStartTime(new Date());
			} else {
				Date startTime = new SimpleDateFormat(datePattern).parse(timeStart);
				caseFollowUp.setStartTime(startTime);
			}
			caseFollowUp.setStatus(callDetails.getString("Status"));
			caseFollowUp.setTo(callDetails.getString("To"));
			caseFollowUp.setUri(callDetails.getString("Uri"));
			casesCallStatusDAO.saveOrUpdate(caseFollowUp);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception raised while persisting followup case={}, phonenumber={}, error={}", scheduledCase,
					phoneNumber, e.getMessage());
		}
		return caseFollowUp;
	}

	private String parseData(String data) {
		return data != null && data.equalsIgnoreCase("{}") ? null : data;
	}

	public void updateCallStatus(String callSid, String status, String recordingUrl, String dateUpdated) {

		try {
			CasesCallStatus caseFollowUp = casesCallStatusDAO.getCaseByCallSid(callSid);
			if (caseFollowUp != null) {
				addComments((InspectionCase) caseDAO.getCaseById(caseFollowUp.getCaseId()),
						appConfigService.getProperty("SYSTEM_COMMENTS_FOR_SCHEDULED_CALLS_ CALLBACK_STATUS",
								"System automated call completed") + " with status: " + status);
				updateCasesCallStatus(caseFollowUp, callSid, status, recordingUrl, dateUpdated);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception raised while updating callback process for callSid={}, status={}, error={}", callSid,status,
					e.getMessage());
		}
	}

	@Transactional
	private void updateCasesCallStatus(CasesCallStatus caseFollowUp, String callSid, String status, String recordingUrl,
			String dateUpdated) throws ParseException {
		String datePattern = "yyyy-MM-dd HH:mm:ss";
		caseFollowUp.setCallbackStatus(status);
		caseFollowUp.setRecordingUrl(recordingUrl);
		caseFollowUp.setDateUpdated(new SimpleDateFormat(datePattern).parse(dateUpdated));
		casesCallStatusDAO.saveOrUpdate(caseFollowUp);
		LOGGER.info("Call back process has updated call status={} against case={} and callSid={}", status,
				caseFollowUp.getCaseId(), caseFollowUp.getCallSid());
	}

}
