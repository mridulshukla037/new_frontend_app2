package com.jadu.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.jadu.helpers.FileHelper;
import com.jadu.helpers.UtilHelper;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class QRService {
    
    @Autowired
    Environment env;
    
    public String generateQRCodeGetBytes(Object data) throws WriterException, IOException{
        
        String randomFileName = env.getProperty("jadu.upload.root.url")+ "/bc/" + UtilHelper.getRandomString(20);
        
        QRCodeWriter qrCodeWriter = new QRCodeWriter();

        BitMatrix bitMatrix = qrCodeWriter.encode(data.toString(), BarcodeFormat.QR_CODE, 500, 500);
        Path path = Paths.get(randomFileName);
        MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);

        return randomFileName;
        
    }
}
