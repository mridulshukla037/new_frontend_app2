package com.jadu.service;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.jadu.dao.CaseDAO;
import com.jadu.dao.CaseLogDAO;
import com.jadu.dao.CasePhotoDAO;
import com.jadu.dao.PhotoTypeDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dao.VehicleDAO;
import com.jadu.dao.VehicleFuelTypeDAO;
import com.jadu.dao.VehicleTypeDAO;
import com.jadu.dto.CaseSubmitDTO;
import com.jadu.dto.PhotoDTO;
import com.jadu.dto.factory.CaseSubmitDTOFactory;
import com.jadu.helpers.EncryptionHelper;
import com.jadu.helpers.FileMimeType;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.CasePhoto;
import com.jadu.model.ClaimCase;
import com.jadu.model.InspectionCase;
import com.jadu.model.PhotoType;
import com.jadu.model.User;
import com.jadu.model.Vehicle;
import com.jadu.push.notification.AndroidPushNotificationsService;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class CaseService {

	@Autowired
	S3DirectUploadService s3DirectUploadService;

	@Autowired
	S3Service s3Service;

	@Autowired
	CaseDAO caseDAO;

	@Autowired
	Environment env;

	@Autowired
	VehicleDAO vehicleDAO;

	@Autowired
	PhotoTypeDAO photoTypeDAO;

	@Autowired
	VehicleFuelTypeDAO vehicleFuelTypeDAO;

	@Autowired
	VehicleTypeDAO vehicleTypeDAO;

	@Autowired
	AndroidPushNotificationsService androidPushNotificationsService;

	@Autowired
	CaseLogDAO caseLogDAO;

	@Autowired
	UserDAOImpl userDAO;

	@Autowired
	CasePhotoDAO casePhotoDAO;

	@Autowired
	SmsService smsService;

	@Autowired
	URLShortnerService urlShortnerService;

	@Autowired
	private AppConfigService appConfigService;

	@Async
	public void uploadZipToS3Async(long caseId) throws Exception {
		uploadZipToS3(caseId, "cases/" + caseId + "/" + caseId + ".zip");
	}

//	public void uploadZipToS3(long caseId) {
//		try {
//			uploadZipToS3(caseId, caseId + "/" + caseId + ".zip");
//		} catch (Exception ex) {
//			Logger.getLogger(CaseService.class.getName()).log(Level.SEVERE, null, ex);
//		}
//	}
//
//	@Async
//	public void uploadZipToS3Async(long caseId, String zipFile) throws Exception {
//		uploadZipToS3(caseId, zipFile);
//	}

	public void uploadZipToS3(long caseId, String zipFile) throws Exception {

		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		InputStream data = s3Service.readFileStream(zipFile);
		String rootFolder = "cases/" + caseId + "/";

//		ZipInputStream stream = new ZipInputStream(data);
//
//		byte[] buffer = new byte[1024];
//		ZipEntry entry;
//
//		while ((entry = stream.getNextEntry()) != null) {
//			String fileName = entry.getName();
//			try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
//				String mimeType = FileMimeType.fromExtension(FilenameUtils.getExtension(fileName)).mimeType();
//
//				int len;
//				while ((len = stream.read(buffer)) > 0) {
//					outputStream.write(buffer, 0, len);
//				}
//
//				try (InputStream is = new ByteArrayInputStream(outputStream.toByteArray())) {
//					ObjectMetadata meta = new ObjectMetadata();
//					meta.setContentLength(outputStream.size());
//					meta.setContentType(mimeType);
//
//					if (FilenameUtils.getExtension(fileName).equals("mp4"))
//						fileName = "video.mp4";
//
//					if (FilenameUtils.getExtension(fileName).equals("json"))
//						fileName = caseId + ".json";
//
//					s3Service.uploadFile(rootFolder + fileName, is, meta);
//				}
//			}
//		}

		InputStream item = s3Service.readFileStream(rootFolder + caseId + ".json");
		StringWriter writer = new StringWriter();
		IOUtils.copy(item, writer);
		String theString = writer.toString();

		JSONObject json = new JSONObject(EncryptionHelper.decrypt(ic.getEncryptionKey(), theString));
		System.out.println(json);

		int makeModelId = json.getInt("makeModel");
		String fuelType = json.getString("fuelType");
		int YOM = json.getInt("yom");
		String vehicleColor = json.getString("vehicleColor");
		String vehicleNumber = json.getString("vehicleNumber");
		String vehicleType = json.getString("vehicleType");

		switch (vehicleType) {
		case "2 Wheeler": {
			vehicleType = "2-wheeler";
			break;
		}
		case "4 Wheeler": {
			vehicleType = "4-wheeler";
			break;
		}
		case "Commercial": {
			vehicleType = "commercial";
			break;
		}
		}

		JSONArray details = new JSONArray(json.getString("details"));
		User user = userDAO.getUserByPhoneOrEmail(ic.getRequestorEmail());

		submitReport(caseId, makeModelId, fuelType, YOM, vehicleColor, vehicleNumber, vehicleType, details, user);
		// IOUtils.closeQuietly(stream);

	}

	public void inspectForCase(long caseId) throws Exception {

		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);

		String rootFolder = "cases/" + caseId + "/";

		// if(ic.getCurrentStage() != 3 || ic.getCurrentStage() != 4)
		// throw new Exception("Invalid stage");

		InputStream item = s3Service.readFileStream(rootFolder + caseId + ".json");
		StringWriter writer = new StringWriter();
		IOUtils.copy(item, writer);
		String theString = writer.toString();

		JSONObject json = new JSONObject(EncryptionHelper.decrypt(ic.getEncryptionKey(), theString));

		int makeModelId = json.getInt("makeModel");
		String fuelType = json.getString("fuelType");
		int YOM = json.getInt("yom");
		String vehicleColor = json.getString("vehicleColor");
		String vehicleNumber = json.getString("vehicleNumber");
		String vehicleType = json.getString("vehicleType");
		String correctVehicleType;

		switch (vehicleType) {
		case "2 Wheeler": {
			correctVehicleType = "2-wheeler";
			break;
		}
		case "4 Wheeler": {
			correctVehicleType = "4-wheeler";
			break;
		}
		case "Commercial": {
			correctVehicleType = "commercial";
			break;
		}
		default:
			correctVehicleType = vehicleType;
		}

		JSONArray details = new JSONArray(json.getString("details"));
		User user = userDAO.getUserByPhoneOrEmail(ic.getRequestorEmail());

		submitReport(caseId, makeModelId, fuelType, YOM, vehicleColor, vehicleNumber, correctVehicleType, details,
				user);

	}

	public void submitReport(long caseId, int makeModelId, String fuelType, int YOM, String vehicleColor,
			String vehicleNumber, String vehicleType, JSONArray detailsObj, User user) throws Exception {
		InspectionCase c = (InspectionCase) caseDAO.getCaseById(caseId);

		Vehicle vehicle = vehicleDAO.getVehicleById(makeModelId);
		List<PhotoType> result = photoTypeDAO.get(c.getInsuranceCompany().getId(), vehicle.getVehicleType().getId(),
				vehicle.getSubType().getId(), c.getPurposeOfInspection().getId(), fuelType);

		/* RC and Number Plate photos are taken separately in application. */
		result.add(photoTypeDAO.get("number-plate"));
		result.add(photoTypeDAO.get("rc"));

		List<CaseSubmitDTO> data = CaseSubmitDTOFactory.getCasesDTO(detailsObj);

		List<CasePhoto> casePhotos = new ArrayList<>();

		for (PhotoType pt : result) {
			CaseSubmitDTO dataRow = CaseSubmitDTOFactory.getCaseDTOById(data, pt.getId());
			if (dataRow != null)
				for (PhotoDTO pd : dataRow.getPhotos()) {
					CasePhoto cp = new CasePhoto();
					cp.setInspectionCase(c);
					cp.setAnswer(pd.getAnswer());
					cp.setQcAnswer(pd.getAnswer());
					cp.setLatitude(pd.getLatitude());
					cp.setLongitude(pd.getLongitude());
					cp.setSnapTime(pd.getSnapTime());
					cp.setFileName(pd.getFilename());
					cp.setPhotoType(pt);
					casePhotos.add(cp);
				}
		}

		CaseSubmitDTO chassisItem = CaseSubmitDTOFactory.getCaseDTOById(data, "chachis-number");
		CaseSubmitDTO vinItem = CaseSubmitDTOFactory.getCaseDTOById(data, "vin-plate-number");
		CaseSubmitDTO odometerItem = CaseSubmitDTOFactory.getCaseDTOById(data, "odometer-rpm");

		if (chassisItem != null && !chassisItem.getPhotos().isEmpty())
			c.setChassisNumber(chassisItem.getPhotos().get(0).getAnswer());

		if (vinItem != null && !vinItem.getPhotos().isEmpty())
			c.setEngineNumber(vinItem.getPhotos().get(0).getAnswer());

		if (odometerItem != null && !odometerItem.getPhotos().isEmpty())
			c.setOdometerReading(odometerItem.getPhotos().get(0).getAnswer());

		c.setVehicleFuelType(vehicleFuelTypeDAO.get(fuelType));
		c.setVehicleYOM(YOM);
		c.setVehicleColor(vehicleColor);
		c.setVehicle(vehicle);
		c.setVehicleNumber(vehicleNumber);
		c.setCurrentStage(4);
		c.setInspectionSubmitTime(UtilHelper.getDate());
		c.setVehicleType(vehicleTypeDAO.get(vehicleType));

		casePhotoDAO.deletePhotosForCase(caseId);
		caseDAO.saveCasePhotos(c, casePhotos, user.getPhoneNumber());

		caseLogDAO.addInspectionComplete(user, c);

		androidPushNotificationsService.sendNotificationToUser(user.getUsername(), "Case created successfully",
				"Inspection Completed for Case no :  " + c.getId() + ". Please check History tab to view QC status.",
				"");
	}

	@Async
	public void sendCaseCreationNotificationsAsync(User user, InspectionCase ic) {
		pushNotificationAndSMS(user, ic.getId(), ic.getVehicleNumber(), ic.getInspectionType(),
				ic.getCustomerPhoneNumber());

	}

	@Async
	public void sendClaimCaseCreationNotificationsAsync(User user, ClaimCase cc) {
		pushNotificationAndSMS(user, cc.getId(), cc.getVehicleNumber(), cc.getInspectionType(),
				cc.getCustomerPhoneNumber());

	}

	private void pushNotificationAndSMS(User user, long caseId, String vehicleNumber, String inspectionType,
			String customerPhoneNumber) {
		try {
			smsService.sendSMS(user.getPhoneNumber(), "Your case has been created with Vehicle Number - "
					+ vehicleNumber + " and Case Id - " + caseId + ".");
		} catch (IOException ex) {
			Logger.getLogger(CaseService.class.getName()).log(Level.SEVERE, null, ex);
		}

		if (inspectionType.equals("SELF_INSPECT")) {
			try {
				if (appConfigService.getBooleanProperty("IS_TO_SEND_SMS_IN_CREATE_CASE_THROUGH_AGENT_SELF_INSPECT",
						false)) {
					smsService.sendSMS(customerPhoneNumber,
							"We have received an inspection request for Vehicle Number - " + vehicleNumber
									+ " from Insurance Company. Our agent will contact you shortly.");
				}
			} catch (IOException ex) {
				Logger.getLogger(CaseService.class.getName()).log(Level.SEVERE, null, ex);
			}
			androidPushNotificationsService.sendNotificationToUser(user.getUsername(), "Case created successfully",
					"Case no : " + caseId + " created for " + vehicleNumber
							+ " . Please check scheduled tab to complete inspection.",
					"");
		}
	}
}
