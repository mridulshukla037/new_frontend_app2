package com.jadu.service;

import com.jadu.dao.AppConfigurationDAO;
import com.jadu.dao.OTPDAO;
import com.jadu.helpers.UtilHelper;
import com.jadu.mail.MailSource;
import com.jadu.model.ClaimCase;
import com.jadu.model.InspectionCase;
import com.jadu.model.OTP;
import com.jadu.model.User;
import freemarker.template.TemplateException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class MailService {

	@Autowired
	OTPDAO otpDAO;

	@Autowired
	Environment env;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	MailSource mailSource;

	@Async
	public void sendRegistrationOTP(User user) {
		sendRegistrationOTP(user, "Account has been created!");
	}

	public void sendRegistrationOTP(User user, String subject) {
		int validity = 300;
		OTP otp = otpDAO.generateEmailOtp(user.getUsername(), UtilHelper.getDateAddMinutes(validity), "REGISTER");

		try {
			Map<String, Object> variables = new HashMap<>();
			variables.put("otp", otp.getOtp());
			mailSource.sendMailTLS(user.getEmail(), subject,
					env.getProperty("jadu.upload.root.url") + "/mail_templates", "account.ftl", variables);
		} catch (IOException | TemplateException | MessagingException ex) {
			Logger.getLogger(MailSource.class.getName()).log(Level.SEVERE, null, ex);
			otp.setSuccess(false);
			otp.setErrorLog(ex.getMessage());
			otpDAO.save(otp);
		}
	}

	public boolean validateRegistrationOTP(User user, String otpVal) throws Exception {
		OTP otp = (OTP) otpDAO.getEmailOtp(user.getUsername(), otpVal);

		if (otp == null || !otp.getOtp().equals(otpVal)) {
			throw new Exception("Invalid OTP");
		}

		if (UtilHelper.getDate().getTime() - otp.getCreationTime().getTime() >= 10 * 60 * 1000) {
			throw new Exception("OTP expired!");
		}

		return true;
	}

	@Async
	public void sendGenaratedReportMail(String email, InspectionCase ic, byte[] file) {

		if (!appConfigService.getBooleanProperty("MAIL_REPORT"))
			return;

		File reportFile = null;

		try {

			String rootFolder = env.getProperty("jadu.upload.root.url") + "/cases/";
			String exportFolder = appConfigService.getProperty("BASE_URL") + "util/cases/download-zip-photos/"
					+ ic.getDownloadKey();

			File dir = new File(rootFolder + ic.getId());

			if (!dir.exists())
				dir.mkdirs();

			reportFile = new File(rootFolder + ic.getId() + "/report.pdf");

			FileUtils.writeByteArrayToFile(reportFile, file);

			mailSource.sendMailTLS(email,
					"Inspection report  for Case: " + ic.getId() + " , Vehicle Number:  " + ic.getVehicleNumber(),
					"Hi, <br/> Please find report attached. " + "<br/>" + "<b>Vehicle Number:</b> "
							+ ic.getVehicleNumber() + "<br/>" + "<b>Customer Name:</b> " + ic.getCustomerName()
							+ "<br/>" + "<b>Status:</b> " + ic.getRemark() + "<br/>" + "<b>Remarks:</b> "
							+ (ic.getRecommendation() == null ? "" : ic.getRecommendation()) + "<br/>"
							+ "<b>Photos:</b> <a href='" + appConfigService.getProperty("BASE_URL") + "preview-images"
							+ "/" + ic.getDownloadKey() + "'>Click here to View Photos</a> <br/>"
							+ "Download inspection photos here: <a href='" + exportFolder + "'>" + ic.getId()
							+ ".zip</a>",
					reportFile);
		} catch (MessagingException | IOException ex) {
			Logger.getLogger(MailService.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			if (reportFile != null)
				reportFile.delete();
		}
	}

	@Async
	public void sendGenaratedReportMail(String email, ClaimCase ic, byte[] file) {

		if (!appConfigService.getBooleanProperty("MAIL_REPORT"))
			return;

		File reportFile = null;

		try {

			String rootFolder = env.getProperty("jadu.upload.root.url") + "/cases/";
			String exportFolder = appConfigService.getProperty("BASE_URL") + "util/cases/download-zip-photos/"
					+ ic.getDownloadKey();

			File dir = new File(rootFolder + ic.getId());

			if (!dir.exists())
				dir.mkdirs();

			reportFile = new File(rootFolder + ic.getId() + "/report.pdf");

			FileUtils.writeByteArrayToFile(reportFile, file);

			mailSource.sendMailTLS(email,
					"Inspection report  for Case: " + ic.getId() + " , Vehicle Number:  " + ic.getVehicleNumber(),
					"Hi, <br/> Please find report attached. " + "<br/>" + "<b>Vehicle Number:</b> "
							+ ic.getVehicleNumber() + "<br/>" + "<b>Customer Name:</b> " + ic.getCustomerName()
							+ "<br/>" + "<b>Status:</b> " + ic.getRemark() + "<br/>" + "<b>Remarks:</b> "
							+ (ic.getRecommendation() == null ? "" : ic.getRecommendation()) + "<br/>"
							+ "<b>Photos:</b> <a href='" + appConfigService.getProperty("BASE_URL") + "preview-images"
							+ "/" + ic.getDownloadKey() + "'>Click here to View Photos</a> <br/>"
							+ "Download inspection photos here: <a href='" + exportFolder + "'>" + ic.getId()
							+ ".zip</a>",
					reportFile);
		} catch (MessagingException | IOException ex) {
			Logger.getLogger(MailService.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			if (reportFile != null)
				reportFile.delete();
		}
	}

	@Async
	public void sendGenaratedReportMail(String email, InspectionCase ic, byte[] file, File zipFile) {

		if (!appConfigService.getBooleanProperty("MAIL_REPORT"))
			return;

		File reportFile = null;

		try {

			String rootFolder = env.getProperty("jadu.upload.root.url") + "/cases/";

			File dir = new File(rootFolder + ic.getId());

			if (!dir.exists())
				dir.mkdirs();

			reportFile = new File(rootFolder + ic.getId() + "/report.pdf");

			FileUtils.writeByteArrayToFile(reportFile, file);

			List<File> filesToMail = new ArrayList<>();

			filesToMail.add(reportFile);
			filesToMail.add(zipFile);

			mailSource.sendMailTLS(email, "Inspection report  for Case: " + ic.getId(),
					"Hi, <br/> Please find report attached.", filesToMail);
		} catch (MessagingException | IOException ex) {
			Logger.getLogger(MailService.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			if (reportFile != null)
				reportFile.delete();
		}
	}
}
