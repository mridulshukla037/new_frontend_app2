package com.jadu.service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jadu.dao.AppConfigurationDAO;
import com.jadu.model.AppConfiguration;

@Service
public class AppConfigService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AppConfigService.class);

	@Autowired
	private AppConfigurationDAO appConfigurationDAO;

	private Map<String, String> propertyMap = new ConcurrentHashMap<>();
	private volatile boolean isLoaded = false;

	public String getProperty(String propertyName, String defaultValue) {
		String value = getProperty(propertyName);
		return value == null ? defaultValue : value;
	}

	public String getPropertyWithEmptyCheck(String propertyName, String defaultValue) {
		String value = getProperty(propertyName);
		return value == null || value.isEmpty() ? defaultValue : value;
	}

	public String getProperty(String propertyName) {
		checkIfLoadOrRefresh();
		return propertyMap.get(propertyName);
	}

	public Boolean getBooleanProperty(String propertyName) {
		checkIfLoadOrRefresh();
		String propertyValue = propertyMap.get(propertyName);
		if (propertyValue == null)
			return null;
		boolean value = Boolean.parseBoolean(propertyValue);
		return value;
	}

	public Boolean getBooleanProperty(String propertyName, Boolean defaultValue) {
		Boolean value = getBooleanProperty(propertyName);
		return value == null ? defaultValue : value;
	}

	public Integer getIntProperty(String propertyName, Integer defaultValue) {
		Integer value = getIntProperty(propertyName);
		return value == null ? defaultValue : value;
	}

	public Long getLongProperty(String propertyName) {
		checkIfLoadOrRefresh();
		String propertyValue = propertyMap.get(propertyName);
		if (propertyValue == null)
			return null;
		try {
			long value = Long.parseLong(propertyValue);
			return value;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Long getLongProperty(String propertyName, Long defaultValue) {
		Long value = getLongProperty(propertyName);
		return value == null ? defaultValue : value;
	}

	public Integer getIntProperty(String propertyName) {
		checkIfLoadOrRefresh();
		String propertyValue = propertyMap.get(propertyName);
		if (propertyValue == null)
			return null;
		try {
			int value = Integer.parseInt(propertyValue);
			return value;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Double getDoubleProperty(String propertyName, Double defaultValue) {
		Double value = getDoubleProperty(propertyName);
		return value == null ? defaultValue : value;
	}

	public Double getDoubleProperty(String propertyName) {
		checkIfLoadOrRefresh();
		String propertyValue = propertyMap.get(propertyName);
		if (propertyValue == null)
			return null;
		try {
			double value = Double.parseDouble(propertyValue);
			return value;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Float getFloatProperty(String propertyName, Float defaultValue) {
		Float value = getFloatProperty(propertyName);
		return value == null ? defaultValue : value;
	}

	public Float getFloatProperty(String propertyName) {
		checkIfLoadOrRefresh();
		String propertyValue = propertyMap.get(propertyName);
		if (propertyValue == null)
			return null;
		try {
			float value = Float.parseFloat(propertyValue);
			return value;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return null;
		}
	}

	private void checkIfLoadOrRefresh() {
		if (isLoaded) {
			return;
		}

		synchronized (propertyMap) {
			if (!isLoaded) {
				List<AppConfiguration> propertyList = appConfigurationDAO.findAll();
				for (AppConfiguration dbConfig : propertyList) {
					propertyMap.put(dbConfig.getTag(), dbConfig.getValue());
				}

				LOGGER.info("App config loaded");

				/**
				 * Not adding to logs
				 */
				System.out.println(propertyMap);

				isLoaded = true;
			}
		}
	}

	public void refresh() {
		LOGGER.info("Refreshed DbConfig");
		List<AppConfiguration> propertyList = appConfigurationDAO.findAll();
		for (AppConfiguration dbConfig : propertyList) {
			propertyMap.put(dbConfig.getTag(), dbConfig.getValue());

		}
		LOGGER.info("db config refreshed :" + propertyMap);

	}

	public void update(AppConfiguration config) {
		appConfigurationDAO.saveOrUpdate(config);
	}

	public List<String> getPropertyValues(String propertyName, List<String> asList) {
		AppConfiguration config = appConfigurationDAO.findByTag(propertyName);
		if (config == null)
			return asList;
		return Arrays.asList(config.getValue());
	}

	public void updateTag(String tag, String value) {
		AppConfiguration config = appConfigurationDAO.findByTag(tag);
		if (config != null) {
			config.setValue(value);
			update(config);
			refresh();
		}
	}

}
