package com.jadu.service;

import com.jadu.dao.AppConfigurationDAO;
import com.jadu.helpers.URLBuilder;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoogleLocationService {

	@Autowired
	private AppConfigService appConfigService;

	public byte[] getMapSnapshotByLatLng(Double latitude, Double longitude) {
		try {
			String api_key = appConfigService.getProperty("GOOGLE_MAP_API_KEY");

			URL url = new URL(new URLBuilder().addBaseUrl("https://maps.googleapis.com/maps/api/staticmap")
					.addParameter("center", latitude + "," + longitude).addParameter("size", "200x120")
					.addParameter("zoom", 12)
					.addParameter("markers", "color:red%7Clabel:C%7C" + latitude + "," + longitude)
					.addParameter("key", api_key).build());

			ByteArrayOutputStream out;
			try (InputStream in = new BufferedInputStream(url.openStream())) {
				out = new ByteArrayOutputStream();
				byte[] buf = new byte[1024];
				int n = 0;
				while (-1 != (n = in.read(buf)))
					out.write(buf, 0, n);
				out.close();
				return out.toByteArray();
			} catch (IOException ex) {
				Logger.getLogger(GoogleLocationService.class.getName()).log(Level.SEVERE, null, ex);
			}

		} catch (MalformedURLException ex) {
			Logger.getLogger(GoogleLocationService.class.getName()).log(Level.SEVERE, null, ex);
		}

		return null;
	}

	public String getLocalityByLatLng(Double latitude, Double longitude) {
		try {
			String api_key = appConfigService.getProperty("GOOGLE_MAP_API_KEY");
			URL url = new URL(new URLBuilder().addBaseUrl("https://maps.googleapis.com/maps/api/geocode/json")
					.addParameter("latlng", latitude + "," + longitude).addParameter("result_type", "street_address")
					.addParameter("key", api_key).build());

			HttpURLConnection con = (HttpURLConnection) url.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
				String inputLine;
				StringBuilder response = new StringBuilder();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}

				JSONObject responseJSON = new JSONObject(response.toString());

				JSONArray results = responseJSON.getJSONArray("results");

				if (results.length() > 0)
					return results.getJSONObject(0).getString("formatted_address");

			}
		} catch (MalformedURLException ex) {
			Logger.getLogger(GoogleLocationService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ProtocolException ex) {
			Logger.getLogger(GoogleLocationService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException | JSONException ex) {
			Logger.getLogger(GoogleLocationService.class.getName()).log(Level.SEVERE, null, ex);
		}

		return null;
	}
}
