package com.jadu.service;

import static com.rosaloves.bitlyj.Bitly.as;
import static com.rosaloves.bitlyj.Bitly.shorten;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class URLShortnerService {

	@Autowired
	private AppConfigService appConfigService;

	public String shortenCaseUrl(long caseId) {
		return as(appConfigService.getProperty("BITLY_ACCESS_KEY"), appConfigService.getProperty("BITLY_SECRET_KEY"))
				.call(shorten(appConfigService.getProperty("UPDATE_ADDRESS_URL") + caseId)).getShortUrl();
	}

	public String shortenUrl(String relativeUrl) {
		return as(appConfigService.getProperty("BITLY_ACCESS_KEY"), appConfigService.getProperty("BITLY_SECRET_KEY"))
				.call(shorten(appConfigService.getProperty("BASE_URL") + relativeUrl)).getShortUrl();
	}

	public String shortenQRCodeUrl(String filename) {
		return as(appConfigService.getProperty("BITLY_ACCESS_KEY"), appConfigService.getProperty("BITLY_SECRET_KEY"))
				.call(shorten(appConfigService.getProperty("QR_CODE_URL") + filename)).getShortUrl();
	}
}
