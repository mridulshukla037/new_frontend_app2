package com.jadu.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class BlockchainService {
    
    @Autowired
    Environment env;
    
    private int getIndex() throws FileNotFoundException, IOException{
        BufferedReader input = new BufferedReader(new FileReader("index.txt"));
        String last = null, line;

        while ((line = input.readLine()) != null) { 
            last = line;
        }

        return Integer.valueOf(last);
    }
    
    public int saveDataToBlockchainGetTransactionId(String data) throws IOException, InterruptedException, Exception{
        int lastIndex = getIndex();
        System.out.println("python3 qr_script.py " + " 1 " + data);
        String[] cmd = {
            "/bin/bash",
            "-c",
            "python3 qr_script.py " + " 1 " + data
        };

        Process p = Runtime.getRuntime().exec(cmd);

        while(p.isAlive()){
            Thread.sleep(100);
        }

        int currentIndex = getIndex();

        if(currentIndex <= lastIndex)
            throw new Exception("Unable to save blockchain hash! Please try again later!");
        
        return currentIndex;
    }
}
