package com.jadu.service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.AppConfigurationDAO;
import com.jadu.dao.InsuranceCompanyDAO;
import com.jadu.dto.AgentsDTO;
import com.jadu.dto.AllCases;
import com.jadu.dto.AllUserDTO;
import com.jadu.dto.CaseHistoryDTO;
import com.jadu.dto.CaseHistoryDTO2;
import com.jadu.dto.ClaimMakeModelVariant;
import com.jadu.dto.ClaimMasterData;
import com.jadu.dto.RestWrapperDTO;
import com.jadu.dto.ScheduledCasesDTO;
import com.jadu.helpers.CompressionUtil;
import com.jadu.model.Admin;
import com.jadu.model.Agent;
import com.jadu.model.ClaimCase;
import com.jadu.model.CompanyBranchDivisionUser;
import com.jadu.model.CompanyUser;
import com.jadu.model.InspectionCase;
import com.jadu.model.QC;
import com.jadu.model.User;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * Created by Asad Ali on 4/04/2019.
 */
@Service("redisService")
public class RedisService {
	@Autowired
	private JedisPool jedisPool;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private AgentDAOImpl agentDAO;

	@Autowired
	private UtilService utilService;

	@Autowired
	private RedisReconcileService redisReconcileService;
        
        @Autowired
        private InsuranceCompanyDAO insuranceCompanyDAO;

	private static Gson gson = new Gson();

	private static final int BATCH_SIZE = 50;

	private static final Logger LOGGER = LoggerFactory.getLogger(RedisService.class);

	public Set<String> getValue(String key) {
		Jedis connection = null;
		try {
			connection = jedisPool.getResource();
			Set<String> values = connection.smembers(key);
			return values;
		} finally {
			if (connection != null) {
				returnConnection(connection);
			}
		}
	}

	public String get(String key) {
		Jedis connection = null;
		try {
			connection = jedisPool.getResource();
			return connection.get(key);
		} finally {
			if (connection != null) {
				returnConnection(connection);
			}
		}
	}

	public void storeKeyValue(String key, String value) {
		storeKeyValue(key, value, null);
	}

	public void storeKeyValue(String key, String value, Integer expiryTime) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.set(key, value);
			if (expiryTime != null) {
				jedis.expire(key, expiryTime);
			}
		} finally {
			if (jedis != null) {
				returnConnection(jedis);
			}
		}
	}

	public Set<String> getKeysFromHash(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.hkeys(key);
		} finally {
			if (jedis != null) {
				returnConnection(jedis);
			}
		}
	}

	private void returnConnection(Jedis connection) {
		if (appConfigService.getBooleanProperty("CLOSE_REDIS_CONNECTION", true)) {
			connection.close();
		} else {
			jedisPool.returnResource(connection);
		}
	}

	public void cacheAllCases(String key, List<AllCases> allCases) {
		LOGGER.info("Pushing all cases of size={} in redis against key={}", allCases != null ? allCases.size() : null,
				key);
		if (allCases != null && key != null && !key.isEmpty()) {
			String s = gson.toJson(allCases);
			cacheComplexData(key, s);

		}
	}

	private void cacheComplexData(String key, String s) {
		byte[] value = CompressionUtil.compressString(s);
		byte[] keyByte = key.getBytes();
		setComplexData(keyByte, value);
	}

	public List<AllCases> getAllCases(String key) {
		List<byte[]> values = getComplexData(key);
		if (values == null || values.isEmpty() || values.get(0) == null)
			return null;
		String data1 = CompressionUtil.decompress(values.get(0));
		Type listType = new TypeToken<List<AllCases>>() {
		}.getType();
		List<AllCases> allCasesList = gson.fromJson(data1, listType);
		return allCasesList;

	}

	private void setComplexData(byte[] key, byte[] value) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.set(key, value);
		} finally {
			if (jedis != null) {
				returnConnection(jedis);
			}
		}

	}

	private List<byte[]> getComplexData(String key) {
		Jedis connection = null;
		try {
			connection = jedisPool.getResource();
			List<byte[]> cacheKeys = new ArrayList<>();
			cacheKeys.add(key.getBytes());
			byte[][] keys = cacheKeys.toArray(new byte[][] {});
			List<byte[]> values = connection.mget(keys);
			return values;
		} finally {
			if (connection != null) {
				returnConnection(connection);
			}
		}
	}

	public void expireKey(String key, int timeInSeconds) {
		if (key != null && !key.isEmpty()) {
			Jedis jedis = null;
			try {
				jedis = jedisPool.getResource();
				jedis.expire(key, timeInSeconds);
				LOGGER.info("Key={} has been expired from redis", key);
			} finally {
				if (jedis != null) {
					returnConnection(jedis);
				}
			}
		}
	}

	public Set<String> getSet(String key) {
		if (key != null && !key.isEmpty()) {
			Jedis jedis = null;
			try {
				jedis = jedisPool.getResource();
				return jedis.smembers(key);
			} finally {
				if (jedis != null) {
					returnConnection(jedis);
				}
			}
		}

		return new HashSet<>();
	}

	public Set<String> intersect(String key1, String key2) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.sdiff(key1, key2);
		} finally {
			if (jedis != null) {
				returnConnection(jedis);
			}
		}
	}

	public Set<String> intersect(String[] values, String key2) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			String key = String.valueOf(System.currentTimeMillis() + values.hashCode());
			jedis.sadd(key, values);
			Set<String> result = jedis.sdiff(key, key2);
			jedis.del(key);
			return result;
		} finally {
			if (jedis != null) {
				returnConnection(jedis);
			}
		}
	}

	public boolean exists(String key) {
		if (key != null && !key.isEmpty()) {
			Jedis jedis = null;
			try {
				jedis = jedisPool.getResource();
				return jedis.exists(key);
			} finally {
				if (jedis != null) {
					returnConnection(jedis);
				}
			}
		}

		return false;
	}

	public void storeHash(String key, Map<String, String> hash) {
		if (hash.isEmpty()) {
			LOGGER.info("not adding hash to redis as it is empty");
			return;
		}
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.hmset(key, hash);
		} finally {
			if (jedis != null) {
				returnConnection(jedis);
			}
		}
	}

	public void deleteFromHash(String key, String... values) {
		if (values.length == 0) {
			LOGGER.info("not deleting from hash as values are empty");
			return;
		}
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.hdel(key, values);
		} finally {
			if (jedis != null) {
				returnConnection(jedis);
			}
		}
	}

	public Map<String, String> getHash(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.hgetAll(key);
		} finally {
			if (jedis != null) {
				returnConnection(jedis);
			}
		}
	}

	public boolean keyExistsInSet(String key, String value) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.hexists(key, value);
		} finally {
			if (jedis != null) {
				returnConnection(jedis);
			}
		}
	}

	public void deleteKey(String key) {
		Jedis connection = null;
		try {
			connection = jedisPool.getResource();
			connection.del(key);
		} finally {
			if (connection != null) {
				returnConnection(connection);
			}
		}
	}

	private String[] getStrings(Long[] newIds) {
		String[] idsStr = new String[newIds.length];
		for (int i = 0; i < newIds.length; i++) {
			idsStr[i] = newIds[i].toString();
		}
		return idsStr;
	}

	public void addIdsToSet(String key, Long... ids) {
		if (ids == null) {
			return;
		}
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			String[] idsStr = new String[ids.length];
			for (int i = 0; i < ids.length; i++) {
				idsStr[i] = ids[i].toString();
			}
			jedis.sadd(key, idsStr);
		} catch (Exception e) {
			LOGGER.info("redis error, error={}", e.getMessage());
		} finally {
			if (jedis != null) {
				returnConnection(jedis);
			}
		}
	}

	public void cacheList(String key, Object... ids) {
		if (ids == null) {
			return;
		}
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.sadd(key, ids.toString());
		} catch (Exception e) {
			LOGGER.info("redis error, error={}", e.getMessage());
		} finally {
			if (jedis != null) {
				returnConnection(jedis);
			}
		}
	}

	public void deleteIdsFromSet(String key, Long... ids) {
		if (ids == null) {
			return;
		}
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			String[] idsStr = getStrings(ids);
			jedis.srem(key, idsStr);
		} catch (Exception e) {
			LOGGER.info("redis error, error={}", e.getMessage());
		} finally {
			if (jedis != null) {
				returnConnection(jedis);
			}
		}
	}

	public Set<Long> getIdsSet(String key) {
		Jedis connection = null;
		try {
			connection = jedisPool.getResource();

			Set<String> ids = connection.smembers(key);
			if (ids != null) {
				Set<Long> set = new HashSet();
				for (String id : ids) {
					set.add(Long.valueOf(id));
				}
				return set;
			}
			return Collections.EMPTY_SET;
		} catch (Exception e) {
			LOGGER.info("redis error, error={}", e.getMessage());
			return Collections.EMPTY_SET;
		} finally {
			if (connection != null) {
				returnConnection(connection);
			}
		}
	}

	public void reconcileIdsSet(String key, Long... newIds) {
		if (newIds == null || newIds.length == 0) {
			return;
		}
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			String[] idsStr = new String[newIds.length];
			for (int i = 0; i < newIds.length; i++) {
				idsStr[i] = newIds[i].toString();
			}

			long time = System.currentTimeMillis();

			String tempKey = key + "_LATEST_" + time;

			jedis.sadd(tempKey, idsStr);

			LOGGER.info("Time taken for adding key {} {} set size {}", tempKey, System.currentTimeMillis() - time,
					idsStr.length);

			time = System.currentTimeMillis();

			jedis.sadd(key, idsStr);

			LOGGER.info("Time taken for adding set of size {} to {} {}", idsStr.length, key,
					System.currentTimeMillis() - time);

			time = System.currentTimeMillis();

			Set<String> sdiff = jedis.sdiff(key, tempKey);

			LOGGER.info("Time taken for set diff {}, {} is {}", key, tempKey, System.currentTimeMillis() - time);

			String[] sdiffArr = sdiff.toArray(new String[] {});

			if (sdiffArr.length > 0) {
				jedis.srem(key, sdiffArr);
			}

			LOGGER.info("removed size [{}] ids during reconcile process of ids ={}", sdiffArr.length,
					Arrays.toString(sdiffArr));

			jedis.del(tempKey);
		} catch (Exception e) {
			LOGGER.info("redis error, error={}", e.getMessage());
		} finally {
			if (jedis != null) {
				returnConnection(jedis);
			}
		}
	}

	public void putKeyWithEndOfDayExpiry(String key, int timeZoneOffset, Long... ids) {
		Jedis jedis = null;
		try {
			addIdsToSet(key, ids);
			jedis = jedisPool.getResource();
			jedis.expire(key, getEODTimeInSecond(timeZoneOffset));
		} catch (Exception e) {
			LOGGER.info("redis error, error={}", e.getMessage());
		} finally {
			if (jedis != null) {
				returnConnection(jedis);
			}
		}
	}

	private int getEODTimeInSecond(int timeZoneOffset) {
		DateTime dateTime = DateTime.now(DateTimeZone.forOffsetMillis(timeZoneOffset * 1000)).toLocalDate()
				.toDateTimeAtStartOfDay(DateTimeZone.forOffsetMillis(timeZoneOffset * 1000)).plusDays(1);

		long l = dateTime.getMillis() - DateTime.now().getMillis();
		int expirySeconds = (int) (l / 1000);
		if (expirySeconds == 0) {
			expirySeconds = 24 * 60 * 60;
		}
		return expirySeconds;
	}

	public List<AgentsDTO> getAllAgentDetails(String key) {
		List<byte[]> values = getComplexData(key);
		if (values == null || values.isEmpty() || values.get(0) == null)
			return null;
		String data1 = CompressionUtil.decompress(values.get(0));
		Type listType = new TypeToken<List<AgentsDTO>>() {
		}.getType();
		List<AgentsDTO> agentsList = gson.fromJson(data1, listType);
		return agentsList;
	}

	public void cacheAllAgentDetails(String key, List<AgentsDTO> allAgents) {
		if (allAgents != null && !allAgents.isEmpty() && key != null && !key.isEmpty()) {
			LOGGER.info("Pushing all agents in redis against key={}, agentsSize={}", key, allAgents.size());
			String s = gson.toJson(allAgents);
			cacheComplexData(key, s);

		}
	}

	public List<AgentsDTO> reconcileAllAgents(String key) {
		List<AgentsDTO> allAgents = agentDAO.getAgentPersonalDetails();
		cacheAllAgentDetails(key, allAgents);
		return allAgents;
	}

	public void cacheAgentDetails(Agent agent) {
		AgentsDTO agentsDTO = new AgentsDTO(agent.getEmail(), agent.getPhoneNumber(), agent.getFirstName(),
				agent.getLastName());
		// Fetch Already cached agents
		List<AgentsDTO> cachedAgents = getAllAgentDetails("ALL_AGENTS_DETAILS");
		if (cachedAgents != null) {
			// Add it to existing list and recahe
			cachedAgents.add(agentsDTO);
			cacheAllAgentDetails("ALL_AGENTS_DETAILS", cachedAgents);

		}
	}

	public List<ScheduledCasesDTO> getScheduledCases(String key) {
		List<byte[]> values = getComplexData(key);
		if (values == null || values.isEmpty() || values.get(0) == null)
			return null;
		String data1 = CompressionUtil.decompress(values.get(0));
		Type listType = new TypeToken<List<ScheduledCasesDTO>>() {
		}.getType();
		List<ScheduledCasesDTO> scheduledCases = gson.fromJson(data1, listType);
		return scheduledCases;
	}

	public void cacheScheduledCases(String key, List<ScheduledCasesDTO> scheduledCases) {
		if (scheduledCases != null && key != null && !key.isEmpty()) {
			LOGGER.info("Pushing all scheduled cases of size={} in redis against key={}", scheduledCases.size(), key);
			String s = gson.toJson(scheduledCases);
			cacheComplexData(key, s);

		}
	}

	public void cacheCaseAgainstUser(User user, InspectionCase ic, long count) {
		String key = "ALL_CASES_FOR_USER_" + user.getUsername();
		AllCases newCase = new AllCases(ic.getId(), ic.getVehicleNumber(), ic.getCustomerName(),
				ic.getCustomerPhoneNumber(), ic.getInspectionTime(), ic.getCloseTime(), ic.getCurrentStage(),
				ic.getInspectionType(), ic.getInsuranceCompany() != null ? ic.getInsuranceCompany().getName() : null,
				null, ic.getRequestorPhoneNumber(), ic.getRemark(), ic.getDownloadKey(), count);
		List<AllCases> cachedCases = getAllCases(key);
		List<ScheduledCasesDTO> scheduledCases = getScheduledCases("SCHEDULED_CASES_FOR_USER_" + user.getUsername());
		if (cachedCases != null) {
			// Add it to existing list and recahe
			LOGGER.info("Added newly created case to existing pool for user={}", user.getUsername());
			cachedCases.add(newCase);
			cacheAllCases(key, cachedCases);
		} else {
			LOGGER.info("All case has been reconciled for user={}", user.getUsername());
			redisReconcileService.reconcileAllCasesForUser(user);
		}
		if (scheduledCases != null) {
			ScheduledCasesDTO scheduledCasesDTO = new ScheduledCasesDTO(ic.getId(), ic.getVehicleNumber(),
					ic.getCustomerName(), ic.getCustomerPhoneNumber(), ic.getInspectionTime(), ic.getCloseTime(),
					ic.getCurrentStage(), ic.getInspectionType(),
					ic.getInsuranceCompany() != null ? ic.getInsuranceCompany().getName() : null,
					ic.getInsuranceCompany() != null ? ic.getInsuranceCompany().getLogoUrl() : null,
					ic.getRequestorPhoneNumber(), count);
			scheduledCases.add(scheduledCasesDTO);
			LOGGER.info("Adding newly created case to scheduled pool for user={}, case={}", user.getUsername(),
					scheduledCasesDTO);
			cacheScheduledCases("SCHEDULED_CASES_FOR_USER_" + user.getUsername(), scheduledCases);
		} else {
			LOGGER.info("All scheduled case has been reconciled for user={}", user.getUsername());
			redisReconcileService.reconcileScheduledCasesForUser(user);
		}
	}

	public void updateCommentAgainstCachedCases(User user, InspectionCase ic) {
		String key = "ALL_CASES_FOR_USER_" + user.getUsername();
		List<AllCases> cachedCases = getAllCases(key);
		if (cachedCases != null) {
			updateAndCacheComments(user, cachedCases, ic, key);
		}

	}

	private void updateAndCacheComments(User user, List<AllCases> cachedCases, InspectionCase ic, String key) {

		for (AllCases allCases : cachedCases) {
			if (allCases.getId() == ic.getId()) {
				/**
				 * Case found, remove it from list , update the commentCount and re add to the
				 * list
				 */
				cachedCases.remove(allCases);
				allCases.setCountComments(allCases.getCountComments() + 1);
				allCases.setRemark(ic.getRemark());
				cachedCases.add(allCases);
				break;
			}
		}
		cacheAllCases(key, cachedCases);

	}

	public void cacheAgent(String key, AllUserDTO allUserDTO) {
		if (allUserDTO != null && key != null && !key.isEmpty()) {
			LOGGER.info("Pushing agent details in redis against key={}, agents={}", key, allUserDTO);
			String s = gson.toJson(allUserDTO);
			cacheComplexData(key, s);

		}
	}

	public List<AllUserDTO> getAgent(String key) {
		List<byte[]> values = getComplexData(key);
		if (values == null || values.isEmpty() || values.get(0) == null)
			return null;
		String data1 = CompressionUtil.decompress(values.get(0));
		Type listType = new TypeToken<AllUserDTO>() {
		}.getType();
		AllUserDTO agentList = gson.fromJson(data1, listType);
		List<AllUserDTO> allUserDTOs = new ArrayList<AllUserDTO>();
		allUserDTOs.add(agentList);
		return allUserDTOs;
	}

	public void cacheAllAgentCases(String key, List<CaseHistoryDTO> allCases) {

		LOGGER.info("Pushing all agent cases of size={} in redis against key={}",
				allCases != null ? allCases.size() : null, key);
		if (allCases != null && !allCases.isEmpty() && key != null && !key.isEmpty()) {
			String s = gson.toJson(allCases);
			cacheComplexData(key, s);

		}

	}

	public List<CaseHistoryDTO> getAllAgentCases(String key) {
		List<byte[]> values = getComplexData(key);
		if (values == null || values.isEmpty() || values.get(0) == null)
			return null;
		String data1 = CompressionUtil.decompress(values.get(0));
		Type listType = new TypeToken<List<CaseHistoryDTO>>() {
		}.getType();
		List<CaseHistoryDTO> allCasesList = gson.fromJson(data1, listType);
		return allCasesList;
	}

	public void cacheAgentAllCases(String key, List<CaseHistoryDTO2> allCases) {
		LOGGER.info("Pushing all agent cases of size={} in redis against key={}", allCases.size(), key);
		if (allCases != null && !allCases.isEmpty() && key != null && !key.isEmpty()) {
			String s = gson.toJson(allCases);
			cacheComplexData(key, s);

		}
	}

	public List<CaseHistoryDTO2> getAgentAllCases(String key) {
		List<byte[]> values = getComplexData(key);
		if (values == null || values.isEmpty() || values.get(0) == null)
			return null;
		String data1 = CompressionUtil.decompress(values.get(0));
		Type listType = new TypeToken<List<CaseHistoryDTO2>>() {
		}.getType();
		List<CaseHistoryDTO2> caseHistoryDTO2 = gson.fromJson(data1, listType);
		return caseHistoryDTO2;
	}

	public void cacheCaseAgainstAgent(Agent agent, InspectionCase ic, int count) {
		CaseHistoryDTO caseHistoryDTO = new CaseHistoryDTO(ic.getId(), ic.getVehicleNumber(), ic.getCustomerName(),
				ic.getRemark(), ic.getInspectionTime(),
				ic.getInsuranceCompany() != null ? ic.getInsuranceCompany().getName() : null,
				ic.getPurposeOfInspection() != null ? ic.getPurposeOfInspection().getName() : null, ic.getModel(),
				ic.getCurrentStage(), ic.getRequestorName(), ic.getRequestorPhoneNumber());
		List<CaseHistoryDTO> cachedCases = getAllAgentCases("ALL_CASES_FOR_AGENT_" + agent.getUsername());
		if (cachedCases != null) {
			// Add it to existing list and recahe
			cachedCases.add(caseHistoryDTO);
			cacheAllAgentCases("ALL_CASES_FOR_AGENT_" + agent.getUsername(), cachedCases);
		}
		CaseHistoryDTO2 CaseHistoryDTO2 = new com.jadu.dto.CaseHistoryDTO2(ic.getId(), ic.getVehicleNumber(),
				ic.getCustomerName(), ic.getRemark(), ic.getInspectionTime(),
				ic.getInsuranceCompany() != null ? ic.getInsuranceCompany().getName() : null,
				ic.getPurposeOfInspection() != null ? ic.getPurposeOfInspection().getName() : null,
				ic.getVehicle() != null ? ic.getVehicle().getId() : null, ic.getCurrentStage(), ic.getRequestorName(),
				ic.getRequestorPhoneNumber());
		List<CaseHistoryDTO2> caseHistoryDTO2List = getAgentAllCases(
				"WRAPPED_ALL_CASES_FOR_AGENT_" + agent.getUsername());
		if (caseHistoryDTO2List != null) {
			// Add it to existing list and recahe
			caseHistoryDTO2List.add(CaseHistoryDTO2);
			cacheAgentAllCases("WRAPPED_ALL_CASES_FOR_AGENT_" + agent.getUsername(), caseHistoryDTO2List);
		}
	}

	public void cacheCaseAgainstAgent(Agent agent, ClaimCase ic, int count) {
		CaseHistoryDTO caseHistoryDTO = new CaseHistoryDTO(ic.getId(), ic.getVehicleNumber(), ic.getCustomerName(),
				ic.getRemark(), ic.getInspectionTime(),
				insuranceCompanyDAO.getInsuranceCompanyByName(ic.getCompanyBranchDivision().getCompany()).getName(),
				ic.getPurposeOfSurvey() != null ? ic.getPurposeOfSurvey().getName() : null, ic.getVehicleDetails().getMakeModelVariant().getModel(),
				ic.getCurrentStage(), ic.getRequestor().getFirstName(), ic.getRequestor().getPhoneNumber());
		List<CaseHistoryDTO> cachedCases = getAllAgentCases("ALL_CASES_FOR_AGENT_" + agent.getUsername());
		if (cachedCases != null) {
			// Add it to existing list and recahe
			cachedCases.add(caseHistoryDTO);
			cacheAllAgentCases("ALL_CASES_FOR_AGENT_" + agent.getUsername(), cachedCases);
		}

	}

	public List<String> getAllAgentsPhoneNumber(User user, Set<CompanyBranchDivisionUser> companyBranchDivisionUsers) {
		// TODO Auto-generated method stub
		return null;
	}

	public void updateAgentBranch(Agent agent, String branch) {
		if (branch != null && !branch.isEmpty()) {
			List<AllUserDTO> agentDetails = getAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + agent.getPhoneNumber());
			if (agentDetails != null && !agentDetails.isEmpty()) {
				AllUserDTO allUserDTO = agentDetails.get(0);
				if (allUserDTO != null) {
					String branchName = allUserDTO.getBranchName();
					if (branchName != null && !branchName.isEmpty()) {
						if (!branch.equalsIgnoreCase(branchName)) {
							allUserDTO.setBranchName(branch);
							cacheAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + agent.getPhoneNumber(), allUserDTO);

						}
					} else {
						findAndCacheAllUserDetails("AGENT_DETAILS_BY_PHONE_NUMBER_" + agent.getPhoneNumber());
					}
				} else {
					findAndCacheAllUserDetails("AGENT_DETAILS_BY_PHONE_NUMBER_" + agent.getPhoneNumber());
				}
			} else {
				findAndCacheAllUserDetails("AGENT_DETAILS_BY_PHONE_NUMBER_" + agent.getPhoneNumber());
			}

		}

	}

	public void updateAgentCode(Agent agent, String agentCode) {
		if (agentCode != null && !agentCode.isEmpty()) {
			List<AllUserDTO> agentDetails = getAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + agent.getPhoneNumber());
			if (agentDetails != null && !agentDetails.isEmpty()) {
				AllUserDTO allUserDTO = agentDetails.get(0);
				if (allUserDTO != null) {
					String agentId = allUserDTO.getAgentCode();
					if (agentId != null && !agentId.isEmpty()) {
						if (!agentCode.equalsIgnoreCase(agentId)) {
							allUserDTO.setAgentCode(agentCode);
							cacheAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + agent.getPhoneNumber(), allUserDTO);

						}
					} else {
						findAndCacheAllUserDetails("AGENT_DETAILS_BY_PHONE_NUMBER_" + agent.getPhoneNumber());
					}
				} else {
					findAndCacheAllUserDetails("AGENT_DETAILS_BY_PHONE_NUMBER_" + agent.getPhoneNumber());
				}
			} else {
				findAndCacheAllUserDetails("AGENT_DETAILS_BY_PHONE_NUMBER_" + agent.getPhoneNumber());
			}

		}

	}

	public AllUserDTO findAndCacheAllUserDetails(String phoneNumber) {
		List<Agent> agentList = agentDAO.getAgentByPhoneNumber(phoneNumber);
		if (agentList != null && !agentList.isEmpty()) {
			Agent agent = (Agent) agentList.get(0);
			return utilService.prepareAllUserDTOAndCache(agent, phoneNumber);
		}
		return null;
	}

	public void updateName(User user, String firstName, String lastName) {
		if (firstName != null || lastName != null) {
			List<AllUserDTO> agentDetails = getAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + user.getPhoneNumber());
			if (agentDetails != null && !agentDetails.isEmpty()) {
				AllUserDTO allUserDTO = agentDetails.get(0);
				if (allUserDTO != null) {
					if (lastName == null || lastName.isEmpty()) {
						updateFirstName(user, allUserDTO, firstName);
					} else if (firstName == null || firstName.isEmpty()) {
						updateLastName(user, allUserDTO, lastName);
					}
				} else {
					findAndCacheAllUserDetails("AGENT_DETAILS_BY_PHONE_NUMBER_" + user.getPhoneNumber());
				}
			} else {
				findAndCacheAllUserDetails("AGENT_DETAILS_BY_PHONE_NUMBER_" + user.getPhoneNumber());
			}

		}

	}

	private void updateLastName(User user, AllUserDTO allUserDTO, String lastName) {

		String last = allUserDTO != null ? allUserDTO.getLastName() : null;
		if (last != null && last.isEmpty()) {
			if (!lastName.equalsIgnoreCase(lastName)) {
				allUserDTO.setLastName(lastName);
				cacheAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + user.getPhoneNumber(), allUserDTO);
			}
		} else {
			allUserDTO.setLastName(lastName);
			cacheAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + user.getPhoneNumber(), allUserDTO);
		}

	}

	private void updateFirstName(User user, AllUserDTO allUserDTO, String firstName) {
		String first = allUserDTO != null ? allUserDTO.getFirstName() : null;
		if (first != null && first.isEmpty()) {
			if (!firstName.equalsIgnoreCase(first)) {
				allUserDTO.setFirstName(firstName);
				cacheAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + user.getPhoneNumber(), allUserDTO);
			}
		} else {
			allUserDTO.setFirstName(firstName);
			cacheAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + user.getPhoneNumber(), allUserDTO);
		}

	}

	public void updatePhoneNumber(User user, String newPhoneNumber, String oldPhoneNumber) {
		if (newPhoneNumber != null && !newPhoneNumber.isEmpty()) {
			List<AllUserDTO> agentDetails = getAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + oldPhoneNumber);
			if (agentDetails != null && !agentDetails.isEmpty()) {
				AllUserDTO allUserDTO = agentDetails.get(0);
				if (allUserDTO != null) {
					allUserDTO.setPhoneNumber(newPhoneNumber);
					cacheAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + newPhoneNumber, allUserDTO);
					// delete old phonenumber associated key and value
					deleteKey("AGENT_DETAILS_BY_PHONE_NUMBER_" + oldPhoneNumber);
					if (getAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + oldPhoneNumber) == null) {
						LOGGER.info("Old phonenumber={} has been removed from cache", oldPhoneNumber);
					}
				} else {
					findAndCacheAllUserDetails("AGENT_DETAILS_BY_PHONE_NUMBER_" + user.getPhoneNumber());
				}
			} else {
				findAndCacheAllUserDetails("AGENT_DETAILS_BY_PHONE_NUMBER_" + user.getPhoneNumber());
			}
		}
	}

	public void updatEmail(User user, String email) {
		if (email != null && !email.isEmpty()) {
			List<AllUserDTO> agentDetails = getAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + user.getPhoneNumber());
			if (agentDetails != null && !agentDetails.isEmpty()) {
				AllUserDTO allUserDTO = agentDetails.get(0);
				if (allUserDTO != null) {
					String oldEmail = allUserDTO.getEmail();
					if (!email.equalsIgnoreCase(oldEmail)) {
						allUserDTO.setEmail(email);
						cacheAgent("AGENT_DETAILS_BY_PHONE_NUMBER_" + user.getPhoneNumber(), allUserDTO);
					}
				} else {
					findAndCacheAllUserDetails("AGENT_DETAILS_BY_PHONE_NUMBER_" + user.getPhoneNumber());
				}
			} else {
				findAndCacheAllUserDetails("AGENT_DETAILS_BY_PHONE_NUMBER_" + user.getPhoneNumber());
			}
		}
	}

	public void cacheAllAgentListForUser(String username, List<Object> allAgents) {
		String key = "ALL_AGENT_FOR_USER_" + username;
		LOGGER.info("Pushing all agent of size={} in redis against key={}", allAgents.size(), key);
		if (allAgents != null && !allAgents.isEmpty() && key != null && !key.isEmpty()) {
			String s = gson.toJson(allAgents);
			cacheComplexData(key, s);

		}

	}

	public List<Object> getAllAgentListForUser(User user) {
		String key = "ALL_AGENT_FOR_USER_" + user.getUsername();
		List<byte[]> values = getComplexData(key);
		if (values == null || values.isEmpty() || values.get(0) == null)
			return null;
		String data1 = CompressionUtil.decompress(values.get(0));
		Type listType = new TypeToken<List<Object>>() {
		}.getType();
		List<Object> agentList = gson.fromJson(data1, listType);
		return agentList;

	}

	public void updateCacheAllAgentListForUser(User user, AllUserDTO allUserDTO) {
		List<Object> allAgents = getAllAgentListForUser(user);
		allAgents.add(allUserDTO);
		cacheAllAgentListForUser(user.getUsername(), allAgents);
	}

	public void updateCompletedCaseInCache(User user, InspectionCase ic) {
		String key = "COMPLETE_CASES_FOR_USER_" + user.getUsername();
		List<AllCases> allCompletCases = getAllCases(key);
		LOGGER.info("Total cached completed cases for user={} are={}", user.getUsername(),
				allCompletCases != null ? allCompletCases.size() : 0);
		if (allCompletCases != null) {
			AllCases newCase = new AllCases(ic.getId(), ic.getVehicleNumber(), ic.getCustomerName(),
					ic.getCustomerPhoneNumber(), ic.getInspectionTime(), ic.getCloseTime(), ic.getCurrentStage(),
					ic.getInspectionType(),
					ic.getInsuranceCompany() != null ? ic.getInsuranceCompany().getName() : null, null,
					ic.getRequestorPhoneNumber(), ic.getRemark(), ic.getDownloadKey(), 0);
			allCompletCases.add(newCase);
			cacheAllCases(key, allCompletCases);
		} else {
			redisReconcileService.reconcileCompleteCasesForUser(user);
		}
	}

	public void updateClosedCases(User user, InspectionCase ic) {
		String key = "CLOSED_CASES_FOR_USER_" + user.getUsername();
		List<AllCases> allClosedCase = getAllCases(key);
		LOGGER.info("Total cached closed cases for user={} are={}", user.getUsername(),
				allClosedCase != null ? allClosedCase.size() : 0);
		AllCases newCase = new AllCases(ic.getId(), ic.getVehicleNumber(), ic.getCustomerName(),
				ic.getCustomerPhoneNumber(), ic.getInspectionTime(), ic.getCloseTime(), ic.getCurrentStage(),
				ic.getInspectionType(), ic.getInsuranceCompany() != null ? ic.getInsuranceCompany().getName() : null,
				null, ic.getRequestorPhoneNumber(), ic.getRemark(), ic.getDownloadKey(), 0);
		allClosedCase.add(newCase);
		cacheAllCases(key, allClosedCase);

	}

	public void removeFromScheduledCases(User user, InspectionCase ic) {
		String key = "SCHEDULED_CASES_FOR_USER_" + user.getUsername();
		List<ScheduledCasesDTO> scheduledCases = getScheduledCases("SCHEDULED_CASES_FOR_USER_" + user.getUsername());
		LOGGER.info("Total cached scheduled cases for user={} were={}", user.getUsername(),
				scheduledCases != null ? scheduledCases.size() : 0);
		ScheduledCasesDTO toRemoveCase = null;
		for (ScheduledCasesDTO scheduledCasesDTO : scheduledCases) {
			if (scheduledCasesDTO.getId() == ic.getId()) {
				toRemoveCase = scheduledCasesDTO;
				break;
			}
		}
		if (toRemoveCase != null) {
			scheduledCases.remove(toRemoveCase);
			LOGGER.info("Total cached scheduled cases for user={} after removing caseId={} are={}", user.getUsername(),
					ic.getId(), scheduledCases != null ? scheduledCases.size() : 0);
			cacheScheduledCases(key, scheduledCases);
		}
	}

	public void updateCaseComment(InspectionCase ic, String comment, User user) {
		try {
			String scheduledKey = "SCHEDULED_CASES_FOR_USER_" + user.getUsername();
			String closedKey = "CLOSED_CASES_FOR_USER_" + user.getUsername();
			String completeKey = "COMPLETE_CASES_FOR_USER_" + user.getUsername();
			List<ScheduledCasesDTO> scheduledCases = getScheduledCases(
					"SCHEDULED_CASES_FOR_USER_" + user.getUsername());
			List<AllCases> allCompletCases = getAllCases(completeKey);
			List<AllCases> allClosedCase = getAllCases(closedKey);
			if (allClosedCase != null && !allClosedCase.isEmpty()) {
				updateAndCacheComments(user, allClosedCase, ic, closedKey);
			}
			if (allCompletCases != null && !allCompletCases.isEmpty()) {
				updateAndCacheComments(user, allCompletCases, ic, completeKey);
			}
			if (scheduledCases != null && !scheduledCases.isEmpty()) {
				for (ScheduledCasesDTO scheduledCasesDTO : scheduledCases) {
					if (scheduledCasesDTO.getId() == ic.getId()) {
						/**
						 * Case found, remove it from list , update the commentCount and re add to the
						 * list
						 */
						scheduledCases.remove(scheduledCasesDTO);
						scheduledCasesDTO.setCountComments(scheduledCasesDTO.getCountComments() + 1);
						scheduledCases.add(scheduledCasesDTO);
						break;
					}
				}
				cacheScheduledCases(scheduledKey, scheduledCases);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception raised while updating comments for user={}, error={}", user.getPhoneNumber(),
					e.getMessage());
		}
	}

	public void cacheClaimMasterData(List<ClaimMasterData> claimMasterData) {
		String key = "CLAIM_MASTER_DATA";
		if (claimMasterData != null && key != null && !key.isEmpty()) {
			String s = gson.toJson(claimMasterData);
			cacheComplexData(key, s);
		}
	}

	public List<ClaimMasterData> getClaimMasterData() {
		String key = "CLAIM_MASTER_DATA";
		List<byte[]> values = getComplexData(key);
		if (values == null || values.isEmpty() || values.get(0) == null)
			return null;
		String data1 = CompressionUtil.decompress(values.get(0));
		Type listType = new TypeToken<List<ClaimMasterData>>() {
		}.getType();
		List<ClaimMasterData> claimMasterData = gson.fromJson(data1, listType);
		return claimMasterData;
	}

	public void cacheClaimMakeModelVariantData(List<ClaimMakeModelVariant> claimMasterData) {
		String key = "CLAIM_MAKE_MODEL_VARIANT_DATA";
		if (claimMasterData != null && key != null && !key.isEmpty()) {
			String s = gson.toJson(claimMasterData);
			cacheComplexData(key, s);
		}

	}

	public List<ClaimMakeModelVariant> getClaimMakeModelVariant() {
		String key = "CLAIM_MAKE_MODEL_VARIANT_DATA";
		List<byte[]> values = getComplexData(key);
		if (values == null || values.isEmpty() || values.get(0) == null)
			return null;
		String data1 = CompressionUtil.decompress(values.get(0));
		Type listType = new TypeToken<List<ClaimMakeModelVariant>>() {
		}.getType();
		List<ClaimMakeModelVariant> claimMasterData = gson.fromJson(data1, listType);
		return claimMasterData;
	}
}