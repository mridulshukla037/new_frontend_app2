package com.jadu.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Created by Asad Ali on 4/4/2019.
 */
@Configuration
@PropertySource(value = { "classpath:/redis.properties" })
public class RedisConfig {
	@Autowired
	private Environment env;

	@Bean
	JedisPool getRedisClient() {
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxTotal(env.getProperty("maxTotal", Integer.class));
		config.setMaxIdle(env.getProperty("maxIdle", Integer.class));
		config.setMinIdle(env.getProperty("minIdle", Integer.class));
		config.setMaxWaitMillis(env.getProperty("maxWaitMillis", Long.class));
		config.setTestOnBorrow(env.getProperty("testOnBorrow", Boolean.class));
		config.setTimeBetweenEvictionRunsMillis(env.getProperty("timeBetweenEvictionRunsMillis", Long.class));
		config.setMinEvictableIdleTimeMillis(env.getProperty("minEvictableIdleTimeMillis", Long.class));

		JedisPool jedisPool = new JedisPool(config, env.getProperty("host"), env.getProperty("port", Integer.class),
				env.getProperty("timeout", Integer.class));
		return jedisPool;
	}
}
