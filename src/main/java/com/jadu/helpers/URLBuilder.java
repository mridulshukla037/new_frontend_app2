package com.jadu.helpers;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class URLBuilder {
    
    private String baseUrl;
    private final Map<String,Object> parameters;  
    
    public URLBuilder(){
        this.parameters = new LinkedHashMap<>();  
    }
    
    public URLBuilder addBaseUrl(String baseUrl){
        this.baseUrl = baseUrl;
        return this;
    }
    
    public URLBuilder addParameter(String key, Object value){
        this.parameters.put(key, value);
        return this;
    }
    
    public String build(){
        StringBuilder result = new StringBuilder();
        
        result.append(this.baseUrl);
        result.append("?");
        
        List<String> params = new ArrayList<>();
        
        for (Map.Entry<String, Object> entry : this.parameters.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            params.add(key + "=" + value);
        }
        
        result.append(String.join("&", params));
        
        return result.toString();
    }
}
