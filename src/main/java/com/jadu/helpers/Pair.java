package com.jadu.helpers;

import java.io.Serializable;

public class Pair<L, R> implements Serializable {
    private L left;
    private R right;
    private static final long serialVersionUID = 100001L;

    public Pair(L left, R right) {
        this.left = left;
        this.right = right;
    }

    public L getLeft() {
        return left;
    }

    public R getRight() {
        return right;
    }
    
    public void setLeft(L left){
        this.left = left;
    }
    
    public void setRight(R right){
        this.right = right;
    }

    @Override
    public int hashCode() {
        return left.hashCode() ^ right.hashCode();
    }

    @Override
    public boolean equals(Object x) {
        if (!(x instanceof Pair))
            return false;

        Pair pairx = (Pair) x;
        return this.left.equals(pairx.getLeft())
                && this.right.equals(pairx.getRight());
    }
    
    @Override
    public String toString(){
        return "["+left+", "+right+"]";
    }
}