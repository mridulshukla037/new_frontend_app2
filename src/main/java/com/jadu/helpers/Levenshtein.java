package com.jadu.helpers;

import java.util.List;

public class Levenshtein {
    static int calculate(String x, String y) {
        int[][] dp = new int[x.length() + 1][y.length() + 1];

        for (int i = 0; i <= x.length(); i++) {
            for (int j = 0; j <= y.length(); j++) {
                if (i == 0) {
                    dp[i][j] = j;
                }
                else if (j == 0) {
                    dp[i][j] = i;
                }
                else {
                    dp[i][j] = min(dp[i - 1][j - 1] 
                     + costOfSubstitution(x.charAt(i - 1), y.charAt(j - 1)), 
                      dp[i - 1][j] + 1, 
                      dp[i][j - 1] + 1);
                }
            }
        }

        return dp[x.length()][y.length()];
    }


    public static int costOfSubstitution(char a, char b) {
        return a == b ? 0 : 1;
    }
 
    public static int min(int... numbers) {
        return  java.util.Arrays.stream(numbers)
          .min().orElse(Integer.MAX_VALUE);
    }
    
    public static double percMatch(String string1, String string2){
        int distance = calculate(string1, string2);
        int maxLength = Math.max(string1.length(), string2.length());
        
        if(maxLength == 0)
            return 0;
        
        return 100 - (distance*100.0)/maxLength;
    }
    
    public static String percMatch(List<String> items, String string2, Double threshold, boolean lowercaseMatch){
        String result = null;
        Double maxMatch = 0.0;
        
        for(String item : items){
            if(lowercaseMatch){
                item = item.toLowerCase();
                string2 = string2.toLowerCase();
            }
            
            double perMatch = percMatch(item, string2);
            if(perMatch > maxMatch){
               result = item;
               maxMatch = perMatch;
            }
        }
        
        if(maxMatch >= threshold)
            return result;
        return null;
    }
}
