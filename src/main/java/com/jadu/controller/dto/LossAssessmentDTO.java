/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.controller.dto;

/**
 *
 * @author gkumar
 */
public class LossAssessmentDTO {
    private long id;
    private long makeModelVariantId;
    private String partTypeId;
    private String partSideId;
    private String vehiclePartParticularTypeId;
    private String damageTypeId;
    private String repairTypeId;
    private double partCost;
    private double labourCost;
    private double paintCost;
    private double depRateForPart;
    private double gstRateForPart;
    private double depRateForLabour;
    private double gstRateForLabour;
    private double depRateForPaint;
    private double gstRateForPaint;

    public LossAssessmentDTO(long id, String partTypeId, String partSideId, String vehiclePartParticularTypeId, String damageTypeId, String repairTypeId, double partCost, double labourCost, double paintCost, double depRateForPart, double gstRateForPart, double depRateForLabour, double gstRateForLabour, double depRateForPaint, double gstRateForPaint) {
        this.id = id;
        this.makeModelVariantId = 1;
        this.partTypeId = partTypeId;
        this.partSideId = partSideId;
        this.vehiclePartParticularTypeId = vehiclePartParticularTypeId;
        this.damageTypeId = damageTypeId;
        this.repairTypeId = repairTypeId;
        this.partCost = partCost;
        this.labourCost = labourCost;
        this.paintCost = paintCost;
        this.depRateForPart = depRateForPart;
        this.gstRateForPart = gstRateForPart;
        this.depRateForLabour = depRateForLabour;
        this.gstRateForLabour = gstRateForLabour;
        this.depRateForPaint = depRateForPaint;
        this.gstRateForPaint = gstRateForPaint;
    }

    public long getId() {
        return id;
    }

    public long getMakeModelVariantId() {
        return makeModelVariantId;
    }

    public String getPartTypeId() {
        return partTypeId;
    }

    public String getPartSideId() {
        return partSideId;
    }

    public String getVehiclePartParticularTypeId() {
        return vehiclePartParticularTypeId;
    }

    public String getDamageTypeId() {
        return damageTypeId;
    }

    public String getRepairTypeId() {
        return repairTypeId;
    }

    public double getPartCost() {
        return partCost;
    }

    public double getLabourCost() {
        return labourCost;
    }

    public double getPaintCost() {
        return paintCost;
    }

    public double getDepRateForPart() {
        return depRateForPart;
    }

    public double getGstRateForPart() {
        return gstRateForPart;
    }

    public double getDepRateForLabour() {
        return depRateForLabour;
    }

    public double getGstRateForLabour() {
        return gstRateForLabour;
    }

    public double getDepRateForPaint() {
        return depRateForPaint;
    }

    public double getGstRateForPaint() {
        return gstRateForPaint;
    }
    
    
}
