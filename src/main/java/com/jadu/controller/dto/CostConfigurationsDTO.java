/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.controller.dto;

import javax.persistence.Column;

/**
 *
 * @author gkumar
 */
public class CostConfigurationsDTO {
	private long id;
	private long makeModelVariantId;
	private String partTypeId;
	private String partSideId;
	private String vehiclePartParticularTypeId;
	private String damageTypeId;
	private String repairTypeId;
	private double partCost;
	private double labourCost;
	private double paintCost;
	private double paintGst;
	private double labourGst;
	private double partGst;

	private double rateOfDepForPart;
	private double rateOfDepForLabour;
	private double rateOfDepForPaint;

	public CostConfigurationsDTO(long id, long makeModelVariantId,
			String partTypeId, String partSideId,
			String vehiclePartParticularTypeId, String damageTypeId,
			String repairTypeId, double partCost, double labourCost,
			double paintCost, double paintGst, double labourGst,
			double partGst, double rateOfDepForPart, double rateOfDepForLabour,
			double rateOfDepForPaint) {
		this.id = id;
		this.makeModelVariantId = makeModelVariantId;
		this.partTypeId = partTypeId;
		this.partSideId = partSideId;
		this.vehiclePartParticularTypeId = vehiclePartParticularTypeId;
		this.damageTypeId = damageTypeId;
		this.repairTypeId = repairTypeId;
		this.partCost = partCost;
		this.labourCost = labourCost;
		this.paintCost = paintCost;
		this.paintGst = paintGst;
		this.labourGst = labourGst;
		this.partGst = partGst;
	}

	public long getId() {
		return id;
	}

	public long getMakeModelVariantId() {
		return makeModelVariantId;
	}

	public String getPartTypeId() {
		return partTypeId;
	}

	public String getPartSideId() {
		return partSideId;
	}

	public String getVehiclePartParticularTypeId() {
		return vehiclePartParticularTypeId;
	}

	public String getDamageTypeId() {
		return damageTypeId;
	}

	public String getRepairTypeId() {
		return repairTypeId;
	}

	public double getPartCost() {
		return partCost;
	}

	public double getLabourCost() {
		return labourCost;
	}

	public double getPaintCost() {
		return paintCost;
	}

	public double getPaintGst() {
		return paintGst;
	}

	public double getLabourGst() {
		return labourGst;
	}

	public double getPartGst() {
		return partGst;
	}

	public double getRateOfDepForPart() {
		return rateOfDepForPart;
	}

	public void setRateOfDepForPart(double rateOfDepForPart) {
		this.rateOfDepForPart = rateOfDepForPart;
	}

	public double getRateOfDepForLabour() {
		return rateOfDepForLabour;
	}

	public void setRateOfDepForLabour(double rateOfDepForLabour) {
		this.rateOfDepForLabour = rateOfDepForLabour;
	}

	public double getRateOfDepForPaint() {
		return rateOfDepForPaint;
	}

	public void setRateOfDepForPaint(double rateOfDepForPaint) {
		this.rateOfDepForPaint = rateOfDepForPaint;
	}

}
