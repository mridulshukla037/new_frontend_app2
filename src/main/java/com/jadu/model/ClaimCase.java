package com.jadu.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Index;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.OneToOne;

@Entity
@Table(name = "claim_cases")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClaimCase implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Index(name = "vehicle_number")
	@Column(name = "vehicle_number")
	private String vehicleNumber;

	@Index(name = "claim_number")
	@Column(name = "claim_number")
	private String claimNumber;

	@OneToOne(mappedBy="claimCase", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, optional = true)
	private VehicleDetails vehicleDetails = null;

	@OneToOne(mappedBy="claimCase", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, optional = true)
	private AccidentDetails accidentDetails;

	@OneToOne(mappedBy="claimCase", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, optional = true)
	private DriverDetails driverDetails;

	@OneToOne(mappedBy="claimCase", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, optional = true)
	private InsuranceDetails insuranceDetails;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "vehicle_fuel_type")
	private VehicleFuelType vehicleFuelType;
        
        @ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "requestor_username")
	private User requestor;
        
        @ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "qc_username")
	private User qc;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "purpose_of_survey")
	private PurposeOfSurvey purposeOfSurvey;
	@Column(name = "customer_name")
	private String customerName;
	@Index(name = "customer_phone")
	@Column(name = "customer_phone")
	private String customerPhoneNumber;
	@Index(name = "customer_email")
	@Column(name = "customer_email")
	private String customerEmail;
	
	@Index(name = "current_stage")
	@Column(name = "current_stage")
	private int currentStage;
	@Column(name = "inspection_address")
	private double inspectionAddress;
	@Column(name = "inspection_latitude")
	private Double inspectionLatitude;
	@Column(name = "inspection_longitude")
	private Double inspectionLongitude;
	@Column(name = "inspection_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date inspectionTime;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "company_branch_division_id")
	private CompanyBranchDivision companyBranchDivision;

	@Column(name = "creation_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date creationTime;

	@Column(name = "reopen_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date reopenTime;

	@Column(name = "reopen_reason")
	private String reopenReason;

	@Column(name = "qc_reopen_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date qcReopenTime;

	@Column(name = "qc_reopen_reason")
	private String qcReopenReason;

	@Column(name = "photos_uploaded")
	private boolean photosUploaded;

	@Column(name = "videos_uploaded")
	private boolean videosUploaded;
//
	@JsonIgnore
	@OneToMany(mappedBy = "claimCase", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private Set<ClaimCaseUserAllocation> claimCaseUserAllocations;

	@JsonIgnore
	@OneToMany(mappedBy = "claimCase", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private Set<ClaimCaseAttemptAllocation> claimCaseAttemptAllocation;

	@Column(name = "recommendation")
	private String recommendation;

	@Column(name = "remark")
	private String remark;

	@Column(name = "comment")
	private String comment;

	@Column(name = "inspection_submit_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date inspectionSubmitTime;

	@Column(name = "inspection_start_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date inspectionStartTime;

	@Column(name = "close_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date closeTime;

	@Column(name = "qc_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date qcTime;

	@Column(name = "close_reason")
	private String closeReason;

	@Column(name = "encryption_key")
	private String encryptionKey;

	@Column(name = "file_available")
	private boolean fileAvailable = false;

	@Column(name = "file_available_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date fileAvailableTime;

	@Column(name = "download_key")
	private String downloadKey;
        
        @Column(name = "inspection_type")
	private String inspectionType;
        
        @OneToOne(mappedBy="claimCase", cascade=CascadeType.ALL, optional = true, fetch = FetchType.LAZY)
        private AssessmentSummary assessmentSummary;
//        
	@OneToMany(mappedBy = "claimCase", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private Set<ClaimCasePhoto> claimCasePhotos;
        
	@OneToMany(mappedBy = "claimCase", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private Set<LossAssessment> lossAssessments;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public int getCurrentStage() {
        return currentStage;
    }

    public void setCurrentStage(int currentStage) {
        this.currentStage = currentStage;
    }

    public double getInspectionAddress() {
        return inspectionAddress;
    }

    public void setInspectionAddress(double inspectionAddress) {
        this.inspectionAddress = inspectionAddress;
    }

    public Double getInspectionLatitude() {
        return inspectionLatitude;
    }

    public void setInspectionLatitude(Double inspectionLatitude) {
        this.inspectionLatitude = inspectionLatitude;
    }

    public Double getInspectionLongitude() {
        return inspectionLongitude;
    }

    public void setInspectionLongitude(Double inspectionLongitude) {
        this.inspectionLongitude = inspectionLongitude;
    }

    public Date getInspectionTime() {
        return inspectionTime;
    }

    public void setInspectionTime(Date inspectionTime) {
        this.inspectionTime = inspectionTime;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getReopenTime() {
        return reopenTime;
    }

    public void setReopenTime(Date reopenTime) {
        this.reopenTime = reopenTime;
    }

    public String getReopenReason() {
        return reopenReason;
    }

    public void setReopenReason(String reopenReason) {
        this.reopenReason = reopenReason;
    }

    public Date getQcReopenTime() {
        return qcReopenTime;
    }

    public void setQcReopenTime(Date qcReopenTime) {
        this.qcReopenTime = qcReopenTime;
    }

    public String getQcReopenReason() {
        return qcReopenReason;
    }

    public void setQcReopenReason(String qcReopenReason) {
        this.qcReopenReason = qcReopenReason;
    }

    public boolean isPhotosUploaded() {
        return photosUploaded;
    }

    public void setPhotosUploaded(boolean photosUploaded) {
        this.photosUploaded = photosUploaded;
    }

    public boolean isVideosUploaded() {
        return videosUploaded;
    }

    public void setVideosUploaded(boolean videosUploaded) {
        this.videosUploaded = videosUploaded;
    }

    public Set<ClaimCaseUserAllocation> getClaimCaseUserAllocations() {
        return claimCaseUserAllocations;
    }

    public void setClaimCaseUserAllocations(Set<ClaimCaseUserAllocation> claimCaseUserAllocations) {
        this.claimCaseUserAllocations = claimCaseUserAllocations;
    }

    public Set<ClaimCaseAttemptAllocation> getClaimCaseAttemptAllocation() {
        return claimCaseAttemptAllocation;
    }

    public void setClaimCaseAttemptAllocation(Set<ClaimCaseAttemptAllocation> claimCaseAttemptAllocation) {
        this.claimCaseAttemptAllocation = claimCaseAttemptAllocation;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getInspectionSubmitTime() {
        return inspectionSubmitTime;
    }

    public void setInspectionSubmitTime(Date inspectionSubmitTime) {
        this.inspectionSubmitTime = inspectionSubmitTime;
    }

    public Date getInspectionStartTime() {
        return inspectionStartTime;
    }

    public void setInspectionStartTime(Date inspectionStartTime) {
        this.inspectionStartTime = inspectionStartTime;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public Date getQcTime() {
        return qcTime;
    }

    public void setQcTime(Date qcTime) {
        this.qcTime = qcTime;
    }

    public String getCloseReason() {
        return closeReason;
    }

    public void setCloseReason(String closeReason) {
        this.closeReason = closeReason;
    }

    public User getQc() {
        return qc;
    }

    public void setQc(User qc) {
        this.qc = qc;
    }

    public String getEncryptionKey() {
        return encryptionKey;
    }

    public void setEncryptionKey(String encryptionKey) {
        this.encryptionKey = encryptionKey;
    }

    public boolean isFileAvailable() {
        return fileAvailable;
    }

    public void setFileAvailable(boolean fileAvailable) {
        this.fileAvailable = fileAvailable;
    }

    public Date getFileAvailableTime() {
        return fileAvailableTime;
    }

    public void setFileAvailableTime(Date fileAvailableTime) {
        this.fileAvailableTime = fileAvailableTime;
    }

    public String getDownloadKey() {
        return downloadKey;
    }

    public void setDownloadKey(String downloadKey) {
        this.downloadKey = downloadKey;
    }

//    public InsuranceDetails getInsuranceDetails() {
//        return insuranceDetails;
//    }
//
//    public void setInsuranceDetails(InsuranceDetails insuranceDetails) {
//        this.insuranceDetails = insuranceDetails;
//    }

    public VehicleFuelType getVehicleFuelType() {
        return vehicleFuelType;
    }

    public void setVehicleFuelType(VehicleFuelType vehicleFuelType) {
        this.vehicleFuelType = vehicleFuelType;
    }

    public CompanyBranchDivision getCompanyBranchDivision() {
        return companyBranchDivision;
    }

    public void setCompanyBranchDivision(CompanyBranchDivision companyBranchDivision) {
        this.companyBranchDivision = companyBranchDivision;
    }

//    public VehicleDetails getVehicleDetails() {
//        return vehicleDetails;
//    }
//
//    public void setVehicleDetails(VehicleDetails vehicleDetails) {
//        this.vehicleDetails = vehicleDetails;
//    }

    public PurposeOfSurvey getPurposeOfSurvey() {
        return purposeOfSurvey;
    }

    public void setPurposeOfSurvey(PurposeOfSurvey purposeOfSurvey) {
        this.purposeOfSurvey = purposeOfSurvey;
    }

    public Set<ClaimCasePhoto> getClaimCasePhotos() {
        return claimCasePhotos;
    }

    public void setClaimCasePhotos(Set<ClaimCasePhoto> claimCasePhotos) {
        this.claimCasePhotos = claimCasePhotos;
        for(ClaimCasePhoto cp : this.claimCasePhotos){
            cp.setClaimCase(this);
        }
    }

    public Set<LossAssessment> getLossAssessments() {
        return lossAssessments;
    }

    public void setLossAssessments(Set<LossAssessment> lossAssessments) {
        this.lossAssessments = lossAssessments;
        for(LossAssessment la : lossAssessments)
            la.setClaimCase(this);
    }

    public VehicleDetails getVehicleDetails() {
        return vehicleDetails;
    }

    public void setVehicleDetails(VehicleDetails vehicleDetails) {
        this.vehicleDetails = vehicleDetails;
        this.vehicleDetails.setClaimCase(this);
    }

    public AccidentDetails getAccidentDetails() {
        return accidentDetails;
    }

    public void setAccidentDetails(AccidentDetails accidentDetails) {
        this.accidentDetails = accidentDetails;
        this.accidentDetails.setClaimCase(this);
    }

    public DriverDetails getDriverDetails() {
        return driverDetails;
    }

    public void setDriverDetails(DriverDetails driverDetails) {
        this.driverDetails = driverDetails;
        this.driverDetails.setClaimCase(this);
    }

    public InsuranceDetails getInsuranceDetails() {
        return insuranceDetails;
    }

    public void setInsuranceDetails(InsuranceDetails insuranceDetails) {
        this.insuranceDetails = insuranceDetails;
        this.insuranceDetails.setClaimCase(this);
    }

    public AssessmentSummary getAssessmentSummary() {
        return assessmentSummary;
    }

    public void setAssessmentSummary(AssessmentSummary assessmentSummary) {
        this.assessmentSummary = assessmentSummary;
        this.assessmentSummary.setClaimCase(this);
    }

    public User getRequestor() {
        return requestor;
    }

    public void setRequestor(User requestor) {
        this.requestor = requestor;
    } 

    public String getInspectionType() {
        return inspectionType;
    }

    public void setInspectionType(String inspectionType) {
        this.inspectionType = inspectionType;
    }
    
    
}
