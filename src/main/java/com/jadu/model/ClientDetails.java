package com.jadu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "client_details")
public class ClientDetails implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "caption")
	private String caption;

	@Column(name = "address")
	private String address;

	@Index(name = "pan_number")
	@Column(name = "pan_number")
	private String pan_number;

	@Index(name = "gstin")
	@Column(name = "gstin")
	private String gstin;

	@Column(name = "landline_number")
	private String landlineNumber;

	@Column(name = "mobile_number")
	private String mobileNumber;
	/**
	 * agent/garage mobile number will be identifier and mapped with this column, an
	 * agent mobile number and this column must have same garage phone number
	 */
	@Column(name = "garage_mobile_number")
	private String garageMobileNumber;

	@Column(name = "email_ids")
	private String emailIds;

	@Column(name = "other_info")
	private String otherInfo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPan_number() {
		return pan_number;
	}

	public void setPan_number(String pan_number) {
		this.pan_number = pan_number;
	}

	public String getGstin() {
		return gstin;
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public String getLandlineNumber() {
		return landlineNumber;
	}

	public void setLandlineNumber(String landlineNumber) {
		this.landlineNumber = landlineNumber;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailIds() {
		return emailIds;
	}

	public void setEmailIds(String emailIds) {
		this.emailIds = emailIds;
	}

	public String getOtherInfo() {
		return otherInfo;
	}

	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}

	public String getGarageMobileNumber() {
		return garageMobileNumber;
	}

	public void setGarageMobileNumber(String garageMobileNumber) {
		this.garageMobileNumber = garageMobileNumber;
	}

	@Override
	public String toString() {
		return "ClientDetails [id=" + id + ", name=" + name + ", caption=" + caption + ", address=" + address
				+ ", pan_number=" + pan_number + ", gstin=" + gstin + ", landlineNumber=" + landlineNumber
				+ ", mobileNumber=" + mobileNumber + ", emailIds=" + emailIds + ", otherInfo=" + otherInfo + "]";
	}

}
