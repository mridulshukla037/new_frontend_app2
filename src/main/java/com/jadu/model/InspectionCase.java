package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "cases")
public class InspectionCase implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Index(name = "vehicle_number")
	@Column(name = "vehicle_number")
	private String vehicleNumber;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "vehicle_make_model")
	private Vehicle vehicle;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "vehicle_fuel_type")
	private VehicleFuelType vehicleFuelType;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "insurance_company_id")
	private InsuranceCompany insuranceCompany;

	@Column(name = "vehicle_color")
	private String vehicleColor;
	@Column(name = "vehicle_yom")
	private int vehicleYOM;
	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "purpose_of_inspection")
	private PurposeOfInspection purposeOfInspection;
	@Column(name = "customer_name")
	private String customerName;
	@Index(name = "customer_phone")
	@Column(name = "customer_phone")
	private String customerPhoneNumber;
	@Index(name = "customer_email")
	@Column(name = "customer_email")
	private String customerEmail;
	@Column(name = "requestor_name")
	private String requestorName;
	@Index(name = "requestor_email")
	@Column(name = "requestor_email")
	private String requestorEmail;
	@Index(name = "requestor_phone")
	@Column(name = "requestor_phone")
	private String requestorPhoneNumber;
	@Index(name = "current_stage")
	@Column(name = "current_stage")
	private int currentStage;
	@Column(name = "inspection_address")
	private double inspectionAddress;
	@Column(name = "inspection_latitude")
	private Double inspectionLatitude;
	@Column(name = "inspection_longitude")
	private Double inspectionLongitude;
	@Column(name = "inspection_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date inspectionTime;
	@Index(name = "inspection_type")
	@Column(name = "inspection_type")
	private String inspectionType;
	@Column(name = "allocation_attempts")
	private int allocationAttempts;
	@Column(name = "allocation_status")
	private int allocationStatus;
	@Column(name = "attempt_expiry_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date attemptExpiryTime;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "company_branch_division_id")
	private CompanyBranchDivision companyBranchDivision;

	@Column(name = "creation_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date creationTime;

	@Column(name = "odometer_reading")
	private String odometerReading = "0";

	@Column(name = "chassis_number")
	private String chassisNumber = "";

	@Column(name = "engine_number")
	private String engineNumber = "";

	@Column(name = "reopen_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date reopenTime;

	@Column(name = "reopen_reason")
	private String reopenReason;

	@Column(name = "qc_reopen_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date qcReopenTime;

	@Column(name = "qc_reopen_reason")
	private String qcReopenReason;

	@Column(name = "photos_uploaded")
	private boolean photosUploaded;

	@Column(name = "videos_uploaded")
	private boolean videosUploaded;

	@JsonIgnore
	@OneToMany(mappedBy = "inspectionCase", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private Set<CaseUserAllocation> caseUserAllocations;

	@JsonIgnore
	@OneToMany(mappedBy = "inspectionCase", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private Set<CaseAttemptAllocation> CaseAttemptAllocation;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "vehicle_type_id")
	private VehicleType vehicleType;

	@Column(name = "recommendation")
	private String recommendation;

	@Column(name = "remark")
	private String remark;

	@Column(name = "state")
	private String state;

	@Column(name = "zone")
	private String zone;

	@Column(name = "division")
	private String division;

	@Column(name = "branch")
	private String branch;

	@Column(name = "make")
	private String make;

	@Column(name = "model")
	private String model;

	@Column(name = "comment")
	private String comment;

	@Column(name = "inspection_submit_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date inspectionSubmitTime;

	@Column(name = "inspection_start_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date inspectionStartTime;

	@Column(name = "close_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date closeTime;

	@Column(name = "qc_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date qcTime;

	@Column(name = "close_reason")
	private String closeReason;

	@Column(name = "qc_name")
	private String qcName;

	@Column(name = "qc_phone")
	private String qcPhone;

	@Column(name = "qc_email")
	private String qcEmail;

	@Column(name = "encryption_key")
	private String encryptionKey;

	@Column(name = "file_available")
	private boolean fileAvailable = false;

	@Column(name = "file_available_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date fileAvailableTime;

	@Column(name = "download_key")
	private String downloadKey;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
		this.make = vehicle.getMake();
		this.model = vehicle.getModel();
	}

	public VehicleFuelType getVehicleFuelType() {
		return vehicleFuelType;
	}

	public void setVehicleFuelType(VehicleFuelType vehicleFuelType) {
		this.vehicleFuelType = vehicleFuelType;
	}

	public String getVehicleColor() {
		return vehicleColor;
	}

	public void setVehicleColor(String vehicleColor) {
		this.vehicleColor = vehicleColor;
	}

	public int getVehicleYOM() {
		return vehicleYOM;
	}

	public void setVehicleYOM(int vehicleYOM) {
		this.vehicleYOM = vehicleYOM;
	}

	public PurposeOfInspection getPurposeOfInspection() {
		return purposeOfInspection;
	}

	public void setPurposeOfInspection(PurposeOfInspection purposeOfInspection) {
		this.purposeOfInspection = purposeOfInspection;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPhoneNumber() {
		return customerPhoneNumber;
	}

	public void setCustomerPhoneNumber(String customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getRequestorEmail() {
		return requestorEmail;
	}

	public void setRequestorEmail(String requestorEmail) {
		this.requestorEmail = requestorEmail;
	}

	public String getRequestorPhoneNumber() {
		return requestorPhoneNumber;
	}

	public void setRequestorPhoneNumber(String requestorPhoneNumber) {
		this.requestorPhoneNumber = requestorPhoneNumber;
	}

	public int getCurrentStage() {
		return currentStage;
	}

	public void setCurrentStage(int currentStage) {
		this.currentStage = currentStage;
	}

	public Double getInspectionLatitude() {
		return inspectionLatitude;
	}

	public void setInspectionLatitude(Double inspectionLatitude) {
		this.inspectionLatitude = inspectionLatitude;
	}

	public Double getInspectionLongitude() {
		return inspectionLongitude;
	}

	public void setInspectionLongitude(Double inspectionLongitude) {
		this.inspectionLongitude = inspectionLongitude;
	}

	public Date getInspectionTime() {
		return inspectionTime;
	}

	public void setInspectionTime(Date inspectionTime) {
		this.inspectionTime = inspectionTime;
	}

	public String getInspectionType() {
		return inspectionType;
	}

	public void setInspectionType(String inspectionType) {
		this.inspectionType = inspectionType;
	}

	public int getAllocationAttempts() {
		return allocationAttempts;
	}

	public void setAllocationAttempts(int allocationAttempts) {
		this.allocationAttempts = allocationAttempts;
	}

	public int getAllocationStatus() {
		return allocationStatus;
	}

	public void setAllocationStatus(int allocationStatus) {
		this.allocationStatus = allocationStatus;
	}

	public Set<CaseUserAllocation> getCaseUserAllocations() {
		return caseUserAllocations;
	}

	public void setCaseUserAllocations(Set<CaseUserAllocation> caseUserAllocations) {
		this.caseUserAllocations = caseUserAllocations;
	}

	public void addCaseUserAllocation(CaseUserAllocation caseUserAllocation) {
		if (this.caseUserAllocations == null)
			this.caseUserAllocations = new HashSet<>();

		this.caseUserAllocations.add(caseUserAllocation);
	}

	public InsuranceCompany getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(InsuranceCompany insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getAttemptExpiryTime() {
		return attemptExpiryTime;
	}

	public void setAttemptExpiryTime(Date attemptExpiryTime) {
		this.attemptExpiryTime = attemptExpiryTime;
	}

	public Set<CaseAttemptAllocation> getCaseAttemptAllocation() {
		return CaseAttemptAllocation;
	}

	public void setCaseAttemptAllocation(Set<CaseAttemptAllocation> CaseAttemptAllocation) {
		this.CaseAttemptAllocation = CaseAttemptAllocation;
	}

	public CompanyBranchDivision getCompanyBranchDivision() {
		return companyBranchDivision;
	}

	public void setCompanyBranchDivision(CompanyBranchDivision companyBranchDivision) {
		this.companyBranchDivision = companyBranchDivision;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public String getRecommendation() {
		return recommendation;
	}

	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}

	public double getInspectionAddress() {
		return inspectionAddress;
	}

	public void setInspectionAddress(double inspectionAddress) {
		this.inspectionAddress = inspectionAddress;
	}

	public VehicleType getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(VehicleType vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getMake() {
		return make;
	}

	public String getModel() {
		return model;
	}

	public Date getInspectionSubmitTime() {
		return inspectionSubmitTime;
	}

	public void setInspectionSubmitTime(Date inspectionSubmitTime) {
		this.inspectionSubmitTime = inspectionSubmitTime;
	}

	public Date getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}

	public String getCloseReason() {
		return closeReason;
	}

	public void setCloseReason(String closeReason) {
		this.closeReason = closeReason;
	}

	public String getQcName() {
		return qcName;
	}

	public void setQcName(String qcName) {
		this.qcName = qcName;
	}

	public String getQcPhone() {
		return qcPhone;
	}

	public void setQcPhone(String qcPhone) {
		this.qcPhone = qcPhone;
	}

	public String getQcEmail() {
		return qcEmail;
	}

	public void setQcEmail(String qcEmail) {
		this.qcEmail = qcEmail;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getInspectionStartTime() {
		return inspectionStartTime;
	}

	public void setInspectionStartTime(Date inspectionStartTime) {
		this.inspectionStartTime = inspectionStartTime;
	}

	public String getEncryptionKey() {
		return encryptionKey;
	}

	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	public String getOdometerReading() {
		return odometerReading;
	}

	public void setOdometerReading(String odometerReading) {
		this.odometerReading = odometerReading;
	}

	public String getChassisNumber() {
		return chassisNumber;
	}

	public void setChassisNumber(String chassisNumber) {
		this.chassisNumber = chassisNumber;
	}

	public String getEngineNumber() {
		return engineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}

	public boolean isFileAvailable() {
		return fileAvailable;
	}

	public void setFileAvailable(boolean fileAvailable) {
		this.fileAvailable = fileAvailable;
	}

	public Date getFileAvailableTime() {
		return fileAvailableTime;
	}

	public void setFileAvailableTime(Date fileAvailableTime) {
		this.fileAvailableTime = fileAvailableTime;
	}

	public Date getReopenTime() {
		return reopenTime;
	}

	public void setReopenTime(Date reopenTime) {
		this.reopenTime = reopenTime;
	}

	public String getReopenReason() {
		return reopenReason;
	}

	public void setReopenReason(String reopenReason) {
		this.reopenReason = reopenReason;
	}

	public Date getQcReopenTime() {
		return qcReopenTime;
	}

	public void setQcReopenTime(Date qcReopenTime) {
		this.qcReopenTime = qcReopenTime;
	}

	public String getQcReopenReason() {
		return qcReopenReason;
	}

	public void setQcReopenReason(String qcReopenReason) {
		this.qcReopenReason = qcReopenReason;
	}

	public Date getQcTime() {
		return qcTime;
	}

	public void setQcTime(Date qcTime) {
		this.qcTime = qcTime;
	}

	public String getDownloadKey() {
		return downloadKey;
	}

	public void setDownloadKey(String downloadKey) {
		this.downloadKey = downloadKey;
	}

	public boolean isPhotosUploaded() {
		return photosUploaded;
	}

	public void setPhotosUploaded(boolean photosUploaded) {
		this.photosUploaded = photosUploaded;
	}

	public boolean isVideosUploaded() {
		return videosUploaded;
	}

	public void setVideosUploaded(boolean videosUploaded) {
		this.videosUploaded = videosUploaded;
	}

	@Override
	public String toString() {
		return "InspectionCase [id=" + id + ", vehicleNumber=" + vehicleNumber + ", vehicle=" + vehicle
				+ ", vehicleFuelType=" + vehicleFuelType + ", insuranceCompany=" + insuranceCompany + ", vehicleColor="
				+ vehicleColor + ", vehicleYOM=" + vehicleYOM + ", purposeOfInspection=" + purposeOfInspection
				+ ", customerName=" + customerName + ", customerPhoneNumber=" + customerPhoneNumber + ", customerEmail="
				+ customerEmail + ", requestorName=" + requestorName + ", requestorEmail=" + requestorEmail
				+ ", requestorPhoneNumber=" + requestorPhoneNumber + ", currentStage=" + currentStage
				+ ", inspectionAddress=" + inspectionAddress + ", inspectionLatitude=" + inspectionLatitude
				+ ", inspectionLongitude=" + inspectionLongitude + ", inspectionTime=" + inspectionTime
				+ ", inspectionType=" + inspectionType + ", allocationAttempts=" + allocationAttempts
				+ ", allocationStatus=" + allocationStatus + ", attemptExpiryTime=" + attemptExpiryTime
				+ ", companyBranchDivision=" + companyBranchDivision + ", creationTime=" + creationTime
				+ ", odometerReading=" + odometerReading + ", chassisNumber=" + chassisNumber + ", engineNumber="
				+ engineNumber + ", reopenTime=" + reopenTime + ", reopenReason=" + reopenReason + ", qcReopenTime="
				+ qcReopenTime + ", qcReopenReason=" + qcReopenReason + ", photosUploaded=" + photosUploaded
				+ ", videosUploaded=" + videosUploaded + ", vehicleType=" + vehicleType
				+ ", recommendation=" + recommendation + ", remark=" + remark + ", state=" + state + ", zone=" + zone
				+ ", division=" + division + ", branch=" + branch + ", make=" + make + ", model=" + model + ", comment="
				+ comment + ", inspectionSubmitTime=" + inspectionSubmitTime + ", inspectionStartTime="
				+ inspectionStartTime + ", closeTime=" + closeTime + ", qcTime=" + qcTime + ", closeReason="
				+ closeReason + ", qcName=" + qcName + ", qcPhone=" + qcPhone + ", qcEmail=" + qcEmail
				+ ", encryptionKey=" + encryptionKey + ", fileAvailable=" + fileAvailable + ", fileAvailableTime="
				+ fileAvailableTime + ", downloadKey=" + downloadKey + "]";
	}

}
