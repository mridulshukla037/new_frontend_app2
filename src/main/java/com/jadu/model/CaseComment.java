package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;


@Entity
@Table(name="cases_comments")
public class CaseComment {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    
    @JsonIgnore  
    @ManyToOne( cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY)
    @JoinColumn(name="case_id", nullable=false)
    private InspectionCase inspectionCase ;
    
    @JsonIgnore  
    @ManyToOne( cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY)
    @JoinColumn(name="username", nullable=false)
    private User user;
    
    
    @Column(name="comment")
    private String comment;
    
    
    @Column(name="comment_time")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date commentTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public InspectionCase getInspectionCase() {
        return inspectionCase;
    }

    public void setInspectionCase(InspectionCase inspectionCase) {
        this.inspectionCase = inspectionCase;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }
    
}
