package com.jadu.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "cost_configurations")
public class CostConfigurations implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "make_model_variant_id")
	private MakeModelVariant makeModelVariant;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "part_type_id")
	private VehiclePartType vehiclePartType;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "part_side_id")
	private VehiclePartSide vehiclePartSide;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "vehicle_part_particular_type_id")
	private VehiclePartParticularType vehiclePartParticularType;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "damage_type_id")
	private VehicleDamageType vehicleDamageType;

	// @ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	// @JoinColumn(name = "repair_type_id")
	// private VehicleRepairType vehicleRepairType;

	@Column(name = "repair_type_id")
	private String repairType;

	@Column(name = "part_cost")
	private double partCost;

	@Column(name = "labour_cost")
	private double labourCost;

	@Column(name = "paint_cost")
	private double paintCost;

	@Column(name = "paint_gst")
	private double paintGst;

	@Column(name = "labour_gst")
	private double labourGst;

	@Column(name = "part_gst")
	private double partGst;
	@Column(name = "rate_of_dep_for_part")
	private double rateOfDepForPart;
	@Column(name = "rate_of_dep_for_labour")
	private double rateOfDepForLabour;
	@Column(name = "rate_of_dep_for_paint")
	private double rateOfDepForPaint;

	@Column(name = "created_date")
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date createdDate;

	@Column(name = "updated_date")
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date updatedDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MakeModelVariant getMakeModelVariant() {
		return makeModelVariant;
	}

	public void setMakeModelVariant(MakeModelVariant makeModelVariant) {
		this.makeModelVariant = makeModelVariant;
	}

	public double getPartCost() {
		return partCost;
	}

	public void setPartCost(double partCost) {
		this.partCost = partCost;
	}

	public double getLabourCost() {
		return labourCost;
	}

	public void setLabourCost(double labourCost) {
		this.labourCost = labourCost;
	}

	public double getPaintCost() {
		return paintCost;
	}

	public void setPaintCost(double paintCost) {
		this.paintCost = paintCost;
	}

	public double getPaintGst() {
		return paintGst;
	}

	public void setPaintGst(double paintGst) {
		this.paintGst = paintGst;
	}

	public double getLabourGst() {
		return labourGst;
	}

	public void setLabourGst(double labourGst) {
		this.labourGst = labourGst;
	}

	public double getPartGst() {
		return partGst;
	}

	public void setPartGst(double partGst) {
		this.partGst = partGst;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public VehiclePartType getVehiclePartType() {
		return vehiclePartType;
	}

	public void setVehiclePartType(VehiclePartType vehiclePartType) {
		this.vehiclePartType = vehiclePartType;
	}

	public VehiclePartSide getVehiclePartSide() {
		return vehiclePartSide;
	}

	public void setVehiclePartSide(VehiclePartSide vehiclePartSide) {
		this.vehiclePartSide = vehiclePartSide;
	}

	public VehiclePartParticularType getVehiclePartParticularType() {
		return vehiclePartParticularType;
	}

	public void setVehiclePartParticularType(
			VehiclePartParticularType vehiclePartParticularType) {
		this.vehiclePartParticularType = vehiclePartParticularType;
	}

	public VehicleDamageType getVehicleDamageType() {
		return vehicleDamageType;
	}

	public void setVehicleDamageType(VehicleDamageType vehicleDamageType) {
		this.vehicleDamageType = vehicleDamageType;
	}

	// public VehicleRepairType getVehicleRepairType() {
	// return vehicleRepairType;
	// }
	//
	// public void setVehicleRepairType(VehicleRepairType vehicleRepairType) {
	// this.vehicleRepairType = vehicleRepairType;
	// }

	public String getRepairType() {
		return repairType;
	}

	public void setRepairType(String repairType) {
		this.repairType = repairType;
	}

	public double getRateOfDepForPart() {
		return rateOfDepForPart;
	}

	public void setRateOfDepForPart(double rateOfDepForPart) {
		this.rateOfDepForPart = rateOfDepForPart;
	}

	public double getRateOfDepForLabour() {
		return rateOfDepForLabour;
	}

	public void setRateOfDepForLabour(double rateOfDepForLabour) {
		this.rateOfDepForLabour = rateOfDepForLabour;
	}

	public double getRateOfDepForPaint() {
		return rateOfDepForPaint;
	}

	public void setRateOfDepForPaint(double rateOfDepForPaint) {
		this.rateOfDepForPaint = rateOfDepForPaint;
	}

}
