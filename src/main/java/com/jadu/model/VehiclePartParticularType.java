package com.jadu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "vehicle_part_particular_type")
public class VehiclePartParticularType implements Serializable {

	@Id
        @Column(name = "id")
	private String id;

	@Column(name = "part_particular_type")
	@Index(name = "part_particular_type")
	private String partParticularType;

	@Column(name = "vehicle_part_type_id")
	@Index(name = "vehicle_part_type_id")
	private String partTypeId;

	@Column(name = "vehicle_part_side_id")
	@Index(name = "vehicle_part_side_id")
	private String partSideId;

	@Column(name = "created_date")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "updated_date")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date updatedDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getPartParticularType() {
		return partParticularType;
	}

	public void setPartParticularType(String partParticularType) {
		this.partParticularType = partParticularType;
	}

	public String getPartTypeId() {
		return partTypeId;
	}

	public void setPartTypeId(String partTypeId) {
		this.partTypeId = partTypeId;
	}

	public String getPartSideId() {
		return partSideId;
	}

	public void setPartSideId(String partSideId) {
		this.partSideId = partSideId;
	}

}
