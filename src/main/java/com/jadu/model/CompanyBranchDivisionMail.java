package com.jadu.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="company_branch_division_mails")
public class CompanyBranchDivisionMail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer  id;
    
    @Column(name="company_branch_division_id")
    private Integer  companyBranchDivisionId;
    
    @Column(name="email")
    private String email;
    
    @Column(name="insert_time")
    private Date insertTime;
    
    @Column(name="enabled")
    private Boolean enabled = true;
    
    @Column(name="inserted_by")
    private String insertedBy = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCompanyBranchDivisionId() {
        return companyBranchDivisionId;
    }

    public void setCompanyBranchDivisionId(Integer companyBranchDivisionId) {
        this.companyBranchDivisionId = companyBranchDivisionId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getInsertedBy() {
        return insertedBy;
    }

    public void setInsertedBy(String insertedBy) {
        this.insertedBy = insertedBy;
    }
}
