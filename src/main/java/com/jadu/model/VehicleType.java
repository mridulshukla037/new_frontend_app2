package com.jadu.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author gautam
 */
@Entity
@Table(name = "vehicle_types")
public class VehicleType implements Serializable {

	@Id
	@Column(name = "id")
	private String id;
	@Column(name = "name")
	private String name;

	@OneToMany(mappedBy = "vehicleType", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private Set<VehicleSubType> vehicleSubTypes;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<VehicleSubType> getVehicleSubTypes() {
		return vehicleSubTypes;
	}

	public void setVehicleSubTypes(Set<VehicleSubType> vehicleSubTypes) {
		this.vehicleSubTypes = vehicleSubTypes;
	}

}
