package com.jadu.model.builder;

import java.util.Date;

import com.jadu.helpers.UtilHelper;
import com.jadu.model.ClaimCase;
import com.jadu.model.CompanyBranchDivision;
import com.jadu.model.InsuranceCompany;
import com.jadu.model.InsuranceDetails;
import com.jadu.model.PurposeOfSurvey;
import com.jadu.model.User;

public class ClaimCaseBuilder {

	ClaimCase cc;

	public ClaimCaseBuilder(String customerName, String customerPhoneNumber, User requestor, String vehicleNumber, PurposeOfSurvey pos,
			CompanyBranchDivision cbd, InsuranceCompany insuranceCompany) {
		cc = new ClaimCase();

		cc.setCustomerName(customerName);
		cc.setCustomerPhoneNumber(customerPhoneNumber);
		cc.setPurposeOfSurvey(pos);
		cc.setRequestor(requestor);
		cc.setVehicleNumber(vehicleNumber);
		cc.setCurrentStage(0);
		cc.setCreationTime(UtilHelper.getDate());
		cc.setCompanyBranchDivision(cbd);
                
                InsuranceDetails id = new InsuranceDetails();
                id.setClaimCase(cc);
                id.setInsurer(insuranceCompany);
                
                cc.setInsuranceDetails(id);
	}

	public ClaimCaseBuilder(ClaimCase cc) {
		this.cc = cc;
	}

	public ClaimCaseBuilder setStage(int stage) {
		this.cc.setCurrentStage(stage);

		return this;
	}

	public ClaimCaseBuilder addCaseToUserForStage(User user, int stage) {

		//cc.addClaimCaseUserAllocation(new CaseUserAllocationBuilder(cc, user, stage).buildClaimCase());

		return this;
	}

	public ClaimCaseBuilder setInspectionTime(Date date, Double latitude, Double longitude) {
		cc.setInspectionTime(date);
		cc.setInspectionLatitude(latitude);
		cc.setInspectionLongitude(longitude);

		return this;
	}

	public ClaimCaseBuilder setInspectionType(String inspectionType) {
		cc.setInspectionType(inspectionType);

		return this;
	}

	public ClaimCase build() {
		return this.cc;
	}
}
