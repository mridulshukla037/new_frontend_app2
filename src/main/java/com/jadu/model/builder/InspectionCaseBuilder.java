package com.jadu.model.builder;

import com.jadu.helpers.UtilHelper;
import com.jadu.model.CompanyBranchDivision;
import com.jadu.model.InspectionCase;
import com.jadu.model.InsuranceCompany;
import com.jadu.model.PurposeOfInspection;
import com.jadu.model.User;
import java.util.Date;

public class InspectionCaseBuilder {
    
    InspectionCase ic;
    
    public InspectionCaseBuilder(
            String customerName,
            String customerPhoneNumber,
            String requestorName,
            String requestorEmail,
            String requestorPhoneNumber,
            String vehicleNumber,
            PurposeOfInspection poi,
            CompanyBranchDivision cbd
    ){
        ic = new InspectionCase();
        
        ic.setCustomerName(customerName);
        ic.setCustomerPhoneNumber(customerPhoneNumber);
        ic.setPurposeOfInspection(poi);
        ic.setRequestorName(requestorName);
        ic.setRequestorEmail(requestorEmail);
        ic.setRequestorPhoneNumber(requestorPhoneNumber);
        ic.setVehicleNumber(vehicleNumber);
        ic.setAllocationAttempts(0);
        ic.setAllocationStatus(0);
        ic.setCurrentStage(0);
        ic.setCreationTime(UtilHelper.getDate());
        ic.setCompanyBranchDivision(cbd);
    }
    
    public InspectionCaseBuilder(InspectionCase ic){
        this.ic = ic;
    }
    
    public InspectionCaseBuilder setStage(
            int stage
    ){
        this.ic.setCurrentStage(stage);
        
        return this;
    }
    
    public InspectionCaseBuilder addCaseToUserForStage(
            User user,
            int stage
    ){
        
        ic.addCaseUserAllocation(
                new CaseUserAllocationBuilder(ic, user, stage)
                .build());  
        
        return this;
    }
    
    public InspectionCaseBuilder setInspectionTime(
            Date date,
            Double latitude,
            Double longitude
    ){
        ic.setInspectionTime(date);
        ic.setInspectionLatitude(latitude);
        ic.setInspectionLongitude(longitude);
        
        return this;
    }
    
    public InspectionCaseBuilder setInspectionType(
            String inspectionType
    ){
        ic.setInspectionType(inspectionType);
        
        return this;
    }
    
    public InspectionCaseBuilder setInsuranceCompany(
            InsuranceCompany company,
            CompanyBranchDivision companyBranchDivision){
        ic.setInsuranceCompany(company);
        ic.setState(companyBranchDivision.getState());
        ic.setZone(companyBranchDivision.getZone());
        ic.setDivision(companyBranchDivision.getDivision());
        ic.setBranch(companyBranchDivision.getBranch());
        return this;
    }
    
    public InspectionCase build(){
        return this.ic;
    }
}
