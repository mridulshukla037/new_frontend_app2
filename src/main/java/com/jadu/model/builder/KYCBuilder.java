package com.jadu.model.builder;

import com.jadu.model.KYC;
import com.jadu.model.User;

public class KYCBuilder {
    
    private final KYC kyc;
    
    public KYCBuilder(User user){
        kyc =  new KYC();
        kyc.setUser(user);
    }
    
    public KYCBuilder(KYC kyc){
        this.kyc = kyc;
    }
    
    public KYCBuilder setUserInfo(
            String aadharNumber,
            String gender,
            String name,
            String dob
    ){
        kyc.setUid(aadharNumber);
        kyc.setGender(gender);
        kyc.setName(name);
        kyc.setDob(dob);
        
        return this;
    }
    
    public KYCBuilder setAddressInfo(
            String co,
            String dist,
            String house,
            String lm,
            String loc,
            String pc,
            String po,
            String state,
            String street,
            String subdist
    ){
        kyc.setCo(co);
        kyc.setDist(dist);
        kyc.setHouse(house);
        kyc.setLm(lm);
        kyc.setLoc(loc);
        kyc.setPc(pc);
        kyc.setPo(po);
        kyc.setState(state);
        kyc.setStreet(street);
        kyc.setSubdist(subdist);
        
        return this;
    }
    
    public KYCBuilder setProfilePhoto(
            String aadharPicPhotoName
    ){
        kyc.setImageUrl(aadharPicPhotoName);
        return this;
    }
    
    public KYC build(){
        return this.kyc;
    }
}
