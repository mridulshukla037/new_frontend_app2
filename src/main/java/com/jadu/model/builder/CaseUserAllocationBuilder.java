package com.jadu.model.builder;

import com.jadu.helpers.UtilHelper;
import com.jadu.model.CaseUserAllocation;
import com.jadu.model.ClaimCase;
import com.jadu.model.ClaimCaseUserAllocation;
import com.jadu.model.InspectionCase;
import com.jadu.model.User;

public class CaseUserAllocationBuilder {

	private CaseUserAllocation caseUserAllocation = null;
	private ClaimCaseUserAllocation claimCaseUserAllocation = null;

	public CaseUserAllocationBuilder(InspectionCase ic, User user, int stage) {
		caseUserAllocation = new CaseUserAllocation();
		caseUserAllocation.setAllocationTime(UtilHelper.getDate());
		caseUserAllocation.setInspectionCase(ic);
		caseUserAllocation.setStage(stage);
		caseUserAllocation.setUser(user);
	}

	public CaseUserAllocationBuilder(ClaimCase cc, User user, int stage) {
		claimCaseUserAllocation = new ClaimCaseUserAllocation();
		claimCaseUserAllocation.setAllocationTime(UtilHelper.getDate());
		claimCaseUserAllocation.setClaimCase(cc);
		claimCaseUserAllocation.setStage(stage);
		claimCaseUserAllocation.setUser(user);
	}

	public CaseUserAllocation build() {
		return this.caseUserAllocation;
	}

	public ClaimCaseUserAllocation buildClaimCase() {
		return this.claimCaseUserAllocation;
	}
}
