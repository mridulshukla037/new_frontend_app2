package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "driver_details")
public class DriverDetails implements Serializable {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        private Long id;
    
	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "claim_id", nullable = false)
	private ClaimCase claimCase;

	@Index(name = "driver_name")
	@Column(name = "driver_name")
	private String driverName;
        
	@Column(name = "driver_relation")
	private String driverRelation;

	@Column(name = "issue_date")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date issueDate;

	@Column(name = "expiry_date")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date expiryDate;

	@Column(name = "issuing_authority")
	private String issuingAuthority;

	@Column(name = "badge_number")
	private String badgeNumber;

	@Column(name = "license_endorsement")
	private String licenseEndorsement;

	@Index(name = "license_number")
	@Column(name = "license_number")
	private String licenseNumber;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "license_type_id")
	private DriverLicenseType driverLicenseType;

	@Column(name = "remarks")
	private String remarks;

    public ClaimCase getClaimCase() {
        return claimCase;
    }

    public void setClaimCase(ClaimCase claimCase) {
        this.claimCase = claimCase;
    }

	

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

    public DriverLicenseType getDriverLicenseType() {
        return driverLicenseType;
    }

    public void setDriverLicenseType(DriverLicenseType driverLicenseType) {
        this.driverLicenseType = driverLicenseType;
    }

	

	public String getIssuingAuthority() {
		return issuingAuthority;
	}

	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}

	public String getBadgeNumber() {
		return badgeNumber;
	}

	public void setBadgeNumber(String badgeNumber) {
		this.badgeNumber = badgeNumber;
	}

	public String getLicenseEndorsement() {
		return licenseEndorsement;
	}

	public void setLicenseEndorsement(String licenseEndorsement) {
		this.licenseEndorsement = licenseEndorsement;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDriverRelation() {
        return driverRelation;
    }

    public void setDriverRelation(String driverRelation) {
        this.driverRelation = driverRelation;
    }
}
