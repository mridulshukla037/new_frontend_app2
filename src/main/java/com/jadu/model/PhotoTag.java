package com.jadu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;

@Entity
@Table(name="photo_tags")
public class PhotoTag implements Serializable {
    @Id
    @Column(name="id")
    private String id;
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="photo_type", nullable=false)
    private PhotoType photoType ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PhotoType getPhotoType() {
        return photoType;
    }

    public void setPhotoType(PhotoType photoType) {
        this.photoType = photoType;
    }
    
    
}
