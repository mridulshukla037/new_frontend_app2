package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Index;

@Entity
@Table(name = "claim_cases_photos")
public class ClaimCasePhoto implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @JsonIgnore
    @ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY)
    @JoinColumn(name = "claim_case_id", nullable = false)
    @Index(name = "claim_case_id")
    private ClaimCase claimCase;

    @Column(name = "snap_time")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @JsonProperty("snapTime")
    private Date snapTime;

    @Column(name = "latitude")
    @JsonProperty("latitude")
    private Double latitude;

    @Column(name = "longitude")
    @JsonProperty("longitude")
    private Double longitude;

    @Column(name = "file_name")
    @JsonProperty("fileName")
    private String fileName;

    @Column(name = "photo_type")
    @JsonProperty("photoType")
    private String photoType;

    public long getId() {
            return id;
    }

    public void setId(long id) {
            this.id = id;
    }

    public Date getSnapTime() {
            return snapTime;
    }

    public void setSnapTime(Date snapTime) {
            this.snapTime = snapTime;
    }

    public Double getLatitude() {
            return latitude;
    }

    public void setLatitude(Double latitude) {
            this.latitude = latitude;
    }

    public Double getLongitude() {
            return longitude;
    }

    public void setLongitude(Double longitude) {
            this.longitude = longitude;
    }

    public String getFileName() {
            return fileName;
    }

    public void setFileName(String fileName) {
            this.fileName = fileName;
    }

    public String getPhotoType() {
            return photoType;
    }

    public void setPhotoType(String photoType) {
            this.photoType = photoType;
    }

    public ClaimCase getClaimCase() {
            return claimCase;
    }

    public void setClaimCase(ClaimCase claimCase) {
            this.claimCase = claimCase;
    }
}
