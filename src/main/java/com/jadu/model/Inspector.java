package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

@Entity
@DiscriminatorValue("INSPECTOR")
public class Inspector extends User implements Serializable {

	@Column(name = "latitude")
	private Double latitude;

	@Column(name = "longitude")
	private Double longitude;

	@Column(name = "current_latitude")
	private Double currentLatitude;

	@Column(name = "current_longitude")
	private Double currentLongitude;

	@Column(name = "current_lat_lng_last_update_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date currentLatLngLastUpdateTime;

	@Column(name = "radius_of_operation")
	private Double radiusOfOperation;

	@Column(name = "available")
	private boolean available;

	@OneToOne(mappedBy = "inspector", cascade = CascadeType.ALL)
	private InspectorDetail inspectorDetail;

	@JsonIgnore
	@OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
	private KYC kyc;

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public InspectorDetail getInspectorDetail() {
		return inspectorDetail;
	}

	public void setInspectorDetail(InspectorDetail inspectorDetail) {
		this.inspectorDetail = inspectorDetail;
	}

	public KYC getKyc() {
		return kyc;
	}

	public void setKyc(KYC kyc) {
		this.kyc = kyc;
	}

	public Double getRadiusOfOperation() {
		return radiusOfOperation;
	}

	public void setRadiusOfOperation(Double radiusOfOperation) {
		this.radiusOfOperation = radiusOfOperation;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public Double getCurrentLatitude() {
		return currentLatitude;
	}

	public void setCurrentLatitude(Double currentLatitude) {
		this.currentLatitude = currentLatitude;
	}

	public Double getCurrentLongitude() {
		return currentLongitude;
	}

	public void setCurrentLongitude(Double currentLongitude) {
		this.currentLongitude = currentLongitude;
	}

	public Date getCurrentLatLngLastUpdateTime() {
		return currentLatLngLastUpdateTime;
	}

	public void setCurrentLatLngLastUpdateTime(Date currentLatLngLastUpdateTime) {
		this.currentLatLngLastUpdateTime = currentLatLngLastUpdateTime;
	}

	@Override
	public String toString() {
		return "Inspector [latitude=" + latitude + ", longitude=" + longitude + ", currentLatitude=" + currentLatitude
				+ ", currentLongitude=" + currentLongitude + ", currentLatLngLastUpdateTime="
				+ currentLatLngLastUpdateTime + ", radiusOfOperation=" + radiusOfOperation + ", available=" + available
				+ ", inspectorDetail=" + inspectorDetail + ", kyc=" + kyc + "]";
	}

}
