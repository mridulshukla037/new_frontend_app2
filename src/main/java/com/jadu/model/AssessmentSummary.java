package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;


@Entity
@Table(name = "assessment_summary")
public class AssessmentSummary implements Serializable {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        private Long id;
      
        @JsonIgnore
	@OneToOne
	@JoinColumn(name = "claim_id", nullable = false)
	private ClaimCase claimCase;

	@Column(name = "total_labour_charges")
	private double totalLabourCharges;

	@Column(name = "supplementry_labour_charges")
	private double supplementryLabourCharges;

	@Column(name = "total_part_costs")
	private double totalPartCharges;

	@Column(name = "supplementry_part_costs")
	private double supplementryPartCharges;
        
        @Column(name = "total_paint_cost")
	private double totalPaintCost;

	@Column(name = "supplementary_paint_cost")
	private double supplementaryPaintCost;

	@Column(name = "less_excess")
	private double lessExcess;

	@Column(name = "less_salavage")
	private double lessSalavage;

	@Column(name = "total")
	private double total;

	@Column(name = "remarks")
	private String remarks;

	@Column(name = "created_date")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "updated_date")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date updatedDate;

	public double getTotalLabourCharges() {
		return totalLabourCharges;
	}

	public void setTotalLabourCharges(double totalLabourCharges) {
		this.totalLabourCharges = totalLabourCharges;
	}

	public double getSupplementryLabourCharges() {
		return supplementryLabourCharges;
	}

	public void setSupplementryLabourCharges(double supplementryLabourCharges) {
		this.supplementryLabourCharges = supplementryLabourCharges;
	}

	public double getTotalPartCharges() {
		return totalPartCharges;
	}

	public void setTotalPartCharges(double totalPartCharges) {
		this.totalPartCharges = totalPartCharges;
	}

	public double getSupplementryPartCharges() {
		return supplementryPartCharges;
	}

	public void setSupplementryPartCharges(double supplementryPartCharges) {
		this.supplementryPartCharges = supplementryPartCharges;
	}

	public double getLessExcess() {
		return lessExcess;
	}

	public void setLessExcess(double lessExcess) {
		this.lessExcess = lessExcess;
	}

	public double getLessSalavage() {
		return lessSalavage;
	}

	public void setLessSalavage(double lessSalavage) {
		this.lessSalavage = lessSalavage;
	}

	public double getTotal() {

		return total;
	}

	public void setTotal(double total) {

		this.total = total;

	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

    public ClaimCase getClaimCase() {
        return claimCase;
    }

    public void setClaimCase(ClaimCase claimCase) {
        this.claimCase = claimCase;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getTotalPaintCost() {
        return totalPaintCost;
    }

    public void setTotalPaintCost(double totalPaintCost) {
        this.totalPaintCost = totalPaintCost;
    }

    public double getSupplementaryPaintCost() {
        return supplementaryPaintCost;
    }

    public void setSupplementaryPaintCost(double supplementaryPaintCost) {
        this.supplementaryPaintCost = supplementaryPaintCost;
    }
        
        
}
