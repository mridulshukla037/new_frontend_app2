package com.jadu.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name="otp")
public class OTP implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    
    @Column(name="username")
    private String username;
    
    @Column(name="otp")
    private String otp;
    
    @Column(name="creation_time")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date creationTime;
    
    @Column(name="otp_type")
    private String otpType;
    
    @Column(name="otp_group")
    private String otpGroup;
    
    @Column(name="expiry_time")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date expiryTime;
    
    @Column(name="error_log")
    private String errorLog = "";
    
    @Column(name="success")
    private Boolean success = true;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getOtpType() {
        return otpType;
    }

    public void setOtpType(String otpType) {
        this.otpType = otpType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOtpGroup() {
        return otpGroup;
    }

    public void setOtpGroup(String otpGroup) {
        this.otpGroup = otpGroup;
    }

    public Date getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Date expiryTime) {
        this.expiryTime = expiryTime;
    } 

    public String getErrorLog() {
        return errorLog;
    }

    public void setErrorLog(String errorLog) {
        this.errorLog = errorLog;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
