package com.jadu.model;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("CUSTOMER")
public class Customer extends User implements Serializable{
}
