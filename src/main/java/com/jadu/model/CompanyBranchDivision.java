package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "company_branch_division")
public class CompanyBranchDivision implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "company")
	private String company;
	@Column(name = "state")
	private String state;
	@Column(name = "division")
	private String division;
	@Index(name = "branch")
	@Column(name = "branch")
	private String branch;
	@Column(name = "zone")
	private String zone;

	@Column(name = "email")
	private String email;

	@JsonIgnore
	@Column(name = "default_agent")
	private String defaultAgent;

	@JsonIgnore
	@Column(name = "api_key")
	private String apiKey;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getDefaultAgent() {
		return defaultAgent;
	}

	public void setDefaultAgent(String defaultAgent) {
		this.defaultAgent = defaultAgent;
	}

	@Override
	public String toString() {
		return "CompanyBranchDivision [id=" + id + ", company=" + company + ", state=" + state + ", division="
				+ division + ", branch=" + branch + ", zone=" + zone + ", email=" + email + ", defaultAgent="
				+ defaultAgent + ", apiKey=" + apiKey + "]";
	}

}
