package com.jadu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Index;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;
import javax.persistence.OneToMany;

@Entity
@Table(name = "loss_assessment")
public class LossAssessment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.DETACH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "claim_case_id", nullable = false)
    @Index(name = "claim_case_id")
    private ClaimCase claimCase;

    @ManyToOne(cascade = {CascadeType.DETACH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "part_particular_type_id", nullable = false)
    private VehiclePartParticularType vehiclePartParticularType;

    @ManyToOne(cascade = {CascadeType.DETACH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "damage_type_id", nullable = false)
    private VehicleDamageType vehicleDamageType;
    
    @ManyToOne(cascade = {CascadeType.DETACH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "replacement_id", nullable = true)
    private ReplacementProduct replacementProduct;

    @ManyToOne(cascade = {CascadeType.DETACH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "repair_id", nullable = true)
    private VehicleRepairType vehicleRepairType;

    @Column(name = "part_cost")
    private double partCost;

    @Column(name = "labour_cost")
    private double labourCost;

    @Column(name = "paint_cost")
    private double paintCost;

    @Column(name = "rate_of_dep_for_part")
    private double depRateForPart;

    @Column(name = "rate_of_gst_for_part")
    private double gstRateForPart;

    @Column(name = "rate_of_dep_for_labour")
    private double depRateForLabour;

    @Column(name = "rate_of_gst_for_labour")
    private double gstRateForLabour;

    @Column(name = "rate_of_dep_for_paint")
    private double depRateForPaint;

    @Column(name = "rate_of_gst_for_paint")
    private double gstRateForPaint;

    @Column(name = "created_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "updated_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updatedDate;
    
    @OneToMany(mappedBy = "lossAssessment", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    private Set<LossAssessmentPhotos> lossAssessmentPhotos;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getPartCost() {
        return partCost;
    }

    public void setPartCost(double partCost) {
        this.partCost = partCost;
    }

    public double getLabourCost() {
        return labourCost;
    }

    public void setLabourCost(double labourCost) {
        this.labourCost = labourCost;
    }

    public double getDepRateForPart() {
        return depRateForPart;
    }

    public void setDepRateForPart(double depRateForPart) {
        this.depRateForPart = depRateForPart;
    }

    public double getGstRateForPart() {
        return gstRateForPart;
    }

    public void setGstRateForPart(double gstRateForPart) {
        this.gstRateForPart = gstRateForPart;
    }

    public double getDepRateForLabour() {
        return depRateForLabour;
    }

    public void setDepRateForLabour(double depRateForLabour) {
        this.depRateForLabour = depRateForLabour;
    }

    public double getGstRateForLabour() {
        return gstRateForLabour;
    }

    public void setGstRateForLabour(double gstRateForLabour) {
        this.gstRateForLabour = gstRateForLabour;
    }

    public ClaimCase getClaimCase() {
        return claimCase;
    }

    public void setClaimCase(ClaimCase claimCase) {
        this.claimCase = claimCase;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public VehiclePartParticularType getVehiclePartParticularType() {
        return vehiclePartParticularType;
    }

    public void setVehiclePartParticularType(VehiclePartParticularType vehiclePartParticularType) {
        this.vehiclePartParticularType = vehiclePartParticularType;
    }

    public VehicleDamageType getVehicleDamageType() {
        return vehicleDamageType;
    }

    public void setVehicleDamageType(VehicleDamageType vehicleDamageType) {
        this.vehicleDamageType = vehicleDamageType;
    }

    public VehicleRepairType getVehicleRepairType() {
        return vehicleRepairType;
    }

    public void setVehicleRepairType(VehicleRepairType vehicleRepairType) {
        this.vehicleRepairType = vehicleRepairType;
    }

    public double getPaintCost() {
        return paintCost;
    }

    public void setPaintCost(double paintCost) {
        this.paintCost = paintCost;
    }

    public double getDepRateForPaint() {
        return depRateForPaint;
    }

    public void setDepRateForPaint(double depRateForPaint) {
        this.depRateForPaint = depRateForPaint;
    }

    public double getGstRateForPaint() {
        return gstRateForPaint;
    }

    public void setGstRateForPaint(double gstRateForPaint) {
        this.gstRateForPaint = gstRateForPaint;
    }

    public Set<LossAssessmentPhotos> getLossAssessmentPhotos() {
        return lossAssessmentPhotos;
    }

    public void setLossAssessmentPhotos(Set<LossAssessmentPhotos> lossAssessmentPhotos) {
        this.lossAssessmentPhotos = lossAssessmentPhotos;
        for(LossAssessmentPhotos lap : lossAssessmentPhotos)
            lap.setLossAssessment(this);
    }

    public ReplacementProduct getReplacementProduct() {
        return replacementProduct;
    }

    public void setReplacementProduct(ReplacementProduct replacementProduct) {
        this.replacementProduct = replacementProduct;
    }
}
