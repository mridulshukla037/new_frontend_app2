package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "insurance_details")
public class InsuranceDetails implements Serializable {
    
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        private Long id;
    
	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "claim_id", nullable = false)
	private ClaimCase claimCase;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "insurer_id")
	private InsuranceCompany insurer;

	@Index(name = "financer")
	@Column(name = "financer")
	private String financer;

	@Index(name = "policy_number")
	@Column(name = "policy_number")
	private String policyNumber;

	@Column(name = "start_date")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date startDate;

	@Column(name = "end_date")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date endDate;

	@Column(name = "insured_name")
	private String insuredName;

	@Column(name = "insured_address")
	private String insuredAddress;

	@Column(name = "insured_mobile_number")
	private String insuredMobileNumber;

	@Column(name = "idv")
	private Integer idv;

	@Column(name = "sum_insured")
	private Integer sumInsured;

	@Column(name = "remarks")
	private String remarks;

    public ClaimCase getClaimCase() {
        return claimCase;
    }

    public void setClaimCase(ClaimCase claimCase) {
        this.claimCase = claimCase;
    }
    
	public InsuranceCompany getInsurer() {
		return insurer;
	}

	public void setInsurer(InsuranceCompany insurer) {
		this.insurer = insurer;
	}

	public String getFinancer() {
		return financer;
	}

	public void setFinancer(String financer) {
		this.financer = financer;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getInsuredName() {
		return insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public String getInsuredAddress() {
		return insuredAddress;
	}

	public void setInsuredAddress(String insuredAddress) {
		this.insuredAddress = insuredAddress;
	}

	public String getInsuredMobileNumber() {
		return insuredMobileNumber;
	}

	public void setInsuredMobileNumber(String insuredMobileNumber) {
		this.insuredMobileNumber = insuredMobileNumber;
	}

	public Integer getIdv() {
		return idv;
	}

	public void setIdv(Integer idv) {
		this.idv = idv;
	}

	public Integer getSumInsured() {
		return sumInsured;
	}

	public void setSumInsured(Integer sumInsured) {
		this.sumInsured = sumInsured;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
