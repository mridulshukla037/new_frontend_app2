package com.jadu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "kyc")
public class KYC implements Serializable {

	@Id
	@Column(name = "username", unique = true, nullable = false)
	@GeneratedValue(generator = "gen")
	@GenericGenerator(name = "gen", strategy = "foreign", parameters = @Parameter(name = "property", value = "user"))
	private String username;

	@Column(name = "uid")
	private String uid;

	@Column(name = "name")
	private String name;

	@Column(name = "gender")
	private String gender;

	@Column(name = "yob")
	private int yob;

	@Column(name = "co")
	private String co;

	@Column(name = "house")
	private String house;

	@Column(name = "street")
	private String street;

	@Column(name = "lm")
	private String lm;

	@Column(name = "loc")
	private String loc;

	@Column(name = "vtc")
	private String vtc;

	@Column(name = "po")
	private String po;

	@Column(name = "dist")
	private String dist;

	@Column(name = "subdist")
	private String subdist;

	@Column(name = "state")
	private String state;

	@Column(name = "pc")
	private String pc;

	@Column(name = "dob")
	private String dob;

	@Column(name = "image_url")
	private String imageUrl;

	@JsonIgnore
	@OneToOne
	@PrimaryKeyJoinColumn
	private User user;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getYob() {
		return yob;
	}

	public void setYob(int yob) {
		this.yob = yob;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCo() {
		return co;
	}

	public void setCo(String co) {
		this.co = co;
	}

	public String getHouse() {
		return house;
	}

	public void setHouse(String house) {
		this.house = house;
	}

	public String getVtc() {
		return vtc;
	}

	public void setVtc(String vtc) {
		this.vtc = vtc;
	}

	public String getPo() {
		return po;
	}

	public void setPo(String po) {
		this.po = po;
	}

	public String getDist() {
		return dist;
	}

	public void setDist(String dist) {
		this.dist = dist;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPc() {
		return pc;
	}

	public void setPc(String pc) {
		this.pc = pc;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getLm() {
		return lm;
	}

	public void setLm(String lm) {
		this.lm = lm;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}

	public String getSubdist() {
		return subdist;
	}

	public void setSubdist(String subdist) {
		this.subdist = subdist;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "KYC [username=" + username + ", uid=" + uid + ", name=" + name + ", gender=" + gender + ", yob=" + yob
				+ ", co=" + co + ", house=" + house + ", street=" + street + ", lm=" + lm + ", loc=" + loc + ", vtc="
				+ vtc + ", po=" + po + ", dist=" + dist + ", subdist=" + subdist + ", state=" + state + ", pc=" + pc
				+ ", dob=" + dob + ", imageUrl=" + imageUrl + ", user=" + user + "]";
	}

}
