package com.jadu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "survey_fee_bill")
public class SurveyFeeBill implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Index(name = "claim_number")
	@Column(name = "claim_number")
	private String claimNumber;

	@Column(name = "reference_number")
	private String referenceNumber;

	@Column(name = "bill_number")
	private String billNumber;

	@Index(name = "claim_case_id")
	@Column(name = "claim_case_id")
	private long claimCaseId;

	@Column(name = "professional_fee")
	private double professionalFee;

	@Column(name = "cgst")
	private double cgst;

	@Column(name = "sgst")
	private double sgst;

	@Column(name = "total")
	private double total;

	@Column(name = "created_date")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "updated_date")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date updatedDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClaimNumber() {
		return claimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public long getClaimCaseId() {
		return claimCaseId;
	}

	public void setClaimCaseId(long claimCaseId) {
		this.claimCaseId = claimCaseId;
	}

	public double getProfessionalFee() {
		return professionalFee;
	}

	public void setProfessionalFee(double professionalFee) {
		this.professionalFee = professionalFee;
	}

	public double getCgst() {
		return cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getSgst() {
		return sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "SurveyFeeBill [id=" + id + ", claimNumber=" + claimNumber + ", referenceNumber=" + referenceNumber
				+ ", billNumber=" + billNumber + ", claimCaseId=" + claimCaseId + ", professionalFee=" + professionalFee
				+ ", cgst=" + cgst + ", sgst=" + sgst + ", total=" + total + ", createdDate=" + createdDate
				+ ", updatedDate=" + updatedDate + "]";
	}

}
