package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="vehicle_sub_types")
public class VehicleSubType implements Serializable {
    @Id
    @Column(name="id")
    private String id;
    
    @Column(name="name")
    private String name;
    
    @JsonIgnore   
    @ManyToOne
    @JoinColumn(name="vehicle_type", nullable=false)
    private VehicleType vehicleType ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

	@Override
	public String toString() {
		return "VehicleSubType [id=" + id + ", name=" + name + ", vehicleType=" + vehicleType + "]";
	}
    
    
    
    
}
