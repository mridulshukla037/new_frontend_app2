package com.jadu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "cases_system_call_status")
public class CasesCallStatus implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Index(name = "case_id")
	@Column(name = "case_id")
	private long caseId;

	@Index(name = "callSid")
	@Column(name = "callSid")
	private String callSid;

	@Column(name = "flow_id")
	private String flowId;

	@Column(name = "parent_call_sid")
	private String parentCallSid;

	@Column(name = "end_time")
	private Date endTime;

	@Column(name = "start_time")
	private Date startTime;

	@Column(name = "account_sid")
	private String accountSid;

	@Column(name = "to_receiver")
	private String to;

	@Column(name = "from_caller")
	private String from;

	@Column(name = "phonenumber_sid")
	private String phoneNumberSid;

	@Column(name = "status")
	private String status;

	@Column(name = "callback_status")
	private String callbackStatus;

	@Column(name = "recordingUrl")
	private String recordingUrl;

	@Column(name = "date_updated")
	private Date dateUpdated;

	@Column(name = "date_created")
	private Date dateCreated;

	@Column(name = "follow_up_time")
	private Date followUpTime;

	@Column(name = "call_type")
	private String callType;

	@Column(name = "duration")
	private String duration;

	@Column(name = "price")
	private float price;

	@Column(name = "direction")
	private String direction;

	@Column(name = "answered_by")
	private String answeredBy;

	@Column(name = "forwarded_from")
	private String forwardedFrom;

	@Column(name = "caller_name")
	private String callerName;

	@Column(name = "uri")
	private String uri;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCaseId() {
		return caseId;
	}

	public void setCaseId(long caseId) {
		this.caseId = caseId;
	}

	public String getCallSid() {
		return callSid;
	}

	public void setCallSid(String callSid) {
		this.callSid = callSid;
	}

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getAccountSid() {
		return accountSid;
	}

	public void setAccountSid(String accountSid) {
		this.accountSid = accountSid;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getFollowUpTime() {
		return followUpTime;
	}

	public void setFollowUpTime(Date followUpTime) {
		this.followUpTime = followUpTime;
	}

	public String getCallType() {
		return callType;
	}

	public void setCallType(String callType) {
		this.callType = callType;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getAnsweredBy() {
		return answeredBy;
	}

	public void setAnsweredBy(String answeredBy) {
		this.answeredBy = answeredBy;
	}

	public String getForwardedFrom() {
		return forwardedFrom;
	}

	public void setForwardedFrom(String forwardedFrom) {
		this.forwardedFrom = forwardedFrom;
	}

	public String getCallerName() {
		return callerName;
	}

	public void setCallerName(String callerName) {
		this.callerName = callerName;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getParentCallSid() {
		return parentCallSid;
	}

	public void setParentCallSid(String parentCallSid) {
		this.parentCallSid = parentCallSid;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getPhoneNumberSid() {
		return phoneNumberSid;
	}

	public void setPhoneNumberSid(String phoneNumberSid) {
		this.phoneNumberSid = phoneNumberSid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRecordingUrl() {
		return recordingUrl;
	}

	public void setRecordingUrl(String recordingUrl) {
		this.recordingUrl = recordingUrl;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getCallbackStatus() {
		return callbackStatus;
	}

	public void setCallbackStatus(String callbackStatus) {
		this.callbackStatus = callbackStatus;
	}

	@Override
	public String toString() {
		return "CasesCallStatus [id=" + id + ", caseId=" + caseId + ", callSid=" + callSid + ", flowId=" + flowId
				+ ", parentCallSid=" + parentCallSid + ", endTime=" + endTime + ", startTime=" + startTime
				+ ", accountSid=" + accountSid + ", to=" + to + ", from=" + from + ", phoneNumberSid=" + phoneNumberSid
				+ ", status=" + status + ", callbackStatus=" + callbackStatus + ", recordingUrl=" + recordingUrl
				+ ", dateUpdated=" + dateUpdated + ", dateCreated=" + dateCreated + ", followUpTime=" + followUpTime
				+ ", callType=" + callType + ", duration=" + duration + ", price=" + price + ", direction=" + direction
				+ ", answeredBy=" + answeredBy + ", forwardedFrom=" + forwardedFrom + ", callerName=" + callerName
				+ ", uri=" + uri + "]";
	}

}
