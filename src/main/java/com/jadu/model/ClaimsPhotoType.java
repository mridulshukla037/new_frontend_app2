package com.jadu.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "claim_photo_types")
public class ClaimsPhotoType implements Serializable {
	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "group_name")
	private String groupName;

	@Column(name = "multiple")
	private Boolean multiple;

	@Column(name = "optional")
	private Boolean optional;

	@Column(name = "photo_order")
	private String photoOrder;

	@Column(name = "input_type")
	private String inputType;

	@Column(name = "angle")
	private String angle;

	@Column(name = "show_in_report")
	private Boolean showInReport;

	@OneToMany(mappedBy = "photoType", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private Set<PhotoTag> photoTags;

	@OneToMany(mappedBy = "photoType", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private Set<PhotoOption> photoOptions;

	@Column(name = "voice_over_text")
	private String voiceOverText;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Boolean getMultiple() {
		return multiple;
	}

	public void setMultiple(Boolean multiple) {
		this.multiple = multiple;
	}

	public Boolean getOptional() {
		return optional;
	}

	public void setOptional(Boolean optional) {
		this.optional = optional;
	}

	public String getPhotoOrder() {
		return photoOrder;
	}

	public void setPhotoOrder(String photoOrder) {
		this.photoOrder = photoOrder;
	}

	public Set<PhotoTag> getPhotoTags() {
		return photoTags;
	}

	public void setPhotoTags(Set<PhotoTag> photoTags) {
		this.photoTags = photoTags;
	}

	public Set<PhotoOption> getPhotoOptions() {
		return photoOptions;
	}

	public void setPhotoOptions(Set<PhotoOption> photoOptions) {
		this.photoOptions = photoOptions;
	}

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	public String getAngle() {
		return angle;
	}

	public void setAngle(String angle) {
		this.angle = angle;
	}

	public String getVoiceOverText() {
		return voiceOverText;
	}

	public void setVoiceOverText(String voiceOverText) {
		this.voiceOverText = voiceOverText;
	}

	public Boolean getShowInReport() {
		return showInReport;
	}

	public void setShowInReport(Boolean showInReport) {
		this.showInReport = showInReport;
	}

}
