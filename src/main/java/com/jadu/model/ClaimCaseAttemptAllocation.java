package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "claim_case_attempt_allocations")
public class ClaimCaseAttemptAllocation implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "case_id", nullable = false)
	private ClaimCase claimCase;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private Inspector inspector;

	@Column(name = "attempt_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date attemptTime;

	@Column(name = "attempt_expiry_time")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date attemptExpiryTime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ClaimCase getClaimCase() {
		return claimCase;
	}

	public void setClaimCase(ClaimCase claimCase) {
		this.claimCase = claimCase;
	}

	public Inspector getInspector() {
		return inspector;
	}

	public void setInspector(Inspector inspector) {
		this.inspector = inspector;
	}

	public Date getAttemptTime() {
		return attemptTime;
	}

	public void setAttemptTime(Date attemptTime) {
		this.attemptTime = attemptTime;
	}

	public Date getAttemptExpiryTime() {
		return attemptExpiryTime;
	}

	public void setAttemptExpiryTime(Date attemptExpiryTime) {
		this.attemptExpiryTime = attemptExpiryTime;
	}

	@Override
	public String toString() {
		return "ClaimCaseAttemptAllocation [id=" + id + ", claimCase=" + claimCase + ", inspector=" + inspector
				+ ", attemptTime=" + attemptTime + ", attemptExpiryTime=" + attemptExpiryTime + "]";
	}

}
