package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;


@Entity
@Table(name = "accident_details")
public class AccidentDetails implements Serializable {
    
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        private Long id;
    
	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "claim_id", nullable = false)
	private ClaimCase claimCase;

	@Column(name = "voice_summary")
	private String voiceSummary;

	@Column(name = "place_of_accident")
	private String accidentLocation;

	@Column(name = "date_of_accident")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date dateOfAccident;

	@Column(name = "date_of_survey")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date dateOfSurvey;

	@Column(name = "date_of_intimation")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date dateOfIntimation;

	@Column(name = "license_number")
	private String licenseNumber;

	@Column(name = "place_of_survey")
	private String locationOfSurvey;

	@Column(name = "third_party_damage")
	private Boolean thirdPartyDamage;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "third_party_damage_type_id")
	private ThirdPartyDamageType thirdPartyDamageType;

	@Column(name = "reported_to_police")
	private Boolean reportedToPolice;

	@Column(name = "name_of_police_station")
	private String policeStationName;

	@Column(name = "station_diary_entry_number")
	private String stationDiaryEntryNumber;

	@Column(name = "punchnama_carried_out")
	private Boolean punchnamaCarriedOut;

	@Column(name = "accident_remarks")
	private String accidentRemarks;

	@Column(name = "third_party_damage_remarks")
	private String thirdPartyDamageRemarks;

	@Column(name = "other_remarks")
	private String otherRemarks;
	
	
	@Column(name = "remarks")
	private String remarks;

    public ClaimCase getClaimCase() {
        return claimCase;
    }

    public void setClaimCase(ClaimCase claimCase) {
        this.claimCase = claimCase;
    }

    public String getVoiceSummary() {
        return voiceSummary;
    }

    public void setVoiceSummary(String voiceSummary) {
        this.voiceSummary = voiceSummary;
    }

	

	

	public String getAccidentLocation() {
		return accidentLocation;
	}

	public void setAccidentLocation(String accidentLocation) {
		this.accidentLocation = accidentLocation;
	}

	public Date getDateOfAccident() {
		return dateOfAccident;
	}

	public void setDateOfAccident(Date dateOfAccident) {
		this.dateOfAccident = dateOfAccident;
	}

	public Date getDateOfSurvey() {
		return dateOfSurvey;
	}

	public void setDateOfSurvey(Date dateOfSurvey) {
		this.dateOfSurvey = dateOfSurvey;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getLocationOfSurvey() {
		return locationOfSurvey;
	}

	public void setLocationOfSurvey(String locationOfSurvey) {
		this.locationOfSurvey = locationOfSurvey;
	}

	public Date getDateOfIntimation() {
		return dateOfIntimation;
	}

	public void setDateOfIntimation(Date dateOfIntimation) {
		this.dateOfIntimation = dateOfIntimation;
	}

	public Boolean getThirdPartyDamage() {
		return thirdPartyDamage;
	}

	public void setThirdPartyDamage(Boolean thirdPartyDamage) {
		this.thirdPartyDamage = thirdPartyDamage;
	}

    public ThirdPartyDamageType getThirdPartyDamageType() {
        return thirdPartyDamageType;
    }

    public void setThirdPartyDamageType(ThirdPartyDamageType thirdPartyDamageType) {
        this.thirdPartyDamageType = thirdPartyDamageType;
    }

	

	public Boolean getReportedToPolice() {
		return reportedToPolice;
	}

	public void setReportedToPolice(Boolean reportedToPolice) {
		this.reportedToPolice = reportedToPolice;
	}

	public String getPoliceStationName() {
		return policeStationName;
	}

	public void setPoliceStationName(String policeStationName) {
		this.policeStationName = policeStationName;
	}

	public String getStationDiaryEntryNumber() {
		return stationDiaryEntryNumber;
	}

	public void setStationDiaryEntryNumber(String stationDiaryEntryNumber) {
		this.stationDiaryEntryNumber = stationDiaryEntryNumber;
	}

	public Boolean getPunchnamaCarriedOut() {
		return punchnamaCarriedOut;
	}

	public void setPunchnamaCarriedOut(Boolean punchnamaCarriedOut) {
		this.punchnamaCarriedOut = punchnamaCarriedOut;
	}

	public String getAccidentRemarks() {
		return accidentRemarks;
	}

	public void setAccidentRemarks(String accidentRemarks) {
		this.accidentRemarks = accidentRemarks;
	}

	public String getThirdPartyDamageRemarks() {
		return thirdPartyDamageRemarks;
	}

	public void setThirdPartyDamageRemarks(String thirdPartyDamageRemarks) {
		this.thirdPartyDamageRemarks = thirdPartyDamageRemarks;
	}

	public String getOtherRemarks() {
		return otherRemarks;
	}

	public void setOtherRemarks(String otherRemarks) {
		this.otherRemarks = otherRemarks;
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
    
    
}
