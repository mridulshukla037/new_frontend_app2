package com.jadu.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "users")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE) // Least normalisation strategy
@DiscriminatorColumn(name = "user_type", discriminatorType = DiscriminatorType.STRING)
public class User implements Serializable {
	@Id
	@Column(name = "username")
	@Index(name = "username")
	private String username;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@JsonIgnore
	@Column(name = "password")
	private String password;

	@Column(name = "enabled")
	private boolean enabled;

	@Column(name = "can_update")
	private boolean canUpdate;

	@Column(name = "phone_number")
	@Index(name = "phone_number")
	private String phoneNumber;

	@Column(name = "email")
	@Index(name = "email")
	private String email;

	@Column(name = "password_change_required")
	private boolean passwordChangeRequired;

	@Column(name = "email_verified")
	private boolean emailVerified;

	@Column(name = "phone_number_verified")
	private boolean phoneNumberVerified;

	@Column(name = "profile_photo_url")
	private String profilePhotoUrl;

	@Column(name = "profile_photo_url_thumb")
	private String profilePhotoUrlThumb;

	@OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
	private Authority authority;

	@Column(name = "device_locked")
	private boolean deviceLocked;

	@Column(name = "device_id")
	private String deviceId;

	@Index(name = "date_of_creation")
	@Column(name = "date_of_creation")
	private Date createdDate = new Date();

	@Index(name = "garage_companies_id")
	@Column(name = "garage_companies_id")
	private String garageCompaniesId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isPasswordChangeRequired() {
		return passwordChangeRequired;
	}

	public void setPasswordChangeRequired(boolean passwordChangeRequired) {
		this.passwordChangeRequired = passwordChangeRequired;
	}

	public boolean isEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(boolean emailVerified) {
		this.emailVerified = emailVerified;
	}

	public boolean isPhoneNumberVerified() {
		return phoneNumberVerified;
	}

	public void setPhoneNumberVerified(boolean phoneNumberVerified) {
		this.phoneNumberVerified = phoneNumberVerified;
	}

	public String isProfilePhotoUrl() {
		return profilePhotoUrl;
	}

	public void setProfilePhotoUrl(String profilePhotoUrl) {
		this.profilePhotoUrl = profilePhotoUrl;
	}

	public Authority getAuthority() {
		return authority;
	}

	public void setAuthority(Authority authority) {
		this.authority = authority;
	}

	public String getProfilePhotoUrl() {
		return profilePhotoUrl;
	}

	public String getProfilePhotoRelativeUrl() {
		return "/util/profile-image/" + profilePhotoUrl;
	}

	public boolean isDeviceLocked() {
		return deviceLocked;
	}

	public void setDeviceLocked(boolean deviceLocked) {
		this.deviceLocked = deviceLocked;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getProfilePhotoUrlThumb() {
		return profilePhotoUrlThumb;
	}

	public void setProfilePhotoUrlThumb(String profilePhotoUrlThumb) {
		this.profilePhotoUrlThumb = profilePhotoUrlThumb;
	}

	public boolean isCanUpdate() {
		return canUpdate;
	}

	public void setCanUpdate(boolean canUpdate) {
		this.canUpdate = canUpdate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@PrePersist
	public void initCreatedDate() {
		if (createdDate == null) {
			createdDate = new Date();
		}
	}

	public String getGarageCompaniesId() {
		return garageCompaniesId;
	}

	public void setGarageCompaniesId(String garageCompaniesId) {
		this.garageCompaniesId = garageCompaniesId;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", firstName=" + firstName + ", lastName=" + lastName + ", password="
				+ password + ", enabled=" + enabled + ", canUpdate=" + canUpdate + ", phoneNumber=" + phoneNumber
				+ ", email=" + email + ", passwordChangeRequired=" + passwordChangeRequired + ", emailVerified="
				+ emailVerified + ", phoneNumberVerified=" + phoneNumberVerified + ", profilePhotoUrl="
				+ profilePhotoUrl + ", profilePhotoUrlThumb=" + profilePhotoUrlThumb + ", authority=" + authority
				+ ", deviceLocked=" + deviceLocked + ", deviceId=" + deviceId + ", createdDate=" + createdDate
				+ ", garageCompaniesId=" + garageCompaniesId + "]";
	}

}
