package com.jadu.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="cases_qc_questions")
public class CaseQCQuestion implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    
    @Column(name="value")
    private String value;
    
    @Column(name="tag")
    private String photoTag;
    
    
    @ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY)
    @JoinColumn(name="vehicle_type")
    private VehicleType vehicleType;
    
    @ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.LAZY)
    @JoinColumn(name="question_group")
    private CaseQCQuestionGroup caseQCQuestionGroup;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public CaseQCQuestionGroup getCaseQCQuestionGroup() {
        return caseQCQuestionGroup;
    }

    public void setCaseQCQuestionGroup(CaseQCQuestionGroup caseQCQuestionGroup) {
        this.caseQCQuestionGroup = caseQCQuestionGroup;
    }

    public String getPhotoTag() {
        return photoTag;
    }

    public void setPhotoTag(String photoTag) {
        this.photoTag = photoTag;
    }
    
    
}
