package com.jadu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "vehicle_details")
@JsonIgnoreProperties(ignoreUnknown = true)
public class VehicleDetails implements Serializable {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        private Long id;
        
        @JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "claim_id", nullable = false)
	private ClaimCase claimCase;

	@Index(name = "registered_owner_name")
	@Column(name = "registered_owner_name")
	private String registeredOwnerName;

	@Index(name = "chassis_number")
	@Column(name = "chassis_number")
	private String chassisNumber;

	@Index(name = "engine_number")
	@Column(name = "engine_number")
	private String engineNumber;

	@Index(name = "permit_number")
	@Column(name = "permit_number")
	private String permitNumber;

	@ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "make_model_variant_id")
	private MakeModelVariant makeModelVariant;
        
        @ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "permit_type_id")
	private VehiclePermitType vehiclePermitType;
        
        @ManyToOne(cascade = { CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "body_type_id")
	private VehicleBodyType vehicleBodyType;

	@Column(name = "fitness_certificate_number")
	private String fitnessCertificateNumber;

	@Column(name = "passenger_carrying_capacity")
	private String passengerCarryingCapacity;

	@Column(name = "date_of_registration")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date dateOfRegistration;

	@Column(name = "valid_upto")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date validUpto;

	@Column(name = "tax_paid_upto")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date taxPaidUpto;

	@Column(name = "remarks")
	private String remarks;
        
        @Column(name = "vehicle_color")
	private String vehicleColor;
	
        @Column(name = "vehicle_yom")
	private int vehicleYOM;
        
        @Column(name = "odometer_reading")
	private String odometerReading = "0";

	public String getRegisteredOwnerName() {
		return registeredOwnerName;
	}

	public void setRegisteredOwnerName(String registeredOwnerName) {
		this.registeredOwnerName = registeredOwnerName;
	}

	public String getChassisNumber() {
		return chassisNumber;
	}

	public void setChassisNumber(String chassisNumber) {
		this.chassisNumber = chassisNumber;
	}

	public String getEngineNumber() {
		return engineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}

	public String getPermitNumber() {
		return permitNumber;
	}

	public void setPermitNumber(String permitNumber) {
		this.permitNumber = permitNumber;
	}

	public String getFitnessCertificateNumber() {
		return fitnessCertificateNumber;
	}

	public void setFitnessCertificateNumber(String fitnessCertificateNumber) {
		this.fitnessCertificateNumber = fitnessCertificateNumber;
	}

	public String getPassengerCarryingCapacity() {
		return passengerCarryingCapacity;
	}

	public void setPassengerCarryingCapacity(String passengerCarryingCapacity) {
		this.passengerCarryingCapacity = passengerCarryingCapacity;
	}

	public Date getDateOfRegistration() {
		return dateOfRegistration;
	}

	public void setDateOfRegistration(Date dateOfRegistration) {
		this.dateOfRegistration = dateOfRegistration;
	}

	public Date getValidUpto() {
		return validUpto;
	}

	public void setValidUpto(Date validUpto) {
		this.validUpto = validUpto;
	}

	public Date getTaxPaidUpto() {
		return taxPaidUpto;
	}

	public void setTaxPaidUpto(Date taxPaidUpto) {
		this.taxPaidUpto = taxPaidUpto;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

    public ClaimCase getClaimCase() {
        return claimCase;
    }

    public void setClaimCase(ClaimCase claimCase) {
        this.claimCase = claimCase;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VehicleBodyType getVehicleBodyType() {
        return vehicleBodyType;
    }

    public void setVehicleBodyType(VehicleBodyType vehicleBodyType) {
        this.vehicleBodyType = vehicleBodyType;
    }

    public MakeModelVariant getMakeModelVariant() {
        return makeModelVariant;
    }

    public void setMakeModelVariant(MakeModelVariant makeModelVariant) {
        this.makeModelVariant = makeModelVariant;
    }

    public VehiclePermitType getVehiclePermitType() {
        return vehiclePermitType;
    }

    public void setVehiclePermitType(VehiclePermitType vehiclePermitType) {
        this.vehiclePermitType = vehiclePermitType;
    }

    public String getVehicleColor() {
        return vehicleColor;
    }

    public void setVehicleColor(String vehicleColor) {
        this.vehicleColor = vehicleColor;
    }

    public int getVehicleYOM() {
        return vehicleYOM;
    }

    public void setVehicleYOM(int vehicleYOM) {
        this.vehicleYOM = vehicleYOM;
    }

    public String getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(String odometerReading) {
        this.odometerReading = odometerReading;
    }
        
        
}
