package com.jadu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "inspector_details")
public class InspectorDetail implements Serializable {
	@Id
	@Column(name = "username", unique = true, nullable = false)
	@GeneratedValue(generator = "gen")
	@GenericGenerator(name = "gen", strategy = "foreign", parameters = @Parameter(name = "property", value = "inspector"))
	private String username;

	@Column(name = "dob")
	private String dateOfBirth;

	@Column(name = "pin_code")
	private String pinCode;

	@Column(name = "state_id")
	private Integer state;

	@Column(name = "division_id")
	private Integer division;

	@JsonIgnore
	@OneToOne
	@PrimaryKeyJoinColumn
	private Inspector inspector;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Inspector getInspector() {
		return inspector;
	}

	public void setInspector(Inspector inspector) {
		this.inspector = inspector;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getDivision() {
		return division;
	}

	public void setDivision(Integer division) {
		this.division = division;
	}

	@Override
	public String toString() {
		return "InspectorDetail [username=" + username + ", dateOfBirth=" + dateOfBirth + ", pinCode=" + pinCode
				+ ", state=" + state + ", division=" + division + ", inspector=" + inspector + "]";
	}

}
