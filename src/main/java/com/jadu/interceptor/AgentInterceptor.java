/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.interceptor;

import com.jadu.dao.UserDAOImpl;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author gauta
 */
public class AgentInterceptor extends HandlerInterceptorAdapter {
    
    @Autowired
    UserDAOImpl userDAO;
    
    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User  p = (User)authentication.getPrincipal();
        
        com.jadu.model.User user = userDAO.getUserByUsername(p.getUsername());
        
        
        
        if(!user.isEnabled()){
            response.setStatus( 403 );
            throw new Exception("ACCOUNT_DISABLED");
        }
        
        if(user.isPasswordChangeRequired()){
            response.setStatus( 403 );
            throw new Exception("PASSWORD_CHANGE_REQUIRED");
        }
        
        if(!user.isPhoneNumberVerified()){
            response.setStatus( 403 );
            throw new Exception("VALIDATE_PHONE");
        }
        
        if(!user.isEmailVerified()){
            response.setStatus( 403 );
            throw new Exception("VALIDATE_EMAIL");
        }
        
        return true;
    }
}
