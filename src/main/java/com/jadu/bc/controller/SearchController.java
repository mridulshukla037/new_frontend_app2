package com.jadu.bc.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.CaseCommentDAO;
import com.jadu.dao.ClaimCaseDAO;
import com.jadu.dao.InsuranceCompanyDAO;
import com.jadu.dto.AllCases;
import com.jadu.dto.AllUserDTO;
import com.jadu.model.Agent;
import com.jadu.model.ClaimCase;
import com.jadu.model.User;
import com.jadu.service.AppConfigService;
import com.jadu.service.UtilService;

@Controller
@RequestMapping("/search")
public class SearchController {

	@Autowired
	private UtilService utilService;

	@Autowired
	private ClaimCaseDAO claimCaseDAO;

	@Autowired
	private CaseCommentDAO caseCommentDAO;

	@Autowired
	private AgentDAOImpl agentDAO;

	@Autowired
	private AppConfigService appConfigService;
        
        @Autowired
        private InsuranceCompanyDAO insuranceCompanyDAO;

	private static final Logger LOGGER = LoggerFactory.getLogger(SearchController.class);

	@RequestMapping(value = "/cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<AllCases> searchCase(@RequestParam(value = "case_id", required = false) String caseId,
			@RequestParam(value = "vehicle_id", required = false) String vehicle_id,
			@RequestParam(value = "customer_phone", required = false) String customer_phone,
			@RequestParam(value = "requester_phone", required = false) String requester_phone) throws Exception {
		LOGGER.info(
				"/search/cases request recieved, parameter case_id={}, vehicle_id={}, customer_phone={}, requester_phone={}",
				caseId, vehicle_id, customer_phone, requester_phone);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = utilService.getUser(authentication);
		if (!"ROLE_ADMIN".equals(user.getAuthority().getAuthority()))
			return null;
		List<ClaimCase> casesList = new ArrayList<ClaimCase>();
		if (caseId != null) {
			casesList.add((ClaimCase) claimCaseDAO.getCaseById(Long.valueOf(caseId)));
		} else if (vehicle_id != null) {
			List<ClaimCase> cases = claimCaseDAO.getCaseByVehicleNumber(vehicle_id);
			if (cases != null && !cases.isEmpty())
				casesList.addAll(cases);
			else if (checkPartialSeachCriteria(vehicle_id)) {
				cases = claimCaseDAO.getCaseLikeVehicleNumber(vehicle_id);
				if (cases != null && !cases.isEmpty())
					casesList.addAll(cases);
			}
		} else if (customer_phone != null) {
			List<ClaimCase> cases = claimCaseDAO.getCaseByCustomerPhoneNumber(customer_phone);
			if (cases != null && !cases.isEmpty())
				casesList.addAll(cases);
			else if (checkPartialSeachCriteria(customer_phone)) {
				cases = claimCaseDAO.getCaseLikeCustomerPhoneNumber(customer_phone);
				if (cases != null && !cases.isEmpty())
					casesList.addAll(cases);
			}
		} else if (requester_phone != null) {
			List<ClaimCase> cases = claimCaseDAO.getCaseByAgentPhoneNumber(requester_phone);
			if (cases != null && !cases.isEmpty())
				casesList.addAll(cases);
			else if (checkPartialSeachCriteria(requester_phone)) {
				cases = claimCaseDAO.getCaseLikeAgentPhoneNumber(requester_phone);
				if (cases != null && !cases.isEmpty())
					casesList.addAll(cases);

			}
		} else {
			LOGGER.info("No valid input found case_id={}, vehicle_id={}, customer_phone={}, requester_phone={}", caseId,
					vehicle_id, customer_phone, requester_phone);
			throw new Exception("No valid input found");

		}
		if (casesList == null || casesList.isEmpty()) {
			LOGGER.info("Case not found for input case_id={}, vehicle_id={}, customer_phone={}, requester_phone={}",
					caseId, vehicle_id, customer_phone, requester_phone);
			throw new Exception("Case not found");
		}
		List<AllCases> allCasesList = new ArrayList<AllCases>();
		for (ClaimCase ic : casesList) {
			if (ic != null) {
				List commentList = caseCommentDAO.get(ic.getId());
				int countComments = commentList != null && !commentList.isEmpty() ? commentList.size() : 0;
				AllCases allCases = new AllCases(ic.getId(), ic.getVehicleNumber(), ic.getCustomerName(),
						ic.getCustomerPhoneNumber(), ic.getInspectionTime(), ic.getInspectionSubmitTime(),
						ic.getCurrentStage(), ic.getInspectionType(),
						insuranceCompanyDAO.getInsuranceCompanyByName(ic.getCompanyBranchDivision().getCompany()).getName(),
						insuranceCompanyDAO.getInsuranceCompanyByName(ic.getCompanyBranchDivision().getCompany()).getLogoUrl(),
						ic.getRequestor().getPhoneNumber(), ic.getRemark(), ic.getDownloadKey(), countComments);
				allCasesList.add(allCases);
			}
		}
		LOGGER.info(
				"Case found with details={} for input case_id={}, vehicle_id={}, customer_phone={}, requester_phone={}",
				allCasesList, caseId, vehicle_id, customer_phone, requester_phone);
		return allCasesList;
	}

	private boolean checkPartialSeachCriteria(String searchKey) {
		return appConfigService.getBooleanProperty("IS_PARTIAL_SEARCH_ON", true) && searchKey != null
				&& searchKey.length() >= appConfigService.getIntProperty("PARTIAL_SEARCH_MIN_CHAR_LENGTH", 4);
	}

	@RequestMapping(value = "/agents", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<AllUserDTO> searchAgent(@RequestParam(value = "contact", required = false) String contact,
			@RequestParam(value = "emailId", required = false) String emailId,
			@RequestParam(value = "agentId", required = false) String agentId,
			@RequestParam(value = "branch", required = false) String branch) throws Exception {
		LOGGER.info("/search/agents request recieved, parameter contact={}, emailId={}, agentId={}, branch={}", contact,
				emailId, agentId, branch);
		List<Agent> agentList = new ArrayList<Agent>();
		if (contact != null && !contact.isEmpty()) {
			List<Agent> agent = agentDAO.getAgentByPhoneNumber(contact);
			if (agent != null && !agent.isEmpty())
				agentList.addAll(agent);
			else if (checkPartialSeachCriteria(contact)) {
				agent = agentDAO.getAgentLikePhoneNumber(contact);
				if (agent != null && !agent.isEmpty())
					agentList.addAll(agent);
			}
		} else if (emailId != null && !emailId.isEmpty()) {
			List<Agent> agent = agentDAO.getAgentByEmail(emailId);
			if (agent != null && !agent.isEmpty())
				agentList.addAll(agent);
			else if (checkPartialSeachCriteria(emailId)) {
				agent = agentDAO.getAgentLikeEmail(emailId);
				if (agent != null && !agent.isEmpty())
					agentList.addAll(agent);
			}
		} else if (agentId != null && !agentId.isEmpty()) {
			List<Agent> agent = (List<Agent>) agentDAO.getAgentById(agentId);
			if (agent != null && !agent.isEmpty())
				agentList.addAll(agent);
			else if (checkPartialSeachCriteria(agentId)) {
				agent = (List<Agent>) agentDAO.getAgentLikeId(agentId);
				if (agent != null && !agent.isEmpty())
					agentList.addAll(agent);
			}
		} else if (branch != null && !branch.isEmpty()) {
			List<Agent> agent = agentDAO.getByBranch(branch);
			if (agent != null && !agent.isEmpty())
				agentList.addAll(agent);
		} else {
			LOGGER.info("No valid input found contact={}, emailId={}, agentId={}, branch={}", contact, emailId, agentId,
					branch);
			throw new Exception("No valid input found");

		}
		if (agentList == null || agentList.isEmpty()) {
			LOGGER.info("Agent not found contact={}, emailId={}, agentId={}, branch={}", contact, emailId, agentId,
					branch);
			throw new Exception("Agent not found");
		}
		List<AllUserDTO> agentDetails = new ArrayList<AllUserDTO>();
		for (Agent agent : agentList) {
			AllUserDTO allUserDTO = utilService.prepareAllUserDTOAndCache(agent, agent.getPhoneNumber());
			if (allUserDTO != null)
				agentDetails.add(allUserDTO);
		}
		LOGGER.info("Agent found with details={} for input contact={}, emailId={}, agentId={}, branch={}", agentDetails,
				contact, emailId, agentId, branch);
		return agentDetails;
	}

}
