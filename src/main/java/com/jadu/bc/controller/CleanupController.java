package com.jadu.bc.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jadu.dao.AuthorityDAOImpl;
import com.jadu.dao.ClaimCaseDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.model.User;
import com.jadu.service.AppConfigService;
import com.jadu.service.UtilService;

@Controller
@RequestMapping("/clean")
public class CleanupController {

	@Autowired
	private UtilService utilService;

	@Autowired
	private ClaimCaseDAO claimCaseDAO;

	@Autowired
	private AuthorityDAOImpl authorityDao;

	@Autowired
	private UserDAOImpl userDAO;

	@Autowired
	private AppConfigService appConfigService;

	private static final Logger LOGGER = LoggerFactory.getLogger(CleanupController.class);

	@RequestMapping(value = "/user", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public ResponseEntity<User> searchCase(@RequestParam(value = "phonenumber", required = false) String phonenumber,
			@RequestParam(value = "secretKey", required = true) String secretKey,
			@RequestParam(value = "role", required = true) String role) throws Exception {
		LOGGER.info("/clean/customer request recieved, parameter phonenumber={}, secretKey={}", phonenumber, secretKey);
		if (secretKey.equals(appConfigService.getProperty("APP_CONFIG_CLEANUP_API_PASSWORD", "magic@W!mwisure"))) {
			if (phonenumber != null && !phonenumber.isEmpty()) {
				List<User> userList = userDAO.getUserByPhoneNumber(phonenumber);
				if (userList != null && !userList.isEmpty()) {
					User user = userList.get(0);
					if (role.equalsIgnoreCase(user.getAuthority().getAuthority())) {
						try {
							userDAO.deleteUser(user);
							authorityDao.deleteAuthority(user.getAuthority());
							LOGGER.info("User={} has been deleted", phonenumber);
							return new ResponseEntity<>(user, HttpStatus.OK);
						} catch (Exception e) {
							e.printStackTrace();
							LOGGER.error("Something went wrong while deleting user={}, error={}", phonenumber,
									e.getMessage());
						}
					} else {
						return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);

					}
				}
			}
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}

}
