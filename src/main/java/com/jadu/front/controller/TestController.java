package com.jadu.front.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.CaseDAO;
import com.jadu.dao.CaseLogDAO;
import com.jadu.dao.CasePhotoDAO;
import com.jadu.dao.ClaimCaseDAO;
import com.jadu.dao.InsuranceCompanyDAO;
import com.jadu.dao.PurposeOfSurveyDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dto.CaseMakeDTO;
import com.jadu.dto.CasePhotoDTO2;
import com.jadu.dto.ClaimsFinalSubmissionDTO;
import com.jadu.dto.ScheduledCasesDTO;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.EncryptionHelper;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.CasesCallStatus;
import com.jadu.model.ClaimCase;
import com.jadu.model.CompanyBranchDivisionUser;
import com.jadu.model.InspectionCase;
import com.jadu.model.User;
import com.jadu.pdf.create.GenerateReport;
import com.jadu.push.notification.AndroidPushNotificationsService;
import com.jadu.service.AppConfigService;
import com.jadu.service.CaseFollowupService;
import com.jadu.service.CaseService;
import com.jadu.service.ClaimCaseService;
import com.jadu.service.ClaimsReportDataGeneratorFactory;
import com.jadu.service.ConnectCustomerToFlow;
import com.jadu.service.CreateUserService;
import com.jadu.service.MailService;
import com.jadu.service.S3DirectUploadService;
import com.jadu.service.S3Service;
import com.jadu.service.SmsService;
import com.jadu.service.URLShortnerService;
import freemarker.template.TemplateException;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/jadu-test")
public class TestController {

	@Autowired
	private AndroidPushNotificationsService androidPushNotificationsService;

	@Autowired
	private CaseDAO caseDAO;

	@Autowired
	private GenerateReport generateReportService;

	@Autowired
	private UserDAOImpl userDAO;

	@Autowired
	private MailService mailService;

	@Autowired
	private SmsService smsService;

	@Autowired
	private S3Service s3Service;

	@Autowired
	private S3DirectUploadService s3DirectUploadService;

	@Autowired
	private CaseService caseService;

	@Autowired
	private URLShortnerService urlShortnerService;

	@Autowired
	private ConnectCustomerToFlow connectCustomerToFlow;

	@Autowired
	private Environment env;

	@Autowired
	private CasePhotoDAO casePhotoDAO;

	@Autowired
	private ClaimCaseDAO claimCaseDAO;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private ClaimCaseService claimCaseService;

	@Autowired
	private CaseFollowupService caseFollowupService;

	@Autowired
	private ClaimsReportDataGeneratorFactory claimsReportDataGeneratorFactory;

	@RequestMapping(value = "/send-mail", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public boolean sendMail() throws IOException, TemplateException, MessagingException {
		// sendMailTLS("gautam@wimwisure.com", "Hello",
		// "C:\\Users\\gauta\\Desktop\\jadu_mails", "account.ftl", null);
		User user = userDAO.getUserByPhoneOrEmail("8985235216");
		mailService.sendRegistrationOTP(user);
		smsService.sendRegistrationOTP(user);
		smsService.sendRegistrationPassword(user, "hello");
		return true;
	}

	@RequestMapping(value = "/get-time", method = RequestMethod.GET)
	@ResponseBody
	public Object getTime() throws IOException, TemplateException, MessagingException {
		return UtilHelper.getDate().toString();
	}

	@RequestMapping(value = "/send-link", method = RequestMethod.GET)
	@ResponseBody
	public void sendLink() throws IOException, TemplateException, MessagingException, Exception {

		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(1403);

		String url = urlShortnerService.shortenUrl("customer-inspection/" + ic.getId());
		smsService.sendSMSAsync(ic.getCustomerPhoneNumber(),
				"We have an insurance inspection request for " + ic.getVehicleNumber() + ". Please download the app "
						+ url + " to complete the inspection. Call 7290049100 for support");
	}

	@RequestMapping(value = "/dddddd", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String ddddd() throws IOException, TemplateException, MessagingException, Exception {
		return (EncryptionHelper.decrypt("ABRSUKJPPQSVX2P0",
				"b0SYIw5q1prbuYKmuK3NteG6h7EHqrRAhJg7HydFFnVBIZKHozUjLNAOS0cmEwEnc78hcAfQp1eygRC+VejRSv5K87zNabKkkZCd2pZ2ux+zXFKcfgNL4khq0LNCaEOs7cFQM4EtX5v3LWWCiHfNPAnTCRuKQCfuIyMlPP5hNYqCUZZCbhVfa6JMbTa0UhMIFTzreIVO1G1OfsNv23q0q51tIVKJD2jaykgr32Q/YxD6EBkAR6m7rWlHi02MPDptEYxT52rN2PzYml1WJCccUYIAKoA2R/loW0D5re8cgY5Uq2Cw7q8nzMzx5TyUBTmpAaL9oi2ynxZU8nAHcpLkg6P+5foItkBMocoA2C8taoegn30RlnAdJKHo6WvaZ9bd3qAyNfih0VhvAKmZc14CVWB7jy2FCo1b4JkTuQtUEgQEExdy8p9UkgDtW4wFsS60hPRkITncdUfY56EMhR/TcvScgTkjnbO8J7g7UJVauf2WbNd9Qv7jLRbLBXnFShoO2cjgg4oxKM5IAcgr99UYyqgtf+dux2o4YfwV48Dui+B50MdlaKrzcfJMQgxUShSQI7dDvqOrJGwMoOcgCuYQzxoxR4QuNkbeH3JgWIB82hOpXUUCspv6jnzGd8FZHEssKg0vtfvaQOwR3PtSMeoO54W1jD86uKd5qZv3rpp/ZNeF5fIvyy7jGLS4GsrvKpzYENHhOffKrskS7mjEVx+ivIqq+YvZJruYsbdd9vaPnQhkcG2BPu/cZJDxaLR6fwglrOdEAYmKtZmYEKNPDWaEeE+WXbT/rp3Kld8eWhllSsK7zUGbMeVrol6jv9IlFg570Vesnqlz7tQ4wD4z2C2Q2xLO7jypo3gfa357dmVORaNknEYWMkKZHGeGM6Py4SWesUy0xu6VeHe4if63ekIG1pIZtwqGGUn1vRLmeNiZ6nnn4Q7DiCkAJalrNmmF6NOhhxII99AvlHsOyWtCEYyrBfOTU1fWwJbMEC4qyPZZPZRBHz9JeKMPsCHCaLJ8bcYupg3xokDpzo4kcmAb/POyBiaS0K+HorO6c2YuhFMn/Igf8KmsDFVvFQEo2UGLQKHu1oBVLMkoacXKjCqwHq7sqy2JJpIv6NQNcMZt1RuHIk4OuVwZYZVJX2Uk+eHEQkE/uSIZdpVG8hwln9YXptobmMbgt+wH2I7tWrDxytmqx6ayYlYU2FQB2pW2fu+eNR+hQtO5YqRpbM3liL5IRiojRBTbzfSNh42SLM83jxM+Ibz6IodpjyhYMguCZCJ68vOGNCqrhH204u9eGaeGRUOEd6Vr6FQY/VKMMYm1/F8o82LJz53HlGZwYtPmdE2wAd9FALDdcyoodFhWMzJvAyU/4DX5eD5b07TNcJg5d/U0gstwdCJ1E/F53Iy0AU9DVi7vO2tcv/Tj1y1fNpyS86MBBdn4NiICjNF9w0Cn8xDj4E3gucSBGv1SQl3DkU3joSU0Zk+4WU4fCfRk5n9NDBqeVwcDAnsbNx+7y7ImVc56fuDUA0+HILCTs0qD7h5Z4jl0lW4lWGIlv1FEZWKWtXxduge6/9aSMktIVq7DtEgGXRyxdf5t+syvU6hV9JJNi/wEZUPM8Bzh394TR0bW1rCFcGAzglqhriCBZOHO3mzB+RH8Fzimur0CZCPqVR0duUfF0ynlM1coPL3TZaQNOLwHKgsc0G86NGPyKNa14ekmsGGCwNaJZKOR3Dz8EfclaRaqTqkgh8Oe88vbcFuf2dCyYgvvLKOcFGfWxQeBSXdS2DbW3RrdxixTYDNWQQTL9JVd46Rvdzyuz52LkKf+2lk41O67WyoLykx+bTkTQk9hn6uzA6O13XTJSTDFHReUyE3o5Z7kmvhIavv3n2AUs9QxcDkSbjRSq4Ru5u70XzZA7kFbkemlknbZGRwEQu0zTQ4Jc3xwylaCzqTEl5Lmiut8oFpYk+Q3CfjVncyLA0G00xJnkanp9xF8B0c97Kfms4nHiLkwtMbs+DXt7IRk9xB3Q89vKuLltz5BMvIj7CeV+Vio+LHSl9awA5G4cFi28xKO8HHzRQGLNkuxw2GPD+jaPC0Q+yfKu05RBGcgp6+TQW1coYtYmxI3R6wyodIBUL1lXI70ZPSynhXTXA91sl0L/gNHc4/T5eckJKJu8CcQn+9Yq5IZX2K5gNPSUjwr6pA9Mw4mVYfyydOgKjNA7gSI5jlcfi5gp4kecbVpmJzh0NguN7HEKGKzXL5W3MPxxv1g9NTB+4hmqUdOA0RcFNPVwSsgmAbsNlOjqzUQ4GWGXGiSL2I1SDy2dC1LHHJ3032FIKY38rSaXq+0bTYgCSHE3wGMpBCsuLmWFmSLhK8zjIUBLlhAIU7+NJZb2C9uRD5R2HzvNIVlcnIfOEZ33ZqB2SHi0cjld4xFshmXJJAwp6IKzEc5R2mRXiG3vabCtMiHBjiHFq3Y7/qBG3Ofakt1oWxhSA1U5tZphyzfBobPfNCYTzwdcALTct3PzgpeEiGuem2TlN8T+J5ZFdyHsKyXoZasU1H05sOFPktsWC+hULVP/exvRcJY/2v1Yh0ok7J8gxNGXCkASOcuXNqk4SntBwoc+7xWoadxD8SC2eAk/fJce84tR6fPanY++139OPDd6OKDK2l3EELT/aEtZSfL+XR6zqrGTEHBUkFbUpcfm9VNmFWETYV7EGegtOcIL9hpldhv3qxmZ7PB+aVjspocHCBOlTf9hgl/sDVsvh2I62riukWa3KqaXRySkCVV5MRLq9P1jQo7y8ieqLNYSzt3sPt10kG9JL+OESouphq9xmeb/YDXV8vxZjmHiebkXr3qoEXkljD+krcF0T1YeNeB+f6KqDlVQyXsk/yC+dVLqtcm7cOLFU7ZfkzxYYRjw1NnJpkXg1EsPbIhcYRzK6gXVotRbCoHGKuriXsNiiccWzbIZh6+HMXkvX+fhqimlO5z"))
						.toString();
	}

	@RequestMapping(value = "/do-qc", method = RequestMethod.GET)
	@ResponseBody
	public void doQC(@RequestParam(value = "case_id", required = true) int case_id)
			throws IOException, TemplateException, MessagingException, Exception {
		InspectionCase c = (InspectionCase) caseDAO.getCaseById(case_id);

		if (c == null)
			return;

		if (c.getCurrentStage() > 4)
			return;

		if (s3Service.exists("cases/" + c.getId() + "/" + c.getId() + ".zip")) {
			caseService.uploadZipToS3(case_id, "cases/" + c.getId() + "/" + c.getId() + ".zip");

			// c.setCurrentStage(4);
			// caseDAO.save(c);

		} else {
			throw new Exception("Inspection file not uploaded to server!");
		}
	}

	@RequestMapping(value = "/create-report", method = RequestMethod.GET)
	@ResponseBody
	public void createReport(@RequestParam(value = "case_id", required = true) int case_id)
			throws IOException, TemplateException, MessagingException, Exception {
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(case_id);

		String outputFile1 = "report.pdf";
		generateReportService.createPdf((InspectionCase) caseDAO.getCaseById(ic.getId()), ic.getId() + ".pdf",
				outputFile1, true);
	}

	@RequestMapping(value = "/call-gautam", method = RequestMethod.GET)
	@ResponseBody
	public void callGautam(@RequestParam(value = "phoneNumber", required = true) String phoneNumber)
			throws IOException, TemplateException, MessagingException, Exception {
		connectCustomerToFlow.reminderCallForScheduledCase(phoneNumber,
				(InspectionCase) caseDAO.getCaseById(appConfigService.getLongProperty("TEST_CASE_ID", 112L)));
	}

	@RequestMapping(value = "/check-for-zip", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String checkForzip() throws IOException, TemplateException, MessagingException, Exception {

		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		List<ScheduledCasesDTO> items = caseDAO.getScheduledCasesDTO(companyBranchDivisionUsers);

		JSONArray result = new JSONArray();

		for (ScheduledCasesDTO item : items) {
			if (s3Service.exists("cases/" + item.getId() + "/" + item.getId() + ".zip")) {
				result.put(item.getId());
			}
		}

		return result.toString();
	}

	@RequestMapping(value = "/check-json", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ClaimCase jsonTest() throws IOException, TemplateException, MessagingException, Exception {
		ClaimCase c = claimCaseDAO.getCaseById(appConfigService.getLongProperty("CLAIM_TEST_CASE_IDs", 8l));
		// String data = "{\"accident_details\":[{\"accident_location\":\"Delhi Airport,
		// New Delhi, Delhi,
		// India\",\"claim_number\":\"OC-19-1103-1801-002426\",\"date\":1558249314151,\"date_of_accident\":0,\"date_of_survey\":0,\"license_number\":\"testLicenceNumber\",\"location_of_survey\":\"delhi\",\"photos\":\"1558249311215.png\",\"third_party_loss\":\"{\\\"third_party_loss\\\":[\\\"Driver\\\"]}\",\"voice_summary\":\"\"}],\"driver_details\":[{\"claim_number\":\"OC-19-1103-1801-002426\",\"date\":1558249291331,\"driver_name\":\"Test
		// Driver\",\"expiry_date\":0,\"issue_date\":0,\"license_number\":\"test driver
		// licence number\",\"license_type\":\"Learner Driving
		// License\",\"photos\":\"1558249287862.png\",\"relation_with_claimant\":\"friend\"}],\"inspection_details\":[{\"id\":\"90-deg-left\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1558249443000.jpg\",\"hash\":\"04a6101abe87bfd4bcd6cff2801fc9fe\",\"latitude\":17.5288904,\"longitude\":78.395838}]},{\"id\":\"90-deg-back\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1558249452041.jpg\",\"hash\":\"3fd62d85b91a41c578e3eaa28fa6100a\",\"latitude\":17.5288772,\"longitude\":78.39584}]},{\"id\":\"90-deg-right\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1558249464560.jpg\",\"hash\":\"4b9a0def85347bbfaa8d08cb5f052877\",\"latitude\":17.5288776,\"longitude\":78.3958341}]},{\"id\":\"chachis-number\",\"photos\":[{\"answer\":\"YBHJHB\",\"fileName\":\"1558249476968.jpg\",\"hash\":\"877ba8cd85cf51e6580be3ab4bf5d4ae\",\"latitude\":17.5288762,\"longitude\":78.395843}]},{\"id\":\"odometer-rpm\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1558249493970.jpg\",\"hash\":\"d163d1d0e601f7f2ff85f9fa825d4d48\",\"latitude\":17.5288769,\"longitude\":78.3958388}]},{\"id\":\"defect\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1558249505245.jpg\",\"hash\":\"08e86574721250bd1661bd69ceb19b2e\",\"latitude\":17.528877,\"longitude\":78.395839}]},{\"id\":\"client-signature\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1558249552070.jpeg\",\"hash\":\"71d662843b7017e1f0c6798e5b80620a\",\"latitude\":0.0,\"longitude\":0.0}]}],\"insurance_details\":[{\"claim_number\":\"OC-19-1103-1801-002426\",\"date\":1558249269144,\"end_date\":-19800000,\"financier\":\"te3st\",\"insured_name\":\"test\",\"insurer\":\"test\",\"ncb_status\":\"NCB
		// Status:\",\"photos\":\"1558249261321.png\",\"policy_number\":\"142442\",\"start_date\":-19800000}],\"lossassement_details\":[{\"claim_number\":\"OC-19-1103-1801-002426\",\"consumables\":\"4949\",\"date\":1558249625763,\"labour\":\"9464\",\"part\":\"5464\",\"partType\":\"Lamp\",\"particulars\":\"LH
		// Head
		// Lamp\",\"photos\":\"1558249621785.png\",\"repairType\":\"Replace\"}],\"vehical_details\":[{\"claim_number\":\"OC-19-1103-1801-002426\",\"date\":1558249328453,\"photos\":\"\",\"vehicle_make\":\"Royal
		// Enfield\",\"vehicle_model\":\"Classic
		// 350\",\"vehicle_number\":\"TS-45-YS-HSH\",\"vehicle_type\":\"2 Wheeler\"}]}";
		String data = "{\"accident_details\":[{\"accident_location\":\"Yeshwanthpur, Bengaluru, Karnataka, India\",\"claim_number\":\"OC-19-1103-1801-002426\",\"date_of_accident\":1563126534473,\"date_of_survey\":0,\"location_of_survey\":\"Unnamed Road, Jalagam Nagar, Khammam, Telangana 507001, India\",\"photos_details\":[{\"id\":\"accident_details\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1563126547885.png\",\"hash\":\"54db3d6e576adddf73d775c1e9469ee5\",\"latitude\":17.2495578,\"longitude\":80.1234034}]}],\"third_party_loss\":\"No\",\"voice_summary\":\"\"}],\"driver_details\":[{\"claim_number\":\"OC-19-1103-1801-002426\",\"date\":1563126532268,\"driver_name\":\"Yes\",\"expiry_date\":1563126519230,\"issue_date\":1563126519230,\"license_number\":\"HI\",\"photos_details\":[{\"id\":\"driver_details\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1563126531426.png\",\"hash\":\"52f3b161e1b9e7f957c9809c31385bbb\",\"latitude\":17.2495678,\"longitude\":80.1233977}]}],\"relation_with_claimant\":\"Read\"}],\"inspection_details\":[{\"id\":\"90-deg-front\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1563126559544.jpg\",\"hash\":\"119e1e780fa5ebd93e9a4486dc515606\",\"latitude\":17.2495748,\"longitude\":80.1233956}]},{\"id\":\"90-deg-left\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1563126567675.jpg\",\"hash\":\"6124c0802a20c1cca36af4db7975500b\",\"latitude\":17.2495578,\"longitude\":80.1234034}]},{\"id\":\"90-deg-back\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1563126572159.jpg\",\"hash\":\"de65bde6b2eeaf78f5da512529edc98d\",\"latitude\":17.2495578,\"longitude\":80.1234034}]},{\"id\":\"90-deg-right\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1563126577117.jpg\",\"hash\":\"afc9af936ec5dc7bf9769602ecfcc2f8\",\"latitude\":17.2495578,\"longitude\":80.1234034}]},{\"id\":\"chachis-number\",\"photos\":[{\"answer\":\"HNHNHN\",\"fileName\":\"1563126580972.jpg\",\"hash\":\"4f6acc369b9c076fd4e8e76797a550e7\",\"latitude\":17.2495578,\"longitude\":80.1234034}]},{\"id\":\"vin-plate-number\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1563126591164.jpg\",\"hash\":\"795114b308a7fe53e876aacddb205213\",\"latitude\":17.2495578,\"longitude\":80.1234034}]},{\"id\":\"defect\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1563126600950.jpg\",\"hash\":\"6ddac2f187dfb84e5df3c21d62a15e8d\",\"latitude\":17.2495578,\"longitude\":80.1234034},{\"answer\":\"\",\"fileName\":\"1563126605996.jpg\",\"hash\":\"1f88359d498c98d8aaa2b8a97955e216\",\"latitude\":17.2495578,\"longitude\":80.1234034}]},{\"id\":\"video_file\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1563126633657.mp4\",\"hash\":\"0c8e6c707cc5e76dcc7c55e70ec1c44d\",\"latitude\":0.0,\"longitude\":0.0}]},{\"id\":\"signature_file\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1563126672604.jpeg\",\"hash\":\"6ead291e3afedcdf8fae99f4873d49b1\",\"latitude\":17.2495578,\"longitude\":80.1234034}]}],\"insurance_details\":[{\"claim_number\":\"OC-19-1103-1801-002426\",\"date\":1563126518265,\"end_date\":-19800000,\"insured_name\":\"IFFCO Tokio General Insurance Co. Ltd.\",\"insurer\":\"Test\",\"photos_details\":[{\"id\":\"insurance_details\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1563126517552.png\",\"hash\":\"b1ff1531efb0c296e3f26fc13161e8cf\",\"latitude\":17.2495578,\"longitude\":80.1234034}]}],\"policy_number\":\"CHVH777HHHHH\",\"start_date\":-19800000}],\"lossassement_details\":[{\"claim_number\":\"OC-19-1103-1801-002426\",\"consumables_cost\":\"\",\"damage_type\":\"REPLACEMENT\",\"date\":1563126651403,\"labour_cost\":\"500.0\",\"part_cost\":\"1000.0\",\"part_particular_type\":\"RH Head Lamp\",\"part_side\":\"Right\",\"part_type\":\"Lamp\",\"photos_details\":[{\"id\":\"driver_details\",\"photos\":[{\"answer\":\"\",\"fileName\":\"1563126649433.png\",\"hash\":\"a47ebdc61c238532fddfc8ea7911e237\",\"latitude\":17.2495578,\"longitude\":80.1234034}]}],\"repair_type\":\"Product 1\"}],\"vehical_details\":[{\"claim_number\":\"OC-19-1103-1801-002426\",\"date\":1563126555650,\"id\":\"1\",\"photos_details\":[{\"id\":\"driver_details\",\"photos\":[]}],\"vehicle_make\":\"Make 1\",\"vehicle_model\":\"Model 1\",\"vehicle_number\":\"HR26DA9108\",\"vehicle_paint_type\":\"Silver\",\"vehicle_type\":\"4-wheeler\",\"vehicle_varaint\":\"Variant 1\"}]}";
		JSONObject json = new JSONObject(data);
		c = claimCaseService.persistClaimDetails(json, c);
		return c;

	}

	@RequestMapping(value = "/followup", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public CasesCallStatus persistCaseFollowup() throws IOException, TemplateException, MessagingException, Exception {
		String data = "{\"Status\":\"in-progress\",\"EndTime\":{},\"DateUpdated\":\"2019-05-19 11:03:14\",\"ParentCallSid\":{},\"StartTime\":\"2019-05-19 11:03:14\",\"DateCreated\":\"2019-05-19 11:03:14\",\"Duration\":{},\"RecordingUrl\":{},\"From\":7066908372,\"Direction\":\"outbound-api\",\"Uri\":\"/v1/Accounts/wimwisure/Calls/11c612bcc0c5aee3cc6afe3a94db6247\",\"AccountSid\":\"wimwisure\",\"Sid\":\"11c612bcc0c5aee3cc6afe3a94db6247\",\"PhoneNumberSid\":\"cf766b8f301d03ca164e3fe34e122f6d\",\"Price\":{},\"To\":9513886363,\"ForwardedFrom\":{},\"AnsweredBy\":{},\"CallerName\":{}}";
		JSONObject json = new JSONObject(data);
		InspectionCase id = (InspectionCase) caseDAO
				.getCaseById(appConfigService.getLongProperty("TEST_CASE_ID", 112L));
		return caseFollowupService.persistCallStatus(json, "7989119669", "188214", id);

	}

	@RequestMapping(value = "/get-random", method = RequestMethod.GET)
	@ResponseBody
	public String getRandom() throws IOException, TemplateException, MessagingException {
		return UtilHelper.getRandomStringLowerCase(16);
	}

	@RequestMapping(value = "/get-time-string", method = RequestMethod.GET)
	@ResponseBody
	public Object getTime(
			@RequestParam(value = "user_specified_time", required = false) @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss") Date date) {
		return date.toString();
	}

	@RequestMapping(value = "/get-buckets", method = RequestMethod.GET)
	@ResponseBody
	public Object getBuckets() {
		return s3Service.getObjectslistFromFolder();
	}

	@RequestMapping(value = "/file-exists", method = RequestMethod.GET)
	@ResponseBody
	public Object fileExists(@RequestParam(value = "file", required = true) String file) {
		return s3DirectUploadService.exists(file);
	}

	@RequestMapping(value = "/generate-test-report", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public void generateTestReport() throws Exception {
		InspectionCase ic = (InspectionCase) caseDAO.getCaseById(1);
		generateReportService.createPdf((InspectionCase) caseDAO.getCaseById(ic.getId()), ic.getId() + ".pdf",
				"report.pdf", true);
	}

	@RequestMapping(value = "/send-push-notification-to-agents", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public void sendPushNotification() throws IOException, TemplateException, MessagingException, JSONException {
		JSONObject data = new JSONObject();
		JSONArray l = new JSONArray();
		l.put("Value1");
		l.put("Value2");
		data.put("ACTION", "SCHEDULED_CASES");
		data.put("VALUES", l);

		androidPushNotificationsService.sendNotificationToUserAsync("gautamnitw@gmail.com", "Testing",
				"This has data payload!", data.toString());
	}

	@RequestMapping(value = "/export-images", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public void exportImages() throws IOException, TemplateException, MessagingException, JSONException {
		String rootFolder = env.getProperty("jadu.upload.root.url");
		List<CaseMakeDTO> result = caseDAO.getCarByMake("Maruti");

		for (CaseMakeDTO temp : result) {
			long item = temp.getId();
			List<CasePhotoDTO2> photos = casePhotoDAO.getCasePhotosByCaseIdLimit(item);

			for (CasePhotoDTO2 photo : photos) {
				InputStream in = s3Service.readFileStream("cases/" + item + "/" + photo.getFilename());

				File dir = new File(rootFolder + "/" + temp.getModel() + "/" + photo.getType());
				if (!dir.exists())
					dir.mkdirs();

				File file = new File(
						rootFolder + "/" + temp.getModel() + "/" + photo.getType() + "\\" + photo.getFilename());

				Files.copy(in, file.toPath());
			}
		}

	}

	@RequestMapping(value = "/send-push-notification-web", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public void sendPushNotificationWeb(@RequestParam(value = "token", required = true) String token)
			throws IOException, TemplateException, MessagingException, JSONException {
		JSONObject data = new JSONObject();
		JSONArray l = new JSONArray();
		l.put("Value1");
		l.put("Value2");
		data.put("ACTION", "SCHEDULED_CASES");
		data.put("VALUES", l);

		androidPushNotificationsService.sendNotificationToDevice(token, "Testing", "This has data payload!",
				data.toString());
	}

	@RequestMapping(value = "/unzip", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public void unzip(@RequestParam(value = "encriptionKey", required = true) String token,
			@RequestParam(value = "path", required = true) String path) throws Exception {
		InputStream item = new FileInputStream(path);
		StringWriter writer = new StringWriter();
		IOUtils.copy(item, writer);
		String theString = writer.toString();

		JSONObject json = new JSONObject(decrypt(token, theString));
		System.out.println(json);
	}

	private String decrypt(String key, String encrypted) throws Exception {
		try {
			IvParameterSpec iv = new IvParameterSpec("WIMWISUREIVector".getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

			byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

			return new String(original);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
				| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
			ex.printStackTrace();
			throw new Exception("Unable to decrypt");
		}
	}

	@ExceptionHandler(Exception.class)
	public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
		ErrorHandler.handleError(ex, response);
		ex.printStackTrace();
	}

//	@RequestMapping(value = "/create-case", method = RequestMethod.GET, produces = "application/json")
//	@ResponseBody
//	public ResponseEntity<ClaimCase> createCase(
//			@RequestParam(value = "purpose_of_survey", required = true) String purposeOfSurvey,
//			@RequestParam(value = "customer_name", required = true) String customerName,
//			@RequestParam(value = "customer_phone_number", required = true) String customerPhoneNumber,
//			@RequestParam(value = "self_inspect", required = false) boolean selfInspect,
//			@RequestParam(value = "inspection_time", required = false) @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss") Date date,
//			@RequestParam(value = "latitude", required = false) Double latitude,
//			@RequestParam(value = "longitude", required = false) Double longitude,
//			@RequestParam(value = "vehicle_number", required = true) String vehicleNumber,
//			@RequestParam(value = "agentId", required = true) String agentId) throws Exception {
//		Agent agent = agentDAO.getAgentByUsername(agentId);
//		CompanyBranchDivision companyBranchDivision = agent.getAgentDetails().getCompanyBranchDivision();
//		ClaimCase cc = null;
//		if (date == null)
//			date = new Date();
//
//		if (companyBranchDivision == null) {
//			LOGGER.info("Invalid company for agent={}. Please contact administrator!", agent.getPhoneNumber());
//			throw new Exception("Invalid company. Please contact administrator!");
//		}
//
//		PurposeOfSurvey poi = posDAO.getPurposeOfSurveyById(purposeOfSurvey);
//
//		if (poi == null) {
//			LOGGER.info("Invalid purpose of inspection={} for user={}", poi, agent.getPhoneNumber());
//			throw new Exception("Invalid purpose of inspection!");
//		}
//		try {
//			ClaimCaseBuilder icBuilder = new ClaimCaseBuilder(customerName, customerPhoneNumber,
//					agent,
//					vehicleNumber, poi, agent.getAgentDetails().getCompanyBranchDivision())
//							.setStage(1).addCaseToUserForStage(agent, 1);
//
//			if (selfInspect) {
//				icBuilder.addCaseToUserForStage(agent, 3).setInspectionType("SELF_INSPECT")
//						.setInspectionTime(date, latitude, longitude).setStage(3);
//			} else {
//				com.jadu.model.User user = userDAO.getUserByPhoneOrEmail(customerPhoneNumber);
//
//				if (user == null)
//					user = createUserService.createCustomer(customerName, customerPhoneNumber);
//
//				icBuilder.addCaseToUserForStage(user, 3).setInspectionType("ASSIGN_TO_CUSTOMER")
//						.setInspectionTime(new Date(), latitude, longitude).setStage(3);
//			}
//
//			cc = icBuilder.build();
//
//			//cc.setEncryptionKey(UtilHelper.getRandomStringUpperCase(16));
//
//			//cc.setDownloadKey(UtilHelper.getRandomString(64) + UtilHelper.getCurrentTimestampAsString() + "_");
//
//			claimCaseDAO.save(cc);
//
//			caseLogDAO.addClaimCreateCase(agent, cc);
//
//			caseService.sendClaimCaseCreationNotificationsAsync(agent, cc);
//
//			if (!selfInspect) {
//				String url = urlShortnerService.shortenUrl("customer-inspection/" + cc.getId());
//				smsService.sendSMSAsync(customerPhoneNumber,
//						"We have an insurance inspection request for " + cc.getVehicleNumber()
//								+ ". Please download the app " + url
//								+ " to complete the inspection. Call 7290049100 for support");
//				connectCustomerToFlow.callCustomerAfterCaseCreate(customerPhoneNumber);
//			}
//			if (appConfigService.getBooleanProperty("IS_TO_PUSH_CREATE_CASE_BY_AGENT_IN_CACHE", true)) {
//				try {
//					if (agent != null && cc != null) {
//						// redisService.cacheCaseAgainstAgent(agent, cc, 0);
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//					LOGGER.error("Exception catched while caching case={}, error={}", cc.getId(), e.getMessage());
//				}
//			}
//			LOGGER.info("case has been created, returning case details={} to the client", cc);
//		} catch (Exception e) {
//			e.printStackTrace();
//			LOGGER.error(
//					"Exception raised while creating case for user={},purpose_of_survey={}, customer_name={}, customer_phone_number={}, self_inspect={}, vehicle_number={}, inspection_time={}, latitude={}, longitude={}, error={} ",
//					agent.getPhoneNumber(), purposeOfSurvey, customerName, customerPhoneNumber, selfInspect,
//					vehicleNumber, date, latitude, longitude, e.getMessage());
//		}
//		return new ResponseEntity<>(cc, HttpStatus.OK);
//	}

	@RequestMapping(value = "/report-data", method = RequestMethod.GET)
	public ClaimsFinalSubmissionDTO getReportData(@RequestParam("case_id") long caseId) throws Exception {
		return claimsReportDataGeneratorFactory.prepareData(null, null, null);

	}
}
