package com.jadu.front.controller;

import com.jadu.dao.CaseDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dao.VehicleDAO;
import com.jadu.dao.VehicleFuelTypeDAO;
import com.jadu.dao.VehicleTypeDAO;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.model.InspectionCase;
import com.jadu.service.S3Service;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/case")
public class CaseController {

	@Autowired
	private S3Service s3Service;

	@Autowired
	private UserDAOImpl userDAO;

	@Autowired
	private CaseDAO caseDAO;

	@Autowired
	private VehicleFuelTypeDAO vehicleFuelTypeDAO;

	@Autowired
	private VehicleTypeDAO vehicleTypeDAO;

	@Autowired
	private VehicleDAO vehicleDAO;

	private static final Logger LOGGER = LoggerFactory.getLogger(CaseController.class);

	@RequestMapping(value = "/inspection-video/{caseId}", method = RequestMethod.GET, produces = "video/mp4")
	public void getCaseVideo(@PathVariable(value = "caseId") int caseId, HttpServletResponse response) {

		try (InputStream inputStream = s3Service.readFileStream("cases/" + caseId + "/video.mp4")) {
			ServletOutputStream outputStream = response.getOutputStream();
			IOUtils.copy(inputStream, outputStream);
			IOUtils.closeQuietly(inputStream);
			IOUtils.closeQuietly(outputStream);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception raised while fetching inspection video for case={}", caseId);
		}
	}

	@RequestMapping(value = "/timeline", method = RequestMethod.GET, produces = "application/json")
	public void getTimeline(@RequestParam(value = "case_id", required = true) Long caseId, HttpServletResponse response)
			throws IOException {

	}

	@RequestMapping(value = "/s3/upload-url", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getSignedUploadUrl(@RequestParam(value = "case_id", required = true) int caseId,
			HttpServletResponse response) throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase ic = (InspectionCase) caseDAO.getCase(caseId, user.getUsername());

		if (ic == null)
			throw new Exception("Case not found");

		if (ic.getCurrentStage() != 3)
			throw new Exception("Case document has either been uploaded or case has been closed");

		JSONObject result = new JSONObject();

		result.put("photo_upload_url", s3Service.getSignedUrl("cases/" + ic.getId() + "/" + ic.getId() + ".zip"));
		result.put("video_upload_url",
				s3Service.getSignedUrl("cases/" + ic.getId() + "/" + ic.getId() + "_videos.zip"));

		return result.toString();
	}

	@RequestMapping(value = "/s3/upload/notify", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String notifyUpload(@RequestParam(value = "case_id", required = true) int caseId,
			@RequestParam(value = "upload_type", required = true) String uploadType, HttpServletResponse response)
			throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase ic = (InspectionCase) caseDAO.getCase(caseId, user.getUsername());

		if (ic == null)
			throw new Exception("Case not found");

		if (ic.getCurrentStage() == 5)
			throw new Exception("Case has already been completed!");

		switch (uploadType) {
		case "_photos": {
			if (!s3Service.exists("cases/" + ic.getId() + "/" + ic.getId() + ".zip"))
				throw new Exception("Photos not uploaded!");
			ic.setPhotosUploaded(true);
			break;
		}
		case "_videos": {

			if (!s3Service.exists("cases/" + ic.getId() + "/" + ic.getId() + "_videos.zip"))
				throw new Exception("Videos not uploaded!");
			ic.setVideosUploaded(true);
			break;
		}
		}

		caseDAO.save(ic);

		JSONObject result = new JSONObject();

		result.put("_photos", ic.isPhotosUploaded());
		result.put("_videos", ic.isVideosUploaded());

		return result.toString();
	}

	@RequestMapping(value = "/s3/upload/status", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String uploadStatus(@RequestParam(value = "case_id", required = true) int caseId,
			HttpServletResponse response) throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase ic = (InspectionCase) caseDAO.getCase(caseId, user.getUsername());

		if (ic == null)
			throw new Exception("Case not found");

		if (ic.getCurrentStage() == 5)
			throw new Exception("Case has already been completed!");

		JSONObject result = new JSONObject();

		result.put("_photos", ic.isPhotosUploaded());
		result.put("_videos", ic.isVideosUploaded());

		return result.toString();
	}

	@RequestMapping(value = "/inspection-start/notify", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String notifyInspectionStart(@RequestParam(value = "case_id", required = true) int caseId,
			@RequestParam(value = "vehicle_number", required = true) String vehicleNumber,
			@RequestParam(value = "vehicle_type", required = true) String vehicleType,
			@RequestParam(value = "vehicle_color", required = true) String vehicleColor,
			@RequestParam(value = "make_model_id", required = true) int makeModelId,
			@RequestParam(value = "fuel_type", required = true) String fuelType,
			@RequestParam(value = "yom", required = true) int YOM, HttpServletResponse response) throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase ic = (InspectionCase) caseDAO.getCase(caseId, user.getUsername());

		if (ic == null)
			throw new Exception("Case not found");

		if (ic.getCurrentStage() == 5)
			throw new Exception("Case has already been completed!");

		ic.setInspectionStartTime(new Date());
		ic.setVehicleNumber(vehicleNumber);
		ic.setVehicleColor(vehicleColor);
		ic.setVehicleYOM(YOM);
		ic.setVehicleFuelType(vehicleFuelTypeDAO.get(fuelType));
		ic.setVehicleType(vehicleTypeDAO.get(vehicleType));
		ic.setVehicle(vehicleDAO.getVehicleById(makeModelId));

		caseDAO.save(ic);

		return new JSONObject().toString();
	}

	@RequestMapping(value = "/inspection-complete/notify", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String notifyInspectionCompletion(@RequestParam(value = "case_id", required = true) int caseId,
			HttpServletResponse response) throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		User securedUser = (User) authentication.getPrincipal();
		com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());

		InspectionCase ic = (InspectionCase) caseDAO.getCase(caseId, user.getUsername());

		if (ic == null)
			throw new Exception("Case not found");

		if (ic.getCurrentStage() == 5)
			throw new Exception("Case has already been completed!");

		caseDAO.save(ic);

		JSONObject result = new JSONObject();
		result.put("success", true);
		return result.toString();
	}

	@ExceptionHandler(Exception.class)
	public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
		ErrorHandler.handleError(ex, response);
		System.out.println(ex.getStackTrace());
	}
}
