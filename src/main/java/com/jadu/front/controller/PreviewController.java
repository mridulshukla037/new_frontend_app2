package com.jadu.front.controller;

import com.jadu.dao.CaseDAO;
import com.jadu.dao.CasePhotoDAO;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.model.InspectionCase;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author gautam
 */
@RestController
@Scope("session")
@PropertySource("/WEB-INF/config.properties")
public class PreviewController {
    
    @Autowired
    private CaseDAO caseDAO;
    
    @Autowired
    private CasePhotoDAO casePhotoDAO;
    
    @RequestMapping(value = "/preview-images/{inspectionCase}")
    public ModelAndView getIndex(
            HttpServletRequest request,
            HttpServletResponse response,
            @PathVariable String inspectionCase,
            ModelMap  model) throws Exception {

        
        InspectionCase ic = (InspectionCase)caseDAO.getInspectionCaseByZipid(inspectionCase);
        
        if(ic == null)
            throw new Exception("Inspection case not found!");
        List<String> items = casePhotoDAO.getCasePhotosByCaseId(ic.getId());
        
        
        model.addAttribute("images", items);
        model.addAttribute("ic", "gauti");
        ModelAndView model1 = new ModelAndView("WEB-INF/jsp/images");
        model1.addObject("images", items);
        model1.addObject("ic",ic);
        return model1;
        
    }
    
    @ExceptionHandler(Exception.class)
    public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
        ErrorHandler.handleError(ex, response);
        ex.printStackTrace();
    }
}
