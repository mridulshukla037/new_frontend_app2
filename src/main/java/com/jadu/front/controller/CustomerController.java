package com.jadu.front.controller;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImage;
import com.itextpdf.text.pdf.PdfIndirectObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.jadu.dao.CaseDAO;
import com.jadu.dao.ConfigurationDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.FileHelper;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.InspectionCase;
import com.jadu.service.BlockchainService;
import com.jadu.service.CaseService;
import com.jadu.service.QRService;
import com.jadu.service.S3Service;
import com.jadu.service.SmsService;
import com.jadu.service.URLShortnerService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author gautam
 */
@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/customer")
@Scope("session")
public class CustomerController {
    
    @Autowired
    private CaseDAO caseDAO;
    
    @Autowired
    private UserDAOImpl userDAO;
    
    @Autowired
    private ConfigurationDAO configurationDAO;
    
    @Autowired
    private URLShortnerService urlShortnerService;
    
    @Autowired
    private S3Service s3Service;
    
    @Autowired
    private SmsService smsService;
    
    @Autowired
    private CaseService caseService;
    
    @Autowired
    private QRService qrService;
    
    @Autowired
    private BlockchainService blockchainService;
    
    @RequestMapping(value = "/profile-details", method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public Object checkAgentDetails(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return userDAO.getUserByUsername(((User)authentication.getPrincipal()).getUsername());
    } 
    
    @RequestMapping(value = "/get-configurations", method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public String getConfigurations() throws JSONException{
        JSONObject result =  new JSONObject();
        result.put("AGENT_SELF_INSPECT", configurationDAO.getConfigurationBooleanValueByTag("AGENT_SELF_INSPECT"));
        return result.toString();
    } 
    
    @RequestMapping(value = "/cases/scheduled-cases", method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public List scheduledCases(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User securedUser = (User)authentication.getPrincipal();
        com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());
        
        return caseDAO.getScheduledCasesSubsetByStage(3, user.getUsername());
    }
    
    @RequestMapping(value = "/cases/all-cases", method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public List allCases(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        User securedUser = (User)authentication.getPrincipal();
        com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());
        
        return caseDAO.getAllCasesByUsername(user.getUsername());
    }
    
    @RequestMapping(value = "/cases/get-case-details", method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public Object getCaseDetails(
            @RequestParam(value = "case_id", required=true ) Long caseId
    ) throws Exception{
        InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
        if(ic == null)
            throw new Exception("Unable to get case details!");
        
        return ic;
    }
    
    @RequestMapping(value = "/cases/start-inspection", produces="application/json")
    @ResponseBody
    public void startInspection(
            @RequestParam(value = "case_id", required=true ) Long caseId
    ) throws Exception{
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User securedUser = (User)authentication.getPrincipal();
        com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());
        
        InspectionCase ic = (InspectionCase) caseDAO.getcaseByStage(3, user.getUsername(), caseId);
        
        if(ic == null)
            throw new Exception("Unable to find case with given details. Either case doesn't exists or is updated!");
        
        ic.setInspectionStartTime(UtilHelper.getDate());
        
        caseDAO.save(ic);
    }
    
    @RequestMapping(value = "/cases/submit-report", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public void submitReport(
            @RequestParam(value = "vehicle_photos", required=true) List<MultipartFile> vehiclePhotos,
            @RequestParam(value = "details", required=true) String details,
            @RequestParam(value = "vehicle_number", required=true) String vehicleNumber,
            @RequestParam(value = "vehicle_type", required=true) String vehicleType,
            @RequestParam(value = "vehicle_color", required=true) String vehicleColor,
            @RequestParam(value = "make_model_id", required=true) int makeModelId,
            @RequestParam(value = "fuel_type", required=true) String fuelType,
            @RequestParam(value = "yom", required=true) int YOM,
            @RequestParam(value = "case_id", required=true) int caseId,
            @RequestParam(value = "vehicle_video", required=true) MultipartFile vehicleVideo
    ) throws JSONException, Exception{
        
        System.out.println(details);
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        User securedUser = (User)authentication.getPrincipal();
        com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());
        
        JSONArray detailsObj = new JSONArray(details);
        
        InspectionCase c = (InspectionCase) caseDAO.getInspectionCase(caseId, user.getUsername());
        
        if(c == null)
            throw new Exception("Unable to find case with given id. ");
        
        if(c.getCurrentStage() > 3)
            throw new Exception("Photos has already been submitted. Can't be submitted again!");
        
        String rootFolder =  "cases/" + caseId;
        
        s3Service.uploadMultipartFile(vehicleVideo, rootFolder + "/video.mp4");
        
        for(MultipartFile vehiclePhoto : vehiclePhotos){
            s3Service.uploadMultipartImage(vehiclePhoto, rootFolder + "/" + vehiclePhoto.getOriginalFilename());
        }
        
        caseService.submitReport(
                caseId, 
                makeModelId, 
                fuelType, 
                YOM, 
                vehicleColor, 
                vehicleNumber, 
                vehicleType, 
                detailsObj, 
                user);
    }
    
    @RequestMapping(value = "/cases/notify-inspection-completion", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public void notifyInspectionCompletion(
            @RequestParam(value = "case_id", required=true) int caseId
            ) throws Exception{
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        User securedUser = (User)authentication.getPrincipal();
        com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());
        
        InspectionCase c = (InspectionCase) caseDAO.getInspectionCase(caseId, user.getUsername());
        
        if(c== null)
            return;
        
        if(c.getCurrentStage() != 3)
            return;
        
        c.setFileAvailable(true);
        c.setFileAvailableTime(new Date());
        
        caseDAO.save(c);
        
    }
    
    @RequestMapping(value = "/cases/notify-zip-upload", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public void notifyZipUpload(
            @RequestParam(value = "case_id", required=true) int caseId
            ) throws Exception{
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        User securedUser = (User)authentication.getPrincipal();
        com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());
        
        InspectionCase c = (InspectionCase) caseDAO.getInspectionCase(caseId, user.getUsername());
        
        if(c== null)
            return;
        
        if(c.getCurrentStage() != 3)
            return;
        
        if(s3Service.exists("cases/" + c.getId() + "/" + c.getId()+".zip")){
            c.setCurrentStage(4);
            caseDAO.save(c);
            
            caseService.uploadZipToS3Async(caseId);
        }else{
            throw new Exception("Inspection file not uploaded to server!");
        }
    }
    
    
    @RequestMapping(value = "/cases/submit-report-zip", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public String submitReportZip(
            @RequestParam(value = "case_id", required=true) int caseId,
            @RequestParam(value = "case_file", required=true) MultipartFile caseFile
            ) throws Exception{
        JSONObject result =  new JSONObject();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        User securedUser = (User)authentication.getPrincipal();
        com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());
        
        InspectionCase c = (InspectionCase) caseDAO.getInspectionCase(caseId, user.getUsername());
        
        if(c == null)
            throw new Exception("Unable to find case with given id. ");
        
        if(c.getCurrentStage() > 3)
            throw new Exception("Case has already been submitted to QC!");
        
        if(c.getCurrentStage() < 3)
            throw new Exception("Case has not been scheduled yet!");
        
        String extension = FilenameUtils.getExtension(caseFile.getOriginalFilename());
        
        if(!extension.equals("zip"))
            throw new Exception("Invalid zip file!");
        
        s3Service.uploadMultipartFileDirect(caseFile, "cases/" + c.getId() + "/" + c.getId()+".zip");
        
        if(s3Service.exists("cases/" + c.getId() + "/" + c.getId()+".zip")){
            result.put("success", true);
            c.setCurrentStage(4);
            caseDAO.save(c);
            
            caseService.uploadZipToS3Async(caseId);
        }else{
            throw new Exception("Inspection file not uploaded to server!");
        }
        
        return result.toString();
        
    }
    
    @RequestMapping(value = "/cases/get-upload-url", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public String getSignedUploadUrl(
            @RequestParam(value = "case_id", required=true) int caseId, HttpServletResponse response) throws Exception{
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
               
        User securedUser = (User)authentication.getPrincipal();
        com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());
        
        InspectionCase ic = (InspectionCase) caseDAO.getCase(caseId, user.getUsername());
        
        if(ic == null)
            throw new Exception("Case not found");
        
        if(ic.getCurrentStage() != 3)
            throw new Exception("Case document has either been uploaded or case has been closed");
        
        JSONObject result = new JSONObject();
        
        result.put("url", s3Service.getSignedUrl("cases/" + ic.getId() + "/" + ic.getId() + ".zip"));
        
        return result.toString();
    }
    
    @ExceptionHandler(Exception.class)
    public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
        ErrorHandler.handleError(ex, response);
        System.out.println(ex.getStackTrace());
    }
    
    public void manipulatePdf(byte[] src, String dest, String img) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        Image image = Image.getInstance(img);
        PdfImage stream = new PdfImage(image, "", null);
        stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
        PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
        image.setDirectReference(ref.getIndirectReference());
        image.setAbsolutePosition(36, 400);
        PdfContentByte over = stamper.getOverContent(1);
        over.addImage(image);
        stamper.close();
        reader.close();
    }
    
    @RequestMapping(value = "/upload-file", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public String uploadFile(
            @RequestParam(value = "file", required=true) MultipartFile fileToBeUploaded
    ) throws Exception{
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        com.jadu.model.User user = (com.jadu.model.User) userDAO.getUserByUsername(((User)authentication.getPrincipal()).getUsername());
        
        
        String fileName = String.valueOf(System.currentTimeMillis()) + "." + FilenameUtils.getExtension(fileToBeUploaded.getOriginalFilename());
        
        s3Service.uploadMultipartFile(fileToBeUploaded, "bc/files/" + fileName);
       
        String md5 = DigestUtils.md5Hex(fileToBeUploaded.getBytes());

        int transactionId = blockchainService.saveDataToBlockchainGetTransactionId(md5);


        JSONObject result = new JSONObject();
        result.put("url", "https://wimwisure.com/RestServer/bc/file/"+ fileName);
        result.put("transaction_id", transactionId);

        String qrCodeFile = qrService.generateQRCodeGetBytes(result);

        manipulatePdf(fileToBeUploaded.getBytes(), "C:\\Users\\gauta\\Desktop\\jadu\\test.pdf", qrCodeFile);
        
        
        s3Service.uploadFile(new File(qrCodeFile), "bc/qr_files/qr_" + transactionId + ".png");
        
        FileHelper.deleteFileIfExists(qrCodeFile);
        
        String shortUrl = urlShortnerService.shortenQRCodeUrl("qr_" + transactionId + ".png");
        
        smsService.sendSMS(user.getPhoneNumber(), "Policy has been saved. Scan this QR Code: " + shortUrl + " to download the file.");
        
        JSONObject ret = new JSONObject();
        ret.put("checksum", md5);
        return ret.toString();
    }
    
    @RequestMapping(value = "update-address/{caseID}")
    public Object getDataBotIndex(
            @PathVariable(value="caseID") int caseId,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        
        InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
        
        ModelAndView model = new ModelAndView("WEB-INF/jsp/customerInput");
        model.addObject("caseId", caseId); 
        model.addObject("customerName", ic.getCustomerName()); 
        return model;
    }
}
