package com.jadu.front.controller;

import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.CompanyBranchDivisionDAO;
import com.jadu.dao.InspectorDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dto.RestWrapperDTO;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.Pair;
import com.jadu.helpers.TimeHelper;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.Agent;
import com.jadu.model.AgentDetail;
import com.jadu.model.Authority;
import com.jadu.model.CompanyBranchDivision;
import com.jadu.model.Inspector;
import com.jadu.model.InspectorDetail;
import com.jadu.model.KYC;
import com.jadu.model.User;
import com.jadu.model.builder.KYCBuilder;
import com.jadu.service.AppConfigService;
import com.jadu.service.ConnectCustomerToFlow;
import com.jadu.service.ImageService;
import com.jadu.service.MailService;
import com.jadu.service.SmsService;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.validator.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("oauth/create-users")
public class CreateUsersController {

	@Autowired
	private AgentDAOImpl agentDAO;

	@Autowired
	private UserDAOImpl userDAO;

	@Autowired
	private CompanyBranchDivisionDAO companyBranchDivisionDAO;

	@Autowired
	private ImageService imageService;

	@Autowired
	private InspectorDAO inspectorDAO;

	@Autowired
	private SmsService smsService;

	@Autowired
	private MailService mailService;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private ConnectCustomerToFlow connectCustomerToFlow;

	@RequestMapping(value = "/inspector", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public void createAgent(@RequestParam(value = "profile_photo", required = true) MultipartFile profilePhoto,
			@RequestParam(value = "first_name", required = true) String firstName,
			@RequestParam(value = "last_name", required = true) String lastName,
			@RequestParam(value = "phone_number", required = true) String phoneNumber,
			@RequestParam(value = "email_id", required = true) String emailId,
			@RequestParam(value = "latitude", required = true) Double latitude,
			@RequestParam(value = "longitude", required = true) Double longitude,
			@RequestParam(value = "radius_of_operation", required = true) double radiusOfOperation,
			@RequestParam(value = "device_id", required = true) String deviceId,
			@RequestParam(value = "state", required = true) int state,
			@RequestParam(value = "division", required = true) int division) throws Exception {

		emailId = emailId.toLowerCase();

		if (!EmailValidator.getInstance().isValid(emailId))
			throw new Exception("Invalid email address!");

		if (userDAO.getUserByEmail(emailId) != null)
			throw new Exception("User exists with same email. Please register with another email!");

		if (!userDAO.getUserByPhoneNumber(phoneNumber).isEmpty())
			throw new Exception("Phone number is already associated with another account. User another phone number!");

		String username = "USER_" + UtilHelper.getNumericString(5) + TimeHelper.getCurrentTimestampString();
		Pair<String, String> profilePicPhotoName = imageService.saveProfileImageGetFileName(profilePhoto);
		String password = phoneNumber;

		Authority authority = new Authority();
		Inspector inspector = new Inspector();
		KYC kyc = new KYCBuilder(inspector).setUserInfo("", "", "", "")
				.setAddressInfo("", "", "", "", "", "", "", "", "", "").setProfilePhoto(null).build();

		InspectorDetail inspectorDetail = new InspectorDetail();
		inspectorDetail.setState(state);
		inspectorDetail.setDivision(division);

		authority.setAuthority("ROLE_INSPECTOR");
		authority.setUser(inspector);

		inspectorDetail.setInspector(inspector);

		inspector.setAuthority(authority);
		inspector.setInspectorDetail(inspectorDetail);
		inspector.setUsername(username);
		inspector.setPassword(password);
		inspector.setFirstName(firstName);
		inspector.setLastName(lastName);
		inspector.setEmailVerified(false);
		inspector.setPhoneNumber(phoneNumber);
		inspector.setPhoneNumberVerified(false);
		inspector.setKyc(kyc);
		inspector.setPasswordChangeRequired(true);
		inspector.setProfilePhotoUrl(profilePicPhotoName.getLeft());
		inspector.setProfilePhotoUrlThumb(profilePicPhotoName.getRight());
		inspector.setEmail(emailId);
		inspector.setEnabled(true);
		inspector.setLatitude(latitude);
		inspector.setLongitude(longitude);
		inspector.setRadiusOfOperation(radiusOfOperation);
		inspector.setDeviceLocked(true);
		inspector.setDeviceId(deviceId);
		inspector.setCreatedDate(new Date());
		inspectorDAO.save(inspector);

		smsService.sendRegistrationPassword(inspector, password);
	}

	@RequestMapping(value = "/agent-without-kyc", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public Object createAgentWithoutKYC(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "profile_photo", required = true) MultipartFile profilePhoto,
			@RequestParam(value = "company_branch_division_id", required = true) int companyBranchDivisionId,
			@RequestParam(value = "first_name", required = true) String firstName,
			@RequestParam(value = "last_name", required = true) String lastName,
			@RequestParam(value = "phone_number", required = true) String phoneNumber,
			@RequestParam(value = "email_id", required = true) String emailId,
			@RequestParam(value = "agent_id", required = true) String agentId,
			@RequestParam(value = "device_id", required = true) String deviceId) throws Exception {

		emailId = emailId.toLowerCase();

		if (!EmailValidator.getInstance().isValid(emailId))
			throw new Exception("Invalid email address!");

		if (userDAO.getUserByEmail(emailId) != null)
			throw new Exception("User exists with same email. Please register with another email!");

		if (!userDAO.getUserByPhoneNumber(phoneNumber).isEmpty())
			throw new Exception("Phone number is already associated with another account. User another phone number!");

		CompanyBranchDivision companyBranchDivision = companyBranchDivisionDAO
				.getCompanyBranchDivisionById(companyBranchDivisionId);

		if (companyBranchDivision == null)
			throw new Exception("Invalid branch selected!");

		String username = "USER_" + UtilHelper.getNumericString(5) + TimeHelper.getCurrentTimestampString();

		Pair<String, String> profilePicPhotoName = imageService.saveProfileImageGetFileName(profilePhoto);

		Authority authority = new Authority();
		Agent agent = new Agent();

		AgentDetail agentDetail = new AgentDetail();
		String password = phoneNumber;

		KYC kyc = new KYCBuilder(agent).setUserInfo("NA", null, null, null).build();

		authority.setAuthority("ROLE_AGENT");
		authority.setUser(agent);

		agentDetail.setCompanyBranchDivision(companyBranchDivision);
		agentDetail.setAgent(agent);
		agentDetail.setAgentId(agentId);

		agent.setAuthority(authority);
		agent.setAgentDetails(agentDetail);
		agent.setUsername(username);
		agent.setPassword(password);
		agent.setFirstName(firstName);
		agent.setLastName(lastName);
		agent.setEmailVerified(appConfigService.getBooleanProperty("IS_TO_SET_EMAIL_VERIFIED_IN_AGENT_CREATION", true));
		agent.setPhoneNumber(phoneNumber);
		agent.setPhoneNumberVerified(
				appConfigService.getBooleanProperty("IS_TO_SET_PHONE_VERIFIED_IN_AGENT_CREATION", false));
		agent.setKyc(kyc);

		agent.setPasswordChangeRequired(appConfigService.getBooleanProperty("PASSWORD_CHANGE_REQUIRED", Boolean.TRUE));
		agent.setDeviceLocked(appConfigService.getBooleanProperty("LOCK_DEVICE_TO_ACCOUNT", Boolean.FALSE));
		agent.setProfilePhotoUrl(profilePicPhotoName.getLeft());
		agent.setProfilePhotoUrlThumb(profilePicPhotoName.getRight());
		agent.setEmail(emailId);
		agent.setEnabled(true);
		agent.setDeviceId(deviceId);
		agent.setCreatedDate(new Date());
		agentDAO.save(agent);

		smsService.sendRegistrationPassword(agent, password);
		connectCustomerToFlow.callCustomerAfterSignup(phoneNumber);

		HashMap<String, Object> result = new HashMap<>();

		return new RestWrapperDTO(result);
	}

	@RequestMapping(value = "/resend-mail-otp", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public void resendMailOTP(@RequestParam(value = "username", required = true) String username) throws Exception {
		User user = userDAO.getUserByUsername(username);

		if (user == null)
			throw new Exception("No user with given username!");

		if (user.isEmailVerified())
			throw new Exception("Email has already been verified!");

		mailService.sendRegistrationOTP(user, "OTP - Use this to validate email address.");
	}

	@RequestMapping(value = "/resend-phone-otp", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public void resendPhoneOTP(@RequestParam(value = "username", required = true) String username) throws Exception {
		User user = userDAO.getUserByUsername(username);

		if (user == null)
			throw new Exception("No user with given username!");

		if (user.isPhoneNumberVerified())
			throw new Exception("Phone number has already been verified!");

		smsService.sendRegistrationOTP(user);
	}

	@RequestMapping(value = "/validate-phone-otp", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public void validatePhoneOTP(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "otp", required = true) String otpVal) throws Exception {
		User user = userDAO.getUserByUsername(username);

		if (user == null)
			throw new Exception("No user with given username!");

		if (smsService.validateRegistrationOTP(user, otpVal))
			userDAO.updatePhoneValidation(user.getUsername(), true);
	}

	@RequestMapping(value = "/validate-email-otp", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public void validateEmailOTP(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "otp", required = true) String otpVal) throws Exception {
		User user = userDAO.getUserByUsername(username);

		if (user == null)
			throw new Exception("No user with given username!");

		if (mailService.validateRegistrationOTP(user, otpVal))
			userDAO.updateEmailValidation(user.getUsername(), true);
	}

	@RequestMapping(value = "/customer/resend-phone-otp", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public void resendPhoneOTPCustomer(@RequestParam(value = "username", required = true) String phoneNumber)
			throws Exception {
		User user = userDAO.getUserByPhoneOrEmail(phoneNumber);

		if (user == null)
			throw new Exception("User account doesn't exist!");

		smsService.sendRegistrationOTP(user);
	}

	@ExceptionHandler(Exception.class)
	public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
		ErrorHandler.handleError(ex, response);
	}
}
