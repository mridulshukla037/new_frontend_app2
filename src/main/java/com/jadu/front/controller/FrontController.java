package com.jadu.front.controller;

import com.jadu.dao.CaseDAO;
import com.jadu.model.InspectionCase;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author gautam
 */
@RestController
public class FrontController {
    
    @Autowired
    private CaseDAO caseDAO;
    
    @RequestMapping(value = "update-address/{caseID}")
    public Object getDataBotIndex(
            @PathVariable(value="caseID") int caseId,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        
        InspectionCase ic = (InspectionCase) caseDAO.getCaseById(caseId);
        
        //if(ic == null)
        //    throw new Exception("Invalid url!");
        
        ModelAndView model = new ModelAndView("WEB-INF/jsp/customerInput");
        model.addObject("caseId", caseId); 
        model.addObject("customerName", ic.getCustomerName()); 
        return model;
    }
    
    @RequestMapping(value = "customer-inspection/{caseID}")
    public Object customerInspection(
            @PathVariable(value="caseID") int caseId,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        
        ModelAndView model = new ModelAndView("WEB-INF/jsp/inspection");
        model.addObject("caseId", caseId);
        return model;
    }
}
