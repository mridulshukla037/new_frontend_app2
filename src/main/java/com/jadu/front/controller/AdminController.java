package com.jadu.front.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.validator.EmailValidator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.ClaimCaseDAO;
import com.jadu.dao.CompanyBranchDivisionDAO;
import com.jadu.dao.OTPDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.TimeHelper;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.Admin;
import com.jadu.model.Agent;
import com.jadu.model.AgentDetail;
import com.jadu.model.Authority;
import com.jadu.model.CompanyBranchDivision;
import com.jadu.model.CompanyBranchDivisionUser;
import com.jadu.model.CompanyUser;
import com.jadu.model.KYC;
import com.jadu.model.QC;
import com.jadu.model.User;
import com.jadu.model.builder.KYCBuilder;
import com.jadu.service.AppConfigService;
import com.jadu.service.RedisReconcileService;
import com.jadu.service.RedisService;
import com.jadu.service.SmsService;
import com.jadu.service.UtilService;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	private ClaimCaseDAO claimCaseDAO;

	@Autowired
	private AgentDAOImpl agentDAO;

	@Autowired
	private UserDAOImpl userDAO;

	@Autowired
	private SmsService smsService;

	@Autowired
	private OTPDAO otpDAO;

	@Autowired
	private CompanyBranchDivisionDAO companyBranchDivisionDAO;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private RedisService redisService;

	@Autowired
	private RedisReconcileService redisReconcileService;

	@Autowired
	private UtilService utilService;

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

	@RequestMapping(value = "/get-unexpired-otp", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object getUnexpiredPhoneOtp(@RequestParam(value = "username", required = true) String username)
			throws JSONException {
		User user = userDAO.getUserByUsername(username);

		return otpDAO.getUnexpiredOtp(user.getUsername(), new Date());
	}

	@RequestMapping(value = "/update-enabled", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object updateEnbabled(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "enabled", required = true) boolean enabled) throws JSONException {
		User user = userDAO.getUserByUsername(username);
		user.setEnabled(enabled);
		userDAO.save(user);

		return user;
	}

	@RequestMapping(value = "/reset-password", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object restPAssword(@RequestParam(value = "username", required = true) String username)
			throws JSONException, IOException {
		User user = userDAO.getUserByUsername(username);
		user.setPassword(user.getPhoneNumber());
		userDAO.save(user);

		smsService.sendSMS(user.getPhoneNumber(),
				"Your WIMWIsure account password has been reset to " + user.getPhoneNumber());
		return user;
	}

	@RequestMapping(value = "/update-email", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object updateEmail(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "email", required = true) String email) throws JSONException, Exception {

		if (!EmailValidator.getInstance().isValid(email))
			throw new Exception("Invalid email address!");

		if (userDAO.getUserByEmail(email) != null)
			throw new Exception("Another user already exists with same email. Please use another email!");

		User user = userDAO.getUserByUsername(username);

		if (user == null)
			throw new Exception("User not found with given username!");

		user.setEmail(email);
		user.setEmailVerified(appConfigService.getBooleanProperty("IS_TO_SET_EMAIL_VERIFIED_IN_AGENT_CREATION", true));
		userDAO.save(user);
		redisService.updatEmail(user, email);
		return user;
	}

	@RequestMapping(value = "/update-phone-number", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object updatePhoneNumber(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "phoneNumber", required = true) String phoneNumber) throws Exception {
		if (!userDAO.getUserByPhoneNumber(phoneNumber).isEmpty())
			throw new Exception("Phone number is already associated with another account. Use another phone number!");
		User user = userDAO.getUserByUsername(username);
		String oldPhoneNumber = user.getPhoneNumber();
		user.setPhoneNumber(phoneNumber);
		user.setPhoneNumberVerified(
				appConfigService.getBooleanProperty("IS_TO_SET_PHONE_VERIFIED_IN_AGENT_CREATION", false));
		userDAO.save(user);
		redisService.updatePhoneNumber(user, phoneNumber, oldPhoneNumber);
		return user;
	}

	@RequestMapping(value = "/update-device-id", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object updateDeviceId(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "deviceId", required = true) String deviceId) throws JSONException {
		User user = userDAO.getUserByUsername(username);
		user.setDeviceId(deviceId);
		userDAO.save(user);

		return user;
	}

	@RequestMapping(value = "/update-device-locked", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object updateDeviceLocked(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "deviceLocked", required = true) boolean deviceLocked) throws JSONException {
		User user = userDAO.getUserByUsername(username);
		user.setDeviceLocked(deviceLocked);
		userDAO.save(user);

		return user;
	}

	@RequestMapping(value = "/get-dashboard-stats", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object getDashboardStatus(@RequestParam(value = "company", required = false) String company,
			@RequestParam(value = "state", required = false) String state,
			@RequestParam(value = "division", required = false) String division,
			@RequestParam(value = "branch", required = false) String branch,
			@RequestParam(value = "duration", required = true) String duration) throws Exception {

		Date fromDate;
		CompanyBranchDivisionUser params = new CompanyBranchDivisionUser();
		params.setCompany(company);
		params.setDivision(division);
		params.setBranch(branch);
		params.setState(state);

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user).getCompanyBranchDivisionUser();
			companyBranchDivisionUsers.add(params);
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user).getCompanyBranchDivisionUser();
			companyBranchDivisionUsers.add(params);
		}
		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user).getCompanyBranchDivisionUser();
		}

		switch (duration) {
		case "TODAY":
			fromDate = UtilHelper.getZerothHourOfDay();
			break;
		case "CURRENT_MONTH":
			fromDate = UtilHelper.getFirstDayOfMonth();
			break;
		case "CURRENT_YEAR":
			fromDate = UtilHelper.getFirstDayOfYear();
			break;
		case "CURRENT_WEEK":
			fromDate = UtilHelper.getFirstDayOfWeek();
			break;
		default:
			throw new Exception("Invalid time duration!");
		}

		return claimCaseDAO.getCasesSummary(fromDate, UtilHelper.getZerothHourOfNextDay(), companyBranchDivisionUsers);
	}

	@RequestMapping(value = "/get-dashboard-table-fields", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getDashboardTableFields() throws JSONException {
		JSONArray result = new JSONArray();

		JSONObject item1 = new JSONObject();
		item1.put("id", "id");
		item1.put("value", "Case ID");
		result.put(item1);

		JSONObject item2 = new JSONObject();
		item2.put("id", "state");
		item2.put("value", "State");
		result.put(item2);

		JSONObject item3 = new JSONObject();
		item3.put("id", "division");
		item3.put("value", "Division");
		result.put(item3);

		JSONObject item4 = new JSONObject();
		item4.put("id", "branch");
		item4.put("value", "Branch");
		result.put(item4);

		JSONObject item5 = new JSONObject();
		item5.put("id", "vehicle_type_id");
		item5.put("value", "Vehicle Type");
		result.put(item5);

		JSONObject item6 = new JSONObject();
		item6.put("id", "creation_time");
		item6.put("value", "Creation Time");
		result.put(item6);

		JSONObject item7 = new JSONObject();
		item7.put("id", "inspection_time");
		item7.put("value", "Inspection Time");
		result.put(item7);

		JSONObject item8 = new JSONObject();
		item8.put("id", "vehicle_number");
		item8.put("value", "Vehicle Number");
		result.put(item8);

		JSONObject item9 = new JSONObject();
		item9.put("id", "make");
		item9.put("value", "Vehicle Make");
		result.put(item9);

		JSONObject item10 = new JSONObject();
		item10.put("id", "model");
		item10.put("value", "Vehicle Model");
		result.put(item10);

		JSONObject item11 = new JSONObject();
		item11.put("id", "remark");
		item11.put("value", "QC Status");
		result.put(item11);

		JSONObject item12 = new JSONObject();
		item12.put("id", "requestor_phone");
		item12.put("value", "Agent Phone Number");
		result.put(item12);

		JSONObject item13 = new JSONObject();
		item13.put("id", "customer_phone");
		item13.put("value", "Cutomer Phone Number");
		result.put(item13);

		JSONObject item14 = new JSONObject();
		item14.put("id", "inspection_type");
		item14.put("value", "Inspection Type");
		result.put(item14);

		return result.toString();
	}

	@RequestMapping(value = "/get-dashboard-table-data", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Map<String, Object> getDashboardTableData(@RequestParam(value = "fields", required = true) String str,
			@RequestParam(value = "from", required = true) int from,
			@RequestParam(value = "to", required = true) int to,
			@RequestParam(value = "duration", required = true) String duration,
			@RequestParam(value = "company", required = false) String company,
			@RequestParam(value = "state", required = false) String state,
			@RequestParam(value = "division", required = false) String division,
			@RequestParam(value = "branch", required = false) String branch) throws Exception {
		Date fromDate;

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user).getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user).getCompanyBranchDivisionUser();
		}
		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user).getCompanyBranchDivisionUser();
		}

		List<String> fields = Arrays.asList(str.split("\\s*,\\s*"));
		switch (duration) {
		case "TODAY":
			fromDate = UtilHelper.getZerothHourOfDay();
			break;
		case "CURRENT_MONTH":
			fromDate = UtilHelper.getFirstDayOfMonth();
			break;
		case "CURRENT_YEAR":
			fromDate = UtilHelper.getFirstDayOfYear();
			break;
		case "CURRENT_WEEK":
			fromDate = UtilHelper.getFirstDayOfWeek();
			break;
		default:
			throw new Exception("Invalid time duration!");
		}

		return claimCaseDAO.getCasesData(fields, from, to, fromDate, company, state, division, branch,
				companyBranchDivisionUsers);
	}

	@RequestMapping(value = "/export-table-data", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void exportTableData(@RequestParam(value = "fields", required = true) String str,
			@RequestParam(value = "duration", required = true) String duration) throws Exception {
		Date fromDate;
		List<String> fields = Arrays.asList(str.split("\\s*,\\s*"));
		switch (duration) {
		case "TODAY":
			fromDate = UtilHelper.getZerothHourOfDay();
			break;
		case "CURRENT_MONTH":
			fromDate = UtilHelper.getFirstDayOfMonth();
			break;
		case "CURRENT_YEAR":
			fromDate = UtilHelper.getFirstDayOfYear();
			break;
		default:
			throw new Exception("Invalid time duration!");
		}

		claimCaseDAO.exportCasesData(fields, fromDate);
	}

	@RequestMapping(value = "/get-agents", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getAgents() {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user).getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user).getCompanyBranchDivisionUser();
		}

		return agentDAO.getAllAgents(companyBranchDivisionUsers);
	}

	@RequestMapping(value = "/get-agents-by-company", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getAgentsByCompany(@RequestParam(value = "company", required = true) String company) {

		/*
		 * List<CompanyBranchDivision> cbdList =
		 * companyBranchDivisionDAO.getCompanyBranchDivisionByCompany(company); String
		 * companyBranchDivisionIds = ""; for (CompanyBranchDivision
		 * companyBranchDivision : cbdList) { if (companyBranchDivisionIds.isEmpty())
		 * companyBranchDivisionIds += companyBranchDivision.getId(); else
		 * companyBranchDivisionIds += " , " + companyBranchDivision.getId();
		 * 
		 * }
		 */
		return agentDAO.getAgentsByCompanyBranchDivision(company);
	}

	@RequestMapping(value = "/get-top-agents", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getTopAgents() {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = utilService.getCompanyBranchDivisionUser(user);
		List<String> topAgents = agentDAO.getLatestAgents(companyBranchDivisionUsers);
		if (appConfigService.getBooleanProperty("IS_TO_FETCH_TOP_AGENTS_FROM_CACHE", true)) {
			List<Object> agentDetails = redisReconcileService.getAgentDetails(user, topAgents, false);
			LOGGER.info("Total agents fetched from cache for user={} are {}", user.getPhoneNumber(),
					topAgents != null ? topAgents.size() : 0);
			if (agentDetails == null || agentDetails.isEmpty()) {
				LOGGER.info("No agents fetched from cache for user={}, reconciling the cache", user.getPhoneNumber(),
						topAgents != null ? topAgents.size() : 0);
				return redisReconcileService.getAgentDetails(user, topAgents, true);
			} else {
				return agentDetails;
			}
		} else {
			return redisReconcileService.getAgentDetails(user, topAgents, true);
		}

	}

	@RequestMapping(value = "/get-all-agents", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getAllAgents() throws Exception {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		List<Object> allAgents = new ArrayList<Object>();
		if (appConfigService.getBooleanProperty("IS_TO_FETCH_FROM_CACHE", true)) {
			allAgents = redisReconcileService.reconcileAllAgentsForUser(user, false);
			LOGGER.info("Total agents fetched from cache for user={} are {}", user.getUsername(),
					allAgents != null ? allAgents.size() : 0);
			if (allAgents == null || allAgents.isEmpty()) {
				LOGGER.info("No agents fetched from cache for user={}, reconciling the cache", user.getUsername(),
						allAgents != null ? allAgents.size() : 0);
				return redisReconcileService.reconcileAllAgentsForUser(user, true);
			}
		} else {
			return redisReconcileService.reconcileAllAgentsForUser(user, true);
		}
		return allAgents;
	}

	@RequestMapping(value = "/create-agent-without-kyc", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public void qcCreateAgentWithoutKYC(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "company_branch_division_id", required = true) int companyBranchDivisionId,
			@RequestParam(value = "first_name", required = true) String firstName,
			@RequestParam(value = "last_name", required = true) String lastName,
			@RequestParam(value = "phone_number", required = true) String phoneNumber,
			@RequestParam(value = "email_id", required = true) String emailId,
			@RequestParam(value = "agent_code", required = true) String agentId) throws Exception {
		LOGGER.info(
				"/create-agent-without-kyc request recieved with company_branch_division_id={}, first_name={},last_name={},phone_number={},email_id={},agent_code={}",
				companyBranchDivisionId, firstName, lastName, phoneNumber, emailId, agentId);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		emailId = emailId.toLowerCase();

		if (!EmailValidator.getInstance().isValid(emailId))
			throw new Exception("Invalid email address!");

		if (userDAO.getUserByEmail(emailId) != null)
			throw new Exception("User exists with same email. Please register with another email!");

		if (!userDAO.getUserByPhoneNumber(phoneNumber).isEmpty())
			throw new Exception("Phone number is already associated with another account. User another phone number!");

		String username = "USER_" + UtilHelper.getNumericString(5) + TimeHelper.getCurrentTimestampString();
		Authority authority = new Authority();
		Agent agent = new Agent();
		CompanyBranchDivision companyBranchDivision = companyBranchDivisionDAO
				.getCompanyBranchDivisionById(companyBranchDivisionId);
		AgentDetail agentDetail = new AgentDetail();
		String password = phoneNumber;

		KYC kyc = new KYCBuilder(agent).setUserInfo("NA", null, null, null).build();

		authority.setAuthority("ROLE_AGENT");
		authority.setUser(agent);

		agentDetail.setCompanyBranchDivision(companyBranchDivision);
		agentDetail.setAgent(agent);
		agentDetail.setAgentId(agentId);

		agent.setAuthority(authority);
		agent.setAgentDetails(agentDetail);
		agent.setUsername(username);
		agent.setPassword(password);
		agent.setFirstName(firstName);
		agent.setLastName(lastName);
		agent.setEmailVerified(appConfigService.getBooleanProperty("IS_TO_SET_EMAIL_VERIFIED_IN_AGENT_CREATION", true));
		agent.setPhoneNumber(phoneNumber);
		agent.setPhoneNumberVerified(
				appConfigService.getBooleanProperty("IS_TO_SET_PHONE_VERIFIED_IN_AGENT_CREATION", false));
		agent.setKyc(kyc);
		agent.setPasswordChangeRequired(appConfigService.getBooleanProperty("PASSWORD_CHANGE_REQUIRED", Boolean.FALSE));
		agent.setProfilePhotoUrl(null);
		agent.setEmail(emailId);
		agent.setEnabled(true);
		agent.setDeviceLocked(appConfigService.getBooleanProperty("LOCK_DEVICE_TO_ACCOUNT", false));
		agent.setDeviceId(null);
		agent.setCreatedDate(new Date());
		agentDAO.save(agent);

		smsService.sendRegistrationPassword(agent, password);
		if (appConfigService.getBooleanProperty("IS_TO_PUSH_IN_CACHE", true)) {
			utilService.prepareAllUserDTOAndCache(agent, phoneNumber);
		}

	}

	@ExceptionHandler(Exception.class)
	public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
		ErrorHandler.handleError(ex, response);
		ex.printStackTrace();
	}

	@RequestMapping(value = "/update-firstName", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object updateFirstName(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "firstname", required = true) String firstName) throws JSONException, Exception {

		User user = userDAO.getUserByUsername(username);

		if (user == null)
			throw new Exception("User not found with given username!");
		if (firstName != null & !firstName.isEmpty()) {
			if (!firstName.equalsIgnoreCase(user.getFirstName())) {
				user.setFirstName(firstName);
				userDAO.save(user);
				redisService.updateName(user, firstName, null);

			}
		}
		return user;
	}

	@RequestMapping(value = "/update-lastName", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object updateLastName(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "lastname", required = true) String lastName) throws JSONException, Exception {
		User user = userDAO.getUserByUsername(username);
		if (user == null)
			throw new Exception("User not found with given username!");
		if (lastName != null && !lastName.isEmpty()) {
			if (!lastName.equalsIgnoreCase(user.getLastName())) {
				user.setLastName(lastName);
				userDAO.save(user);
				redisService.updateName(user, null, lastName);
			}
		}

		return user;
	}

	@RequestMapping(value = "/update-agentCode", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object updateAgent(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "agentcode", required = true) String agentCode) throws JSONException, Exception {

		Agent agent = agentDAO.getAgentByUsername(username);

		if (agent == null)
			throw new Exception("Agent not found with given username!");

		AgentDetail agentDetail = agent.getAgentDetails();
		agentDetail.setAgentId(agentCode);
		agent.setAgentDetails(agentDetail);
		agentDAO.saveOrUpdate(agent);
		redisService.updateAgentCode(agent, agentCode);
		return agent.getAuthority().getUser();
	}

	@RequestMapping(value = "/update-branch", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object updateBranch(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "branch", required = true) String branch) throws JSONException, Exception {

		Agent agent = agentDAO.getAgentByUsername(username);

		if (agent == null)
			throw new Exception("Agent not found with given username!");
		if (branch == null || branch.isEmpty())
			throw new Exception("No branch found, kindly enter your branch!");

		CompanyBranchDivision companyBranchDivision = agent.getAgentDetails().getCompanyBranchDivision();
		List<CompanyBranchDivision> cbdList = companyBranchDivisionDAO
				.getCompanyBranchDivisionByCompany(companyBranchDivision.getCompany());
		for (CompanyBranchDivision cbd : cbdList) {
			if (branch.equalsIgnoreCase(cbd.getBranch())) {
				companyBranchDivision.setBranch(branch);
				companyBranchDivisionDAO.save(companyBranchDivision);
				redisService.updateAgentBranch(agent, branch);
				return agent.getAuthority().getUser();
			}
		}
		throw new Exception("No such branch found!, Kindly re-check and enter your correct branch!");

	}
}
