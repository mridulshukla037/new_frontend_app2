package com.jadu.front.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/predict")
public class PredictController {
    
    @Autowired
    private Environment env;
    
    
    @RequestMapping(value = "/predict-image-type", method = RequestMethod.POST)
    @ResponseBody
    public String predictImageType(
            @RequestParam(value = "photo", required=true) MultipartFile photo
    ) throws Exception{
        String rootUploadFolder = env.getProperty("jadu.upload.root.url")+ "/test/";
        
        Path fileToDeletePath = Paths.get(rootUploadFolder + "output.txt");
        Files.deleteIfExists(fileToDeletePath);

        String photoName = 
                "test." + FilenameUtils.getExtension(photo.getOriginalFilename());
        photo.transferTo(new File( rootUploadFolder + photoName));
        String[] cmd = {
            "/bin/bash",
            "-c",
            "python yourapp.py " + rootUploadFolder + photoName + " "+ rootUploadFolder + "output.txt"
        };
        Process p = Runtime.getRuntime().exec(cmd);
        while(p.isAlive()){
            Thread.sleep(1000);
        }
        
        if(!Files.exists(fileToDeletePath))
            throw new Exception("Unable to predict. Please contact administrator");
        
        String text;
        try (Scanner scanner = new Scanner( new File(rootUploadFolder + "output.txt") )) {
            text = scanner.useDelimiter("\\A").next();
        }
        switch(text){
            case "0":
                return "front";
            case "1":
                return "back";
            case "2":
                return "left";
            case "3":
                return "right";
            default:
                    throw new Exception("Invalid outcome");
        }
    }
}
