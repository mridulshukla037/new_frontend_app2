package com.jadu.front.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.AssessmentSummaryDAO;
import com.jadu.dao.CaseLogDAO;
import com.jadu.dao.CaseQCAnswerDAO;
import com.jadu.dao.CaseQCQuestionDAO;
import com.jadu.dao.CaseQCQuestionOptionDAO;
import com.jadu.dao.CaseUserAllocationDAO;
import com.jadu.dao.ClaimCaseCommentDAO;
import com.jadu.dao.ClaimCaseDAO;
import com.jadu.dao.ClaimCasePhotoDAO;
import com.jadu.dao.ClaimsPhotoTypeDAO;
import com.jadu.dao.InspectionTypeDAO;
import com.jadu.dao.InspectorDAO;
import com.jadu.dao.InsuranceCompanyDAO;
import com.jadu.dao.LossAssessmentDAOImpl;
import com.jadu.dao.LossAssessmentPhotosDAO;
import com.jadu.dao.PhotoTypeDAO;
import com.jadu.dao.PurposeOfInspectionDAO;
import com.jadu.dao.PurposeOfSurveyDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dao.VehicleDetailsDAOImpl;
import com.jadu.dao.VehicleFuelTypeDAO;
import com.jadu.dao.VehicleSubTypeDAO;
import com.jadu.dao.VehicleTypeDAO;
import com.jadu.dto.AgentsDTO;
import com.jadu.dto.AllCases;
import com.jadu.dto.CaseCommentDTO;
import com.jadu.dto.ClaimCaseDto;
import com.jadu.dto.ClaimsFinalSubmissionDTO;
import com.jadu.dto.ScheduledCasesDTO;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.BoundingBox;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.Admin;
import com.jadu.model.Agent;
import com.jadu.model.ClaimCase;
import com.jadu.model.ClaimCasePhoto;
import com.jadu.model.ClaimCaseUserAllocation;
import com.jadu.model.CompanyBranchDivision;
import com.jadu.model.CompanyBranchDivisionUser;
import com.jadu.model.CompanyUser;
import com.jadu.model.GeoLocation;
import com.jadu.model.Inspector;
import com.jadu.model.PurposeOfSurvey;
import com.jadu.model.QC;
import com.jadu.model.VehicleDetails;
import com.jadu.model.builder.ClaimCaseBuilder;
import com.jadu.pdf.create.GenerateReport;
import com.jadu.push.notification.AndroidPushNotificationsService;
import com.jadu.service.AppConfigService;
import com.jadu.service.CaseService;
import com.jadu.service.ClaimCaseService;
import com.jadu.service.ClaimsReportDataGeneratorFactory;
import com.jadu.service.ConnectCustomerToFlow;
import com.jadu.service.CreateUserService;
import com.jadu.service.MailService;
import com.jadu.service.RedisService;
import com.jadu.service.S3DirectUploadService;
import com.jadu.service.S3Service;
import com.jadu.service.SmsService;
import com.jadu.service.URLShortnerService;
import com.jadu.service.UtilService;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/qc")
public class QCController {

	@Autowired
	private UserDAOImpl userDAO;

	@Autowired
	private AgentDAOImpl agentDAO;

	@Autowired
	private InsuranceCompanyDAO insuranceCompanyDAO;

	@Autowired
	private CaseQCQuestionOptionDAO caseQCQuestionOptionDAO;

	@Autowired
	private CaseQCAnswerDAO caseQCAnswerDAO;

	@Autowired
	private SmsService smsService;

	@Autowired
	private URLShortnerService urlShortnerService;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private CaseLogDAO caseLogDAO;

	@Autowired
	private S3Service s3Service;

	@Autowired
	private Environment env;

	@Autowired
	private MailService mailService;

	@Autowired
	private VehicleDetailsDAOImpl vehicleDetailsDAO;

	@Autowired
	private S3DirectUploadService s3DirectUploadService;

	@Autowired
	private CaseService caseService;

	@Autowired
	private InspectorDAO inspectorDAO;

	@Autowired
	private InspectionTypeDAO inspectionTypeDAO;

	@Autowired
	private CreateUserService createUserService;

	@Autowired
	private ClaimCaseCommentDAO claimCaseCommentDAO;

	@Autowired
	private ConnectCustomerToFlow connectCustomerToFlow;

	@Autowired
	private CaseUserAllocationDAO caseUserAllocationDAO;

	@Autowired
	private RedisService redisService;

	@Autowired
	private UtilService utilService;

	@Autowired
	private PurposeOfSurveyDAO posDAO;

	@Autowired
	private ClaimCaseDAO claimCaseDAO;

	@Autowired
	private ClaimsReportDataGeneratorFactory claimsReportDataGeneratorFactory;

	@Autowired
	private ClaimCasePhotoDAO claimCasePhotoDAO;

	@Autowired
	private ClaimCaseService claimCaseService;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(QCController.class);

	@RequestMapping(value = "/cases/inspection", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> submitInspectionFile(
			@RequestParam(value = "case_id", required = true) int caseId,
			@RequestParam(value = "case_file", required = true) MultipartFile caseFile)
			throws Exception {
		JSONObject result = new JSONObject();

		ClaimCase c = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (c == null) {
			LOGGER.info("Unable to find case with given case_id={} ", caseId);
			return new ResponseEntity<>(
					"Unable to find case with given case_id=" + caseId,
					HttpStatus.NOT_EXTENDED);
		}

		if (c.getCurrentStage() > 3) {
			LOGGER.info("Case={} has already been submitted to QC!", caseId);
			return new ResponseEntity<>("Case=" + caseId
					+ "has already been submitted to QC",
					HttpStatus.ALREADY_REPORTED);

		}

		if (c.getCurrentStage() < 3) {
			LOGGER.info("Case={} has not been scheduled yet!", caseId);
			return new ResponseEntity<>("Case=" + caseId
					+ "has not been scheduled yet!",
					HttpStatus.ALREADY_REPORTED);
		}

		String extension = FilenameUtils.getExtension(caseFile
				.getOriginalFilename());

		if (!extension.equals("zip"))
			throw new Exception("Invalid zip file!");
		try {
			s3Service.uploadMultipartFileDirect(caseFile, "cases/" + c.getId()
					+ "/" + c.getId() + ".zip");

			if (s3Service.exists("cases/" + c.getId() + "/" + c.getId()
					+ ".zip")) {

				claimCaseService.uploadZipToS3(c, "cases/" + c.getId() + "/"
						+ c.getId() + ".zip");

				c.setCurrentStage(4);
				claimCaseDAO.save(c);

				result.put("success", true);
			} else {
				LOGGER.info("Claims zip file not uploaded to server for case={}");
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(
					"Exception raised while uploading claim zip case={}, error={}",
					caseId, e.getMessage());
		}
		return new ResponseEntity<>(result.toString(), HttpStatus.OK);

	}

	@RequestMapping(value = "/get-inspection-types")
	@ResponseBody
	public List getInspectionTypes() throws JSONException, Exception {
		return inspectionTypeDAO.getAll();
	}

	@RequestMapping(value = "/move-to-qc")
	@ResponseBody
	public void moveToQC(
			@RequestParam(value = "case_id", required = true) long caseId,
			@RequestParam(value = "vehicle_type", required = true) String type)
			throws JSONException, Exception {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((User) authentication.getPrincipal())
						.getUsername());
		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);
		if (ic.getCurrentStage() != 3)
			return;
		ic.setCurrentStage(4);
		ic.setInspectionSubmitTime(new Date());
		claimCaseDAO.save(ic);

	}

	@RequestMapping(value = "/survey/move-to-qc")
	@ResponseBody
	public void moveToQCClaims(
			@RequestParam(value = "case_id", required = true) long caseId,
			@RequestParam(value = "vehicle_type", required = true) String type)
			throws JSONException, Exception {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((User) authentication.getPrincipal())
						.getUsername());
		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);
		if (ic.getCurrentStage() != 3)
			return;
		ic.setCurrentStage(4);
		ic.setInspectionSubmitTime(new Date());
		claimCaseDAO.save(ic);
		if (appConfigService.getBooleanProperty(
				"IS_TO_REMOVE_FROM_SCHEDULED_CACHED_CASES", true)) {
			try {
				// redisService.removeFromScheduledCases(user, ic);
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error(
						"Exception raised while removing case from scheduled case, caseId={}, vehicle_type={},user={}, error={}",
						caseId, type, user.getPhoneNumber(), e.getMessage());
			}
		}
	}

	@RequestMapping(value = "/VehicleDetails", method = RequestMethod.POST)
	@ResponseBody
	public void addVehicle(
			@RequestParam(value = "vehicle_make", required = true) String make,
			@RequestParam(value = "vehicle_model", required = true) String model,
			@RequestParam(value = "vehicle_type", required = true) String type,
			@RequestParam(value = "vehicle_subtype", required = true) String subtype)
			throws JSONException, Exception {

		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((User) authentication.getPrincipal())
						.getUsername());

		VehicleDetails VehicleDetails = new VehicleDetails();
		vehicleDetailsDAO.save(VehicleDetails);
	}

	/*
	 * @RequestMapping(value = "/VehicleDetails/toggle", method =
	 * RequestMethod.POST)
	 * 
	 * @ResponseBody public void addVehicle(@RequestParam(value = "vehicle_id",
	 * required = true) int vehicle_id,
	 * 
	 * @RequestParam(value = "enabled", required = true) boolean enabled) throws
	 * JSONException, Exception {
	 * 
	 * VehicleDetails VehicleDetails =
	 * vehicleDetailsDAO.getVehicleById(vehicle_id);
	 * 
	 * VehicleDetails.setEnabled(enabled);
	 * 
	 * vehicleDAO.save(VehicleDetails); }
	 */

	@RequestMapping(value = "/get-inspectors-in-radius")
	@ResponseBody
	public List getInspectorInRadius(
			@RequestParam(value = "latitude", required = true) Double latitude,
			@RequestParam(value = "longitude", required = true) Double longitude,
			@RequestParam(value = "radius", required = true) Double radius)
			throws JSONException, Exception {
		List<GeoLocation> result = BoundingBox.getBoundingBox(latitude,
				longitude, radius);
		return inspectorDAO.getInspectorBoundingBox(result.get(0),
				result.get(1));
	}

	@RequestMapping(value = "/assign-case-to-inspector")
	@ResponseBody
	public void assignCaseToInspector(
			@RequestParam(value = "case_id", required = true) long caseId,
			@RequestParam(value = "username", required = true) String username)
			throws JSONException, Exception {
		Inspector inspector = inspectorDAO.getInspectorByUsername(username);

		if (inspector == null)
			throw new Exception("Inspector doesn't exists with given username");

		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Inspection case doesn't exists with given id!");

		if (ic.getCurrentStage() != 1 || ic.getCurrentStage() != 2)
			throw new Exception("Invalid stage!");

		// ic.addClaimCaseUserAllocation(new CaseUserAllocationBuilder(ic,
		// inspector, 3).buildClaimCase());

		ic.setCurrentStage(3);

		claimCaseDAO.save(ic);
	}

	@RequestMapping(value = "/assign-case-to-inspector-by-phone")
	@ResponseBody
	public void assignCaseToInspectorByPhone(
			@RequestParam(value = "case_id", required = true) long caseId,
			@RequestParam(value = "phone_number", required = true) String phoneNumber)
			throws JSONException, Exception {
		Inspector inspector = (Inspector) inspectorDAO
				.getInspectorByPhoneNumber(phoneNumber);

		if (inspector == null)
			throw new Exception("Inspector doesn't exists with given username");

		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Inspection case doesn't exists with given id!");

		if (ic.getCurrentStage() != 1 || ic.getCurrentStage() != 2)
			throw new Exception("Invalid stage!");

		// ic.addClaimCaseUserAllocation(new CaseUserAllocationBuilder(ic,
		// inspector, 3).buildClaimCase());

		ic.setCurrentStage(3);

		claimCaseDAO.save(ic);
	}

	@RequestMapping(value = "/manual-qc", method = RequestMethod.POST)
	@ResponseBody
	public void manualQC(
			@RequestParam(value = "case_id", required = true) long caseId,
			@RequestParam(value = "zip_file", required = true) String zipFile)
			throws JSONException, Exception {
		zipFile = zipFile.replace("\\", "/");
		caseService.uploadZipToS3(caseId, zipFile);
	}

	@RequestMapping(value = "/get-bucket-objects", method = RequestMethod.GET)
	@ResponseBody
	public Object getBucketObjects() {
		return s3DirectUploadService.getObjectslistFromFolder();
	}

	@RequestMapping(value = "/get-configurations", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getConfigurations() throws JSONException {
		JSONObject result = new JSONObject();
		result.put("ENABLE_INSPECTOR",
				appConfigService.getProperty("ENABLE_INSPECTOR"));
		return result.toString();
	}

	@RequestMapping(value = "/get-user-details", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getUserDetails(
			@RequestParam(value = "username", required = true) String username)
			throws JSONException {
		return userDAO.getUserByUsername(username);
	}

	@RequestMapping(value = "/get-all-agents", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getAllAgents() throws JSONException {
		if (appConfigService.getBooleanProperty(
				"IS_TO_FETCH_QC_ALL_AGENTS_FROM_CACHE", true)) {
			List<AgentsDTO> allAgents = redisService
					.getAllAgentDetails("ALL_AGENTS_DETAILS");
			if (allAgents == null) {
				allAgents = redisService
						.reconcileAllAgents("ALL_AGENTS_DETAILS");
			}
			return allAgents;
		}
		return agentDAO.getAgentPersonalDetails();
	}

	@RequestMapping(value = "/update-VehicleDetails-number", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateVehicleNumber(
			@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "vehicle_number", required = true) String vehicleNumber)
			throws JSONException, Exception {
		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Unable to get case details!");

		ic.setVehicleNumber(vehicleNumber);

		claimCaseDAO.save(ic);
	}

	@RequestMapping(value = "/update-customer-phone-number", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateCustomerPhoneNumber(
			@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "customer_phone_number", required = true) String customerPhoneNumber)
			throws JSONException, Exception {
		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Unable to get case details!");

		if (ic.getInspectionType().equals("ASSIGN_TO_CUSTOMER")) {
			com.jadu.model.User customer = userDAO
					.getUserByPhoneOrEmail(customerPhoneNumber);

			if (customer == null)
				customer = createUserService.createCustomer("",
						customerPhoneNumber);

			ic.setCustomerPhoneNumber(customerPhoneNumber);

			claimCaseDAO.save(ic);
			caseUserAllocationDAO.update(ic.getId(), 3, customer);

			String url = urlShortnerService.shortenUrl("customer-inspection/"
					+ ic.getId());
			smsService
					.sendSMSAsync(
							customerPhoneNumber,
							"We have an insurance inspection request for "
									+ ic.getVehicleNumber()
									+ ". Please download the app "
									+ url
									+ " to complete the inspection. Call 7290049100 for support");
			connectCustomerToFlow
					.callCustomerAfterCaseCreate(customerPhoneNumber);
		} else {
			ic.setCustomerPhoneNumber(customerPhoneNumber);

			claimCaseDAO.save(ic);
			smsService
					.sendSMS(
							ic.getCustomerPhoneNumber(),
							"We have received an inspection request for VehicleDetails Number - "
									+ ic.getVehicleNumber()
									+ " from Insurance Company. Our agent will contact you shortly.");
		}
	}

	@RequestMapping(value = "/update-inspector-phone-number", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateInspectorPhoneNumber(
			@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "phone_number", required = true) String phoneNumber)
			throws JSONException, Exception {
		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Unable to get case details!");

		Agent agent = agentDAO.getUniqueAgentByPhoneNumber(phoneNumber);

		if (agent == null)
			throw new Exception("No agent exists with given phone number!");

		ic.setRequestor(agent);

		if (ic.getInspectionType().equals("SELF_INSPECT")) {

			claimCaseDAO.save(ic);
			caseUserAllocationDAO.update(ic.getId(), 3, agent);
		} else {

			claimCaseDAO.save(ic);
		}

	}

	// @RequestMapping(value = "/update-case-VehicleDetails-details", method =
	// RequestMethod.POST, produces = "application/json")
	// @ResponseBody
	// public void updateCaseVehicleDetails(@RequestParam(value = "case_id",
	// required = true) Long caseId,
	// @RequestParam(value = "vehicle_number", required = true) String
	// vehicleNumber,
	// @RequestParam(value = "vehicle_type", required = true) String
	// vehicleType,
	// @RequestParam(value = "vehicle_color", required = true) String
	// vehicleColor,
	// @RequestParam(value = "make_model_id", required = true) int makeModelId,
	// @RequestParam(value = "fuel_type", required = true) String fuelType,
	// @RequestParam(value = "yom", required = true) int YOM,
	// @RequestParam(value = "chachis_number", required = false, defaultValue =
	// "") String chachis_number,
	// @RequestParam(value = "vin_plate_number", required = false, defaultValue
	// = "") String vin_plate_number,
	// @RequestParam(value = "odometer_reading", required = false, defaultValue
	// = "") String odometer_reading)
	// throws JSONException, Exception {
	// ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);
	//
	// if (ic == null)
	// throw new Exception("Unable to get case details!");
	//
	// VehicleDetails VehicleDetails =
	// vehicleDetailsDAO.getVehicleById(makeModelId);
	//
	// ic.setVehicleFuelType(vehicleFuelTypeDAO.get(fuelType));
	// ic.setVehicleYOM(YOM);
	// ic.setVehicleColor(vehicleColor);
	// ic.setVehicleDetails(VehicleDetails);
	// ic.setVehicleNumber(vehicleNumber);
	// ic.setVehicleType(vehicleTypeDAO.get(vehicleType));
	//
	// if (!chachis_number.equals("")) {
	// claimCasePhotoDAO.updateQCAnswer(caseId, "chachis-number",
	// chachis_number);
	// ic.setChassisNumber(chachis_number);
	// }
	//
	// if (!vin_plate_number.equals("")) {
	// claimCasePhotoDAO.updateQCAnswer(caseId, "vin-plate-number",
	// vin_plate_number);
	// ic.setEngineNumber(vin_plate_number);
	// }
	//
	// if (!odometer_reading.equals("")) {
	// claimCasePhotoDAO.updateQCAnswer(caseId, "odometer-rpm",
	// odometer_reading);
	// ic.setOdometerReading(odometer_reading);
	// }
	//
	// claimCaseDAO.save(ic);
	// }
	//
	// @RequestMapping(value = "/cases/get-qc-questions", method =
	// RequestMethod.GET, produces = "application/json")
	// @ResponseBody
	// public List getQCQuestions(@RequestParam(value = "case_id", required =
	// true) Long caseId) throws Exception {
	//
	// ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);
	//
	// if (ic == null)
	// throw new Exception("Unable to get case details!");
	//
	// return caseQCQuestionDAO.getQuestions(ic.getVehicleType().getId());
	// }

	@RequestMapping(value = "/cases/get-qc-options", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getQCOptions() throws Exception {

		return caseQCQuestionOptionDAO.getAll();
	}

	@RequestMapping(value = "/cases/case-details", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getCaseDetails(
			@RequestParam(value = "case_id", required = true) Long caseId)
			throws Exception {
		// ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		// if (ic == null)
		// throw new Exception("Unable to get case details!");

		return "{id:39,vehicleNumber:GOVINDA,claimNumber:null,vehicleDetails:{id:9,registeredOwnerName:Test Kumar,chassisNumber:Chassis Kumar,engineNumber:Engine Kumar,permitNumber:Permit Kumar,makeModelVariant:{id:2,type:4-wheeler,make:Make 2,model:Model 2,createdDate:1560261454000,updatedDate:null,variant:Variant 2,subType:Sub Type 2,enabled:true,createdBy:admin,updatedBy:null},vehiclePermitType:null,vehicleBodyType:null,fitnessCertificateNumber:Fitness Kumar,passengerCarryingCapacity:2,dateOfRegistration:null,validUpto:null,taxPaidUpto:null,remarks:Remarks Kumar,vehicleColor:Blue,vehicleYOM:0,odometerReading:0},accidentDetails:{id:10,voiceSummary:1582116521756.mp3,accidentLocation:Kondapur, Hyderabad, Telangana, India,dateOfAccident:1580039306000,dateOfSurvey:1582116537000,dateOfIntimation:null,licenseNumber:License Kumar,locationOfSurvey:Plot no.413, Naga Teju Residency, Raja Rajeshwara Nagar, Kondapur, Hyderabad, Telangana 500084, India,thirdPartyDamage:false,thirdPartyDamageType:null,reportedToPolice:false,policeStationName:Police Kumar,stationDiaryEntryNumber:Station Kumar,punchnamaCarriedOut:null,accidentRemarks:Remarks Kumar,thirdPartyDamageRemarks:Third Kumar,otherRemarks:Other Kumar,remarks:null},driverDetails:{id:11,driverName:Ravinded,driverRelation:Self,issueDate:1580647668000,expiryDate:1582980468000,issuingAuthority:Authority Kumar,badgeNumber:Badge Kumar,licenseEndorsement:License Endorsement Kumar,licenseNumber:FVGBHNJMK,driverLicenseType:null,remarks:Remarks Kumar},insuranceDetails:{id:21,insurer:{id:IFFCO Tokio General Insurance Co. Ltd.,name:IFFCO Tokio General Insurance Co. Ltd.,logoUrl:iffco.png,type:Private},financer:test,policyNumber:TFVGHNKBV,startDate:1580581800000,endDate:1582914600000,insuredName:Govinda,insuredAddress:Kondapur, Near Hyundai service center,insuredMobileNumber:8297395048,idv:123456,sumInsured:123456,remarks:null},vehicleFuelType:null,requestor:null,qc:null,purposeOfSurvey:null,customerName:null,customerPhoneNumber:null,customerEmail:null,currentStage:4,inspectionAddress:0.0,inspectionLatitude:null,inspectionLongitude:null,inspectionTime:null,companyBranchDivision:null,creationTime:null,reopenTime:null,reopenReason:null,qcReopenTime:null,qcReopenReason:null,photosUploaded:false,videosUploaded:false,recommendation:null,remark:null,comment:null,inspectionSubmitTime:null,inspectionStartTime:null,closeTime:null,qcTime:null,closeReason:null,encryptionKey:null,fileAvailable:false,fileAvailableTime:null,downloadKey:null,inspectionType:null,assessmentSummary:{id:8,totalLabourCharges:1.0,supplementryLabourCharges:2.0,totalPartCharges:3.0,supplementryPartCharges:4.0,totalPaintCost:5.0,supplementaryPaintCost:6.0,lessExcess:7.0,lessSalavage:8.0,total:9.0,remarks:ss,createdDate:null,updatedDate:null},claimCasePhotos:[{id:576,snapTime:1582116503000,latitude:17.4680588,longitude:78.3529758,fileName:1582116485611.png,photoType:driver_details},{id:573,snapTime:1582116466000,latitude:17.4679295,longitude:78.3530168,fileName:1582116459812.png,photoType:insurance_details},{id:575,snapTime:1582116760000,latitude:17.4680618,longitude:78.3529763,fileName:1582116760759.jpeg,photoType:vehicle_details},{id:574,snapTime:1582116537000,latitude:17.4680089,longitude:78.3530496,fileName:1582116532701.png,photoType:accident_details},{id:577,snapTime:1582116503000,latitude:17.4680588,longitude:78.3529758,fileName:1582116498780.png,photoType:driver_details}],lossAssessments:[{id:115,vehiclePartParticularType:{id:Intercooler,partParticularType:Intercooler,partTypeId:Body,partSideId:Front,createdDate:1560259846000,updatedDate:1560259846000},vehicleDamageType:{id:Replacement,damageType:Replacement,createdDate:1560259847000,updatedDate:1560259847000},replacementProduct:{id:Product 2,productName:Product 2,createdDate:1563367611000,updatedDate:1563367611000},vehicleRepairType:{id:Minor,repairType:Minor,createdDate:1560259847000,updatedDate:1560259847000},partCost:234.0,labourCost:2.0,paintCost:333.0,depRateForPart:2.0,gstRateForPart:1.0,depRateForLabour:2.0,gstRateForLabour:22.0,depRateForPaint:33.0,gstRateForPaint:33.0,createdDate:null,updatedDate:null,lossAssessmentPhotos:[]},{id:113,vehiclePartParticularType:{id:Bulkhead,partParticularType:Bulkhead,partTypeId:Body,partSideId:Front,createdDate:1560259847000,updatedDate:1560259847000},vehicleDamageType:{id:Repair,damageType:Repair,createdDate:1560259847000,updatedDate:1560259847000},replacementProduct:null,vehicleRepairType:{id:Minor,repairType:Minor,createdDate:1560259847000,updatedDate:1560259847000},partCost:115.0,labourCost:100.0,paintCost:90.0,depRateForPart:5.0,gstRateForPart:5.0,depRateForLabour:7.0,gstRateForLabour:6.0,depRateForPaint:2.0,gstRateForPaint:2.0,createdDate:null,updatedDate:null,lossAssessmentPhotos:[]},{id:121,vehiclePartParticularType:{id:RH Front door,partParticularType:RH Front door,partTypeId:Door,partSideId:Right,createdDate:1560259847000,updatedDate:1560259847000},vehicleDamageType:{id:Repair,damageType:Repair,createdDate:1560259847000,updatedDate:1560259847000},replacementProduct:null,vehicleRepairType:null,partCost:1.0,labourCost:1.0,paintCost:1.0,depRateForPart:1.0,gstRateForPart:1.0,depRateForLabour:1.0,gstRateForLabour:1.0,depRateForPaint:1.0,gstRateForPaint:1.0,createdDate:null,updatedDate:null,lossAssessmentPhotos:[]},{id:109,vehiclePartParticularType:{id:Dicky Glass,partParticularType:Dicky Glass,partTypeId:Glass,partSideId:Back,createdDate:1560259846000,updatedDate:1560259846000},vehicleDamageType:{id:Replacement,damageType:Replacement,createdDate:1560259847000,updatedDate:1560259847000},replacementProduct:{id:Product 1,productName:Product 1,createdDate:1563367609000,updatedDate:1563367609000},vehicleRepairType:null,partCost:179.0,labourCost:141.0,paintCost:0.0,depRateForPart:0.0,gstRateForPart:0.0,depRateForLabour:0.0,gstRateForLabour:0.0,depRateForPaint:0.0,gstRateForPaint:0.0,createdDate:null,updatedDate:null,lossAssessmentPhotos:[{id:103,snapTime:1582116747000,latitude:17.4679603,longitude:78.3530388,fileName:1582116740969.png,photoType:loss_assessment}]},{id:108,vehiclePartParticularType:{id:Front Bumper,partParticularType:Front Bumper,partTypeId:Engine,partSideId:Front,createdDate:1560259847000,updatedDate:1560259847000},vehicleDamageType:{id:Replacement,damageType:Replacement,createdDate:1560259847000,updatedDate:1560259847000},replacementProduct:{id:Product 1,productName:Product 1,createdDate:1563367609000,updatedDate:1563367609000},vehicleRepairType:{id:Major,repairType:Major,createdDate:1560259847000,updatedDate:1560259847000},partCost:116.0,labourCost:105.0,paintCost:0.0,depRateForPart:1.0,gstRateForPart:0.0,depRateForLabour:0.0,gstRateForLabour:0.0,depRateForPaint:0.0,gstRateForPaint:0.0,createdDate:null,updatedDate:null,lossAssessmentPhotos:[{id:102,snapTime:1582116729000,latitude:17.4679441,longitude:78.3529925,fileName:1582116722564.png,photoType:loss_assessment}]},{id:114,vehiclePartParticularType:{id:Instrument Panel,partParticularType:Instrument Panel,partTypeId:5,partSideId:Other,createdDate:1560259847000,updatedDate:1560259847000},vehicleDamageType:{id:Repair,damageType:Repair,createdDate:1560259847000,updatedDate:1560259847000},replacementProduct:null,vehicleRepairType:{id:Major,repairType:Major,createdDate:1560259847000,updatedDate:1560259847000},partCost:1000.0,labourCost:900.0,paintCost:700.0,depRateForPart:1.0,gstRateForPart:1.0,depRateForLabour:5.0,gstRateForLabour:3.0,depRateForPaint:44.0,gstRateForPaint:55.0,createdDate:null,updatedDate:null,lossAssessmentPhotos:[]}]}";
	}

	@RequestMapping(value = "/survey/cases/case-details", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Object getClaimCaseDetails(
			@RequestParam(value = "case_id", required = true) Long caseId)
			throws Exception {
		LOGGER.info("/survey/cases/case-details request received for case={}",
				caseId);
		return (ClaimCase) claimCaseDAO.getCaseById(caseId);
	}

	@RequestMapping(value = "/cases/comments", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getComments(
			@RequestParam(value = "case_id", required = true) Long caseId) {

		return claimCaseCommentDAO.get(caseId);
	}

	@RequestMapping(value = "/agents/comments", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getAgentComments(
			@RequestParam(value = "phonenumber", required = true) String phonenumber,
			@RequestParam(value = "from", required = true) Long from,
			@RequestParam(value = "to", required = true) Long to) {
		List<ClaimCase> cases = claimCaseDAO
				.getCaseByAgentPhoneNumberAndCreatedTime(phonenumber, new Date(
						from), new Date(to));
		List<CaseCommentDTO> caseCommentDTOs = new ArrayList<CaseCommentDTO>();
		for (ClaimCase ClaimCase : cases) {
			caseCommentDTOs.addAll(claimCaseCommentDAO
					.getCommentsByCommentTimeAndCaseId(ClaimCase.getId(),
							new Date(from), new Date(to)));
		}
		return caseCommentDTOs;
	}

	@RequestMapping(value = "/cases/comments", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void addComment(
			@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "comment", required = true) String comment)
			throws Exception {
		ClaimCase cc = (ClaimCase) claimCaseDAO.getCaseById(caseId);
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((org.springframework.security.core.userdetails.User) authentication
						.getPrincipal()).getUsername());

		claimCaseCommentDAO.add(cc, comment, user);
	}

	@RequestMapping(value = "/cases/retag-photos", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<ClaimCaseDto> retagPhotos(
			@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "payload", required = true) String payload)
			throws JSONException {

		JSONArray payloadArr = new JSONArray(payload);

		for (int i = 0; i < payloadArr.length(); i++) {
			long photoId = payloadArr.getJSONObject(i).getInt("id");
			String photoType = payloadArr.getJSONObject(i).getString(
					"photoType");

			ClaimCasePhoto photo = (ClaimCasePhoto) claimCasePhotoDAO
					.get(photoId);
			// photo.setPhotoType(claimsPhotoTypeDAO.get(photoType));
			claimCasePhotoDAO.saveOrUpdate(photo);
		}

		return null;
	}

	@RequestMapping(value = "/cases/address-requested-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List addressRequestedCases() {

		return claimCaseDAO.getAllCasesByStage(1);
	}

	@RequestMapping(value = "/cases/get-case-photos", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getCasePhotos(
			@RequestParam(value = "case_id", required = true) Long caseId) {
		return claimCasePhotoDAO.getCasePhotosDTOByCaseId(caseId);
	}

	@RequestMapping(value = "/cases/requested-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List requestedCases() {

		return claimCaseDAO.getAllCasesByStage(2);
	}

	@RequestMapping(value = "/cases/qc-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List qcCases() {

		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((org.springframework.security.core.userdetails.User) authentication
						.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user)
					.getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user)
					.getCompanyBranchDivisionUser();
		}

		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user)
					.getCompanyBranchDivisionUser();
		}

		return claimCaseDAO.getQCCasesDTO(companyBranchDivisionUsers);
	}

	@RequestMapping(value = "/survey/cases/qc-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List qcClaimCases() {

		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((org.springframework.security.core.userdetails.User) authentication
						.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user)
					.getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user)
					.getCompanyBranchDivisionUser();
		}

		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user)
					.getCompanyBranchDivisionUser();
		}

		return claimCaseDAO.getQCCasesDTO(companyBranchDivisionUsers);
	}

	@RequestMapping(value = "/cases/scheduled-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<ScheduledCasesDTO> scheduledCases() {

		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((org.springframework.security.core.userdetails.User) authentication
						.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user)
					.getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user)
					.getCompanyBranchDivisionUser();
		}

		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user)
					.getCompanyBranchDivisionUser();
		}

		if (appConfigService.getBooleanProperty(
				"IS_TO_FETCH_SCHEDULED_CASES_FROM_CACHE", true)) {
			List<ScheduledCasesDTO> scheduledCases = redisService
					.getScheduledCases("SCHEDULED_CASES_FOR_USER_"
							+ user.getUsername());
			if (scheduledCases == null || scheduledCases.isEmpty()) {
				scheduledCases = claimCaseDAO
						.getScheduledCasesDTO(companyBranchDivisionUsers);
				redisService.cacheScheduledCases("SCHEDULED_CASES_FOR_USER_"
						+ user.getUsername(), scheduledCases);
				return scheduledCases;
			}
			return scheduledCases;
		}
		return claimCaseDAO.getScheduledCasesDTO(companyBranchDivisionUsers);
	}

	@RequestMapping(value = "/survey/cases/scheduled-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<ScheduledCasesDTO> surveyScheduledCases() {

		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((org.springframework.security.core.userdetails.User) authentication
						.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user)
					.getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user)
					.getCompanyBranchDivisionUser();
		}

		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user)
					.getCompanyBranchDivisionUser();
		}

		return claimCaseDAO.getScheduledCasesDTO(companyBranchDivisionUsers);
	}

	@RequestMapping(value = "/cases/completed-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<AllCases> completedCases() {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((org.springframework.security.core.userdetails.User) authentication
						.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user)
					.getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user)
					.getCompanyBranchDivisionUser();
		}
		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user)
					.getCompanyBranchDivisionUser();
		}
		if (appConfigService.getBooleanProperty("IS_TO_FETCH_FROM_CACHE", true)) {
			List<AllCases> completedCase = redisService
					.getAllCases("COMPLETE_CASES_FOR_USER_"
							+ user.getUsername());
			if (completedCase == null || completedCase.isEmpty()) {
				List<AllCases> completedCases = claimCaseDAO
						.getAllCasesByStageDTO(5, companyBranchDivisionUsers);
				redisService.cacheAllCases(
						"COMPLETE_CASES_FOR_USER_" + user.getUsername(),
						completedCases);
				return completedCases;
			}
			return completedCase;
		}
		return claimCaseDAO
				.getAllCasesByStageDTO(5, companyBranchDivisionUsers);
	}

	@RequestMapping(value = "/cases/closed-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<AllCases> closedCases() {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((org.springframework.security.core.userdetails.User) authentication
						.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user)
					.getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user)
					.getCompanyBranchDivisionUser();
		}
		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user)
					.getCompanyBranchDivisionUser();
		}
		if (appConfigService.getBooleanProperty("IS_TO_FETCH_FROM_CACHE", true)) {
			List<AllCases> closedCase = redisService
					.getAllCases("CLOSED_CASES_FOR_USER_" + user.getUsername());
			if (closedCase == null || closedCase.isEmpty()) {
				List<AllCases> closedCases = claimCaseDAO
						.getAllCasesByStageDTO(-1, companyBranchDivisionUsers);
				redisService.cacheAllCases(
						"CLOSED_CASES_FOR_USER_" + user.getUsername(),
						closedCases);
				return closedCases;
			}
			return closedCase;
		}
		return claimCaseDAO.getAllCasesByStageDTO(-1,
				companyBranchDivisionUsers);
	}

	@RequestMapping(value = "/cases/all-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<AllCases> allCases() {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((org.springframework.security.core.userdetails.User) authentication
						.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user)
					.getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user)
					.getCompanyBranchDivisionUser();
		}

		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user)
					.getCompanyBranchDivisionUser();
		}

		if (appConfigService.getBooleanProperty("IS_TO_FETCH_FROM_CACHE", true)) {
			List<AllCases> allCase = redisService
					.getAllCases("ALL_CASES_FOR_USER_" + user.getUsername());
			if (allCase == null || allCase.isEmpty()) {
				List<AllCases> allCases = claimCaseDAO
						.getAllDTO(companyBranchDivisionUsers);
				redisService.cacheAllCases(
						"ALL_CASES_FOR_USER_" + user.getUsername(), allCases);
				return allCases;
			}
			return allCase;
		}
		return claimCaseDAO.getAllDTO(companyBranchDivisionUsers);

	}

	@RequestMapping(value = "/survey/cases/all-cases", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<AllCases> allClaimCases() {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((org.springframework.security.core.userdetails.User) authentication
						.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;

		if (user instanceof Admin) {
			companyBranchDivisionUsers = ((Admin) user)
					.getCompanyBranchDivisionUser();
		}

		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user)
					.getCompanyBranchDivisionUser();
		}

		if (user instanceof QC) {
			companyBranchDivisionUsers = ((QC) user)
					.getCompanyBranchDivisionUser();
		}

		return claimCaseDAO.getAllDTO(companyBranchDivisionUsers);

	}

	@RequestMapping(value = "/cases/qc-answers", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getQCAnswers(
			@RequestParam(value = "case_id", required = true) Long caseId) {

		return caseQCAnswerDAO.getQCAnswers(caseId);
	}

	@RequestMapping(value = "/cases/update-VehicleDetails-data", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List updateVehicleData(
			@RequestParam(value = "case_id", required = true) Long caseId) {

		return caseQCAnswerDAO.getQCAnswers(caseId);
	}

	@RequestMapping(value = "/create-case", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Object> createCase(
			@RequestParam(value = "purpose_of_survey", required = true) String purpose_of_survey,
			@RequestParam(value = "insurance_company", required = true) String insuranceCompany,
			@RequestParam(value = "customer_name", required = true) String customerName,
			@RequestParam(value = "customer_phone_number", required = true) String customerPhoneNumber,
			@RequestParam(value = "inspection_type", required = true) String inspectionType,
			@RequestParam(value = "vehicle_number", required = true) String vehicleNumber,
			@RequestParam(value = "claim_number", required = true) String claimNumber,
			@RequestParam(value = "agent_id", required = true) String agentId,
			@RequestParam(value = "inspection_time", required = false) @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss") Date date,
			@RequestParam(value = "latitude", required = false, defaultValue = "0.0") Double latitude,
			@RequestParam(value = "longitude", required = false, defaultValue = "0.0") Double longitude)
			throws Exception {
		LOGGER.info(
				"/create-case request recieved with data purpose_of_inspection={}, customer_name={}, customer_phone_number={}, inspection_type={}, vehicle_number={}, agent_id={}, inspection_time={}, latitude={}, longitude={}",
				purpose_of_survey, customerName, customerPhoneNumber,
				inspectionType, vehicleNumber, agentId, date, latitude,
				longitude);
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((User) authentication.getPrincipal())
						.getUsername());
		PurposeOfSurvey poi = posDAO.getPurposeOfSurveyById(purpose_of_survey);
		Agent agent = agentDAO.getAgentByUsername(agentId);

		ClaimCase cc = null;
		if (date == null)
			date = new Date();
		if (agent == null) {
			LOGGER.info("No agent found with agentId={} for user={}", agentId,
					user.getUsername());
			throw new Exception("Referred agent account doesn't exists");
		}

		if (poi == null) {
			LOGGER.info("Invalid purpose of inspection!={} for user={}",
					purpose_of_survey, user.getUsername());
			throw new Exception("Invalid purpose of inspection!");
		}
		try {
			ClaimCaseBuilder icBuilder = new ClaimCaseBuilder(customerName,
					customerPhoneNumber, agent, vehicleNumber, poi, agent
							.getAgentDetails().getCompanyBranchDivision(),
					insuranceCompanyDAO
							.getInsuranceCompanyByName(insuranceCompany))
					.setStage(1).addCaseToUserForStage(agent, 1);
			switch (inspectionType) {
			case "SELF_INSPECT": {
				icBuilder.addCaseToUserForStage(agent, 3)
						.setInspectionType("SELF_INSPECT").setStage(3);
				break;
			}
			case "ASSIGN_TO_CUSTOMER": {
				com.jadu.model.User customer = userDAO
						.getUserByPhoneOrEmail(customerPhoneNumber);

				if (customer == null)
					customer = createUserService.createCustomer("",
							customerPhoneNumber);

				icBuilder.addCaseToUserForStage(customer, 3)
						.setInspectionTime(new Date(), latitude, longitude)
						.setInspectionType("ASSIGN_TO_CUSTOMER").setStage(3);
				break;
			}
			case "ASSIGN_TO_INSPECTOR": {
				break;
			}
			default:
				throw new Exception("Invalid inspection type!");
			}

			cc = icBuilder.build();

			cc.setDownloadKey(UtilHelper.getRandomString(64)
					+ UtilHelper.getCurrentTimestampAsString() + "_");

			cc.setEncryptionKey(UtilHelper.getRandomStringUpperCase(16));
			cc.setClaimNumber(claimNumber);
			claimCaseDAO.save(cc);

			caseLogDAO.addClaimCreateCase(agent, cc);

			caseService.sendClaimCaseCreationNotificationsAsync(agent, cc);
			LOGGER.info("case={} has been created, for user={}", cc,
					user.getUsername());

			if (null != inspectionType) {
				switch (inspectionType) {
				case "SELF_INSPECT":
					if (appConfigService
							.getBooleanProperty(
									"IS_TO_SEND_SMS_IN_SELLF_CASE_CREATION_BY_QC",
									true)) {
						if (appConfigService
								.getBooleanProperty(
										"IS_TO_SEND_SMS_TO_AGENT_IN_SELLF_CASE_CREATION_BY_QC",
										true))
							smsService
									.sendSMS(
											agent.getPhoneNumber(),
											"We have received an inspection request for VehicleDetails Number - "
													+ cc.getVehicleNumber()
													+ " from Insurance Company. Our agent will contact you shortly.");
						else {
							smsService
									.sendSMS(
											cc.getCustomerPhoneNumber(),
											"We have received an inspection request for VehicleDetails Number - "
													+ cc.getVehicleNumber()
													+ " from Insurance Company. Our agent will contact you shortly.");

						}
					}
					break;
				case "ASSIGN_TO_INSPECTOR":
					smsService
							.sendSMS(
									cc.getCustomerPhoneNumber(),
									"Hi, Please provide location & schedule for inspection of "
											+ cc.getVehicleNumber()
											+ " for insurance policy."
											+ " Visit "
											+ urlShortnerService
													.shortenCaseUrl(cc.getId())
											+ ". Please call 7290049100 for any assistance.");
					break;
				case "ASSIGN_TO_CUSTOMER": {
					String url = urlShortnerService
							.shortenUrl("customer-inspection/" + cc.getId());
					smsService
							.sendSMSAsync(
									customerPhoneNumber,
									"We have an insurance inspection request for "
											+ cc.getVehicleNumber()
											+ ". Please download the app "
											+ url
											+ " to complete the inspection. Call 7290049100 for support");
					connectCustomerToFlow
							.callCustomerAfterCaseCreate(customerPhoneNumber);
				}
				}
			}

			if (appConfigService.getBooleanProperty(
					"IS_TO_PUSH_CREATE_CASE_IN_CACHE", true)) {
				try {
					// redisService.cacheCaseAgainstUser(user, cc, 0);
				} catch (Exception e) {
					e.printStackTrace();
					LOGGER.error(
							"Exception raised while caching case={} for user={}, error={}",
							cc.getId(), user.getPhoneNumber(), e.getMessage());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(
					"Exception raised while creating case for user={}, error={}",
					user.getUsername(), e.getMessage());
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/resend-sms", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public void resendSms(
			@RequestParam(value = "case_id", required = true) Long caseId)
			throws Exception {
		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Case not found!");

		if (ic.getCurrentStage() != 3)
			return;

		switch (ic.getInspectionType()) {
		case "ASSIGN_TO_CUSTOMER": {
			String url = urlShortnerService.shortenUrl("customer-inspection/"
					+ ic.getId());
			smsService
					.sendSMS(
							ic.getCustomerPhoneNumber(),
							"We have an insurance inspection request for "
									+ ic.getVehicleNumber()
									+ ". Please download the app "
									+ url
									+ " to complete the inspection. Call 7290049100 for support");
			break;
		}
		}
	}

	@RequestMapping(value = "/cases/allot-user-manual", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void allotUserManual(
			@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "username", required = true) String username)
			throws Exception {
		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Case not found!");

		com.jadu.model.User user = userDAO.getUserByUsername(username);

		ic.setCurrentStage(3);
		ClaimCaseUserAllocation allocate2 = new ClaimCaseUserAllocation();
		allocate2.setAllocationTime(UtilHelper.getDate());
		allocate2.setClaimCase(ic);
		allocate2.setStage(3);
		allocate2.setUser(user);
		// ic.addClaimCaseUserAllocation(allocate2);
		claimCaseDAO.save(ic);
	}

	@RequestMapping(value = "/update-inspection-time", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateInspectionTime(
			@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "new_inspection_time", required = true) @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss") Date date,
			@RequestParam(value = "reason", required = true) String reason)
			throws Exception {

		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception(
					"Unable to find case with given details. Either case doesn't exists or is updated!");

		claimCaseDAO.updateCaseInspectionTime(caseId, date);
	}

	@RequestMapping(value = "/cases/submit-qc-report", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> submitQCReport(
			@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "recommendation", required = true) String recommendation,
			@RequestParam(value = "answers", required = true) String answers,
			@RequestParam(value = "remarks", required = true) String remarks)
			throws Exception {

		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((User) authentication.getPrincipal())
						.getUsername());
		try {
			JSONArray answersArray = new JSONArray(answers);
			ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

			if (ic.getCurrentStage() != 4) {
				LOGGER.info("Case={} is not at QC stage!", caseId);
				return new ResponseEntity<>("Case is not at QC stage!",
						HttpStatus.CONFLICT);

			}

			CompanyBranchDivision cbd = ((Agent) ic.getRequestor())
					.getAgentDetails().getCompanyBranchDivision();

			/*
			 * QCQuestionsFactory qcQuestionsFactory = new
			 * QCQuestionsFactory(caseQCQuestionDAO.getAll());
			 * CaseQCQuestionOptionFactory qcOptionsFactory = new
			 * CaseQCQuestionOptionFactory(
			 * caseQCQuestionOptionDAO.getAllObjects());
			 * 
			 * for (int i = 0; i < answersArray.length(); i++) { JSONObject item
			 * = answersArray.getJSONObject(i); CaseQCQuestion questionId =
			 * qcQuestionsFactory.get(item.getInt("id")); CaseQCQuestionOption
			 * answer = qcOptionsFactory.get(item.getInt("answer"));
			 * CaseQCAnswer qcAnswer = new CaseQCAnswer(); //
			 * qcAnswer.setClaimCase(ic); qcAnswer.setOption(answer);
			 * qcAnswer.setQuestion(questionId); caseQCAnswerDAO.save(qcAnswer);
			 * }
			 */

			ic.setRemark(recommendation);

			if (!recommendation.equals("hold"))
				ic.setCurrentStage(5);
			else
				ic.setCurrentStage(4);

			ic.setComment(remarks);

			ic.setQc(user);
			ic.setQcTime(new Date());

			claimCaseDAO.save(ic);

			if (!recommendation.equals("hold")) {

				String outputFile1 = "report.pdf";
				// String outputFile2 = "inspection_report-" + ic.getId() +
				// ".pdf";

				/*
				 * generateReportService.createPdf((ClaimCase)
				 * claimCaseDAO.getCaseById(ic.getId()), ic.getId() + ".pdf",
				 * outputFile1, true);
				 */
				// generateReportService.createPdf((ClaimCase)claimCaseDAO.getCaseById(ic.getId()),
				// ic.getId() + ".pdf", outputFile2, false);

				/*
				 * List<String> casePhotos =
				 * claimCasePhotoDAO.getCasePhotosByCaseId(ic.getId());
				 * 
				 * List<byte[]> files = new ArrayList<>();
				 * 
				 * for(String ClaimCasePhoto : casePhotos){
				 * files.add(s3Service.readFile("cases/" + caseId + "/" +
				 * ClaimCasePhoto)); }
				 * 
				 * files.add(s3Service.readFile("cases/" + caseId + "/" +
				 * "report.pdf")); casePhotos.add("report.pdf");
				 * 
				 * File zipfile = UtilHelper.zipBytes(files, casePhotos,
				 * "export.zip");
				 */

				/*
				 * mailService.sendGenaratedReportMail(cbd.getEmail(), ic,
				 * s3Service.readFile("cases/" + ic.getId() + "/" +
				 * outputFile1));
				 * 
				 * mailService.sendGenaratedReportMail(ic.getRequestorEmail(),
				 * ic, s3Service.readFile("cases/" + ic.getId() + "/" +
				 * outputFile1));
				 */

				smsService
						.sendSMSAsync(
								ic.getRequestor().getPhoneNumber(),
								"Your case request for "
										+ ic.getVehicleNumber()
										+ " is "
										+ recommendation.toUpperCase()
										+ ". Download the report from your mobile app or check email.");
			} else {
				if (ic.getInspectionType().equals("ASSIGN_TO_CUSTOMER")) {
					smsService
							.sendSMSAsync(
									ic.getCustomerPhoneNumber(),
									"Your Case Id - "
											+ ic.getId()
											+ " for VehicleDetails Number - "
											+ ic.getVehicleNumber()
											+ " is on HOLD. Call "
											+ appConfigService
													.getBooleanProperty("SUPPORT_CONTACT_NUMBER")
											+ " for support");
				} else {
					smsService
							.sendSMSAsync(
									ic.getRequestor().getPhoneNumber(),
									"Your Case Id - "
											+ ic.getId()
											+ " for VehicleDetails Number - "
											+ ic.getVehicleNumber()
											+ " is on HOLD. Call "
											+ appConfigService
													.getBooleanProperty("SUPPORT_CONTACT_NUMBER")
											+ " for support");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info(
					"Exception raised while submitting case={} for user={}, error={}",
					caseId, user.getPhoneNumber(), e.getMessage());
			return new ResponseEntity<>("Something went wrong",
					HttpStatus.EXPECTATION_FAILED);

		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/close-case", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void submitReport(
			@RequestParam(value = "case_id", required = true) int caseId,
			@RequestParam(value = "reason", required = true) String reason)
			throws Exception {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = userDAO
				.getUserByUsername(((User) authentication.getPrincipal())
						.getUsername());
		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Case not found");

		if (ic.getCurrentStage() == -1)
			throw new Exception("Case has already been closed!");

		ic.setCurrentStage(-1);
		ic.setCloseTime(UtilHelper.getDate());
		ic.setCloseReason(reason);
		claimCaseDAO.save(ic);

	}

	@RequestMapping(value = "/case/reopen-qc", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void reopenForQC(
			@RequestParam(value = "case_id", required = true) int caseId,
			@RequestParam(value = "reason", required = true) String reason)
			throws Exception {
		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Case not found");

		ic.setCurrentStage(4);
		ic.setQcReopenTime(new Date());
		ic.setQcReopenReason(reason);
		claimCaseDAO.save(ic);
		caseQCAnswerDAO.deleteAnswersForCase(ic.getId());
	}

	@RequestMapping(value = "/case/reopen", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void reopen(
			@RequestParam(value = "case_id", required = true) int caseId,
			@RequestParam(value = "reason", required = true) String reason)
			throws Exception {
		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Case not found");

		ic.setCurrentStage(3);
		ic.setReopenReason(reason);
		ic.setReopenTime(new Date());

		claimCaseDAO.save(ic);

		caseQCAnswerDAO.deleteAnswersForCase(ic.getId());

		String ts = String.valueOf(System.currentTimeMillis());

		if (s3Service.exists("cases/" + ic.getId() + "/" + ic.getId() + ".zip")) {
			s3Service.copyFile("cases/" + ic.getId() + "/" + ic.getId()
					+ ".zip", "cases/" + ic.getId() + "/" + ts + ".zip");
		}

		if (s3Service
				.exists("cases/" + ic.getId() + "/" + ic.getId() + ".json")) {
			s3Service.copyFile("cases/" + ic.getId() + "/" + ic.getId()
					+ ".json", "cases/" + ic.getId() + "/" + ts + ".json");
		}
	}

	@RequestMapping(value = "/replace-photo", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void replacePhoto(
			@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "case_file", required = true) MultipartFile caseFile,
			@RequestParam(value = "comment", required = true) String comment,
			@RequestParam(value = "photo_type", required = true) String photoType)
			throws Exception {

		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception(
					"Unable to find case with given details. Either case doesn't exists or is updated!");

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		String currTime = String.valueOf(timestamp.getTime());

		String newFileName = currTime + "_" + caseFile.getOriginalFilename();

		s3Service.uploadMultipartFile(caseFile, "cases/" + ic.getId() + "/"
				+ newFileName);

		List<Long> casePhotos = claimCasePhotoDAO.getCasePhotosByPhotoType(
				caseId, photoType);

		if (casePhotos.isEmpty())
			throw new Exception("No photo to replace!");

		ClaimCasePhoto cp = (ClaimCasePhoto) claimCasePhotoDAO.get(casePhotos
				.get(0));

		cp.setFileName(newFileName);

		claimCasePhotoDAO.save(cp);

	}

	@RequestMapping(value = "/add-photo", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void addPhoto(
			@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "case_file", required = true) MultipartFile caseFile,
			@RequestParam(value = "comment", required = true) String comment,
			@RequestParam(value = "photo_type", required = true) String photoType)
			throws Exception {

		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception(
					"Unable to find case with given details. Either case doesn't exists or is updated!");

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		String currTime = String.valueOf(timestamp.getTime());

		String newFileName = currTime + "_" + caseFile.getOriginalFilename();

		s3Service.uploadMultipartFile(caseFile, "cases/" + ic.getId() + "/"
				+ newFileName);

		List<Long> casePhotos = claimCasePhotoDAO.getCasePhotosByPhotoType(
				caseId, photoType);

		if (casePhotos.isEmpty())
			throw new Exception("No photo to replace!");

		ClaimCasePhoto cp = new ClaimCasePhoto();
		cp.setFileName(newFileName);
		cp.setClaimCase(ic);
		cp.setLatitude(0.0);
		cp.setLongitude(0.0);
		cp.setSnapTime(new Date());

		claimCasePhotoDAO.save(cp);

	}

	@RequestMapping(value = "/cases/download-uploaded-zip/{case_id}", method = RequestMethod.GET, produces = "application/zip")
	@ResponseBody
	public void downloadZipPhotos(@PathVariable long case_id,
			HttpServletResponse response) throws Exception {

		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(case_id);

		if (ic == null)
			throw new Exception("No case with given ID exists for user!");

		InputStream zipfile = s3Service.readFileStream("cases/" + ic.getId()
				+ "/" + ic.getId() + ".zip");

		if (zipfile == null)
			throw new Exception(
					"Unable to zip all files! Please try again later.");

		try {
			response.setHeader("Content-Disposition", "attachment; filename=\""
					+ ic.getId() + ".zip" + "\"");
			IOUtils.copy(zipfile, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");
		}
	}

	@RequestMapping(value = "/cases/delete-case-photo", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void deletePhoto(
			@RequestParam(value = "photo_id", required = true) Long photoId)
			throws Exception {

		claimCasePhotoDAO.deletePhoto(photoId);
	}

	@RequestMapping(value = "/cases/update-customer-name", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateCustomerName(
			@RequestParam(value = "case_id", required = true) Long caseId,
			@RequestParam(value = "customer_name", required = true) String customerName)
			throws Exception {
		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic.getCurrentStage() == 5)
			throw new Exception("Case is already completed/closed");

		ic.setCustomerName(customerName);
		claimCaseDAO.save(ic);
	}

	@RequestMapping(value = "/send-report", method = RequestMethod.GET)
	public void sendReport(@RequestParam("case_id") long caseId)
			throws Exception {

		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);
		List<Agent> refAgent = agentDAO.getAgentByPhoneNumber(ic.getRequestor()
				.getPhoneNumber());
		CompanyBranchDivision cbd = refAgent.get(0).getAgentDetails()
				.getCompanyBranchDivision();
		String outputFile1 = "report.pdf";
		mailService.sendGenaratedReportMail(cbd.getEmail(), ic,
				s3Service.readFile("cases/" + ic.getId() + "/" + outputFile1));

		mailService.sendGenaratedReportMail(ic.getRequestor().getEmail(), ic,
				s3Service.readFile("cases/" + ic.getId() + "/" + outputFile1));

		smsService
				.sendSMSAsync(
						ic.getRequestor().getPhoneNumber(),
						"Your case request for "
								+ ic.getVehicleNumber()
								+ " is "
								+ ic.getRemark().toUpperCase()
								+ ". Download the report from your mobile app or check email.");
	}

	@RequestMapping(value = "/report-data", method = RequestMethod.GET)
	public ClaimsFinalSubmissionDTO getReportData(
			@RequestParam("case_id") long caseId) throws Exception {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = utilService.getUser(authentication);
		if (user != null) {
			ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);
			List<Agent> refAgent = agentDAO.getAgentByPhoneNumber(ic
					.getRequestor().getPhoneNumber());
			return claimsReportDataGeneratorFactory.prepareData(user, ic,
					refAgent);

		}
		return null;
	}

	@ExceptionHandler(Exception.class)
	public void handleAllException(Exception ex, HttpServletResponse response)
			throws IOException {
		ErrorHandler.handleError(ex, response);
		ex.printStackTrace();
	}

	@RequestMapping(value = "/case/search", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public List<AllCases> searchCase(
			@RequestParam(value = "case_id", required = false) String caseId,
			@RequestParam(value = "vehicle_id", required = false) String vehicle_id,
			@RequestParam(value = "customer_phone", required = false) String customer_phone,
			@RequestParam(value = "requester_phone", required = false) String requester_phone)
			throws Exception {
		LOGGER.info(
				"/case/search request recieved, parameter case_id={},vehicle_id={},customer_phone={},requester_phone={}",
				caseId, vehicle_id, customer_phone, requester_phone);
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		com.jadu.model.User user = utilService.getUser(authentication);
		// Set<CompanyBranchDivisionUser> companyBranchDivisionUsers =
		// utilService.getCompanyBranchDivisionUser(user);
		List<ClaimCase> casesList = new ArrayList<ClaimCase>();
		if (caseId != null) {
			casesList.add((ClaimCase) claimCaseDAO.getCaseById(Integer
					.valueOf(caseId)));
		} else if (vehicle_id != null) {
			casesList.addAll(claimCaseDAO.getCaseByVehicleNumber(vehicle_id));
		} else if (customer_phone != null) {
			casesList.addAll(claimCaseDAO
					.getCaseByCustomerPhoneNumber(vehicle_id));
		} else if (requester_phone != null) {
			casesList
					.addAll(claimCaseDAO.getCaseByAgentPhoneNumber(vehicle_id));
		} else {
			LOGGER.info("No valid input found");
			throw new Exception("No valid input found");

		}
		if (casesList == null || casesList.isEmpty())
			throw new Exception("Case not found");
		List<AllCases> allCasesList = new ArrayList<AllCases>();
		for (ClaimCase ic : casesList) {
			List commentList = claimCaseCommentDAO.count(ic.getId());
			int countComments = commentList != null && !commentList.isEmpty() ? commentList
					.size() : 0;
			AllCases allCases = new AllCases(ic.getId(), ic.getVehicleNumber(),
					ic.getCustomerName(), ic.getCustomerPhoneNumber(),
					ic.getInspectionTime(), ic.getInspectionSubmitTime(),
					ic.getCurrentStage(), ic.getInspectionType(),
					insuranceCompanyDAO.getInsuranceCompanyByName(
							ic.getCompanyBranchDivision().getCompany())
							.getName(), insuranceCompanyDAO
							.getInsuranceCompanyByName(
									ic.getCompanyBranchDivision().getCompany())
							.getLogoUrl(), ic.getRequestor().getPhoneNumber(),
					ic.getRemark(), ic.getDownloadKey(), countComments);
			allCasesList.add(allCases);
		}
		return allCasesList;
	}

	@RequestMapping(value = "/case/updatetimagebyexisting", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public List<ClaimCasePhoto> updatetimagebyexisting(
			@RequestBody ClaimCasePhoto claimCasePhoto) {
		return claimCasePhotoDAO.updatetimagebyexisting(claimCasePhoto);
	}

}
