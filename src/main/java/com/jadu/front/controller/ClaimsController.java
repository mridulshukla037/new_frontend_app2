package com.jadu.front.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.jadu.dao.ClaimCaseDAO;
import com.jadu.dao.LossAssessmentDAOImpl;
import com.jadu.model.AccidentDetails;
import com.jadu.model.ClaimCase;
import com.jadu.model.DriverDetails;
import com.jadu.model.InsuranceDetails;
import com.jadu.model.LossAssessment;
import com.jadu.model.VehicleDetails;
import com.jadu.service.ClaimCaseService;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/claims")
@Scope("session")
public class ClaimsController {

    @Autowired
    private ClaimCaseDAO claimCaseDAO;

    @Autowired
    private ClaimCaseService claimCaseService;

    @Autowired
    private LossAssessmentDAOImpl lossAssessmentDAO;

    @RequestMapping(value = "/claim-case", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ClaimCase getClaimCase(@RequestParam(value = "id", required = true) long id) throws Exception {
        ClaimCase claimCase = claimCaseDAO.getCaseById(id);
        return claimCase;
    }
    
    @RequestMapping(value = "/update-claim-case", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ClaimCase updateClaimCase(HttpEntity<String> httpEntity) throws Exception {
        String inputItem = httpEntity.getBody();
        ClaimCase claimCase = new ObjectMapper().readValue(inputItem, ClaimCase.class);
        claimCaseDAO.save(claimCase);
        return claimCaseDAO.getCaseById(claimCase.getId());
    }
    
     @RequestMapping(value = "/publish", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void publishClaimCase(@RequestParam(value = "id", required = true) long id) throws Exception {
        ClaimCase claimCase = claimCaseDAO.getCaseById(id);
        claimCaseService.loadDetailsFromFile(claimCase);
        claimCaseDAO.save(claimCase);
    }
    
    
    @RequestMapping(value = "/vehicle-details", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public VehicleDetails getVehicleDetails(@RequestParam(value = "claim_number", required = false) String claim_number,
                    @RequestParam(value = "caseId", required = false) Long caseId) throws Exception {
        return claimCaseDAO.getCaseByIdOrNumber(caseId, claim_number).getVehicleDetails();
    }

    @RequestMapping(value = "/insurance-details", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public InsuranceDetails getInsuranceDetails(
                    @RequestParam(value = "claim_number", required = false) String claim_number,
                    @RequestParam(value = "caseId", required = false) Long caseId) throws Exception {
        return claimCaseDAO.getCaseByIdOrNumber(caseId, claim_number).getInsuranceDetails();
    }

    @RequestMapping(value = "/loss-assessement-details", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<LossAssessment> getLossAssesesmentDetails(
                    @RequestParam(value = "claim_number", required = false) String claim_number,
                    @RequestParam(value = "caseId", required = false) Long caseId) throws Exception {
        return lossAssessmentDAO.getByClaimIDOrNumber(caseId, claim_number);
    }

    @RequestMapping(value = "/driver-details", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public DriverDetails getDriverDetails(@RequestParam(value = "claim_number", required = false) String claim_number,
                    @RequestParam(value = "caseId", required = false) Long caseId) throws Exception {
        return claimCaseDAO.getCaseByIdOrNumber(caseId, claim_number).getDriverDetails();
    }

    @RequestMapping(value = "/accident-details", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public AccidentDetails getAccidentDetails(
                    @RequestParam(value = "claim_number", required = false) String claim_number,
                    @RequestParam(value = "caseId", required = false) Long caseId) throws Exception {
        return claimCaseDAO.getCaseByIdOrNumber(caseId, claim_number).getAccidentDetails();
    }
}
