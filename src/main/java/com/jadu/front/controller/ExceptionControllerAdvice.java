package com.jadu.front.controller;

import com.jadu.model.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
 
@ControllerAdvice
public class ExceptionControllerAdvice {
 
	/*@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setMessage("Please contact your administrator");
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
	}
        
        @ExceptionHandler(NoHandlerFoundException.class)
        public ResponseEntity<ErrorResponse> notFoundHanlder(NoHandlerFoundException ex) {
            ErrorResponse error = new ErrorResponse();
            error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            error.setMessage("Please contact your administrator");
            return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
        }

        @ExceptionHandler(AccessDeniedException.class)
        public ResponseEntity<ErrorResponse> handleAuthorizationException(AccessDeniedException ex) {
            ErrorResponse error = new ErrorResponse();
            error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            error.setMessage("Please contact your administrator");
            return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
        }*/
        
    //@RequestMapping(produces = "application/json")
    //@ExceptionHandler(Throwable.class)
    public ResponseEntity<?> handleException(Throwable ex) {
        ErrorResponse error = new ErrorResponse();
        //error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setMessage("Please contact your administrator");
        return new ResponseEntity<>(error, HttpStatus.OK);
    }
}