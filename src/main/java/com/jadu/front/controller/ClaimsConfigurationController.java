package com.jadu.front.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jadu.dao.BooleanSelectorDAO;
import com.jadu.dao.CostConfigurationsDAOImpl;
import com.jadu.dao.DriverLicenseTypeDAO;
import com.jadu.dao.InsuranceCompanyDAO;
import com.jadu.dao.MakeModelVariantDAOImpl;
import com.jadu.dao.PassengerCarryingCapacityDao;
import com.jadu.dao.PuchnamaCarriedOutDao;
import com.jadu.dao.ReplacementProductDAO;
import com.jadu.dao.ReportedToPoliceDao;
import com.jadu.dao.ThirdPartyDamageDao;
import com.jadu.dao.ThirdPartyDamageTypeDAO;
import com.jadu.dao.VehicleBodyTypeDAO;
import com.jadu.dao.VehicleClassTypeDAO;
import com.jadu.dao.VehicleDamageTypeDAO;
import com.jadu.dao.VehicleLabourCostDAO;
import com.jadu.dao.VehiclePartCostDAO;
import com.jadu.dao.VehiclePartParticularTypeDAO;
import com.jadu.dao.VehiclePartSideDAO;
import com.jadu.dao.VehiclePartTypeDAO;
import com.jadu.dao.VehiclePermitTypeDAO;
import com.jadu.dao.VehicleRepairTypeDAO;
import com.jadu.dao.VehicleTypeDAO;
import com.jadu.model.CostConfigurations;
import com.jadu.model.MakeModelVariant;
import com.jadu.model.PassengerCarryingCapacity;
import com.jadu.model.PuchnamaCarriedOut;
import com.jadu.model.ReportedToPolice;

import java.util.HashMap;
import java.util.Map;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/claims")
public class ClaimsConfigurationController {

	@Autowired
	private VehicleRepairTypeDAO vehicleRepairTypeDAO;

	@Autowired
	private VehiclePermitTypeDAO vehiclePermitTypeDAO;

	@Autowired
	private VehiclePartTypeDAO vehiclePartTypeDAO;

	@Autowired
	private VehiclePartSideDAO vehiclePartSideDAO;

	@Autowired
	private VehiclePartParticularTypeDAO vehiclePartParticularTypeDAO;

	@Autowired
	private VehicleLabourCostDAO vehicleLabourCostDAO;

	@Autowired
	private VehicleDamageTypeDAO vehicleDamageTypeDAO;

	@Autowired
	private VehicleClassTypeDAO vehicleClassTypeDAO;

	@Autowired
	private VehiclePartCostDAO vehiclePartCostDAO;

	@Autowired
	private BooleanSelectorDAO booleanSelectorDAO;

	@Autowired
	private DriverLicenseTypeDAO driverLicenseTypeDAO;

	@Autowired
	private ThirdPartyDamageTypeDAO thirdPartyDamageTypeDAO;

	@Autowired
	private VehicleBodyTypeDAO vehicleBodyTypeDAO;

	@Autowired
	private MakeModelVariantDAOImpl makeModelVariantDAO;

	@Autowired
	private VehicleTypeDAO vehicleTypeDAO;

	@Autowired
	private ReplacementProductDAO replacementProductDAO;
	@Autowired
	private InsuranceCompanyDAO insuranceCompanyDAO;
	@Autowired
	private ReportedToPoliceDao reportedToPoliceDao;
	@Autowired
	private PuchnamaCarriedOutDao puchnamaCarriedOutDao;
	@Autowired
	private ThirdPartyDamageDao thirdPartyDamageDao;
	@Autowired
	private PassengerCarryingCapacityDao passengerCarryingCapacity;

	@Autowired
	CostConfigurationsDAOImpl costConfigurationsDAO;

	@RequestMapping(value = "/config/cost-configurations", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<CostConfigurations> getCostConfigurations() {
		return costConfigurationsDAO.getAll();
	}

	@RequestMapping(value = "/config/make-model-variant", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<MakeModelVariant> getMakeModelVariant(
			@RequestParam(value = "id", required = false) Long id)
			throws Exception {
		return id != null ? makeModelVariantDAO.getById(id)
				: makeModelVariantDAO.findAll();
	}

	@RequestMapping(value = "/config-details", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Map<String, Object> getDetails() throws Exception {
		Map<String, Object> result = new HashMap<>();
		result.put("thirdPartyDamageItems", booleanSelectorDAO.findAll());
		result.put("reportToPoliceItems", booleanSelectorDAO.findAll());
		result.put("punchnamaCarriedOutItems", booleanSelectorDAO.findAll());
		result.put("vehicleBodyTypes", vehicleBodyTypeDAO.findAll());
		result.put("vehicleClassTypes", vehicleClassTypeDAO.findAll());
		result.put("vehicleType", vehicleTypeDAO.getAll());
		result.put("vehiclePermitTypes", vehiclePermitTypeDAO.findAll());
		result.put("driverLicenseTypes", driverLicenseTypeDAO.findAll());
		result.put("thirdPartyDamageTypes", thirdPartyDamageTypeDAO.findAll());
		result.put("vehiclePartType", vehiclePartTypeDAO.findAll());
		result.put("vehiclePartSide", vehiclePartSideDAO.findAll());
		result.put("vehiclePartParticularType",
				vehiclePartParticularTypeDAO.findAll());
		result.put("vehicleDamageType", vehicleDamageTypeDAO.findAll());
		result.put("vehicleRepairType", vehicleRepairTypeDAO.findAll());
		result.put("vehiclePartCost", vehiclePartCostDAO.findAll());
		result.put("vehicleLabourCost", vehicleLabourCostDAO.findAll());
		result.put("replacementProduct", replacementProductDAO.findAll());
		result.put("makeModelVariant", makeModelVariantDAO.findAll());

		result.put("insuranceCompanylist",
				insuranceCompanyDAO.getInsuranceCompanies());
		result.put("puchnamaCarriedOut", puchnamaCarriedOutDao.findAll());
		result.put("thirdPartyDamage", thirdPartyDamageDao.findAll());
		result.put("reportedToPolice", reportedToPoliceDao.findAll());
		result.put("passengerCarryingCapacity",
				passengerCarryingCapacity.findAll());

		return result;
	}

	@RequestMapping(value = "/cost-configuration", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List getCostConfiguration(
			@RequestParam(value = "make_model_variant_id", required = true) Long makeModelVariantId)
			throws Exception {
		return costConfigurationsDAO.get(makeModelVariantId);
	}

	@RequestMapping(value = "/cost-configuration-record", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object getCostConfigurationRecord(
			@RequestParam(value = "makeModelVariantId", required = true) Long makeModelVariantId,
			@RequestParam(value = "partParticularType", required = true) String partParticularType,
			@RequestParam(value = "repairType", required = true) String repairType,
			@RequestParam(value = "damageType", required = true) String damageType)
			throws Exception {
		return costConfigurationsDAO.get(makeModelVariantId,
				partParticularType, damageType, repairType);
	}
}
