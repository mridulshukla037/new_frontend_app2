package com.jadu.front.controller;

import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.CaseDAO;
import com.jadu.dao.CompanyBranchDivisionDAO;
import com.jadu.dao.InsuranceCompanyDAO;
import com.jadu.dao.PurposeOfInspectionDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.Agent;
import com.jadu.model.CompanyBranchDivision;
import com.jadu.model.InspectionCase;
import com.jadu.model.PurposeOfInspection;
import com.jadu.model.builder.InspectionCaseBuilder;
import com.jadu.service.CreateUserService;
import com.jadu.service.SmsService;
import com.jadu.service.URLShortnerService;
import java.io.IOException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/company")
public class CompanyController {
    
    @Autowired
    private UserDAOImpl userDAO;
    
    @Autowired
    private PurposeOfInspectionDAO poiDAO;
    
    @Autowired 
    private AgentDAOImpl agentDAO;
    
    @Autowired
    private InsuranceCompanyDAO insuranceCompanyDAO;
    
    @Autowired
    private CreateUserService createUserService;
    
    @Autowired
    private SmsService smsService;
    
    @Autowired
    private CaseDAO caseDAO;
    
    @Autowired
    private URLShortnerService urlShortnerService;
    
    @Autowired
    private CompanyBranchDivisionDAO companyBranchDivisionDAO;
    
    
    @RequestMapping(value = "/case", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public void createCase(
            @RequestParam(value = "purpose_of_inspection", required=true ) String purposeOfInspection,
            @RequestParam(value = "customer_name", required=true ) String customerName,
            @RequestParam(value = "customer_phone_number", required=true ) String customerPhoneNumber,
            @RequestParam(value = "vehicle_number", required=true ) String vehicleNumber,
            @RequestParam(value = "agent_phone_number", required=true ) String agentPhoneNumber,
            HttpServletRequest request
    ) throws Exception{
        
        PurposeOfInspection poi = poiDAO.getPurposeOfInspectionById(purposeOfInspection);
        
        if(poi == null)
            throw new Exception("Invalid purpose of inspection!");
        
        Integer branchId = (Integer) request.getAttribute("branch_id");
        
        if(branchId == null)
            throw new Exception("Invalid branch. Please contact administrator!");
        
        CompanyBranchDivision cbd = companyBranchDivisionDAO.getCompanyBranchDivisionById(branchId);
        
        if(cbd==null)
            throw new Exception("Invalid branch. Please contact administrator!");
        
        Agent refAgent = agentDAO.getUniqueAgentByPhoneNumber(agentPhoneNumber);
        
        if(refAgent == null)
            refAgent = agentDAO.getUniqueAgentByPhoneNumber(cbd.getDefaultAgent());
        
        if(refAgent == null)
            throw new Exception("Default agent is not setup yet. Please contact administrator!");
        
        InspectionCaseBuilder  icBuilder =  new InspectionCaseBuilder(
                customerName,
                customerPhoneNumber,
                refAgent.getFirstName() + " " + refAgent.getLastName(),
                refAgent.getEmail(),
                refAgent.getPhoneNumber(),
                vehicleNumber,
                poi,
                cbd
        )
        .setInsuranceCompany(insuranceCompanyDAO.getInsuranceCompanyById(cbd.getCompany()), cbd)
        .setInspectionTime(new Date(), 0.0, 0.0)
        .setStage(1);
        
        com.jadu.model.User customer =  userDAO.getUserByPhoneOrEmail(customerPhoneNumber);

        if(customer == null)
            customer = createUserService.createCustomer("", customerPhoneNumber);

        icBuilder.addCaseToUserForStage(refAgent, 1);
            
        icBuilder
            .addCaseToUserForStage(customer, 3)
            .setInspectionType("ASSIGN_TO_CUSTOMER")
            .setStage(3);
        
        InspectionCase ic = icBuilder.build();
        
        ic.setEncryptionKey(UtilHelper.getRandomStringUpperCase(16));
        
        ic.setDownloadKey(UtilHelper.getRandomString(64)  + UtilHelper.getCurrentTimestampAsString() + "_");
        
        caseDAO.save(ic);
        
        String url = urlShortnerService.shortenUrl("customer-inspection/" + ic.getId());
        
        smsService.sendSMSAsync(
                customerPhoneNumber, 
                "We have an insurance inspection request for " + ic.getVehicleNumber() + ". Please download the app " + url + " to complete the inspection. Call 7290049100 for support");
    }
    
    @ExceptionHandler(Exception.class)
    public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
        ErrorHandler.handleError(ex, response);
        System.out.println(ex.getStackTrace());
    }
}
