package com.jadu.front.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.AppVersionDAO;
import com.jadu.dao.CarRegistrationAPIDAO;
import com.jadu.dao.ClaimCaseDAO;
import com.jadu.dao.ClaimCasePhotoDAO;
import com.jadu.dao.ClaimsPhotoTypeDAO;
import com.jadu.dao.CompanyBranchDivisionDAO;
import com.jadu.dao.CompanyPhotoReqDAO;
import com.jadu.dao.DivisionDAO;
import com.jadu.dao.IRDAIAgentDataDAO;
import com.jadu.dao.InspectorDAO;
import com.jadu.dao.InsuranceCompanyDAO;
import com.jadu.dao.KYCDAO;
import com.jadu.dao.PinPointDAO;
import com.jadu.dao.PurposeOfInspectionDAO;
import com.jadu.dao.PurposeOfSurveyDAO;
import com.jadu.dao.ReportingCompanyDAO;
import com.jadu.dao.StateDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dao.VehicleDAO;
import com.jadu.dao.VehicleFuelTypeDAO;
import com.jadu.dao.VehiclePhotoReqDAO;
import com.jadu.dao.VehicleTypeDAO;
import com.jadu.dto.CaseDTO;
import com.jadu.dto.RestWrapperDTO;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.Levenshtein;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.Admin;
import com.jadu.model.Agent;
import com.jadu.model.AppVersion;
import com.jadu.model.CompanyBranchDivision;
import com.jadu.model.CompanyBranchDivisionUser;
import com.jadu.model.CompanyUser;
import com.jadu.model.IRDAIAgentData;
import com.jadu.model.ClaimCase;
import com.jadu.model.ClaimCasePhoto;
import com.jadu.model.ClaimCaseUserAllocation;
import com.jadu.model.InsuranceCompany;
import com.jadu.model.PinPoint;
import com.jadu.model.PurposeOfSurvey;
import com.jadu.model.QC;
import com.jadu.model.User;
import com.jadu.model.Vahan;
import com.jadu.model.VahanJSON;
import com.jadu.model.Vehicle;
import com.jadu.model.VehicleFuelType;
import com.jadu.model.builder.ClaimCaseBuilder;
import com.jadu.service.CaseService;
import com.jadu.service.S3Service;
import com.jadu.service.UtilService;
import freemarker.template.TemplateException;
import org.springframework.web.bind.annotation.ExceptionHandler;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/util")
public class UtilController {

	@Autowired
	private InsuranceCompanyDAO insuranceCompanyDAO;

	@Autowired
	private AgentDAOImpl agentDAO;

	@Autowired
	private IRDAIAgentDataDAO irdaiAgentDataDAO;

	@Autowired
	private VehicleTypeDAO vehicleTypeDAO;

	@Autowired
	private VehicleFuelTypeDAO vehicleFuelTypeDAO;

	@Autowired
	private PurposeOfInspectionDAO poiDAO;

	@Autowired
	private CompanyPhotoReqDAO companyPhotoReqDAO;

	@Autowired
	private VehiclePhotoReqDAO vehiclePhotoReqDAO;

	@Autowired
	private CompanyBranchDivisionDAO companyBranchDivisionDAO;

	@Autowired
	private VehicleDAO vehicleDAO;

	@Autowired
	private UserDAOImpl userDAO;

	@Autowired
	private KYCDAO kycDAO;

	@Autowired
	private PinPointDAO pinPointDAO;

	@Autowired
	private CarRegistrationAPIDAO carRegistrationAPIDAO;

	@Autowired
	private InspectorDAO inspectorDAO;

	@Autowired
	private UtilService utilService;

	@Autowired
	private S3Service s3Service;

	@Autowired
	private AppVersionDAO appVersionDAO;

	@Autowired
	private StateDAO stateDAO;

	@Autowired
	private DivisionDAO divisionDAO;

	@Autowired
	private S3Service s3service;

	@Autowired
	private CaseService caseService;

	@Autowired
	private ClaimCasePhotoDAO claimCasePhotoDAO;

	@Autowired
	private PurposeOfSurveyDAO posDAO;

	@Autowired
	private ClaimsPhotoTypeDAO claimsPhotoTypeDAO;

	@Autowired
	private ClaimCaseDAO claimCaseDAO;
        
        @Autowired
        private ReportingCompanyDAO reportingCompanyDAO;

	private static final Logger LOGGER = LoggerFactory.getLogger(UtilController.class);

	@RequestMapping(value = "/upload-customer-image", method = RequestMethod.POST)
	public void uploadImage2(@RequestParam("image_value") String imageValue, @RequestParam("image_name") String name,
			@RequestParam("image_type") String type, @RequestParam("case_id") long caseId) throws Exception {
		// This will decode the String which is encoded by using Base64 class
		byte[] imageByte = Base64.decodeBase64(imageValue);

		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Unable to find case!");

		if (ic.getCurrentStage() > 3)
			throw new Exception("Photos has already been submitted. Can't be submitted again!");

		s3service.uploadFile("cases/" + ic.getId() + "/" + name, imageByte, type);

	}

	@RequestMapping(value = "/upload-customer-json", method = RequestMethod.POST)
	public void uploadJson(@RequestParam(value = "details", required = true) String details,
			@RequestParam(value = "vehicle_number", required = true) String vehicleNumber,
			@RequestParam(value = "vehicle_type", required = true) String vehicleType,
			@RequestParam(value = "vehicle_color", required = true) String vehicleColor,
			@RequestParam(value = "make_model_id", required = true) int makeModelId,
			@RequestParam(value = "fuel_type", required = true) String fuelType,
			@RequestParam(value = "yom", required = true) int YOM,
			@RequestParam(value = "case_id", required = true) int caseId) throws Exception {

		JSONArray detailsObj = new JSONArray(details);

		ClaimCase c = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		Set<ClaimCaseUserAllocation> items = c.getClaimCaseUserAllocations();

		User user = null;

		for (ClaimCaseUserAllocation item : items) {
			if (item.getStage() == 3) {
				user = item.getUser();
			}
		}

		if (user == null)
			throw new Exception("Invalid case!");

		if (c == null)
			throw new Exception("Unable to find case!");

		if (c.getCurrentStage() > 3)
			throw new Exception("Photos has already been submitted. Can't be submitted again!");

		caseService.submitReport(caseId, makeModelId, fuelType, YOM, vehicleColor, vehicleNumber, vehicleType,
				detailsObj, user);

	}

	@RequestMapping(value = "/customer-case-image", method = RequestMethod.POST)
	public void uploadImage2(@RequestParam("image_value") String imageValue, @RequestParam("image_name") String name,
			@RequestParam("image_type") String type, @RequestParam("case_id") long caseId,
			@RequestParam("latitude") Double latitude, @RequestParam("longitude") Double longitude,
			@RequestParam("photo_type") String photoType,
			@RequestParam(value = "snap_time", required = false) @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss") Date snap_time)
			throws Exception {
		// This will decode the String which is encoded by using Base64 class
		byte[] imageByte = Base64.decodeBase64(imageValue);

		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic.getCurrentStage() != 3)
			throw new Exception("Inspection case has already been completed!");

		s3service.uploadFile("cases/" + ic.getId() + "/" + name, imageByte, type);

		ClaimCasePhoto casePhoto = new ClaimCasePhoto();
		casePhoto.setFileName(name);
		casePhoto.setClaimCase(ic);
		casePhoto.setLatitude(latitude);
		casePhoto.setLongitude(longitude);
		casePhoto.setSnapTime(snap_time);

		claimCasePhotoDAO.save(casePhoto);
	}

	@RequestMapping(value = "/customer-case-info", method = RequestMethod.POST)
	public void uploadJson(@RequestParam(value = "vehicle_number", required = true) String vehicleNumber,
			@RequestParam(value = "vehicle_type", required = true) String vehicleType,
			@RequestParam(value = "vehicle_color", required = true) String vehicleColor,
			@RequestParam(value = "make_model_id", required = true) int makeModelId,
			@RequestParam(value = "fuel_type", required = true) String fuelType,
			@RequestParam(value = "yom", required = true) int YOM,
			@RequestParam(value = "case_id", required = true) int caseId) throws Exception {

		ClaimCase c = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		Set<ClaimCaseUserAllocation> items = c.getClaimCaseUserAllocations();

		User user = null;

		for (ClaimCaseUserAllocation item : items) {
			if (item.getStage() == 3) {
				user = item.getUser();
			}
		}

		if (user == null)
			throw new Exception("Invalid case!");

		if (c == null)
			throw new Exception("Unable to find case.");

		if (c.getCurrentStage() > 3)
			throw new Exception("Photos has already been submitted. Can't be submitted again!");

		c.setVehicleFuelType(vehicleFuelTypeDAO.get(fuelType));
		c.setVehicleNumber(vehicleNumber);

		c.setInspectionStartTime(new Date());
		claimCaseDAO.save(c);
	}

	@RequestMapping(value = "/customer-case-notify-completion", method = RequestMethod.POST)
	public void customerCaseNotifyCompletion(@RequestParam(value = "case_id", required = true) int caseId)
			throws Exception {

		ClaimCase c = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (c.getCurrentStage() == 3) {
			c.setCurrentStage(4);
			c.setInspectionSubmitTime(new Date());
			claimCaseDAO.save(c);
		}
	}

	@RequestMapping(value = "/get-app-version", method = RequestMethod.GET, produces = "application/json")
	public String getAppVersion() throws IOException, TemplateException, MessagingException, JSONException {
		JSONObject result = new JSONObject();
		result.put("version", "");
		result.put("message", "");

		List dbResult = appVersionDAO.getAll();

		if (!dbResult.isEmpty()) {
			AppVersion appVersion = (AppVersion) dbResult.get(0);

			result.put("version", appVersion.getValue());
			result.put("message", appVersion.getMessage());
		}

		return result.toString();
	}

	@RequestMapping(value = "/get-insurance-companies", method = RequestMethod.GET, produces = "application/json")
	public List getInsuranceCompanies() throws IOException, TemplateException, MessagingException {

		return insuranceCompanyDAO.getInsuranceCompanies();
	}

	@RequestMapping(value = "/get-insurance-companies-auth", method = RequestMethod.GET, produces = "application/json")
	public List getInsuranceCompaniesAuth() throws IOException, TemplateException, MessagingException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null) {
			com.jadu.model.User user = userDAO.getUserByUsername(
					((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
			Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;
			if (user instanceof CompanyUser) {
				List<InsuranceCompany> list = new ArrayList<InsuranceCompany>();
				companyBranchDivisionUsers = ((CompanyUser) user).getCompanyBranchDivisionUser();
				for (CompanyBranchDivisionUser companyBranchDivisionUser : companyBranchDivisionUsers) {
					if (companyBranchDivisionUser.getCompany() != null) {
						list.add(insuranceCompanyDAO.getInsuranceCompanyByName(companyBranchDivisionUser.getCompany()));
					} else {
						LOGGER.info("No company found in company-branch_division_users table for user={}",
								user.getPhoneNumber());
					}
				}
				return list;
			}
		}
		return insuranceCompanyDAO.getInsuranceCompanies();
	}

	@RequestMapping(value = "/insurance-companies", method = RequestMethod.GET, produces = "application/json")
	public Object insuranceCompanies() throws IOException, TemplateException, MessagingException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		com.jadu.model.User user = userDAO.getUserByUsername(
				((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
		Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;
		if (user instanceof CompanyUser) {
			companyBranchDivisionUsers = ((CompanyUser) user).getCompanyBranchDivisionUser();
			for (CompanyBranchDivisionUser companyBranchDivisionUser : companyBranchDivisionUsers) {
				return insuranceCompanyDAO.getInsuranceCompanyByName(companyBranchDivisionUser.getCompany());
			}
		}
		return new RestWrapperDTO(insuranceCompanyDAO.getInsuranceCompanies());
	}

	@RequestMapping(value = "/check-report-status", method = RequestMethod.GET, produces = "application/json")
	public String checkReportStatus() throws IOException, TemplateException, MessagingException {

		List cases = claimCaseDAO.getCompletedCases(5, "amit_iffco_ro");
		JSONArray result = new JSONArray();

		for (Object ic : cases) {
			CaseDTO temp = (CaseDTO) ic;
			if (!s3Service.exists("cases/" + temp.getId() + "/report.pdf"))
				result.put(temp.getId());
		}

		return result.toString();
	}

	@RequestMapping(value = "/company-image/{imageId:.+}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getCompanyImage(@PathVariable(value = "imageId") String fileName) throws IOException {

		return utilService.getCompanyImage(fileName);
	}

	@RequestMapping(value = "/profile-image/{imageId:.+}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getProfileImage(@PathVariable(value = "imageId") String fileName) throws IOException {

		return utilService.getProfileImage(fileName);
	}

	@RequestMapping(value = "/case-image/{caseId}/{imageId:.+}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getCaseImage(@PathVariable(value = "imageId") String fileName,
			@PathVariable(value = "caseId") int caseId) throws IOException {

		return utilService.getCaseImage(fileName, caseId);
	}

	@RequestMapping(value = "/get-agent-by-irdai-code", method = RequestMethod.GET, produces = "application/json")
	public IRDAIAgentData getAgentByIRDAICode(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "irdai_code", required = true) String irdaiCode) throws Exception {

		IRDAIAgentData result = (irdaiAgentDataDAO.getIRDAIAgentByID(irdaiCode));

		if (result == null)
			throw new javassist.NotFoundException("No agent found for IRDAI code: " + irdaiCode);

		return result;
	}

	@RequestMapping(value = "/get-vehicle-types", method = RequestMethod.GET, produces = "application/json")
	public List getVehicleTypes() throws IOException, TemplateException, MessagingException, Exception {

		return vehicleTypeDAO.getAll();
		// throw new IOException("Not working");
	}

	@RequestMapping(value = "/vehicle-types", method = RequestMethod.GET, produces = "application/json")
	public RestWrapperDTO vehicleTypes() throws IOException, TemplateException, MessagingException, JSONException {
		return new RestWrapperDTO(vehicleTypeDAO.getAll());
	}

	@RequestMapping(value = "/get-vehicle-make-models", method = RequestMethod.GET, produces = "application/json")
	public List getVehicleMakeModels() throws IOException, TemplateException, MessagingException {

		return vehicleDAO.getAll();
	}

	@RequestMapping(value = "/vehicle-make-models", method = RequestMethod.GET, produces = "application/json")
	public Object vehicleMakeModels() throws IOException, TemplateException, MessagingException {
		return new RestWrapperDTO(vehicleDAO.getAllDTO());
	}

	@RequestMapping(value = "/get-vehicle-fuel-types", method = RequestMethod.GET, produces = "application/json")
	public List getVehicleFuelTypes() throws IOException, TemplateException, MessagingException {

		return vehicleFuelTypeDAO.getAll();
	}

	@RequestMapping(value = "/vehicle-fuel-types", method = RequestMethod.GET, produces = "application/json")
	public Object vehicleFuelTypes() throws IOException, TemplateException, MessagingException {
		return new RestWrapperDTO(vehicleFuelTypeDAO.getAll());
	}

	@RequestMapping(value = "/get-purpose-of-inspections", method = RequestMethod.GET, produces = "application/json")
	public List getPurposeOfInspections() throws IOException, TemplateException, MessagingException {

		return poiDAO.getAll();
	}

	@RequestMapping(value = "/get-purpose-of-survey", method = RequestMethod.GET, produces = "application/json")
	public List<PurposeOfSurvey> getPurposeOfSurvey() throws IOException, TemplateException, MessagingException {
		return posDAO.getAll();
	}

	@RequestMapping(value = "/purpose-of-inspections", method = RequestMethod.GET, produces = "application/json")
	public Object getAllPurposeOfInspections() throws IOException, TemplateException, MessagingException {

		return new RestWrapperDTO(poiDAO.getAll());
	}

	@RequestMapping(value = "/get-company-photo-req", method = RequestMethod.GET, produces = "application/json")
	public List getCompanyPhotoReq() throws IOException, TemplateException, MessagingException {

		return companyPhotoReqDAO.get();
	}

	@RequestMapping(value = "/get-vehicle-photo-req", method = RequestMethod.GET, produces = "application/json")
	public List getVehiclePhotoReq() throws IOException, TemplateException, MessagingException {

		return vehiclePhotoReqDAO.get();
	}

	/**
	 * 
	 * @param company
	 * @return
	 * @throws IOException
	 * @throws TemplateException
	 * @throws MessagingException
	 * 
	 *                            Request for WEB
	 */
	@RequestMapping(value = "/get-branch-division-by-company-auth", method = RequestMethod.GET, produces = "application/json")
	public List getBranchDivisionByCompanyIdAuth(@RequestParam(value = "company", required = false) String company)
			throws IOException, TemplateException, MessagingException {
		List<CompanyBranchDivision> companyBranchDivisions = null;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = utilService.getUser(authentication);
		if (user != null) {
			companyBranchDivisions = utilService.fetchCompanyBranchDivisionList(user);
		} else {
			companyBranchDivisions = companyBranchDivisionDAO.getCompanyBranchDivisionByCompany(company);
		}
		return companyBranchDivisions;
	}

	@RequestMapping(value = "/get-branch-division-by-company", method = RequestMethod.GET, produces = "application/json")
	public List getBranchDivisionByCompanyId(@RequestParam(value = "company", required = true) String company)
			throws IOException, TemplateException, MessagingException {

		return companyBranchDivisionDAO.getCompanyBranchDivisionByCompany(company);
	}

	@RequestMapping(value = "/get-branches-by-company-auth", method = RequestMethod.GET, produces = "application/json")
	public List<String> getBranchesByCompanyIdAuth(@RequestParam(value = "company", required = true) String company)
			throws IOException, TemplateException, MessagingException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = utilService.getUser(authentication);
		List<String> branchList = new ArrayList<String>();
		if (user != null) {
			List<CompanyBranchDivision> companyBranchDivisions = utilService.fetchCompanyBranchDivisionList(user);
			if (companyBranchDivisions != null) {
				for (CompanyBranchDivision companyBranchDivision : companyBranchDivisions) {
					branchList.add(companyBranchDivision.getBranch());
				}
			}
		}
		return branchList;
	}

	@RequestMapping(value = "/get-branches-by-user", method = RequestMethod.GET, produces = "application/json")
	public Set<String> getBranchesByUser() throws IOException, TemplateException, MessagingException {
		LOGGER.info("/util/get-branches-by-user request recieved");
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = utilService.getUser(authentication);
		try {
			return utilService.fetchBranches(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new HashSet<String>();
	}

	private List<CompanyBranchDivision> fetchCompanyBranchDivisionList(Authentication authentication, String company) {
		List<CompanyBranchDivision> list = new ArrayList<CompanyBranchDivision>();
		if (authentication != null) {
			com.jadu.model.User user = userDAO.getUserByUsername(
					((org.springframework.security.core.userdetails.User) authentication.getPrincipal()).getUsername());
			Set<CompanyBranchDivisionUser> companyBranchDivisionUsers = null;
			if (user instanceof Admin) {
				return companyBranchDivisionDAO.getCompanyBranchDivisionByCompany(company);
			}
			if (user instanceof CompanyUser) {
				companyBranchDivisionUsers = ((CompanyUser) user).getCompanyBranchDivisionUser();
			}
			if (user instanceof QC) {
				return companyBranchDivisionDAO.getCompanyBranchDivisionByCompany(company);
			}
			for (CompanyBranchDivisionUser companyBranchDivisionUser : companyBranchDivisionUsers) {
				list.addAll(companyBranchDivisionDAO
						.getCompanyBranchDivisionByCompany(companyBranchDivisionUser.getCompany()));
			}
			return list;
		}
		return list;
	}

	@RequestMapping(value = "/branch-division-by-company", method = RequestMethod.GET, produces = "application/json")
	public Object branchDivisionByCompanyId(@RequestParam(value = "company", required = true) String company)
			throws IOException, TemplateException, MessagingException {

		return new RestWrapperDTO(companyBranchDivisionDAO.getCompanyBranchDivisionByCompany(company));
	}

	@RequestMapping(value = "/get-photo-types", method = RequestMethod.GET, produces = "application/json")
	public List getPhotoTypes() throws IOException, TemplateException, MessagingException {

		return claimsPhotoTypeDAO.getAll();
	}

	@RequestMapping(value = "/get-claims-photo-types", method = RequestMethod.GET, produces = "application/json")
	public List getClaimsPhotoTypes() throws IOException, TemplateException, MessagingException {

		return claimsPhotoTypeDAO.getAll();
	}

	@RequestMapping(value = "/get-agent-details-by-phone-number", method = RequestMethod.GET, produces = "application/json")
	public List getAgentDetailsByPhoneNumber() throws IOException, TemplateException, MessagingException {
		return claimsPhotoTypeDAO.getAll();
	}

	@RequestMapping(value = "/get-agent-by-username", method = RequestMethod.GET, produces = "application/json")
	public Object getAgentByUsername(@RequestParam(value = "username", required = true) String username)
			throws IOException, TemplateException, MessagingException, Exception {

		Agent agent = agentDAO.getAgentByUsername(username);
		if (agent == null)
			throw new Exception("No agent exists for the given username!");
		return agent;
	}

	@RequestMapping(value = "/get-agent-by-phone-number", method = RequestMethod.GET, produces = "application/json")
	public Object getAgentByPhoneNumber(@RequestParam(value = "phone_number", required = true) String phoneNumber)
			throws IOException, TemplateException, MessagingException, Exception {

		return utilService.getAgentByPhoneNumber(phoneNumber, false);
	}
        
        @RequestMapping(value = "/get-agent-by-company", method = RequestMethod.GET, produces = "application/json")
	public Object getAgentByCompany(@RequestParam(value = "company", required = true) String company)
			throws IOException, TemplateException, MessagingException, Exception {

		return reportingCompanyDAO.findAll(company);
	}

	@RequestMapping(value = "/get-inspector-by-phone-number", method = RequestMethod.GET, produces = "application/json")
	public Object getInspectorsByPhoneNumber(@RequestParam(value = "phone_number", required = true) String phoneNumber)
			throws IOException, TemplateException, MessagingException, Exception {

		List result = inspectorDAO.getAgentByPhoneNumber(phoneNumber);
		if (result.isEmpty())
			throw new Exception("No inspector exists for the given phone number!");

		if (result.size() > 1)
			throw new Exception("Multiple inspectors exists for given phone number!");

		return result.get(0);
	}

	@RequestMapping(value = "/get-user-by-phone-number", method = RequestMethod.GET, produces = "application/json")
	public Object getUserByPhoneNumber(@RequestParam(value = "phone_number", required = true) String phoneNumber)
			throws IOException, TemplateException, MessagingException, Exception {

		com.jadu.model.User result = userDAO.getUserByPhoneOrEmail(phoneNumber);
		if (result == null)
			throw new Exception("No user exists for the given phone number!");

		return result;
	}

	@RequestMapping(value = "/check-if-mail-already-used", method = RequestMethod.GET)
	public Long checkIfMailAlreayUsed(@RequestParam(value = "email", required = true) String emailId) {
		return userDAO.getCountOfEmail(emailId);
	}

	@RequestMapping(value = "/check-if-phone-already-used", method = RequestMethod.GET)
	public Long checkIfPhoneAlreayUsed(@RequestParam(value = "phone_number", required = true) String phoneNumber) {
		return userDAO.getCountOfPhoneNumber(phoneNumber);
	}

	@RequestMapping(value = "/check-if-kyc-already-used", method = RequestMethod.GET)
	public Long checkIfAadharAlreayUsed(@RequestParam(value = "uid", required = true) String uid) {
		return kycDAO.getCountOfUID(uid);
	}

	@RequestMapping(value = "/get-pin", method = RequestMethod.GET, produces = "application/json")
	public Object getPin(@RequestParam(value = "pin_code", required = true) String pincode,
			@RequestParam(value = "latitude", required = true) Double latitude,
			@RequestParam(value = "longitude", required = true) Double longitude) throws JSONException {
		List<PinPoint> pinPoints = pinPointDAO.getPins(pincode);
		JSONObject result = new JSONObject();
		result.put("latitude", latitude);
		result.put("longitude", longitude);

		if (pinPoints.isEmpty())
			return result;

		boolean inLine = false;

		for (PinPoint p : pinPoints) {
			if (UtilHelper.distance(latitude, longitude, p.getLatitude(), p.getLongitude()) <= 10) {
				inLine = true;
			}
		}

		if (!inLine) {
			result.put("latitude", pinPoints.get(0).getLatitude());
			result.put("longitude", pinPoints.get(0).getLongitude());
		}

		return result.toString();

	}

	@RequestMapping(value = "/get-vehicle-details", method = RequestMethod.GET)
	public Object getVehicleDetails(@RequestParam(value = "make", required = true) String make,
			@RequestParam(value = "model", required = true) String model) {
		return Levenshtein.percMatch(vehicleDAO.get(), make, 75.0, true);
	}

	@RequestMapping(value = "/submit-address", method = RequestMethod.POST)
	public void submitAddress(@RequestParam(value = "ic", required = true) long ic,
			@RequestParam(value = "inspection_time", required = true) @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm") Date date,
			@RequestParam(value = "latitude", required = true) Double latitude,
			@RequestParam(value = "longitude", required = true) Double longitude) throws Exception {
		ClaimCase ClaimCase = (ClaimCase) claimCaseDAO.getCaseById(ic);

		if (ClaimCase.getCurrentStage() > 2)
			throw new Exception("Case has already been updated!");

		claimCaseDAO
				.save(new ClaimCaseBuilder(ClaimCase).setStage(2).setInspectionTime(date, latitude, longitude).build());
	}

	@RequestMapping(value = "/submit-address-qc", method = RequestMethod.POST)
	public void submitAddressQC(@RequestParam(value = "ic", required = true) long ic,
			@RequestParam(value = "inspection_time", required = true) @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss") Date date,
			@RequestParam(value = "latitude", required = true) Double latitude,
			@RequestParam(value = "longitude", required = true) Double longitude) throws Exception {
		ClaimCase ClaimCase = (ClaimCase) claimCaseDAO.getCaseById(ic);

		if (ClaimCase.getCurrentStage() > 2)
			throw new Exception("Case has already been updated!");

		claimCaseDAO
				.save(new ClaimCaseBuilder(ClaimCase).setStage(2).setInspectionTime(date, latitude, longitude).build());
	}

	@RequestMapping(value = "/get-vahan-details", method = RequestMethod.GET, produces = "application/json")
	public Object getVahanDetails(@RequestParam(value = "vehicle_number", required = true) String vehicleNumber)
			throws Exception {
		Vahan vahan = carRegistrationAPIDAO.getVehicleDetails(vehicleNumber);
		VahanJSON json = new VahanJSON(vahan.getXml());

		String vahanMake = json.getMake();
		String vahanModel = json.getModel();
		String vahanFuelType = json.getFuelType();

		JSONObject result = new JSONObject();
		result.put("year", json.getYear());
		result.put("engineNumber", json.getEngineNumber());
		result.put("chasisNumber", json.getChasisNumber());
		result.put("color", json.getColor());

		if (vahanMake != null) {
			List<String> makes = vehicleDAO.get();
			String detectedMake = Levenshtein.percMatch(makes, vahanMake, 50.0, true);
			if (detectedMake != null) {
				List<Vehicle> vehicles = vehicleDAO.getModels(detectedMake);
				List<String> models = new ArrayList<>();
				for (Vehicle v : vehicles) {
					models.add(UtilHelper
							.removeMultipleSpaces(UtilHelper.removeSubStringLowercase(v.getModel(), v.getMake())));
				}

				if (vahanModel != null) {
					String detectedModel = Levenshtein.percMatch(models, vahanModel, 65.0, true);

					if (detectedModel == null) {
						result.put("vehicle", vehicles.get(0).getId());
					} else {
						int index = models.indexOf(detectedModel);
						if (index == -1) {
							result.put("vehicle", vehicles.get(0).getId());
						} else {
							result.put("vehicle", vehicles.get(index).getId());
						}
					}

				} else {
					result.put("vehicle", vehicles.get(0).getId());
				}

			}
		}

		if (vahanFuelType != null) {
			List<VehicleFuelType> fTypes = vehicleFuelTypeDAO.getAll();
			List<String> ids = new ArrayList<>();

			for (VehicleFuelType v : fTypes) {
				ids.add(v.getId().toLowerCase());
			}

			String detectedFuelType = Levenshtein.percMatch(ids, vahanFuelType.toLowerCase(), 75.0, true);
			if (detectedFuelType != null)
				result.put("vehicleFuelType", fTypes.get(ids.indexOf(detectedFuelType)).getId());
		}

		return result.toString();
	}

	@RequestMapping(value = "/get-vahan-details-xml", method = RequestMethod.GET, produces = "application/xhtml+xml")
	public Object getVahanDetailsXml(HttpServletResponse httpServletResponse,
			@RequestParam(value = "vehicle_number", required = true) String vehicleNumber) throws Exception {
		httpServletResponse.setContentType("text/xml");
		httpServletResponse.setHeader("Content-type", "application/xhtml+xml");
		Vahan vahan = carRegistrationAPIDAO.getVehicleDetails(vehicleNumber);

		return vahan.getXml();
	}

	@RequestMapping(value = "/get-states", produces = "application/json")
	public List getStates() {
		return stateDAO.getAll();
	}

	@RequestMapping(value = "/states", produces = "application/json")
	public Object getStatesV2() {
		return new RestWrapperDTO(stateDAO.getAll());
	}

	@RequestMapping(value = "/get-divisions", produces = "application/json")
	public List getDivisions(@RequestParam(value = "state", required = true) int stateId) {
		return divisionDAO.getAll(stateId);
	}

	@RequestMapping(value = "/divisions", produces = "application/json")
	public Object getDivisionsV2(@RequestParam(value = "state", required = true) int stateId) {
		return new RestWrapperDTO(divisionDAO.getAll(stateId));
	}

	@RequestMapping(value = "/get-photos-required", produces = "application/json")
	public Object getPhotosRequired(@RequestParam(value = "case_id", required = true) long caseId)
			throws IOException, Exception {
		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("Invalid inspection case!");

		return companyPhotoReqDAO.get(ic.getCompanyBranchDivision().getCompany());

	}

	@RequestMapping(value = "/cases/download-report", method = RequestMethod.GET, produces = "application/pdf")
	public HttpEntity<byte[]> downloadReport(@RequestParam(value = "case_id", required = true) Long caseId,
			HttpServletResponse response) throws Exception {

		ClaimCase ic = (ClaimCase) claimCaseDAO.getCaseById(caseId);

		if (ic == null)
			throw new Exception("No case with given ID exists for user!");

		if (ic.getCurrentStage() < 5)
			throw new Exception("Inspection report has not been generated yet!");

		byte[] document = s3Service.readFile("cases/" + caseId + "/" + "report.pdf");

		if (document == null)
			throw new Exception("Report not found!");

		HttpHeaders header = new HttpHeaders();
		header.setContentType(new MediaType("application", "pdf"));
		header.set("Content-Disposition", "inline; filename=Report.pdf");
		header.setContentLength(document.length);

		return new HttpEntity<>(document, header);
	}

	@RequestMapping(value = "/cases/download-zip-photos/{zipid}", method = RequestMethod.GET, produces = "application/zip")
	public void downloadZipPhotos(@PathVariable String zipid, HttpServletResponse response) throws Exception {

		ClaimCase ic = (ClaimCase) claimCaseDAO.getClaimCaseByZipid(zipid);

		if (ic == null)
			throw new Exception("No case with given ID exists for user!");

		List<String> casePhotos = claimCasePhotoDAO.getCasePhotosByCaseId(ic.getId());

		List<byte[]> files = new ArrayList<>();

		for (String casePhoto : casePhotos) {
			files.add(s3Service.readFile("cases/" + ic.getId() + "/" + casePhoto));
		}

		File zipfile = UtilHelper.zipBytes(files, casePhotos, ic.getId() + ".zip");

		if (zipfile == null)
			throw new Exception("Unable to zip all files! Please try again later.");

		try {
			response.setHeader("Content-Disposition", "attachment; filename=\"" + ic.getId() + ".zip" + "\"");
			InputStream is = new FileInputStream(zipfile);
			IOUtils.copy(is, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");
		}
	}

	@RequestMapping(value = "/cases/download-uploaded-photos/{zipid}", method = RequestMethod.GET, produces = "application/zip")
	public void downloadUploadedPhotos(@PathVariable String zipid, HttpServletResponse response) throws Exception {

		ClaimCase ic = (ClaimCase) claimCaseDAO.getClaimCaseByZipid(zipid);

		if (ic == null)
			throw new Exception("No case with given ID exists for user!");

		InputStream zipfile = s3Service.readFileStream("cases/" + ic.getId() + "/" + ic.getId() + ".zip");

		if (zipfile == null)
			throw new Exception("Unable to zip all files! Please try again later.");

		try {
			response.setHeader("Content-Disposition", "attachment; filename=\"" + ic.getId() + ".zip" + "\"");
			IOUtils.copy(zipfile, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");
		}
	}
        
    @ExceptionHandler(Exception.class)
    public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
        ErrorHandler.handleError(ex, response);
    }
}
