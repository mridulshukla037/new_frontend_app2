package com.jadu.front.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author gautam
 */
@RestController
public class AgentAppController {

    @RequestMapping(value = "/agentapp")
    public ModelAndView getIndex(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        return new ModelAndView("redirect:https://play.google.com/store/apps/details?id=com.proj.iffcotokio");

    }
    
    @RequestMapping(value = "/inspectiondemo")
    public ModelAndView getVideo(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        return new ModelAndView("redirect:https://youtu.be/1TeYam7d0EQ");

    }
}
