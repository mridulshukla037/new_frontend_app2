package com.jadu.front.controller;

import com.jadu.dao.AgentDAOImpl;
import com.jadu.dao.CaseDAO;
import com.jadu.dao.InspectorDAO;
import com.jadu.dao.InsuranceCompanyDAO;
import com.jadu.dao.PurposeOfInspectionDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.Agent;
import com.jadu.model.CaseUserAllocation;
import com.jadu.model.CompanyBranchDivision;
import com.jadu.model.InspectionCase;
import com.jadu.model.Inspector;
import com.jadu.model.PurposeOfInspection;
import com.jadu.model.builder.InspectionCaseBuilder;
import com.jadu.push.notification.AndroidPushNotificationsService;
import com.jadu.service.SmsService;
import static com.rosaloves.bitlyj.Bitly.as;
import static com.rosaloves.bitlyj.Bitly.shorten;
import com.rosaloves.bitlyj.Url;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/inspector")
public class InspectorController {
    
    @Autowired
    private InspectorDAO inspectorDAO;
    
    @Autowired
    private PurposeOfInspectionDAO poiDAO;
    
    @Autowired
    private CaseDAO caseDAO;
    
    @Autowired
    private AgentDAOImpl agentDAO;
    
    @Autowired
    private InsuranceCompanyDAO insuranceCompanyDAO;
    
    @Autowired
    private UserDAOImpl userDAO;
    
    @Autowired
    private AndroidPushNotificationsService androidPushNotificationsService;
    
    @Autowired
    private SmsService smsService;
    
    @RequestMapping(value = "/create-case", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public InspectionCase createCase(
            @RequestParam(value = "purpose_of_inspection", required=true ) String purposeOfInspection,
            @RequestParam(value = "customer_name", required=true ) String customerName,
            @RequestParam(value = "customer_phone_number", required=true ) String customerPhoneNumber,
            @RequestParam(value = "self_inspect", required=true ) boolean selfInspect,
            @RequestParam(value = "vehicle_number", required=true ) String vehicleNumber,
            @RequestParam(value = "agent_id", required=true ) String agentId,
            @RequestParam(value = "inspection_time", required=false) @DateTimeFormat(pattern="yyyy/MM/dd HH:mm:ss") Date date,
            @RequestParam(value = "latitude", required=false ) Double latitude,
            @RequestParam(value = "longitude", required=false ) Double longitude
    ) throws Exception{
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Inspector inspector = inspectorDAO.getInspectorByUsername(((User)authentication.getPrincipal()).getUsername());
        PurposeOfInspection poi = poiDAO.getPurposeOfInspectionById(purposeOfInspection);
        Agent refAgent = agentDAO.getAgentByUsername(agentId);
        CompanyBranchDivision companyBranchDivision = refAgent.getAgentDetails().getCompanyBranchDivision();
        if(refAgent == null){
            throw new Exception("Referred agent account doesn't exists");
        }
        
        InspectionCaseBuilder  icBuilder =  new InspectionCaseBuilder(
                customerName,
                customerPhoneNumber,
                refAgent.getFirstName() + " " + refAgent.getLastName(),
                refAgent.getEmail(),
                refAgent.getPhoneNumber(),
                vehicleNumber,
                poi,
                refAgent.getAgentDetails().getCompanyBranchDivision()
        )
        .setInsuranceCompany(insuranceCompanyDAO.getInsuranceCompanyById(companyBranchDivision.getCompany()), companyBranchDivision)
        .addCaseToUserForStage(inspector, 1)
        .setStage(1);
        
        if(selfInspect){
            icBuilder
                    .addCaseToUserForStage(inspector, 3)
                    .setInspectionType("SELF_INSPECT")
                    .setInspectionTime(date, latitude, longitude)
                    .setStage(3);
        }else{
            icBuilder.setStage(1);
        }
        
        InspectionCase ic = icBuilder.build();
        
        caseDAO.save(ic);
        
        smsService.sendSMS(inspector.getPhoneNumber(), "Your case has been created with Vehicle Number - "+ic.getVehicleNumber()+" and Case Id - "+ic.getId()+".");
        smsService.sendSMS(refAgent.getPhoneNumber(), "Your case has been created with Vehicle Number - "+ic.getVehicleNumber()+" and Case Id - "+ic.getId()+".");
        
        if(selfInspect){
            smsService.sendSMS(ic.getCustomerPhoneNumber(), "We have received an inspection request for Vehicle Number - "+ic.getVehicleNumber()+" from Insurance Company. Our agent will contact you shortly.");   
            androidPushNotificationsService.sendNotificationToUserAsync(
                inspector.getUsername(), 
                "Case created successfully", 
                "Case no : "+ic.getId()+" created for "+ic.getVehicleNumber()+" . Please check scheduled tab to complete inspection.",
                "");
        }else{
            String urlString = "https://wimwisure.com/RestServer/update-address/"+ic.getId();
            Url url = as("o_6thofi6g44", "R_0b24f994e76f44058f2f51f6bc8a5ec4").call(shorten(urlString));
            smsService.sendSMS(ic.getCustomerPhoneNumber(), "Hi, Please provide location & schedule for inspection of "+ic.getVehicleNumber()+" for insurance policy. Visit "+url.getShortUrl()+". Please call 7290049100 for any assistance.");  
            
            androidPushNotificationsService.sendNotificationToUserAsync(
                inspector.getUsername(), 
                "Case created successfully", 
                "Case no : "+ic.getId()+" created for "+ic.getVehicleNumber()+" . Please check My cases tab to view progress.",
                "");
        }
        
        return ic;
    }
    
    @RequestMapping(value = "/accept-case", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public void acceptCase(
            @RequestParam(value = "case_id", required=true ) int caseId
    ) throws Exception{
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        InspectionCase ic = (InspectionCase)caseDAO.getCaseById(caseId);
        Inspector inspector = inspectorDAO.getInspectorByUsername(((User)authentication.getPrincipal()).getUsername());
        
        ic.setCurrentStage(3);
        CaseUserAllocation allocate2 = new CaseUserAllocation();
        allocate2.setAllocationTime(UtilHelper.getDate());
        allocate2.setInspectionCase(ic);
        allocate2.setStage(3);
        allocate2.setUser(inspector);
        ic.addCaseUserAllocation(allocate2);
        caseDAO.save(ic);
        
    }
    
    
    @RequestMapping(value = "/cases/requested-cases", method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public List scheduledCases(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User securedUser = (User)authentication.getPrincipal();
        com.jadu.model.User user = userDAO.getUserByUsername(securedUser.getUsername());
        
        return caseDAO.getRequestedCases(2, user.getUsername());
    }
    
    
    @RequestMapping(value = "/set-availability", method = RequestMethod.POST)
    @ResponseBody
    public int setAvailability(
            @RequestParam(value = "availability", required=true ) boolean availability
    ){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User securedUser = (User)authentication.getPrincipal();
        com.jadu.model.Inspector inspector = inspectorDAO.getInspectorByUsername(securedUser.getUsername());
        
        return inspectorDAO.setAvailability(inspector.getUsername(), availability);
    }
    
    @RequestMapping(value = "/update-lat-long", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public String updateLatLong(
            @RequestParam(value = "latitude", required=false ) Double latitude,
            @RequestParam(value = "longitude", required=false ) Double longitude
    ) throws JSONException{
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User securedUser = (User)authentication.getPrincipal();
        com.jadu.model.Inspector inspector = inspectorDAO.getInspectorByUsername(securedUser.getUsername());
        
        inspector.setCurrentLatitude(latitude);
        inspector.setCurrentLongitude(longitude);
        inspector.setCurrentLatLngLastUpdateTime(new Date());
        
        userDAO.save(inspector);
        
        JSONObject result = new JSONObject();
        result.put("success", true);
        
        return result.toString();
    }
    
    @ExceptionHandler(Exception.class)
    public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
        ErrorHandler.handleError(ex, response);
        System.out.println(ex.getStackTrace());
    }
}
