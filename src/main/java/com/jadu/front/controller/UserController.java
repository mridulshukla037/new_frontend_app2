package com.jadu.front.controller;

import com.jadu.dao.AndroidDeviceDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.dao.UserNotificationDAO;
import com.jadu.dao.WebDeviceDAO;
import com.jadu.error.handler.ErrorHandler;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.AndroidDevice;
import com.jadu.model.Device;
import com.jadu.model.WebDevice;
import freemarker.template.TemplateException;
import java.io.IOException;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/user")
public class UserController {
    
    @Autowired
    private UserDAOImpl userDAO;
    
    @Autowired
    private UserNotificationDAO userNotificationDAO;
    
    @Autowired
    private AndroidDeviceDAO androidDeviceDAO;
    
    @Autowired
    private WebDeviceDAO webDeviceDAO;
    
    @RequestMapping(value = "/check-access-token-validity", method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public void checkAccessTokenValidity(){  
    } 
    
    
    @RequestMapping(value = "/profile-details", method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public Object checkAgentDetails(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User  p = (User)authentication.getPrincipal();
        return userDAO.getUserByUsername(p.getUsername());
    } 
    
    @RequestMapping(value = "/change-password", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public void changePassword(
            @RequestParam(value = "current_password", required=true ) String currentPassword,
            @RequestParam(value = "new_password", required=true ) String newPassword
    ) throws Exception{
        
        if(newPassword == null || newPassword.length() < 6)
            throw new Exception("Password should be of minimum 6 characters!");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User  p = (User)authentication.getPrincipal();
        com.jadu.model.User currentUser= userDAO.getUserByUsername(p.getUsername());
        if(!currentUser.getPassword().equals(currentPassword))
            throw new Exception("Invalid current password!");
        userDAO.updatePassword(currentUser.getUsername(), newPassword);
    } 
    
    
    @RequestMapping(value = "/subscribe-web-device", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public void subscribeWebDevice(
            @RequestParam(value = "device_id", required=true ) String deviceId
    ) throws IOException, TemplateException, MessagingException{
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User  p = (User)authentication.getPrincipal();
        com.jadu.model.User currentUser = userDAO.getUserByUsername(p.getUsername()); 
        webDeviceDAO.delete(p.getUsername());
        if(webDeviceDAO.get(deviceId, currentUser.getUsername()) == null){
            Device device = new WebDevice();
            device.setDeviceId(deviceId);
            device.setUsername(currentUser.getUsername());
            device.setUpdateTime(UtilHelper.getDate());
            webDeviceDAO.save(device);
        }
    }
    
    @RequestMapping(value = "/subscribe-device", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public void subscribe(
            @RequestParam(value = "device_id", required=true ) String deviceId
    ) throws IOException, TemplateException, MessagingException{
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User  p = (User)authentication.getPrincipal();
        com.jadu.model.User currentUser = userDAO.getUserByUsername(p.getUsername()); 
        androidDeviceDAO.delete(p.getUsername());
        if(androidDeviceDAO.get(deviceId, currentUser.getUsername()) == null){
            Device device = new AndroidDevice();
            device.setDeviceId(deviceId);
            device.setUsername(currentUser.getUsername());
            device.setUpdateTime(UtilHelper.getDate());
            androidDeviceDAO.save(device);
        }
    }
    
    @RequestMapping(value = "/unsubscribe-device", method = RequestMethod.POST, produces="application/json")
    @ResponseBody
    public void unsubscribe(
            @RequestParam(value = "device_id", required=true ) String deviceId
    ) throws IOException, TemplateException, MessagingException{
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User  p = (User)authentication.getPrincipal();
        com.jadu.model.User currentUser = userDAO.getUserByUsername(p.getUsername()); 
        Device device = androidDeviceDAO.get(deviceId, currentUser.getUsername());
        if(device != null){
            androidDeviceDAO.delete(device);
        }
    }
    
    @RequestMapping(value = "/get-count-of-unread-notifications", method = RequestMethod.GET)
    @ResponseBody
    public Object getCountOfUnreadNotifications(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User  p = (User)authentication.getPrincipal();
        return userNotificationDAO.getCountOfUnreadNotifications(p.getUsername());
    }
    
    @RequestMapping(value = "/get-notifications", method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public Object getNotifications(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User  p = (User)authentication.getPrincipal();
        return userNotificationDAO.getAll(p.getUsername());
    }
    
    @ExceptionHandler(Exception.class)
    public void handleAllException(Exception ex, HttpServletResponse response) throws IOException {
        ErrorHandler.handleError(ex, response);
    }
}
