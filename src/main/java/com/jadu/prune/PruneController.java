package com.jadu.prune;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jadu.dao.CaseDAO;
import com.jadu.dao.InspectionCasesHistoryDAO;
import com.jadu.service.AppConfigService;
import com.jadu.service.PruneCaseService;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/prune")
public class PruneController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PruneController.class);

	@Autowired
	private CaseDAO caseDAO;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private InspectionCasesHistoryDAO inspectionCasesHistoryDAO;

	@Autowired
	private PruneCaseService pruneCaseService;

	@RequestMapping(value = "/offline/cases")
	@ResponseBody
	public ResponseEntity pruneOfflineCases(@RequestParam(value = "secret_key", required = true) String secret_key,
			@RequestParam(value = "from", required = true) long from,
			@RequestParam(value = "to", required = true) long to) {
		if (secret_key.equals(appConfigService.getProperty("PRUNE_CASES_API_PASSWORD", "magic@W!mwisure"))) {
			LOGGER.info("/prune/old/cases request recieved with from ={} and to={}", from, to);
			Date fromDate = new Date(from);
			Date toDate = new Date(to);
			if (from != 0 && to != 0) {
				// caseDAO.getCaseByTypeAndStageAndCreatedTime(currentStage, inspectionType,
				// createdTime)
				pruneCaseService.persistInInspectionCasesHistory();
			}
			return new ResponseEntity<>(HttpStatus.OK);

		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

	@RequestMapping(value = "/online/cases")
	@ResponseBody
	public ResponseEntity pruneOnlineCases(@RequestParam(value = "secret_key", required = true) String secret_key,
			@RequestParam(value = "from", required = true) long from,
			@RequestParam(value = "to", required = true) long to) {
		if (secret_key.equals(appConfigService.getProperty("PRUNE_CASES_API_PASSWORD", "magic@W!mwisure"))) {
			LOGGER.info("/prune/old/cases request recieved with from ={} and to={}", from, to);
			Date fromDate = new Date(from);
			Date toDate = new Date(to);
			if (from != 0 && to != 0) {
				pruneCaseService.persistInInspectionCasesHistory();
			}
			return new ResponseEntity<>(HttpStatus.OK);

		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

}
