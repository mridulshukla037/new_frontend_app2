package com.jadu.reconcile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jadu.service.CaseFollowupService;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/reconcile-callback")
public class ReconcileCallbackController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReconcileCallbackController.class);

	@Autowired
	private CaseFollowupService caseFollowupService;

	@RequestMapping(value = "/status", method = RequestMethod.POST)
	@ResponseBody
	public void reloadAppConfiguration(@RequestParam(value = "CallSid", required = false) String callSid,
			@RequestParam(value = "Status", required = false) String status,
			@RequestParam(value = "RecordingUrl", required = false) String recordingUrl,
			@RequestParam(value = "DateUpdated", required = false) String dateUpdated) {
		LOGGER.info(
				"/callback/status request recieved with parameter callSid={},status={},recordingUrl={},dateUpdated={}",
				callSid, status, recordingUrl, dateUpdated);
		caseFollowupService.updateCallStatus(callSid, status, recordingUrl, dateUpdated);

	}

}
