package com.jadu.reconcile;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jadu.dao.AuthorityDAOImpl;
import com.jadu.dao.UserDAOImpl;
import com.jadu.model.Authority;
import com.jadu.service.AppConfigService;
import com.jadu.service.RedisReconcileService;
import com.jadu.service.RedisService;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/reconcile")
public class RedisReconcileController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RedisReconcileController.class);

	@Autowired
	private AuthorityDAOImpl authorityDao;

	@Autowired
	private UserDAOImpl userDAO;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private RedisReconcileService redisReconcileService;

	@Autowired
	private RedisService redisService;

	@RequestMapping(value = "/cases", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity reconcileAllCases(@RequestParam(value = "id", required = true) String userName,
			@RequestParam(value = "secret_key", required = true) String secret_key,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "caseStage", required = false) String caseStage,
			@RequestParam(value = "allUser", required = false) boolean allUser,
			@RequestParam(value = "role", required = false) String role) throws Exception {
		LOGGER.info("reconcile/cases request recieved for user={}, and caseStage={}, allUser={}, role={}", userName,
				caseStage, allUser, role);
		if (secret_key == null || secret_key.isEmpty() || password == null || password.isEmpty()) {
			LOGGER.info("Invalid secret key={} or password={}", secret_key, password);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		com.jadu.model.User u = userDAO.getUserByUsername(userName);
		if (u == null) {
			LOGGER.info("User={} not found", userName);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		if (isAuthorized(password, secret_key, u, "CASES")) {
			if (allUser) {
				String roleType = null;
				List<Authority> users = null;
				if (role != null && !role.isEmpty()) {
					roleType = role.equalsIgnoreCase("COMPANY") ? "ROLE_COMPANY_USER"
							: role.equalsIgnoreCase("ADMIN") ? "ROLE_ADMIN" : "ROLE_QC";
					if (roleType != null && !roleType.isEmpty()) {
						users = authorityDao.getUserByRoleType(roleType);
					}
				} else {
					users = authorityDao.getUserByRoleType("ROLE_COMPANY_USER");
					users.addAll(authorityDao.getUserByRoleType("ROLE_ADMIN"));
					users.addAll(authorityDao.getUserByRoleType("ROLE_QC"));
				}
				LOGGER.info("Total user found for reconcile/cases={} for role={} for caseStage={}",
						users != null ? users.size() : 0, role, caseStage);
				for (Authority user : users) {
					redisReconcileService.reconcileCasesForByStageAndByUser(user.getUser(), caseStage);
				}
			} else {
				redisReconcileService.reconcileCasesForByStageAndByUser(u, caseStage);
			}
			return new ResponseEntity<>(HttpStatus.OK);

		} else {
			LOGGER.info("Invalid user or secret key={} or password={}", secret_key, password);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

		}
	}

	@RequestMapping(value = "/all-agents", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Object> reconcileAllAgents(@RequestParam(value = "id", required = true) String userName,
			@RequestParam(value = "secret_key", required = true) String secret_key,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "all", required = true) boolean all) throws Exception {
		LOGGER.info("reconcile/all-agents request recieved for user={}, and all={}", userName, all);
		if (secret_key == null || secret_key.isEmpty() || password == null || password.isEmpty()) {
			LOGGER.info("Invalid secret key={} or password={}", secret_key, password);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		com.jadu.model.User user = userDAO.getUserByUsername(userName);
		if (user == null) {
			LOGGER.info("User={} not found", userName);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		if (isAuthorized(password, secret_key, user, "AGENTS")) {
			redisReconcileService.reconcileAllAgentsForUser(user, all);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			LOGGER.info("Invalid user or secret key={} or password={}", secret_key, password);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

		}
	}

	@RequestMapping(value = "/all-agents-details", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Object> reconcileAllAgentDetais(@RequestParam(value = "id", required = true) String userName,
			@RequestParam(value = "secret_key", required = true) String secret_key,
			@RequestParam(value = "password", required = true) String password) throws Exception {
		LOGGER.info("reconcile/all-agents-details request recieved for user={}, and all={}", userName);
		if (secret_key == null || secret_key.isEmpty() || password == null || password.isEmpty()) {
			LOGGER.info("Invalid secret key={} or password={}", secret_key, password);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		com.jadu.model.User user = userDAO.getUserByUsername(userName);
		if (user == null) {
			LOGGER.info("User={} not found", userName);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		if (isAuthorized(password, secret_key, user, "AGENTS")) {
			redisService.reconcileAllAgents("ALL_AGENTS_DETAILS");
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			LOGGER.info("Invalid user or secret key={} or password={}", secret_key, password);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

		}
	}

	/**
	 * s
	 * 
	 * @param password
	 * @param secret_key
	 * @param currentUser
	 * @param caseStage
	 * @return
	 * 
	 *         Verify user password and api secret key, if valid then check whether
	 *         allowed hit quota has been finished by user or not(This is for
	 *         security purpose, so that outsider could not call in loop it if they
	 *         got access key or password of the api)
	 */
	private boolean isAuthorized(String password, String secret_key, com.jadu.model.User currentUser, String api) {
		// perform basic check
		boolean isvalid = currentUser != null && currentUser.getPassword().equals(password) && appConfigService
				.getProperty("RECONCILE_ALL_" + api.toUpperCase() + "_SECRET_KEY", "jadu@wimwisure").equals(secret_key);
		if (isvalid) {
			if (checkAndCasheAPIcall(currentUser)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * 
	 * @param currentUser
	 * @return
	 * 
	 *         API call must be within given range due to security purpose
	 */
	private boolean checkAndCasheAPIcall(com.jadu.model.User currentUser) {
		String reconcileCount = redisService.get("RECONCILE_API_CALLED_TODAY_BY_USER_" + currentUser.getUsername());
		int count = reconcileCount == null ? 0 : Integer.valueOf(reconcileCount);
		int reconcileMaxCount = appConfigService.getIntProperty("RECONCILE_LIMIT_PER_USER", 10);
		if (count < reconcileMaxCount) {
			redisService.storeKeyValue("RECONCILE_API_CALLED_TODAY_BY_USER_" + currentUser.getUsername(),
					String.valueOf(count + 1),
					appConfigService.getIntProperty("RECONCILE_API_CALLED_EXPIRY_TIME_PER_USER", 24 * 60 * 60));
			return true;
		} else {
			LOGGER.info("API call given quota is over={} for user={}", count, currentUser.getUsername());
			return false;
		}
	}
}
