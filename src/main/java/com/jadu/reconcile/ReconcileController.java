package com.jadu.reconcile;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jadu.service.AppConfigService;
import com.jadu.service.ReconcileService;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/reconcile")
public class ReconcileController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReconcileController.class);

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private ReconcileService reconcileService;

	@RequestMapping(value = "/appConfig/refresh")
	@ResponseBody
	public ResponseEntity reloadAppConfiguration(
			@RequestParam(value = "secret_key", required = true) String secret_key) {
		if (secret_key.equals(appConfigService.getProperty("APP_CONFIG_RECONCILE_API_PASSWORD", "magic@W!mwisure"))) {
			appConfigService.refresh();
			LOGGER.info("AppConfiguration has been refreshed");
			return new ResponseEntity<>(HttpStatus.OK);

		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

	@RequestMapping(value = "/appConfig/update")
	@ResponseBody
	public ResponseEntity updateAppConfiguration(@RequestParam(value = "secret_key", required = true) String secret_key,
			@RequestParam(value = "tag", required = true) String tag,
			@RequestParam(value = "value", required = true) String value) {
		if (secret_key.equals(appConfigService.getProperty("APP_CONFIG_RECONCILE_API_PASSWORD", "magic@W!mwisure"))) {
			appConfigService.updateTag(tag, value);
			LOGGER.info("AppConfiguration has been refreshed");
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

	@RequestMapping(value = "/case/reminder", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity remindCaseInspection(@RequestParam(value = "secret_key", required = true) String secret_key,
			@RequestParam(value = "last_one_day", required = false) boolean lastOneDay,
			@RequestParam(value = "self_inspection", required = false) boolean self_inspection) {
		if (secret_key.equals(appConfigService.getProperty("APP_CONFIG_RECONCILE_API_PASSWORD", "magic@W!mwisure"))) {
			LOGGER.info("reconcile/case/reminder job started at", new Date());
			reconcileService.remindCaseInspection(lastOneDay, self_inspection);
			LOGGER.info("reconcile/case/reminder job finished at ", new Date());
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

	}

}
