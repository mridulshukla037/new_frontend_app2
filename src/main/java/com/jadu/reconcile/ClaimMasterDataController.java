package com.jadu.reconcile;

import com.jadu.dao.MakeModelVariantDAOImpl;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jadu.dto.ClaimMakeModelVariant;
import com.jadu.dto.ClaimMasterData;
import com.jadu.model.MakeModelVariant;
import com.jadu.service.AppConfigService;
import com.jadu.service.RedisService;

@RestController
@PropertySource("/WEB-INF/config.properties")
@RequestMapping("/master")
public class ClaimMasterDataController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClaimMasterDataController.class);

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private RedisService redisService;
        
        @Autowired
        private MakeModelVariantDAOImpl makeModelVariantDAOImpl;

	@RequestMapping(value = "/data/cost", method = RequestMethod.GET)
	@ResponseBody
	public List<ClaimMasterData> reconcileMasterData(
			@RequestParam(value = "update", required = false) boolean reconcile) throws Exception {
		LOGGER.info("master/data/cost request recieved ");
		if (reconcile) {
			List<ClaimMasterData> claimMasterData = getMasterData();
			redisService.cacheClaimMasterData(claimMasterData);
		} else {
			List<ClaimMasterData> claimMasterData = redisService.getClaimMasterData();
			if (claimMasterData == null || claimMasterData.isEmpty()) {
				claimMasterData = getMasterData();
				redisService.cacheClaimMasterData(claimMasterData);
			}
		}
		return redisService.getClaimMasterData();

	}

	@RequestMapping(value = "/make-model-variant", method = RequestMethod.GET)
	@ResponseBody
	public List<MakeModelVariant> reconcileMakeModelVariant(
			@RequestParam(value = "update", required = false) boolean update) throws Exception {
		
            return makeModelVariantDAOImpl.findAll();
	}

	private List<ClaimMakeModelVariant> getMakeModelVariantData() {

		List<ClaimMakeModelVariant> claimMakeModelVariantDataList = new ArrayList<ClaimMakeModelVariant>();
		String csvFile = appConfigService.getProperty("CLAIM_MAKE_MODEL_VARIANT_DATA_PATH",
				"C:\\Users\\asad.ali\\Desktop\\make-model-variant.csv");
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = "\\,";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				line = line.replaceAll("\\t", "").trim();
				String[] csvValues = line.split(cvsSplitBy);
				ClaimMakeModelVariant claimMakeModelVariant = new ClaimMakeModelVariant();
				claimMakeModelVariant.setId(Long.valueOf(csvValues[5]));
				claimMakeModelVariant.setType(csvValues[0]);
				claimMakeModelVariant.setMake(csvValues[1]);
				claimMakeModelVariant.setModel(csvValues[2]);
				claimMakeModelVariant.setVariant(csvValues[3]);
				claimMakeModelVariant.setSubType(csvValues[4]);
				boolean enabled = csvValues[6] != null ? csvValues[6] == "1" ? true : false : false;
				claimMakeModelVariant.setEnabled(enabled);
				claimMakeModelVariant.setCreatedBy(csvValues[7]);
				claimMakeModelVariantDataList.add(claimMakeModelVariant);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return claimMakeModelVariantDataList;

	}

	@RequestMapping(value = "/data", method = RequestMethod.GET)
	@ResponseBody
	public List<ClaimMasterData> fetchMasterData() throws Exception {
		LOGGER.info("master/data/ request recieved ");
		return redisService.getClaimMasterData();

	}

	public List<ClaimMasterData> getMasterData() {
		List<ClaimMasterData> claimMasterDataList = new ArrayList<ClaimMasterData>();
		String csvFile = appConfigService.getProperty("CLAIM_MASTER_DATA_PATH",
				"C:\\Users\\asad.ali\\Desktop\\finalMastarData.csv");
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = "\\,";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				line = line.replaceAll("\\t", "").trim();
				String[] csvValues = line.split(cvsSplitBy);
				ClaimMasterData claimMasterData = new ClaimMasterData();
				claimMasterData.setId(Long.valueOf(csvValues[0]));
				claimMasterData.setMakeModelVariantId(Integer.valueOf(csvValues[1]));
				claimMasterData.setPartTypeId(Integer.valueOf(csvValues[2]));
				claimMasterData.setPartType(csvValues[3]);
				claimMasterData.setPartSideId(Integer.valueOf(csvValues[4]));
				claimMasterData.setPartSide(csvValues[5]);
				claimMasterData.setPartParticularTypeId(Integer.valueOf(csvValues[6]));
				claimMasterData.setPartParticularType(csvValues[7]);
				claimMasterData.setDamageTypeId(Integer.valueOf(csvValues[8]));
				claimMasterData.setDamageType(csvValues[9]);
				claimMasterData.setRepairTypeId(Integer.valueOf(csvValues[10]));
				claimMasterData.setRepairType(csvValues[11]);
				claimMasterData.setPartCostId(Integer.valueOf(csvValues[12]));
				claimMasterData.setPartCost(Double.parseDouble(csvValues[13]));
				claimMasterData.setLabourCostId(Integer.valueOf(csvValues[14]));
				claimMasterData.setLabourCost(Double.parseDouble(csvValues[15]));
				claimMasterDataList.add(claimMasterData);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return claimMasterDataList;

	}
}
