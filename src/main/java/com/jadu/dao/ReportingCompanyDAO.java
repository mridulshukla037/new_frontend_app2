/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dao;

import com.jadu.model.ReportingCompany;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gkumar
 */
public class ReportingCompanyDAO {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
            this.sessionFactory = sessionFactory;
    }
        
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List findAll(String insuranceCompany) {
            String hql = "SELECT new com.jadu.dao.MappedAgentsDAO(r.agent.username, r.agent.firstName, r.agent.lastName, r.agent.phoneNumber) FROM ReportingCompany r WHERE r.insuranceCompany.name=?";
            Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
            query.setString(0, insuranceCompany);
            return query.list();
    } 
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List findAll() {
            String hql = "FROM ReportingCompany";
            Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
            return query.list();
    } 
}
