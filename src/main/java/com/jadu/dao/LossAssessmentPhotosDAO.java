package com.jadu.dao;

import com.jadu.model.LossAssessment;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.LossAssessmentPhotos;

/**
 * 
 * @author Asad.ali
 *
 */
public class LossAssessmentPhotosDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(LossAssessmentPhotos claimCasePhoto) {
		this.sessionFactory.getCurrentSession().save(claimCasePhoto);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(LossAssessmentPhotos claimCasePhoto) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(claimCasePhoto);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<LossAssessmentPhotos> getPhotosClaimNumber(String claim_number) {
		String hql = "SELECT la FROM LossAssessmentPhotos la  WHERE la.claimNumber=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, claim_number);
		List<LossAssessmentPhotos> result = query.list();

		if (result.isEmpty())
			return null;

		return result;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<LossAssessmentPhotos> getLossAssessmentPhotos(LossAssessment lossAssessment) {
		String hql = "SELECT cp FROM LossAssessmentPhotos cp  WHERE cp.LossAssessment.id=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setLong(0, lossAssessment.getId());
		List<LossAssessmentPhotos> result = query.list();
		return result;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<LossAssessmentPhotos> getLossAssessmentPhotosByPhotoType(long claimCase, String photoType) {
		String hql = "SELECT cp FROM LossAssessmentPhotos cp  WHERE cp.claimCase=? and cp.photoType=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setLong(0, claimCase);
		query.setString(1, photoType);
		List<LossAssessmentPhotos> result = query.list();
		return result;
	}

}
