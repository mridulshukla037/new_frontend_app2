package com.jadu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.ClientDetails;

public class ClientDetailsDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(ClientDetails assessmentSummary) {
		this.sessionFactory.getCurrentSession().save(assessmentSummary);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(ClientDetails assessmentSummary) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(assessmentSummary);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public ClientDetails findBygarageMobileNumber(String mobileNumber) {
		String hql = "SELECT a FROM ClientDetails a WHERE a.garageMobileNumber=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, mobileNumber);
		List<ClientDetails> result = query.list();
		if (result.isEmpty())
			return null;
		return result.get(0);
	}

}
