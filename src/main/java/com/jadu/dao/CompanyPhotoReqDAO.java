package com.jadu.dao;

import com.jadu.model.CompanyPhotoReq;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class CompanyPhotoReqDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getAll(){
        return this.sessionFactory
                .getCurrentSession()
                .createCriteria(CompanyPhotoReq.class)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List get(){
        String hql = "select new com.jadu.dto.CompanyPhotoReqDTO(id as id, vehicleType.id as vehicleType, company.id,  photoType.id) from CompanyPhotoReq";
        
        return this.sessionFactory.getCurrentSession().createQuery(hql).list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List get(
            String company
    ){
        String hql = "select new com.jadu.dto.CompanyPhotoReqDTO(id as id, vehicleType.id as vehicleType, company.id,  photoType.id) from CompanyPhotoReq WHERE company.id=:company";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setParameter("company", company)
                .list();
    }
}
