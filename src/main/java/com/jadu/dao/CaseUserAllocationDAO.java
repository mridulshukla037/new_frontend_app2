/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dao;

import com.jadu.model.CaseUserAllocation;
import com.jadu.model.User;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gautam
 */
public class CaseUserAllocationDAO {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public CaseUserAllocation get(long caseId, int stage){
        String hql = "FROM CaseUserAllocation ic WHERE ic.inspectionCase.id=?  AND ic.stage=?";
        
        return (CaseUserAllocation)this.sessionFactory.getCurrentSession().createQuery(hql)
                .setLong(0, caseId)
                .setInteger(1, stage)
                .uniqueResult();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void update(long caseId, int stage, User user) throws Exception{
        CaseUserAllocation cua = this.get(caseId, stage);
        
        if(cua == null)
            throw new Exception("Unable to update details");
        
        cua.setUser(user);
        
        this.sessionFactory.getCurrentSession().saveOrUpdate(cua);
    }
}
