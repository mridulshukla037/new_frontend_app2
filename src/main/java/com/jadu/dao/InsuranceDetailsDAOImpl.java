package com.jadu.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.InsuranceDetails;
import com.jadu.model.VehicleDetails;

/**
 * 
 * @author Asad.ali
 *
 */
public class InsuranceDetailsDAOImpl {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public InsuranceDetails save(InsuranceDetails insuranceDetails) {
		this.sessionFactory.getCurrentSession().save(insuranceDetails);
		return insuranceDetails;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(InsuranceDetails insuranceDetails) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(insuranceDetails);
	}
}
