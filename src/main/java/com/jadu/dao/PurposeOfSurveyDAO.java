package com.jadu.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.PurposeOfSurvey;

public class PurposeOfSurveyDAO {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public PurposeOfSurvey getPurposeOfSurveyById(String id) {

		return (PurposeOfSurvey) this.sessionFactory.getCurrentSession().get(PurposeOfSurvey.class, id);

	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<PurposeOfSurvey> getAll() {
		return this.sessionFactory.getCurrentSession().createCriteria(PurposeOfSurvey.class).list();
	}
}
