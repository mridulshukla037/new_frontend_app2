package com.jadu.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.ClaimsPhotoType;

public class ClaimsPhotoTypeDAO {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public ClaimsPhotoType get(String id) {
		return (ClaimsPhotoType) this.sessionFactory.getCurrentSession().createCriteria(ClaimsPhotoType.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAll() {
		return this.sessionFactory.getCurrentSession().createCriteria(ClaimsPhotoType.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List get(String companyId, String vehicleType, String vehicleSubType, String purposeOfInspection,
			String fuelType) {
		String hql = "from ClaimsPhotoType pt   where pt.id in (SELECT vp.photoType.id from VehiclePhotoReq vp WHERE vp.vehicleType.id=? AND vp.vehicleSubType.id=? AND vp.vehicleFuelType.id=? AND vp.purposeOfInspection.id=?) AND pt.id in (SELECT cp.photoType.id FROM CompanyPhotoReq cp WHERE cp.company.id=? AND cp.vehicleType.id=?) ";

		return this.sessionFactory.getCurrentSession().createQuery(hql).setParameter(0, vehicleType)
				.setParameter(1, vehicleSubType).setParameter(2, fuelType).setParameter(3, purposeOfInspection)
				.setParameter(4, companyId).setParameter(5, vehicleType).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getPhotoTypesToBeAddedInReport() {
		String hql = "SELECT id from ClaimsPhotoType pt   WHERE pt.showInReport = true ";

		return this.sessionFactory.getCurrentSession().createQuery(hql).list();
	}
}
