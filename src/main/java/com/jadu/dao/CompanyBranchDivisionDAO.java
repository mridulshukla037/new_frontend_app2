/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dao;

import com.jadu.model.AppConfiguration;
import com.jadu.model.CompanyBranchDivision;
import com.jadu.model.User;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gauta
 */
public class CompanyBranchDivisionDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void save(CompanyBranchDivision companyBranchDivision){
        this.sessionFactory.getCurrentSession().saveOrUpdate(companyBranchDivision);
    }
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public CompanyBranchDivision getCompanyBranchDivisionById(Integer  id){
        
        return  (CompanyBranchDivision) 
                this.sessionFactory
                        .getCurrentSession()
                        .get(CompanyBranchDivision.class, id);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getCompanyBranchDivisionByCompany(String  company){
        String hql = "from CompanyBranchDivision where company=:company ORDER BY state, division, branch ASC";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setString("company", company)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public CompanyBranchDivision getByApiKey(String apiKey){
        String hql = "from CompanyBranchDivision where apiKey=:apiKey";
        
        return (CompanyBranchDivision) this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setString("apiKey", apiKey)
                .uniqueResult();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<CompanyBranchDivision> findAll() {
		String hql = "FROM CompanyBranchDivision";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}
}
