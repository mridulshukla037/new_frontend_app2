package com.jadu.dao;

import com.jadu.dto.CaseCommentDTO;
import com.jadu.model.CaseComment;
import com.jadu.model.InspectionCase;
import com.jadu.model.User;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class CaseCommentDAO {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void add(InspectionCase ic, String comment, User user) {
		CaseComment cc = new CaseComment();
		cc.setComment(comment);
		cc.setInspectionCase(ic);
		cc.setCommentTime(new Date());
		cc.setUser(user);
		this.sessionFactory.getCurrentSession().saveOrUpdate(cc);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List get(long caseId) {
		String hql = "SELECT new com.jadu.dto.CaseCommentDTO(c.id, c.comment, c.commentTime, c.user.firstName, c.user.lastName) from CaseComment c join c.inspectionCase ic  where ic.id=:id";

		return this.sessionFactory.getCurrentSession().createQuery(hql).setLong("id", caseId).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List count(long caseId) {
		String hql = "SELECT FROM CaseComment c join c.inspectionCase ic  where ic.id=:id";

		return this.sessionFactory.getCurrentSession().createQuery(hql).setLong("id", caseId).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Collection<CaseCommentDTO> getCommentsByCommentTimeAndCaseId(long id, Date from, Date to) {
		String hql = "SELECT new com.jadu.dto.CaseCommentDTO(c.id, c.comment, c.commentTime, c.user.firstName, c.user.lastName, ic.id) from ClaimCaseComment c join c.claimCase ic  where ic.id=:id and (c.commentTime>=:from and c.commentTime<=:to)";
		return this.sessionFactory.getCurrentSession().createQuery(hql).setLong("id", id).setDate("from", from)
				.setDate("to", to).list();
	}

}
