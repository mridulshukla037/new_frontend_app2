package com.jadu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.AppConfiguration;
import com.jadu.model.MakeModelVariant;

/**
 * 
 * @author Asad.ali
 *
 */
public class MakeModelVariantDAOImpl {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<MakeModelVariant> getById(Long id) {
		String hql = "FROM MakeModelVariant m where m.id=:id";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql).setLong("id", id);
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<MakeModelVariant> findAll() {
		String hql = "FROM MakeModelVariant";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

}
