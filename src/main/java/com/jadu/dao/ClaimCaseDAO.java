package com.jadu.dao;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.Work;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.dto.FilterDTO;
import com.jadu.dto.ScheduledCasesDTO;
import com.jadu.helpers.UtilHelper;
import com.jadu.model.ClaimCase;
import com.jadu.model.ClaimCasePhoto;
import com.jadu.model.CompanyBranchDivisionUser;

public class ClaimCaseDAO {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(ClaimCase caseToSave) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(caseToSave);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getCasesByStage(int stage, String username) {
		String hql = "SELECT new com.jadu.dto.CaseDTO(ic.id, ic.vehicleNumber, ic.customerName, ic.customerPhoneNumber, ic.inspectionLatitude, ic.inspectionLongitude, ic.purposeOfInspection.id) FROM ClaimCase ic join ic.claimCaseUserAllocations cu WHERE ic.currentStage=?  AND (cu.stage=? AND cu.user.username=?) GROUP BY ic.id";

		return this.sessionFactory.getCurrentSession().createQuery(hql).setInteger(0, stage).setInteger(1, stage)
				.setString(2, username).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getCarByMake(String make) {
		String hql = "SELECT new com.jadu.dto.CaseMakeDTO(id, model) from ClaimCase ic where currentStage=5 and make=?";

		return this.sessionFactory.getCurrentSession().createQuery(hql).setString(0, make).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getCompletedCases(int stage, String username) {
		String hql = "SELECT new com.jadu.dto.CaseDTO(ic.id, ic.vehicleNumber, ic.customerName, ic.customerPhoneNumber, ic.inspectionLatitude, ic.inspectionLongitude, ic.purposeOfInspection.id) FROM ClaimCase ic join ic.claimCaseUserAllocations cu WHERE ic.currentStage=?  AND cu.user.username=? GROUP BY ic.id";

		return this.sessionFactory.getCurrentSession().createQuery(hql).setInteger(0, stage).setString(1, username)
				.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAllCasesByStage(int stage, String username) {
		String hql = "SELECT ic FROM ClaimCase ic join ic.claimCaseUserAllocations cu WHERE ic.currentStage=?  AND (cu.stage=? AND cu.user.username=?) GROUP BY ic.id";

		return this.sessionFactory.getCurrentSession().createQuery(hql).setInteger(0, stage).setInteger(1, stage)
				.setString(2, username).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAllCasesSubsetByStage(int stage, String phoneNumber) {
		String hql = "SELECT ic FROM ClaimCase ic WHERE ic.currentStage=?  AND ic.requestor.phoneNumber=? GROUP BY ic.id";

		return this.sessionFactory.getCurrentSession().createQuery(hql).setInteger(0, stage).setString(1, phoneNumber)
				.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAllCasesByStage(int stage) {
		String hql = "SELECT ic FROM ClaimCase ic WHERE ic.currentStage=? GROUP BY ic.id";

		return this.sessionFactory.getCurrentSession().createQuery(hql).setInteger(0, stage).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAll() {
		String hql = "SELECT ic FROM ClaimCase ic GROUP BY ic.id";

		return this.sessionFactory.getCurrentSession().createQuery(hql).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Object getcaseByStage(int stage, String username, long caseId) {
		String hql = "SELECT ic FROM ClaimCase ic join ic.claimCaseUserAllocations cu WHERE ic.currentStage=?  AND (cu.stage=? AND cu.user.username=? AND ic.id=?)";

		List result = this.sessionFactory.getCurrentSession().createQuery(hql).setInteger(0, stage).setInteger(1, stage)
				.setString(2, username).setLong(3, caseId).list();

		if (result.isEmpty())
			return null;
		return result.get(0);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAllCasesByUsername(String username) {
		String hql = "SELECT new com.jadu.dto.CaseHistoryDTO(" + "ic.id," + "ic.vehicleNumber," + "ic.customerName,"
				+ "ic.remark," + "ic.inspectionTime," + "inc.name," + "poc.name," + "vc.model," + "ic.currentStage,"
				+ "ic.requestorName,"
				+ "ic.customerPhoneNumber) FROM ClaimCase ic join ic.insuranceCompany inc join ic.purposeOfInspection poc join ic.vehicle vc  join ic.claimCaseUserAllocations cu WHERE  cu.user.username=? GROUP BY ic.id";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, username);
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAllCasesByUsername2(String username) {
		String hql = "SELECT new com.jadu.dto.CaseHistoryDTO2(" + "ic.id," + "ic.vehicleNumber," + "ic.customerName,"
				+ "ic.remark," + "ic.inspectionTime," + "inc.id," + "poc.id," + "vc.id," + "ic.currentStage,"
				+ "ic.requestorName,"
				+ "ic.customerPhoneNumber) FROM ClaimCase ic join ic.insuranceCompany inc join ic.purposeOfInspection poc join ic.vehicle vc  join ic.claimCaseUserAllocations cu WHERE  cu.user.username=? GROUP BY ic.id";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, username);
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getScheduledCasesByUsername(String username) {
		String hql = "SELECT new com.jadu.dto.CaseScheduledDTO(" + "ic.id," + "ic.vehicleNumber," + "ic.customerName,"
				+ "ic.remark," + "ic.inspectionTime," + "inc.id," + "poc.id," + "vc.id," + "ic.currentStage,"
				+ "ic.requestorName," + "ic.customerPhoneNumber," + "ic.vehicleFuelType.id," + "ic.vehicleColor,"
				+ "ic.vehicleYOM) FROM ClaimCase ic join ic.insuranceCompany inc join ic.purposeOfSurvey poc join ic.vehicle vc  join ic.claimCaseUserAllocations cu WHERE  cu.user.username=? AND ic.currentStage=3 GROUP BY ic.id";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, username);
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public ClaimCase getCaseById(long id) throws Exception {
		String hql = "FROM ClaimCase ic  WHERE ic.id=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setLong(0, id);
		return (ClaimCase)query.uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public ClaimCase getCaseByClaimnumber(String claimNumber) throws Exception {
		String hql = "SELECT ic FROM ClaimCase ic  WHERE ic.claimNumber=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, claimNumber);
		List result = query.list();

		if (result.isEmpty())
			return null;

		return (ClaimCase) result.get(0);
	}
        
        @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public ClaimCase getCaseByIdOrNumber(long id, String claimNumber) throws Exception {
            String hql = "SELECT ic FROM ClaimCase ic  WHERE ic.id=? OR ic.claimNumber=?";
            Session session = this.sessionFactory.getCurrentSession();
            Query query = session.createQuery(hql);
            query.setLong(0, id);
            query.setString(1, claimNumber);
            
            ClaimCase result = (ClaimCase) query.uniqueResult();
            
            System.out.println(result);
            
            assert result != null;

            return result;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public ClaimCase getInspectionCase(long id) {
            String hql = "SELECT ic FROM ClaimCase ic WHERE ic.id=? GROUP BY ic.id";
            return (ClaimCase) this.sessionFactory.getCurrentSession().createQuery(hql).setLong(0, id).uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public ClaimCase getClaimCaseByZipid(String downloadKey) {
		String hql = "SELECT ic FROM ClaimCase ic join ic.claimCaseUserAllocations ca  WHERE ic.downloadKey=? GROUP BY ic.id";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setParameter(0, downloadKey);
		return (ClaimCase) query.uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveCasePhotos(ClaimCase caseToSave, List<ClaimCasePhoto> casePhotos, String phoneNumber) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(caseToSave);
		for (ClaimCasePhoto cp : casePhotos) {
			session.save(cp);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getQcCases() {
		String hql = "SELECT ic FROM ClaimCase ic WHERE ic.currentStage > 3";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getOpenCasesByUsername(String username, String email) {
		/*
		 * String hql =
		 * "SELECT ic FROM ClaimCase ic join ic.claimCaseUserAllocations cu WHERE  (cu.user.username=:username OR ic.requestorEmail=:username) AND ic.currentStage <= 3 GROUP BY ic.id"
		 * ; Session session = this.sessionFactory.getCurrentSession(); Query query =
		 * session.createQuery(hql); query.setString("username", username); return query
		 * .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY) .list();
		 */

		String hql = "SELECT new com.jadu.dto.CaseHistoryDTO(" + "ic.id," + "ic.vehicleNumber," + "ic.customerName,"
				+ "ic.remark," + "ic.inspectionTime," + "inc.name," + "poc.name," + "vc.model," + "ic.currentStage,"
				+ "ic.requestorName,"
				+ "ic.customerPhoneNumber) FROM ClaimCase ic join ic.insuranceCompany inc join ic.purposeOfInspection poc left outer join ic.vehicle vc  join ic.claimCaseUserAllocations cu WHERE  (cu.user.username=:username OR ic.requestorEmail=:email) AND ic.currentStage <= 3 GROUP BY ic.id";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setParameter("username", username);
		query.setParameter("email", email);
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateCaseInspectionTime(long caseId, Date inspectionTime) {
		String hqlUpdate = "update ClaimCase ic set ic.inspectionTime = :inspectionTime where ic.id = :id";

		sessionFactory.getCurrentSession().createQuery(hqlUpdate).setLong("id", caseId)
				.setTimestamp("inspectionTime", inspectionTime).executeUpdate();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Object getCase(long id, String username) throws Exception {
		String hql = "SELECT ic FROM ClaimCase ic WHERE ic.id=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setLong(0, id);
		return (ClaimCase) query.uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getCasesToBeAllocated() {

		Calendar cal = UtilHelper.getCalenderInstance();
		cal.add(Calendar.HOUR_OF_DAY, -2);

		String hql = "" + " SELECT ic FROM ClaimCase ic" + " WHERE ic.currentStage=2" + " "
				+ " AND ic.allocationAttempts < 5" + " AND current_timestamp() > COALESCE(ic.attemptExpiryTime, 0)"
				+ " AND ic.inspectionTime > :inspectionCutOffTime";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql).setParameter("inspectionCutOffTime",
				cal.getTime());
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getRequestedCases(int stage, String username) {

		String hql = "FROM ClaimCase ic join ic.claimCaseAttemptAllocation ca  WHERE ic.currentStage=:currentStage  AND ca.inspector.username=:username AND ca.attemptExpiryTime >=  :currentTime  GROUP BY ic.id";

		return this.sessionFactory.getCurrentSession().createQuery(hql).setInteger("currentStage", stage)
				.setString("username", username).setParameter("currentTime", Calendar.getInstance().getTime()).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateCaseAddress(long caseId, String inspectionAddress) {
		String hqlUpdate = "update ClaimCase ic set ic.inspectionAddress = :inspectionAddress where ic.id = :id";

		sessionFactory.getCurrentSession().createQuery(hqlUpdate).setLong("id", caseId)
				.setString("inspectionAddress", inspectionAddress).executeUpdate();
	}

	private FilterDTO getQuery(String prefix, Set<CompanyBranchDivisionUser> companyBranchDivisionUser) {

		if (companyBranchDivisionUser == null || companyBranchDivisionUser.isEmpty())
			return null;

		List<String> result = new ArrayList<>();
		HashMap<String, Object> map = new HashMap();

		int i = 1;
		for (CompanyBranchDivisionUser item : companyBranchDivisionUser) {
			List<String> conditions = new ArrayList<>();
			if (item.getCompany() != null) {
				String[] companies = item.getCompany().split("\\,");
				if (companies.length == 1) {
					conditions.add(prefix + ".company = :company" + i);
					map.put("company" + i, item.getCompany());
				} else {
					String companyCSV = null;
					for (String company : companies) {
						if (companyCSV == null)
							companyCSV = prefix + ".company = '" + company.trim() + "'".trim();
						else
							companyCSV += " OR " + prefix + ".company = '" + company.trim() + "'".trim();
					}
					conditions.add("( " + companyCSV + " )");

				}
			}

			if (item.getState() != null) {
				conditions.add(prefix + ".state = :state" + i);
				map.put("state" + i, item.getState());
			}

			if (item.getDivision() != null) {
				conditions.add(prefix + ".division = :division" + i);
				map.put("division" + i, item.getDivision());
			}

			if (item.getBranch() != null) {
				String[] branches = item.getBranch().split("\\,");
				if (branches.length == 1) {
					conditions.add(prefix + ".branch = :branch" + i);
					map.put("branch" + i, item.getBranch());
				} else {
					String branchCSV = null;
					for (String branch : branches) {
						if (branchCSV == null)
							branchCSV = prefix + ".branch = '" + branch.trim() + "'".trim();
						else
							branchCSV += " OR " + prefix + ".branch = '" + branch.trim() + "'".trim();
					}
					conditions.add("( " + branchCSV + " )");

				}
			}

			i++;

			if (!conditions.isEmpty())
				result.add(" ( " + String.join(" AND ", conditions) + " ) ");
		}

		if (result.isEmpty())
			return null;

		FilterDTO filterDTO = new FilterDTO();
		filterDTO.setMap(map);
		filterDTO.setWhereClause(" ( " + String.join(" OR ", result) + " ) ");
		System.out.println(filterDTO.getWhereClause());
		return filterDTO;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<ScheduledCasesDTO> getScheduledCasesDTO(Set<CompanyBranchDivisionUser> companyBranchDivisionUser) {

		String cbdQuery = "SELECT id from CompanyBranchDivision c WHERE 1=1 ";
		FilterDTO filterDTO = this.getQuery("c", companyBranchDivisionUser);

		if (filterDTO != null) {
			cbdQuery += " AND " + filterDTO.getWhereClause();
		}

		String hql = "SELECT new com.jadu.dto.ScheduledCasesDTO(" + "ic.id," + "ic.vehicleNumber," + "ic.customerName,"
				+ "ic.customerPhoneNumber," + "ic.inspectionTime," + "ic.closeTime," + "ic.currentStage,"
				+ "ic.inspectionType," + "ic.companyBranchDivision.company," + "(SELECT logoUrl FROM InsuranceCompany where id=ic.companyBranchDivision.company)," + "ic.requestor.phoneNumber,"
				+ " (SELECT count(*) FROM ClaimCaseComment cc  WHERE cc.claimCase.id = ic.id ) as countComments ) "
				+ "FROM ClaimCase ic  WHERE ic.companyBranchDivision.id IN (" + cbdQuery
				+ ") AND ic.currentStage=3 GROUP BY ic.id ORDER BY ic.id DESC";

		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);

		if (filterDTO != null) {
			for (Map.Entry<String, Object> entry : filterDTO.getMap().entrySet()) {
				String[] csv = String.valueOf(entry.getValue()).split(",");
				if (Arrays.asList(csv).size() > 1)
					query.setParameterList(entry.getKey(), csv);
				else
					query.setParameter(entry.getKey(), entry.getValue());

			}
		}

		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getQCCasesDTO(Set<CompanyBranchDivisionUser> companyBranchDivisionUser) {

		String cbdQuery = "SELECT id from CompanyBranchDivision c WHERE 1=1 ";
		FilterDTO filterDTO = this.getQuery("c", companyBranchDivisionUser);

		if (filterDTO != null) {
			cbdQuery += " AND " + filterDTO.getWhereClause();
		}

		String hql = "FROM ClaimCase ic WHERE ic.currentStage=4 GROUP BY ic.id";

		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);

		if (filterDTO != null) {
			for (Map.Entry<String, Object> entry : filterDTO.getMap().entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}
		}

		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAllDTO(Set<CompanyBranchDivisionUser> companyBranchDivisionUser) {

		String cbdQuery = "SELECT id from CompanyBranchDivision c WHERE 1=1 ";
		FilterDTO filterDTO = this.getQuery("c", companyBranchDivisionUser);

		if (filterDTO != null) {
			cbdQuery += " AND " + filterDTO.getWhereClause();
		}

		String hql = "SELECT new com.jadu.dto.AllCases(" + "ic.id," + "ic.vehicleNumber," + "ic.customerName,"
				+ "ic.customerPhoneNumber," + "ic.inspectionTime," + "ic.closeTime," + "ic.currentStage,"
				+ "ic.inspectionType," + "c.name," + "c.logoUrl," + "ic.requestorPhoneNumber," + "ic.remark,"
				+ "ic.downloadKey, "
				+ "(SELECT count(*) FROM ClaimCaseComment cc  WHERE cc.claimCase.id = ic.id ) as countComments ) "
				+ "FROM ClaimCase ic  JOIN ic.insuranceCompany c WHERE ic.companyBranchDivision.id IN (" + cbdQuery
				+ ") GROUP BY ic.id ORDER BY ic.id DESC";

		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);

		if (filterDTO != null) {
			for (Map.Entry<String, Object> entry : filterDTO.getMap().entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}
		}

		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAllCasesByStageDTO(int stage, Set<CompanyBranchDivisionUser> companyBranchDivisionUser) {

		String cbdQuery = "SELECT id from CompanyBranchDivision c WHERE 1=1 ";
		FilterDTO filterDTO = this.getQuery("c", companyBranchDivisionUser);

		if (filterDTO != null) {
			cbdQuery += " AND " + filterDTO.getWhereClause();
		}

		String hql = "SELECT new com.jadu.dto.AllCases(" + "ic.id," + "ic.vehicleNumber," + "ic.customerName,"
				+ "ic.customerPhoneNumber," + "ic.inspectionTime," + "ic.closeTime," + "ic.currentStage,"
				+ "ic.inspectionType," + "c.name," + "c.logoUrl," + "ic.requestorPhoneNumber," + "ic.remark,"
				+ "ic.downloadKey  ,"
				+ "(SELECT count(*) FROM ClaimCaseComment cc  WHERE cc.claimCase.id = ic.id ) as countComments) "
				+ "FROM ClaimCase ic  JOIN ic.insuranceCompany c WHERE ic.companyBranchDivision.id IN (" + cbdQuery
				+ ") AND ic.currentStage=:currentStage GROUP BY ic.id ORDER BY ic.id DESC";

		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);

		query.setParameter("currentStage", stage);

		if (filterDTO != null) {
			for (Map.Entry<String, Object> entry : filterDTO.getMap().entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}
		}

		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Object getCasesSummary(

			Date startDuration, Date endDuration, Set<CompanyBranchDivisionUser> companyBranchDivisionUser) {

		String cbdQuery = "SELECT id from CompanyBranchDivision c WHERE 1=1 ";

		FilterDTO filterDTO = this.getQuery("c", companyBranchDivisionUser);

		if (filterDTO != null) {
			cbdQuery += " AND " + filterDTO.getWhereClause();
		}

		String hql = "SELECT new com.jadu.dto.CasesSummary("
				+ " COALESCE(SUM(CASE WHEN ic.creationTime >= :startTime                                THEN 1  ELSE 0 END), 0) as created, "
				+ " COALESCE(SUM(CASE WHEN ic.qcTime >= :startTime          AND ic.currentStage = 5     THEN 1  ELSE 0 END), 0) as completed, "
				+ " COALESCE(SUM(CASE WHEN ic.currentStage IN (0,1,2,3,4) and ic.inspectionTime < :endTime   THEN 1  ELSE 0 END), 0) as pending, "
				+ " COALESCE(SUM(CASE WHEN ic.vehicleType.id = '2-wheeler'  AND ic.currentStage = 5 AND ic.qcTime >= :startTime     THEN 1  ELSE 0 END), 0) as two_wheeler, "
				+ " COALESCE(SUM(CASE WHEN ic.vehicleType.id = '4-wheeler'  AND ic.currentStage = 5 AND ic.qcTime >= :startTime     THEN 1  ELSE 0 END), 0) as four_wheeler, "
				+ " COALESCE(SUM(CASE WHEN ic.vehicleType.id = 'commercial' AND ic.currentStage = 5 AND ic.qcTime >= :startTime     THEN 1  ELSE 0 END), 0) as commercial, "
				+ " COALESCE(SUM(CASE WHEN ic.remark = 'recommended'        AND ic.currentStage = 5 AND ic.qcTime >= :startTime     THEN 1  ELSE 0 END), 0) as recommended, "
				+ " COALESCE(SUM(CASE WHEN ic.remark = 'not-recommended'    AND ic.currentStage = 5 AND ic.qcTime >= :startTime     THEN 1  ELSE 0 END), 0) as not_recommended, "
				+ " COALESCE(SUM(CASE WHEN ic.remark = 'underwriter'        AND ic.currentStage = 5 AND ic.qcTime >= :startTime     THEN 1  ELSE 0 END), 0) as underwriter "
				+ " ) FROM ClaimCase ic WHERE ic.companyBranchDivision.id IN (" + cbdQuery + ") ";

		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);

		if (filterDTO != null) {
			for (Map.Entry<String, Object> entry : filterDTO.getMap().entrySet()) {
				String[] csv = String.valueOf(entry.getValue()).split("\\,");
				if (csv.length > 1)
					query.setParameterList(entry.getKey(), Arrays.asList(csv));
				else
					query.setParameter(entry.getKey(), entry.getValue());

			}
		}

		return query.setParameter("startTime", startDuration).setParameter("endTime", endDuration).uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Map<String, Object> getCasesData(List<String> fields, int from, int to, Date startDuration, String company,
			String state, String division, String branch, Set<CompanyBranchDivisionUser> companyBranchDivisionUser) {

		String cbdQuery = "SELECT id from company_branch_division cbd WHERE 1=1 ";

		FilterDTO filterDTO = this.getQuery("cbd", companyBranchDivisionUser);

		if (filterDTO != null) {
			cbdQuery += " AND " + filterDTO.getWhereClause();
		}

		Map<String, Object> result = new HashMap<>();

		List<String> cols = new ArrayList<>();

		for (String field : fields)
			cols.add("ic." + field);

		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery("SELECT " + String.join(", ", cols)
				+ " FROM cases ic JOIN company_branch_division cbd ON ic.company_branch_division_id = cbd.id WHERE cbd.id IN ("
				+ cbdQuery + ") AND creation_time >= :startDuration ");

		query.setParameter("startDuration", startDuration);
		query.setFirstResult(from);
		query.setMaxResults(to - from);
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);

		SQLQuery query2 = this.sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT count(*) FROM cases ic JOIN company_branch_division cbd ON ic.company_branch_division_id = cbd.id WHERE cbd.id IN  ("
						+ cbdQuery + ") AND creation_time >= :startDuration ");
		query2.setParameter("startDuration", startDuration);

		if (filterDTO != null) {
			for (Map.Entry<String, Object> entry : filterDTO.getMap().entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
				query2.setParameter(entry.getKey(), entry.getValue());
			}
		}

		result.put("data", query.list());
		result.put("count", (BigInteger) query2.uniqueResult());
		result.put("to", to);
		result.put("from", from);

		return result;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void exportCasesData(final List<String> fields, final Date startDuration) {

		/*
		 * SQLQuery query = this.sessionFactory .getCurrentSession()
		 * .createSQLQuery("SELECT " + String.join(", ", fields) +
		 * " INTO OUTFILE 'export.csv'\n" +
		 * "  FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'\n" +
		 * "  LINES TERMINATED BY '\\n' FROM cases WHERE inspection_time >= :startDuration"
		 * );
		 * 
		 * query.setParameter("startDuration", startDuration);
		 * query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		 */

		this.sessionFactory.getCurrentSession().doWork(new Work() {

			@Override
			public void execute(Connection connection) throws SQLException {
				PreparedStatement pStmt = null;
				try {
					pStmt = connection
							.prepareStatement("SELECT " + String.join(", ", fields) + " INTO OUTFILE 'export.csv'\n"
									+ "  FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'\n"
									+ "  LINES TERMINATED BY '\\n' FROM cases WHERE inspection_time >= ? ");
					pStmt.setTimestamp(1, new Timestamp(((Date) startDuration).getTime()));
					pStmt.execute();
				} finally {
					if (pStmt != null) {
						pStmt.close();
					}
				}

			}
		});
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<ClaimCase> getCaseByVehicleNumber(String id) {
		String hql = "SELECT ic FROM ClaimCase ic  WHERE ic.vehicleNumber=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, id);
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<ClaimCase> getCaseByCustomerPhoneNumber(String customerPhoneNumber) {
		String hql = "SELECT ic FROM ClaimCase ic  WHERE ic.customerPhoneNumber=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, customerPhoneNumber);
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<ClaimCase> getCaseByAgentPhoneNumber(String requestorPhoneNumber) {
		String hql = "SELECT ic FROM ClaimCase ic  WHERE ic.requestorPhoneNumber=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, requestorPhoneNumber);
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<ClaimCase> getCaseLikeVehicleNumber(String vehicle_id) {
		String hql = "SELECT ic FROM ClaimCase ic  WHERE ic.vehicleNumber LIKE ?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, "%" + vehicle_id + "%");
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<ClaimCase> getCaseLikeCustomerPhoneNumber(String customer_phone) {
		String hql = "SELECT ic FROM ClaimCase ic  WHERE ic.customerPhoneNumber LIKE ?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, "%" + customer_phone + "%");
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<ClaimCase> getCaseLikeAgentPhoneNumber(String requester_phone) {
		String hql = "SELECT ic FROM ClaimCase ic  WHERE ic.requestorPhoneNumber LIKE ?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, "%" + requester_phone + "%");
		return query.list();
	}

	public List<ClaimCase> getCaseByTypeAndStage(int currentStage, String inspectionType) {
		String hql = "SELECT ic FROM ClaimCase ic  WHERE ic.currentStage=? AND ic.inspectionType=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setInteger(0, currentStage);
		query.setString(1, inspectionType);
		return query.list();

	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<ClaimCase> getCaseByAgentPhoneNumberAndCreatedTime(String requestorPhoneNumber, Date from, Date to) {
		String hql = "SELECT ic FROM ClaimCase ic  WHERE ic.requestorPhoneNumber=? and ( ic.creationTime >=? and ic.creationTime <=? )";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, requestorPhoneNumber);
		query.setDate(1, from);
		query.setDate(2, to);
		return query.list();
	}
}
