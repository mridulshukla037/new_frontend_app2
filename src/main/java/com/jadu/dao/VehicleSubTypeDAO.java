package com.jadu.dao;

import com.jadu.model.VehicleSubType;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class VehicleSubTypeDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public VehicleSubType get(String id){
        return (VehicleSubType) this.sessionFactory
                .getCurrentSession()
                .createCriteria(VehicleSubType.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }
}
