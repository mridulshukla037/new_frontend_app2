package com.jadu.dao;

import com.jadu.model.User;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author gauta
 */
public class UserDAOImpl {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(User user) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(user);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public User getUserByUsername(String username) throws UsernameNotFoundException {
		return (User) this.sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.eq("username", username)).uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<User> getUserByUserType(String userType) throws UsernameNotFoundException {
		return (List<User>) this.sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.eq("user_type", userType)).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public User getUserByEmail(String email) throws UsernameNotFoundException {
		return (User) this.sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.eq("email", email)).uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getUserByPhoneNumber(String phoneNumber) {
		return this.sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.eq("phoneNumber", phoneNumber)).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updatePhoneValidation(String username, boolean status) {
		String hqlUpdate = "update User u set u.phoneNumberVerified = :status where u.username = :username";

		sessionFactory.getCurrentSession().createQuery(hqlUpdate).setBoolean("status", status)
				.setString("username", username).executeUpdate();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateEmailValidation(String username, boolean status) {

		String hqlUpdate = "update User u set u.emailVerified = :status where u.username = :username";

		sessionFactory.getCurrentSession().createQuery(hqlUpdate).setBoolean("status", status)
				.setString("username", username).executeUpdate();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updatePassword(String username, String password) {

		String hqlUpdate = "update User u set u.password = :password, u.passwordChangeRequired=false where u.username = :username";

		sessionFactory.getCurrentSession().createQuery(hqlUpdate).setString("password", password)
				.setString("username", username).executeUpdate();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Long getCountOfEmail(String email) {
		String hqlQuery = "SELECT count(*) FROM User u where  u.email = :email";

		return (Long) sessionFactory.getCurrentSession().createQuery(hqlQuery).setString("email", email).uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Long getCountOfPhoneNumber(String phoneNumber) {
		String hqlQuery = "SELECT count(*) FROM User u where  u.phoneNumber = :phoneNumber";

		return (Long) sessionFactory.getCurrentSession().createQuery(hqlQuery).setString("phoneNumber", phoneNumber)
				.uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public User getUserByPhoneOrEmail(String username) throws UsernameNotFoundException {
		String hqlQuery = "FROM User u where  u.email = :username OR u.phoneNumber= :username";

		return (User) sessionFactory.getCurrentSession().createQuery(hqlQuery).setString("username", username)
				.uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deleteUser(User user) {
		this.sessionFactory.getCurrentSession().delete(user);
	}

}
