package com.jadu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.ReplacementProduct;

/**
 * 
 * @author Asad.ali
 *
 */
public class ReplacementProductDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<ReplacementProduct> findAll() {
		String hql = "FROM ReplacementProduct";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

}
