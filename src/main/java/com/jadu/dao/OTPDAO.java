package com.jadu.dao;

import com.jadu.helpers.UtilHelper;
import com.jadu.model.OTP;
import java.util.Date;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


public class OTPDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void save(OTP otp){
        this.sessionFactory.getCurrentSession().saveOrUpdate(otp);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public OTP generateOtp(String username, String otpType, Date expiryTime, String otpGroup){
        OTP otp = new OTP();
        otp.setCreationTime(UtilHelper.getDate());
        otp.setOtpType(otpType);
        otp.setUsername(username);
        otp.setOtp(UtilHelper.getNumericString(4));
        otp.setExpiryTime(expiryTime);
        otp.setOtpGroup(otpGroup);
        
        this.sessionFactory.getCurrentSession().persist(otp);
        
        return otp;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public OTP generatePhoneOtp(String username, Date expiryTime, String otpGroup){
        return this.generateOtp(username, "PHONE", expiryTime, otpGroup);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public OTP generateEmailOtp(String username, Date expiryTime, String otpGroup){
        return this.generateOtp(username, "EMAIL", expiryTime, otpGroup);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Object getPhoneOtp(String username, String otp){
        
        return this.sessionFactory
                .getCurrentSession()
                .createCriteria(OTP.class)
                .add(Restrictions.eq("username", username))
                .add(Restrictions.eq("otpType", "PHONE"))
                .add(Restrictions.eq("otp", otp))
                .uniqueResult();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getUnexpiredOtp(String username, Date expiryTime){
        String hql = "FROM OTP WHERE username=:username AND expiryTime > :expiryTime ORDER BY expiryTime DESC LIMIT 1";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setParameter("username", username)
                .setParameter("expiryTime", expiryTime)
                .list();
    }
    
    /*@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getUnexpiredPhoneOtp(String username, Date expiryTime){
        String hql = "FROM OTP WHERE username=:username AND otpType=:otpType AND expiryTime > :expiryTime ORDER BY expiryTime DESC LIMIT 1";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setParameter("username", username)
                .setParameter("otpType", "PHONE")
                .setParameter("expiryTime", expiryTime)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getUnexpiredEmailOtp(String username, Date expiryTime){
        String hql = "FROM OTP WHERE username=:username AND otpType=:otpType AND expiryTime > :expiryTime ORDER BY expiryTime DESC LIMIT 1";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setParameter("username", username)
                .setParameter("otpType", "EMAIL")
                .setParameter("expiryTime", expiryTime)
                .list();
    }*/
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Object getEmailOtp(String username, String otp){
        return this.sessionFactory
                .getCurrentSession()
                .createCriteria(OTP.class)
                .add(Restrictions.eq("username", username))
                .add(Restrictions.eq("otpType", "EMAIL"))
                .add(Restrictions.eq("otp", otp))
                .uniqueResult();
    }
}
