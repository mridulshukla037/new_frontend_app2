package com.jadu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.ReportedToPolice;
import com.jadu.model.VehiclePartType;

public class ReportedToPoliceDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<ReportedToPolice> findAll() {
		String hql = "FROM ReportedToPolice";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}
}
