package com.jadu.dao;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class CompanyBranchDivisionMailDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getCompanyBranchDivisionByCompanyEmails(int branchId){
        String hql = "from CompanyBranchDivisionMail where companyBranchDivisionId=:branchId";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setParameter("branchId", branchId)
                .list();
    }
}
