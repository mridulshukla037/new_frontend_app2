package com.jadu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.SurveyFeeBill;

public class SurveyFeeBillDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(SurveyFeeBill surveyFeeBill) {
		this.sessionFactory.getCurrentSession().save(surveyFeeBill);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(SurveyFeeBill surveyFeeBill) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(surveyFeeBill);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public SurveyFeeBill findByClaimNumber(String claimNumber) {
		String hql = "SELECT a FROM SurveyFeeBill a WHERE a.claimNumber=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, claimNumber);
		List<SurveyFeeBill> result = query.list();
		if (result.isEmpty())
			return null;
		return result.get(0);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public SurveyFeeBill findByCaseId(Long caseId) {
		String hql = "SELECT a FROM SurveyFeeBill a WHERE a.claimCaseId=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setLong(0, caseId);
		List<SurveyFeeBill> result = query.list();
		if (result.isEmpty())
			return null;
		return result.get(0);
	}

}
