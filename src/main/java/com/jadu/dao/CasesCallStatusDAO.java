package com.jadu.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.CasesCallStatus;

public class CasesCallStatusDAO {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(CasesCallStatus caseStatus) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(caseStatus);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public CasesCallStatus getCaseByCallSid(String callSid) {
		String hql = "SELECT ic FROM CasesCallStatus ic WHERE ic.callSid=?";
		List<CasesCallStatus> callStatusList = this.sessionFactory.getCurrentSession().createQuery(hql)
				.setString(0, callSid).list();
		return callStatusList.size() != 0 ? callStatusList.get(0) : null;
	}

}
