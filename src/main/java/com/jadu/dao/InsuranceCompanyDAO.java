package com.jadu.dao;

import com.jadu.model.InsuranceCompany;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class InsuranceCompanyDAO {
    
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getInsuranceCompanies() {
        return this.sessionFactory
                .getCurrentSession()
                .createCriteria(InsuranceCompany.class)
                .add(Restrictions.eq("enable",true))
                .addOrder(Order.asc("name"))
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public InsuranceCompany getInsuranceCompanyById(String id){
        return (InsuranceCompany) 
                this.sessionFactory
                        .getCurrentSession()
                        .get(InsuranceCompany.class, id);      
    }
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public InsuranceCompany getInsuranceCompanyByName(String name){
        return (InsuranceCompany) 
                this.sessionFactory
                        .getCurrentSession()
                        .get(InsuranceCompany.class, name);      
    }
}
