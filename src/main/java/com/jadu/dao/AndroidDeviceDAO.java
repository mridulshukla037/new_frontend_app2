package com.jadu.dao;

import com.jadu.model.AndroidDevice;
import com.jadu.model.Device;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class AndroidDeviceDAO extends DeviceDAO{
    
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    @Override
    public Device get(
            String deviceId,
            String username
    ){
        return (Device)this.sessionFactory
                .getCurrentSession()
                .createCriteria(AndroidDevice.class)
                .add(Restrictions.eq("deviceId", deviceId))
                .add(Restrictions.eq("username", username))
                .uniqueResult();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void delete(String username){
        Query q = this.sessionFactory.getCurrentSession().createQuery("delete AndroidDevice where username = :username");
        q.setString("username", username);
        q.executeUpdate();
    }
}
