package com.jadu.dao;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class ConfigurationDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getConfigurationsByTag(String tag){
        String hql = "FROM Configuration c WHERE c.tag=:tag";
        
        return this.sessionFactory.getCurrentSession().createQuery(hql)
                .setString("tag", tag)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public String getConfigurationValueByTag(String tag){
        String hql = "SELECT c.value FROM Configuration c WHERE c.tag=:tag";
        
        return (String) this.sessionFactory.getCurrentSession().createQuery(hql)
                .setString("tag", tag)
                .uniqueResult();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public boolean getConfigurationBooleanValueByTag(String tag){
        String hql = "SELECT c.value FROM Configuration c WHERE c.tag=:tag";
        
        return  Boolean.parseBoolean((String) this.sessionFactory.getCurrentSession().createQuery(hql)
                .setString("tag", tag)
                .uniqueResult());
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public int getConfigurationIntegerValueByTag(String tag){
        String hql = "SELECT c.value FROM Configuration c WHERE c.tag=:tag";
        
        return  Integer.parseInt((String) this.sessionFactory.getCurrentSession().createQuery(hql)
                .setString("tag", tag)
                .uniqueResult());
    }
}
