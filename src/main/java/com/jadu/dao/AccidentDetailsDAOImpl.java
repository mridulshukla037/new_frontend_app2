package com.jadu.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.AccidentDetails;

/**
 * 
 * @author Asad.ali
 *
 */
public class AccidentDetailsDAOImpl {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public AccidentDetails save(AccidentDetails accidentDetails) {
		this.sessionFactory.getCurrentSession().save(accidentDetails);
		return accidentDetails;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public AccidentDetails saveOrUpdate(AccidentDetails accidentDetails) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(accidentDetails);
		return accidentDetails;
	}
}
