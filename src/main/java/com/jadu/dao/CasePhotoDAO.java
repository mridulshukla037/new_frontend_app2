package com.jadu.dao;

import com.jadu.model.CasePhoto;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class CasePhotoDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void saveAll(List<CasePhoto> casePhotos){
        for(CasePhoto cp : casePhotos){
            this.sessionFactory.getCurrentSession().save(cp);
        }
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void save(CasePhoto casePhoto){
        this.sessionFactory.getCurrentSession().saveOrUpdate(casePhoto);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getCasePhotosByCaseId(long caseId){
        String hql = "select c.fileName  from CasePhoto c join c.inspectionCase ic  where ic.id=:id";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setLong("id", caseId)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getCasePhotosByCaseIdLimit(long caseId){
        String hql = "select new com.jadu.dto.CasePhotoDTO2(c.fileName, c.photoType.id)  from CasePhoto c join c.inspectionCase ic  where ic.id=:id AND c.photoType.id IN ('90-deg-front', '90-deg-left', '90-deg-back', '90-deg-right')";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setLong("id", caseId)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Object get(long id){
        String hql = "from CasePhoto c where c.id=:id";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setLong("id", id)
                .uniqueResult();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getCasePhotosByPhotoType(long caseId, String photoType){
        String hql = "select c.id  from CasePhoto c join c.inspectionCase ic  join c.photoType pt where ic.id=:id AND pt.id=:photoType";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setLong("id", caseId)
                .setString("photoType", photoType)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getCasePhotosDTOByCaseId(long caseId){
        String hql = "select new com.jadu.dto.CasePhotoDTO(c.id, c.latitude, c.longitude, c.answer, c.qcAnswer, c.fileName, c.photoType.id, c.snapTime) from CasePhoto c join c.inspectionCase ic   where ic.id=:id";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setLong("id", caseId)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getCasePhotosDTOByCaseId(long caseId, String photoType){
        String hql = "select new com.jadu.dto.CasePhotoDTO(c.id, c.latitude, c.longitude, c.answer, c.qcAnswer, c.fileName, c.photoType.id, c.snapTime) from CasePhoto c join c.inspectionCase ic   where ic.id=:id AND c.photoType.id=:photoType";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setLong("id", caseId)
                .setParameter("photoType", photoType)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public int updateQCAnswer(long caseId, String photoType, String qcAnswer){
        String hql = "UPDATE CasePhoto c SET c.qcAnswer=:qcAnswer where c.inspectionCase.id=:caseId AND c.photoType.id=:photoType";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setLong("caseId", caseId)
                .setString("photoType", photoType)
                .setString("qcAnswer", qcAnswer)
                .executeUpdate();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public int deletePhotosForCase(long caseId){
        String hql = "delete from CasePhoto c  where c.inspectionCase.id=:caseId";
        
        return this.sessionFactory
                .getCurrentSession()
                .createQuery(hql)
                .setLong("caseId", caseId)
                .executeUpdate();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deletePhoto(long photoId){
        this.sessionFactory
                .getCurrentSession().delete(this.get(photoId));
    }
    
}
