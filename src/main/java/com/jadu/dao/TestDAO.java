package com.jadu.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class TestDAO {
    
    
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public String test(){
        
        
        Session session = sessionFactory.getCurrentSession();
        System.out.println(session.getTransaction().isActive());
        
        session.createSQLQuery("INSERT INTO test VALUES('gautam')").executeUpdate();
        session.createSQLQuery("INSERT INTO test VALUES('ravinder', 'kumar')").executeUpdate();
        return session.toString();
    }
}
