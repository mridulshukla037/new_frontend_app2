package com.jadu.dao;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class UserNotificationDAO {
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getAll(String username) {
        String hql = "FROM UserNotification un WHERE un.username=:username ORDER BY un.creationTime DESC";
        
        return this.sessionFactory.getCurrentSession().createQuery(hql)
                .setString("username", username)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Long getCountOfUnreadNotifications(String username) {
        String hql = "SELECT count(*) FROM UserNotification un WHERE un.username=:username AND un.seen=false";
        
        return (Long)this.sessionFactory.getCurrentSession().createQuery(hql)
                .setString("username", username)
                .uniqueResult();
    }
    
}
