/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dao;

/**
 *
 * @author gkumar
 */
public class MappedAgentsDAO {
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String username;

    public MappedAgentsDAO(String username, String firstName, String lastName, String phoneNumber) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
    }

    public String getUsername() {
        return username;
    }
    

    public String getName() {
        return firstName + " " + lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
