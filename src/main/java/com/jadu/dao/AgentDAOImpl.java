package com.jadu.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.dto.AgentsDTO;
import com.jadu.dto.FilterDTO;
import com.jadu.model.Agent;
import com.jadu.model.CompanyBranchDivisionUser;
import com.jadu.service.AppConfigService;
import com.jadu.service.UtilService;

public class AgentDAOImpl {
	private SessionFactory sessionFactory;

	@Autowired
	private UtilService utilService;

	@Autowired
	private AppConfigService appConfigService;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(Agent agent) {
		this.sessionFactory.getCurrentSession().persist(agent);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(Agent agent) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(agent);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAllAgents() {
		return this.sessionFactory.getCurrentSession().createCriteria(Agent.class).list();
	}

	private FilterDTO getQuery(String prefix, Set<CompanyBranchDivisionUser> companyBranchDivisionUser) {

		if (companyBranchDivisionUser == null || companyBranchDivisionUser.isEmpty())
			return null;

		List<String> result = new ArrayList<>();
		HashMap<String, Object> map = new HashMap();

		int i = 1;
		for (CompanyBranchDivisionUser item : companyBranchDivisionUser) {
			List<String> conditions = new ArrayList<>();
			if (item.getCompany() != null) {
				conditions.add(prefix + ".company = :company" + i);
				map.put("company" + i, item.getCompany());
			}

			if (item.getState() != null) {
				conditions.add(prefix + ".state = :state" + i);
				map.put("state" + i, item.getState());
			}

			if (item.getDivision() != null) {
				conditions.add(prefix + ".division = :division" + i);
				map.put("division" + i, item.getDivision());
			}

			if (item.getBranch() != null) {
				conditions.add(prefix + ".branch = :branch" + i);
				map.put("branch" + i, item.getBranch());
			}

			i++;

			if (!conditions.isEmpty())
				result.add(" ( " + String.join(" AND ", conditions) + " ) ");
		}

		if (result.isEmpty())
			return null;

		FilterDTO filterDTO = new FilterDTO();
		filterDTO.setMap(map);
		filterDTO.setWhereClause(" ( " + String.join(" AND ", result) + " ) ");
		System.out.println(filterDTO.getWhereClause());
		return filterDTO;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAllAgents(Set<CompanyBranchDivisionUser> companyBranchDivisionUser) {

		String cbdQuery = "SELECT id from CompanyBranchDivision c WHERE 1=1 ";
		FilterDTO filterDTO = utilService.getQuery("c", companyBranchDivisionUser);

		if (filterDTO != null) {
			cbdQuery += " AND " + filterDTO.getWhereClause();
		}

		String hql = "SELECT a.phoneNumber FROM Agent a JOIN  a.agentDetail ad WHERE ad.companyBranchDivision.id IN ( "
				+ cbdQuery + " ) ";

		Query query = utilService.createQuery(this.sessionFactory.getCurrentSession().createQuery(hql), filterDTO);

		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getLatestAgents(Set<CompanyBranchDivisionUser> companyBranchDivisionUser) {

		String cbdQuery = "SELECT id from CompanyBranchDivision c WHERE 1=1 ";
		FilterDTO filterDTO = utilService.getQuery("c", companyBranchDivisionUser);

		if (filterDTO != null) {
			cbdQuery += " AND " + filterDTO.getWhereClause();
		}

		String hql = "SELECT a.phoneNumber FROM Agent a JOIN  a.agentDetail ad WHERE ad.companyBranchDivision.id IN ( "
				+ cbdQuery + " ) order by a.createdDate DESC";

		Query query = utilService.createQuery(this.sessionFactory.getCurrentSession().createQuery(hql), filterDTO);

		return query.setFirstResult(0).setMaxResults(appConfigService.getIntProperty("TOP_LATEST_AGENTS_LIMITS", 50))
				.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Agent getAgentByUsername(String username) {
		return (Agent) this.sessionFactory.getCurrentSession().get(Agent.class, username);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAgentByPhoneNumber(String phoneNumber) {
		return this.sessionFactory.getCurrentSession().createCriteria(Agent.class)
				.add(Restrictions.eq("phoneNumber", phoneNumber)).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Agent getUniqueAgentByPhoneNumber(String phoneNumber) {
		return (Agent) this.sessionFactory.getCurrentSession().createCriteria(Agent.class)
				.add(Restrictions.eq("phoneNumber", phoneNumber)).uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<AgentsDTO> getAgentPersonalDetails() {
		String hql = "SELECT new com.jadu.dto.AgentsDTO(a.email, a.phoneNumber, a.firstName, a.lastName) FROM Agent a";

		return this.sessionFactory.getCurrentSession().createQuery(hql).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAgentByEmail(String email) {
		return this.sessionFactory.getCurrentSession().createCriteria(Agent.class).add(Restrictions.eq("email", email))
				.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Agent> getByBranch(String branch) {
		String hql = "SELECT a FROM Agent a JOIN  a.agentDetail ad WHERE ad.companyBranchDivision.branch=:branch";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString("branch", branch);
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Agent> getAgentById(String agentId) {
		String hql = "SELECT a FROM Agent a JOIN a.agentDetail ad WHERE ad.agentId=:agentId ";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString("agentId", agentId);
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Agent> getAgentLikeId(String agentId) {
		String hql = "SELECT a FROM Agent a JOIN a.agentDetail ad WHERE ad.agentId LIKE ? ";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString(0, "%" + agentId + "%");
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Agent> getAgentLikePhoneNumber(String phoneNumber) {
		String hql = "SELECT a FROM Agent a WHERE a.phoneNumber LIKE ? ";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString(0, "%" + phoneNumber + "%");
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Agent> getAgentLikeEmail(String emailId) {
		String hql = "SELECT a FROM Agent a WHERE a.email LIKE ? ";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString(0, "%" + emailId + "%");
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAgentsByCompanyBranchDivision(String companyBranchDivisionIds) {

		String hql = "SELECT a.phoneNumber FROM Agent a WHERE a.garageCompaniesId LIKE ? ";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString(0, "%" + companyBranchDivisionIds + "%");
		return query.list();
	}

}
