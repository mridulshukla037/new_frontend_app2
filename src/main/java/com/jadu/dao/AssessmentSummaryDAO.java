package com.jadu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.AssessmentSummary;

public class AssessmentSummaryDAO {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(AssessmentSummary assessmentSummary) {
		this.sessionFactory.getCurrentSession().save(assessmentSummary);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public AssessmentSummary saveOrUpdate(AssessmentSummary assessmentSummary) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(assessmentSummary);
		return assessmentSummary;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public AssessmentSummary findByClaimNumber(String claimNumber) {
		String hql = "SELECT a FROM AssessmentSummary a WHERE a.claimNumber=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, claimNumber);
		List<AssessmentSummary> result = query.list();
		if (result.isEmpty())
			return null;
		return result.get(0);
	}

}
