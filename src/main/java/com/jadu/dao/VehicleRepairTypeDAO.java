package com.jadu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.VehicleRepairType;
import com.jadu.model.MakeModelVariant;

/**
 * 
 * @author Asad.ali
 *
 */
public class VehicleRepairTypeDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<VehicleRepairType> findAll() {
		String hql = "FROM VehicleRepairType";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

}
