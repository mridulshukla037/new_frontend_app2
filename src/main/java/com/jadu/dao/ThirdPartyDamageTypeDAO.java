package com.jadu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.ThirdPartyDamageType;

/**
 * 
 * @author Asad.ali
 *
 */
public class ThirdPartyDamageTypeDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<ThirdPartyDamageType> findAll() {
		String hql = "FROM ThirdPartyDamageType";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

}
