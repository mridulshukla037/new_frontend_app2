package com.jadu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.LossAssessment;
import org.hibernate.Criteria;

/**
 * 
 * @author Asad.ali
 *
 */
public class LossAssessmentDAOImpl {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public LossAssessment save(LossAssessment lossAssessment) {
		return (LossAssessment) this.sessionFactory.getCurrentSession().save(lossAssessment);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(LossAssessment lossAssessment) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(lossAssessment);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List getCaseById(Long caseId) {
		String hql = "SELECT new com.jadu.controller.dto.LossAssessmentDTO("
                        + "la.id, "
                        + "la.vehiclePartParticularType.partTypeId,"
                        + "la.vehiclePartParticularType.partSideId,"
                        + "la.vehiclePartParticularType.id,"
                        + "la.vehicleDamageType.id,"
                        + "la.vehicleRepairType.id,"
                        + "partCost,"
                        + "labourCost,"
                        + "paintCost,"
                        + "depRateForPart,"
                        + "gstRateForPart,"
                        + "depRateForLabour,"
                        + "gstRateForLabour,"
                        + "depRateForPaint,"
                        + "gstRateForPaint) FROM LossAssessment la  WHERE la.claimCase=? GROUP BY la.id";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setLong(0, caseId);
		return query.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<LossAssessment> getByClaimNumber(String claim_number) {
		String hql = "SELECT la FROM LossAssessment la  WHERE la.claimNumber=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, claim_number);
		List<LossAssessment> result = query.list();

		if (result.isEmpty())
			return null;

		return result;
	}
        
        @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<LossAssessment> getByClaimIDOrNumber(Long id, String claim_number) throws Exception {
		String hql = "SELECT la FROM LossAssessment la  WHERE la.claimCase=? OR la.claimNumber=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setLong(0, id);
                query.setString(1, claim_number);
		List<LossAssessment> result = query.list();

		if (result.isEmpty())
			throw new Exception("Unable to get case details!");

		return result;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public int deleteByClaimNumber(String claim_number) {
		String hql = "delete FROM LossAssessment la  WHERE la.claimNumber=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, claim_number);
		return query.executeUpdate();

	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public int deleteCaseById(Long caseId) {
		String hql = "delete FROM LossAssessment la  WHERE la.claimCase=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setLong(0, caseId);
		return query.executeUpdate();

	}

}
