package com.jadu.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.AccidentDetails;
import com.jadu.model.DriverDetails;

/**
 * 
 * @author Asad.ali
 *
 */
public class DriverDetailsDAOImpl {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(DriverDetails driverDetails) {
		this.sessionFactory.getCurrentSession().save(driverDetails);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DriverDetails saveOrUpdate(DriverDetails driverDetails) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(driverDetails);
		return driverDetails;
	}
}
