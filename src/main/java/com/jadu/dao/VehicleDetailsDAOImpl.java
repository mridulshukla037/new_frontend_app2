package com.jadu.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.VehicleDetails;

/**
 * 
 * @author Asad.ali
 *
 */
public class VehicleDetailsDAOImpl {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public VehicleDetails save(VehicleDetails vehicleDetails) {
		this.sessionFactory.getCurrentSession().save(vehicleDetails);
		return vehicleDetails;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(VehicleDetails vehicleDetails) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(vehicleDetails);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAll() {
		return this.sessionFactory.getCurrentSession().createCriteria(VehicleDetails.class).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAllDTO() {
		String hql = "select new com.jadu.dto.MakeModelDTO(c.id, c.vehicleType, c.subType, c.vehicleMake, c.vehicleModel) from VehicleDetails c ";

		return this.sessionFactory.getCurrentSession().createQuery(hql).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public VehicleDetails getVehicleById(int id) {
		return (VehicleDetails) this.sessionFactory.getCurrentSession().get(VehicleDetails.class, id);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List get() {
		String hql = "select distinct vehicleMake from VehicleDetails";
		return this.sessionFactory.getCurrentSession().createQuery(hql).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getModels(String make) {
		String hql = "from VehicleDetails v WHERE v.vehicleMake=:make";
		return this.sessionFactory.getCurrentSession().createQuery(hql).setParameter("make", make).list();
	}

}
