package com.jadu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.PassengerCarryingCapacity;

public class PassengerCarryingCapacityDao {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<PassengerCarryingCapacity> findAll() {
		String hql = "FROM  PassengerCarryingCapacity";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}
}
