package com.jadu.dao;

import com.jadu.model.Device;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class DeviceDAO {
    
    protected SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Device get(
            String deviceId,
            String username
    ){
        return (Device)this.sessionFactory
                .getCurrentSession()
                .createCriteria(Device.class)
                .add(Restrictions.eq("deviceId", deviceId))
                .add(Restrictions.eq("username", username))
                .uniqueResult();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void save(Device device){
        this.sessionFactory.getCurrentSession().persist(device);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void delete(Device device){
        this.sessionFactory.getCurrentSession().delete(device);
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List getDevicesByUsername(String username){
        String hql = "from Device v WHERE v.username=:username";
        return this.sessionFactory.getCurrentSession().createQuery(hql).setString("username", username).list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void delete(String username){
        Query q = this.sessionFactory.getCurrentSession().createQuery("delete Device where username = :username");
        q.setString("username", username);
        q.executeUpdate();
    }
}
