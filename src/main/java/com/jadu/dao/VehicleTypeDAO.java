package com.jadu.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.VehicleType;

public class VehicleTypeDAO {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getAll() {
		/*
		 * return this.sessionFactory .getCurrentSession()
		 * .createCriteria(VehicleType.class) .list();
		 */

		String hql = "FROM VehicleType vt GROUP BY vt.id";
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery(hql).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public VehicleType get(String id) {
		return (VehicleType) this.sessionFactory.getCurrentSession().createCriteria(VehicleType.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}
}
