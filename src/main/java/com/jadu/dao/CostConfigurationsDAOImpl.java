package com.jadu.dao;

import com.jadu.controller.dto.CostConfigurationsDTO;
import com.jadu.model.CostConfigurations;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Asad.ali
 *
 */
public class CostConfigurationsDAOImpl {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<CostConfigurations> getAll() {
        return this.sessionFactory
                .getCurrentSession()
                .createQuery("from CostConfigurations")
                .list();
    }
 
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List get(Long makeModelVariantId){
        String hql = "SELECT new com.jadu.controller.dto.CostConfigurationsDTO (c.id, c.makeModelVariant.id, c.vehiclePartType.id, c.vehiclePartSide.id, c.vehiclePartParticularType.id, c.vehicleDamageType.id, repairType, partCost, labourCost, paintCost, paintGst, labourGst, partGst) FROM CostConfigurations c WHERE c.makeModelVariant.id=:makeModelVariantId";
        
        return this.sessionFactory.getCurrentSession().createQuery(hql)
                .setLong("makeModelVariantId", makeModelVariantId)
                .list();
    }
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public CostConfigurationsDTO get(Long makeModelVariantId, String partParticularType, String damageType, String repairType){
        String hql = "SELECT new com.jadu.controller.dto.CostConfigurationsDTO (c.id, c.makeModelVariant.id, c.vehiclePartType.id, c.vehiclePartSide.id, c.vehiclePartParticularType.id, c.vehicleDamageType.id, repairType, partCost, labourCost, paintCost, paintGst, labourGst, partGst, rateOfDepForPart,rateOfDepForLabour,rateOfDepForPaint) FROM"
                + " CostConfigurations c WHERE"
                + " c.makeModelVariant.id=:makeModelVariantId AND "
                + " c.vehiclePartParticularType.id = :partParticularType AND"
                + " c.vehicleDamageType.id = :damageType AND"
                + " c.repairType = :repairType ";
        
        return (CostConfigurationsDTO) this.sessionFactory.getCurrentSession().createQuery(hql)
                .setLong("makeModelVariantId", makeModelVariantId)
                .setString("partParticularType", partParticularType)
                .setString("damageType", damageType)
                .setString("repairType", repairType)
                .uniqueResult();
    }

}
