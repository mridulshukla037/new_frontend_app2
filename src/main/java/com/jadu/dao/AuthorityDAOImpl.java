package com.jadu.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.Authority;

/**
 *
 * @author gauta
 */
public class AuthorityDAOImpl {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Authority> getUserByRoleType(String roleType) throws UsernameNotFoundException {
		return (List<Authority>) this.sessionFactory.getCurrentSession().createCriteria(Authority.class)
				.add(Restrictions.eq("authority", roleType)).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public void deleteAuthority(Authority authority) {
		this.sessionFactory.getCurrentSession().delete(authority);
	}

}
