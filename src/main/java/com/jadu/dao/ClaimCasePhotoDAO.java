package com.jadu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jadu.model.ClaimCasePhoto;

/**
 * 
 * @author Asad.ali
 *
 */
public class ClaimCasePhotoDAO {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(ClaimCasePhoto claimCasePhoto) {
		this.sessionFactory.getCurrentSession().save(claimCasePhoto);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(ClaimCasePhoto claimCasePhoto) {
		this.sessionFactory.getCurrentSession().saveOrUpdate(claimCasePhoto);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<ClaimCasePhoto> getPhotosClaimNumber(String claim_number) {
		String hql = "SELECT la FROM ClaimCasePhoto la  WHERE la.claimNumber=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setString(0, claim_number);
		List<ClaimCasePhoto> result = query.list();

		if (result.isEmpty())
			return null;

		return result;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<ClaimCasePhoto> getPhotosClaimCase(Long claimCase) {
		String hql = "SELECT cp FROM ClaimCasePhoto cp  join cp.photoType pt WHERE cp.claimCase=?";
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setLong(0, claimCase);
		List<ClaimCasePhoto> result = query.list();

		if (result.isEmpty())
			return null;

		return result;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveAll(List<ClaimCasePhoto> casePhotos) {
		for (ClaimCasePhoto cp : casePhotos) {
			this.sessionFactory.getCurrentSession().save(cp);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getCasePhotosByCaseId(long caseId) {
		String hql = "select c.fileName  from ClaimCasePhoto c join c.claimCase ic  where ic.id=:id";

		return this.sessionFactory.getCurrentSession().createQuery(hql)
				.setLong("id", caseId).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getCasePhotosByCaseIdLimit(long caseId) {
		String hql = "select new com.jadu.dto.CasePhotoDTO2(c.fileName, c.photoType.id)  from ClaimCasePhoto c join c.claimCase ic  where ic.id=:id AND c.photoType.id IN ('90-deg-front', '90-deg-left', '90-deg-back', '90-deg-right')";

		return this.sessionFactory.getCurrentSession().createQuery(hql)
				.setLong("id", caseId).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Object get(long id) {
		String hql = "from ClaimCasePhoto c where c.id=:id";

		return this.sessionFactory.getCurrentSession().createQuery(hql)
				.setLong("id", id).uniqueResult();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getCasePhotosByPhotoType(long caseId, String photoType) {
		String hql = "select c.id  from ClaimCasePhoto c join c.claimCase ic  join c.photoType pt where ic.id=:id AND pt.id=:photoType";

		return this.sessionFactory.getCurrentSession().createQuery(hql)
				.setLong("id", caseId).setString("photoType", photoType).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getCasePhotosDTOByCaseId(long caseId) {
		String hql = "select new com.jadu.dto.CasePhotoDTO(c.id, c.latitude, c.longitude, c.answer, c.qcAnswer, c.fileName, c.photoType.id, c.snapTime) from ClaimCasePhoto c join c.claimCase ic   where ic.id=:id";

		return this.sessionFactory.getCurrentSession().createQuery(hql)
				.setLong("id", caseId).list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List getCasePhotosDTOByCaseId(long caseId, String photoType) {
		String hql = "select new com.jadu.dto.CasePhotoDTO(c.id, c.latitude, c.longitude, c.answer, c.qcAnswer, c.fileName, c.photoType.id, c.snapTime) from ClaimCasePhoto c join c.claimCase ic   where ic.id=:id AND c.photoType.id=:photoType";

		return this.sessionFactory.getCurrentSession().createQuery(hql)
				.setLong("id", caseId).setParameter("photoType", photoType)
				.list();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public int updateQCAnswer(long caseId, String photoType, String qcAnswer) {
		String hql = "UPDATE ClaimCasePhoto c SET c.qcAnswer=:qcAnswer where c.claimCase.id=:caseId AND c.photoType.id=:photoType";

		return this.sessionFactory.getCurrentSession().createQuery(hql)
				.setLong("caseId", caseId).setString("photoType", photoType)
				.setString("qcAnswer", qcAnswer).executeUpdate();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public int deletePhotosForCase(long caseId) {
		String hql = "delete from ClaimCasePhoto c  where c.claimCase.id=:caseId";

		return this.sessionFactory.getCurrentSession().createQuery(hql)
				.setLong("caseId", caseId).executeUpdate();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deletePhoto(long photoId) {
		this.sessionFactory.getCurrentSession().delete(this.get(photoId));
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<ClaimCasePhoto> updatetimagebyexisting(
			ClaimCasePhoto claimCasePhoto) {
		this.sessionFactory.getCurrentSession().update(claimCasePhoto);
		return this.sessionFactory.getCurrentSession()
				.createQuery("From ClaimCasePhoto ccp where ccp.id=:caseid")
				.setParameter("caseid", claimCasePhoto.getClaimCase().getId())
				.list();
	}

}
