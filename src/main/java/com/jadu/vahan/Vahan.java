package com.jadu.vahan;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class Vahan {
    public static void getVehicleDetails(
            String vahanUrl
    ){
        try {
            URL obj = new URL(vahanUrl);
            JSONObject item = new JSONObject("{\"ABICode\":\"07054601\",\"Description\":\"2006 BMW 318 I Se (128), 1995CC Petrol, 4DR, Manual\",\"RegistrationYear\":\"2006\",\"CarMake\":{\"CurrentTextValue\":\"BMW\"},\"CarModel\":{\"CurrentTextValue\":\"318\"},\"EngineSize\":{\"CurrentTextValue\":\"1995CC\"},\"FuelType\":{\"CurrentTextValue\":\"Petrol\"},\"MakeDescription\":\"BMW\",\"ModelDescription\":\"318\",\"Immobiliser\":{\"CurrentTextValue\":\"\"},\"NumberOfSeats\":{\"CurrentTextValue\":5},\"IndicativeValue\":{\"CurrentTextValue\":\"\"},\"DriverSide\":{\"CurrentTextValue\":\"RHD\"},\"ImageUrl\":\"http://www.regcheck.org.uk/image.aspx/@Qk1XIDMxOA==\",\"VehicleInsuranceGroup\":\"12\"}");
            System.out.println(item);
        } catch (MalformedURLException | JSONException ex) {
            Logger.getLogger(Vahan.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
}
