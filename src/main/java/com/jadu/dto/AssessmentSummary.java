package com.jadu.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;

import org.hibernate.annotations.Index;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AssessmentSummary implements Serializable {

	private String claimNumber;

	private double totalLabourCharges;

	private double supplementryLabourCharges;

	private double totalPartCharges;

	private double supplementryPartCharges;

	private double lessExcess;

	private double lessSalavage;

	private double total;

	private String remarks;

	private String summaryNotes;
	private List<String> summaryRemarks;
	private Map<String, Float> estimates;
	private Map<String, Float> assessedFor;

	public String getSummaryNotes() {
		return summaryNotes;
	}

	public void setSummaryNotes(String summaryNotes) {
		this.summaryNotes = summaryNotes;
	}

	public Map<String, Float> getEstimates() {
		return estimates;
	}

	public void setEstimates(Map<String, Float> estimates) {
		this.estimates = estimates;
	}

	public Map<String, Float> getAssessedFor() {
		return assessedFor;
	}

	public void setAssessedFor(Map<String, Float> assessedFor) {
		this.assessedFor = assessedFor;
	}

	public List<String> getSummaryRemarks() {
		return summaryRemarks;
	}

	public void setSummaryRemarks(List<String> summaryRemarks) {
		this.summaryRemarks = summaryRemarks;
	}

	public String getClaimNumber() {
		return claimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}

	public double getTotalLabourCharges() {
		return totalLabourCharges;
	}

	public void setTotalLabourCharges(double totalLabourCharges) {
		this.totalLabourCharges = totalLabourCharges;
	}

	public double getSupplementryLabourCharges() {
		return supplementryLabourCharges;
	}

	public void setSupplementryLabourCharges(double supplementryLabourCharges) {
		this.supplementryLabourCharges = supplementryLabourCharges;
	}

	public double getTotalPartCharges() {
		return totalPartCharges;
	}

	public void setTotalPartCharges(double totalPartCharges) {
		this.totalPartCharges = totalPartCharges;
	}

	public double getSupplementryPartCharges() {
		return supplementryPartCharges;
	}

	public void setSupplementryPartCharges(double supplementryPartCharges) {
		this.supplementryPartCharges = supplementryPartCharges;
	}

	public double getLessExcess() {
		return lessExcess;
	}

	public void setLessExcess(double lessExcess) {
		this.lessExcess = lessExcess;
	}

	public double getLessSalavage() {
		return lessSalavage;
	}

	public void setLessSalavage(double lessSalavage) {
		this.lessSalavage = lessSalavage;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "AssessmentSummary [claimNumber=" + claimNumber + ", totalLabourCharges=" + totalLabourCharges
				+ ", supplementryLabourCharges=" + supplementryLabourCharges + ", totalPartCharges=" + totalPartCharges
				+ ", supplementryPartCharges=" + supplementryPartCharges + ", lessExcess=" + lessExcess
				+ ", lessSalavage=" + lessSalavage + ", total=" + total + ", remarks=" + remarks + ", summaryNotes="
				+ summaryNotes + ", summaryRemarks=" + summaryRemarks + ", estimates=" + estimates + ", assessedFor="
				+ assessedFor + "]";
	}

}
