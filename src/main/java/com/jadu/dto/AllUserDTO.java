package com.jadu.dto;

import java.io.Serializable;

public class AllUserDTO implements Serializable {

	private String username;

	private String firstName;

	private String lastName;

	private boolean enabled;

	private boolean canUpdate;

	private String phoneNumber;

	private String email;

	private boolean passwordChangeRequired;

	private boolean emailVerified;

	private boolean phoneNumberVerified;

	private String profilePhotoUrl;

	private String profilePhotoUrlThumb;

	private String authority;

	private boolean deviceLocked;

	private String deviceId;

	private String profilePhotoRelativeUrl;

	private String agentCode;

	private String agentCompany;

	private String branchName;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isPasswordChangeRequired() {
		return passwordChangeRequired;
	}

	public void setPasswordChangeRequired(boolean passwordChangeRequired) {
		this.passwordChangeRequired = passwordChangeRequired;
	}

	public boolean isEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(boolean emailVerified) {
		this.emailVerified = emailVerified;
	}

	public boolean isPhoneNumberVerified() {
		return phoneNumberVerified;
	}

	public void setPhoneNumberVerified(boolean phoneNumberVerified) {
		this.phoneNumberVerified = phoneNumberVerified;
	}

	public String isProfilePhotoUrl() {
		return profilePhotoUrl;
	}

	public void setProfilePhotoUrl(String profilePhotoUrl) {
		this.profilePhotoUrl = profilePhotoUrl;
	}

	public String getProfilePhotoUrl() {
		return profilePhotoUrl;
	}

	public String getProfilePhotoRelativeUrl() {
		return "/util/profile-image/" + profilePhotoUrl;
	}

	public boolean isDeviceLocked() {
		return deviceLocked;
	}

	public void setDeviceLocked(boolean deviceLocked) {
		this.deviceLocked = deviceLocked;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getProfilePhotoUrlThumb() {
		return profilePhotoUrlThumb;
	}

	public void setProfilePhotoUrlThumb(String profilePhotoUrlThumb) {
		this.profilePhotoUrlThumb = profilePhotoUrlThumb;
	}

	public boolean isCanUpdate() {
		return canUpdate;
	}

	public void setCanUpdate(boolean canUpdate) {
		this.canUpdate = canUpdate;
	}

	public void setProfilePhotoRelativeUrl(String profilePhotoRelativeUrl) {
		this.profilePhotoRelativeUrl = profilePhotoRelativeUrl;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentCompany() {
		return agentCompany;
	}

	public void setAgentCompany(String agentCompany) {
		this.agentCompany = agentCompany;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	@Override
	public String toString() {
		return "AllUserDTO [username=" + username + ", firstName=" + firstName + ", lastName=" + lastName + ", enabled="
				+ enabled + ", canUpdate=" + canUpdate + ", phoneNumber=" + phoneNumber + ", email=" + email
				+ ", passwordChangeRequired=" + passwordChangeRequired + ", emailVerified=" + emailVerified
				+ ", phoneNumberVerified=" + phoneNumberVerified + ", profilePhotoUrl=" + profilePhotoUrl
				+ ", profilePhotoUrlThumb=" + profilePhotoUrlThumb + ", authority=" + authority + ", deviceLocked="
				+ deviceLocked + ", deviceId=" + deviceId + ", profilePhotoRelativeUrl=" + profilePhotoRelativeUrl
				+ ", agentCode=" + agentCode + ", agentCompany=" + agentCompany + ", branchName=" + branchName + "]";
	}

}
