package com.jadu.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jadu.model.AccidentDetails;
import com.jadu.model.DriverDetails;
import com.jadu.model.InsuranceDetails;
import com.jadu.model.VehicleDetails;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ClaimsFinalSubmissionDTO implements Serializable {
	private InsuranceDetails insuranceDetails;
	private VehicleDetails vehicleDetails;
	private DriverDetails driverDetails;
	private AccidentDetails accidentDetails;
	private LossAssessmentDTO lossAssessmentDTO;
	private AssessmentSummary assessmentSummary;
	private ReportHeaderInfo reportHeaderInfo;
	private List<String> enclosed;

	public InsuranceDetails getInsuranceDetails() {
		return insuranceDetails;
	}

	public void setInsuranceDetails(InsuranceDetails insuranceDetails) {
		this.insuranceDetails = insuranceDetails;
	}

	public VehicleDetails getVehicleDetails() {
		return vehicleDetails;
	}

	public void setVehicleDetails(VehicleDetails vehicleDetails) {
		this.vehicleDetails = vehicleDetails;
	}

	public DriverDetails getDriverDetails() {
		return driverDetails;
	}

	public void setDriverDetails(DriverDetails driverDetails) {
		this.driverDetails = driverDetails;
	}

	public AccidentDetails getAccidentDetails() {
		return accidentDetails;
	}

	public void setAccidentDetails(AccidentDetails accidentDetails) {
		this.accidentDetails = accidentDetails;
	}

	public ReportHeaderInfo getReportHeaderInfo() {
		return reportHeaderInfo;
	}

	public void setReportHeaderInfo(ReportHeaderInfo reportHeaderInfo) {
		this.reportHeaderInfo = reportHeaderInfo;
	}

	public LossAssessmentDTO getLossAssessmentDTO() {
		return lossAssessmentDTO;
	}

	public void setLossAssessmentDTO(LossAssessmentDTO lossAssessmentDTO) {
		this.lossAssessmentDTO = lossAssessmentDTO;
	}

	public AssessmentSummary getAssessmentSummary() {
		return assessmentSummary;
	}

	public void setAssessmentSummary(AssessmentSummary assessmentSummary) {
		this.assessmentSummary = assessmentSummary;
	}

	public List<String> getEnclosed() {
		return enclosed;
	}

	public void setEnclosed(List<String> enclosed) {
		this.enclosed = enclosed;
	}

	@Override
	public String toString() {
		return "ClaimsFinalSubmissionDTO [insuranceDetails=" + insuranceDetails + ", vehicleDetails=" + vehicleDetails
				+ ", driverDetails=" + driverDetails + ", accidentDetails=" + accidentDetails + ", lossAssessmentDTO="
				+ lossAssessmentDTO + ", assessmentSummary=" + assessmentSummary + ", reportHeaderInfo="
				+ reportHeaderInfo + ", enclosed=" + enclosed + "]";
	}

}
