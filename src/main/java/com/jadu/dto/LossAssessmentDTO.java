package com.jadu.dto;

import java.io.Serializable;
import java.util.List;

import com.jadu.model.LossAssessment;

public class LossAssessmentDTO implements Serializable {
	private String leftNotes;
	private String rightNotes;
	private List<LossAssessment> lossAssessment;
	private Float totalLabourCharge;
	private Float totalPartsCharges;

	public String getLeftNotes() {
		return leftNotes;
	}

	public void setLeftNotes(String leftNotes) {
		this.leftNotes = leftNotes;
	}

	public String getRightNotes() {
		return rightNotes;
	}

	public void setRightNotes(String rightNotes) {
		this.rightNotes = rightNotes;
	}

	public List<LossAssessment> getLossAssessment() {
		return lossAssessment;
	}

	public void setLossAssessment(List<LossAssessment> lossAssessment) {
		this.lossAssessment = lossAssessment;
	}

	public Float getTotalLabourCharge() {
		return totalLabourCharge;
	}

	public void setTotalLabourCharge(Float totalLabourCharge) {
		this.totalLabourCharge = totalLabourCharge;
	}

	public Float getTotalPartsCharges() {
		return totalPartsCharges;
	}

	public void setTotalPartsCharges(Float totalPartsCharges) {
		this.totalPartsCharges = totalPartsCharges;
	}

	@Override
	public String toString() {
		return "LossAssessmentDTO [leftNotes=" + leftNotes + ", rightNotes=" + rightNotes + ", lossAssessment="
				+ lossAssessment + ", totalLabourCharge=" + totalLabourCharge + ", totalPartsCharges="
				+ totalPartsCharges + "]";
	}

}
