package com.jadu.dto;

import java.io.Serializable;
import java.util.Date;

public class CompletedCasesDAO implements Serializable{
    private long id;
    private String vehicleNumber;
    private String customerName;
    private String customerPhoneNumber;
    private Date inspectionTime;
    private Date closeTime;
    private int currentStage;
    
    public CompletedCasesDAO(
            long id,
            String vehicleNumber,
            String customerName,
            String customerPhoneNumber,
            Date inspectionTime,
            Date closeTime,
            int currentStage
    ){
        this.id = id;
        this.vehicleNumber = vehicleNumber;
        this.customerName = customerName;
        this.customerPhoneNumber = customerPhoneNumber;
        this.inspectionTime = inspectionTime;
        this.closeTime = closeTime;
        this.currentStage = currentStage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public Date getInspectionTime() {
        return inspectionTime;
    }

    public void setInspectionTime(Date inspectionTime) {
        this.inspectionTime = inspectionTime;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public int getCurrentStage() {
        return currentStage;
    }

    public void setCurrentStage(int currentStage) {
        this.currentStage = currentStage;
    }
}
