/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author gautam
 */
public class CaseHistoryDTO2 implements Serializable{
    private final long id;
    private final String vehicleNumber;
    private final String customerName;
    private final String remark;
    private final Date inspectionTime;
    private final String insuranceCompany;
    private final String purposeOfInspection;
    private final int vehicle;
    private final int currentStage;
    private final String requestorName;
    private final String customerPhoneNumber;
    
    public CaseHistoryDTO2(
            long id,
            String vehicleNumber,
            String customerName,
            String remark,
            Date inspectionTime,
            String insuranceCompany, 
            String purposeOfInspection,
            int model,
            int currentStage,
            String requestorName,
            String customerPhoneNumber){
        this.id = id;
        this.vehicleNumber = vehicleNumber;
        this.customerName = customerName;
        this.remark = remark;
        this.inspectionTime = inspectionTime;
        this.insuranceCompany =  insuranceCompany;
        this.purposeOfInspection =  purposeOfInspection;
        this.vehicle = model;
        this.currentStage = currentStage;
        this.requestorName = requestorName;
        this.customerPhoneNumber = customerPhoneNumber;
    }
    
    
    public class InsuranceCompany{
        private final String name;
        
        public InsuranceCompany(String name){
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
    
    
    public class PurposeOfInspection{
        
        public PurposeOfInspection(String name){
            this.name = name;
        }
        
        private final String name;

        public String getName() {
            return name;
        }
    }
    
    public class Vehicle{
        private final String model;
        
        public Vehicle(String model){
            
            if(model == null)
                model="";
            this.model = model;
        }

        public String getModel() {
            return model;
        }
        
        
    }

    public long getId() {
        return id;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getRemark() {
        return remark;
    }

    public Date getInspectionTime() {
        return inspectionTime;
    }

    public String getInsuranceCompany() {
        return insuranceCompany;
    }

    public String getPurposeOfInspection() {
        return purposeOfInspection;
    }

    public int getVehicle() {
        return vehicle;
    }

    public int getCurrentStage() {
        return currentStage;
    }

    public String getRequestorName() {
        return requestorName;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }
    
    
}
