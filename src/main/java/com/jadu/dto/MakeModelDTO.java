/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dto;

import java.io.Serializable;

/**
 *
 * @author gautam
 */
public class MakeModelDTO implements Serializable{
    private int id;
    private String vehicleType;
    private String subType;
    private String make;
    private String model;

    public MakeModelDTO(int id, String vehicleType, String subType, String make, String model) {
        this.id = id;
        this.vehicleType = vehicleType;
        this.subType = subType;
        this.make = make;
        this.model = model;
    }
    
    

    public int getId() {
        return id;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public String getSubType() {
        return subType;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }
    
    
}
