package com.jadu.dto;

import java.io.Serializable;
import java.util.Date;

public class CaseCommentDTO implements Serializable{
    private long id;
    private String comment;
    private Date commentTime;
    private String firstName;
    private String lastName;
    
    public CaseCommentDTO(
            long id,
            String comment,
            Date commentTime,
            String firstName,
            String lastName
    ){
        this.id = id;
        this.comment = comment;
        this.commentTime = commentTime;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public long getId() {
        return id;
    }

    public String getComment() {
        return comment;
    }

    public Date getCommentTime() {
        return commentTime;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
    
    
}
