package com.jadu.dto;

import java.io.Serializable;
import java.util.List;

import com.jadu.model.ClaimCasePhoto;
import com.jadu.model.LossAssessment;
import com.jadu.model.LossAssessmentPhotos;

public class ClaimCasePhotosDTO implements Serializable {
	private List<ClaimCasePhoto> claimCasePhoto;
	private List<LossAssessmentPhotos> lossAssessmentPhotos;

	public List<ClaimCasePhoto> getClaimCasePhoto() {
		return claimCasePhoto;
	}

	public void setClaimCasePhoto(List<ClaimCasePhoto> claimCasePhoto) {
		this.claimCasePhoto = claimCasePhoto;
	}

	public List<LossAssessmentPhotos> getLossAssessmentPhotos() {
		return lossAssessmentPhotos;
	}

	public void setLossAssessmentPhotos(List<LossAssessmentPhotos> lossAssessmentPhotos) {
		this.lossAssessmentPhotos = lossAssessmentPhotos;
	}

	@Override
	public String toString() {
		return "ClaimCaseDto [claimCasePhoto=" + claimCasePhoto + ", lossAssessmentPhotos=" + lossAssessmentPhotos
				+ "]";
	}
}
