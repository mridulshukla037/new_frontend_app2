package com.jadu.dto;

import java.io.Serializable;
import java.util.Date;

public class CaseHistoryDTO implements Serializable{
    private final long id;
    private final String vehicleNumber;
    private final String customerName;
    private final String remark;
    private final Date inspectionTime;
    private final InsuranceCompany insuranceCompany;
    private final PurposeOfInspection purposeOfInspection;
    private final Vehicle vehicle;
    private final int currentStage;
    private final String requestorName;
    private final String customerPhoneNumber;
    
    public CaseHistoryDTO(
            long id,
            String vehicleNumber,
            String customerName,
            String remark,
            Date inspectionTime,
            String insuranceCompany, 
            String purposeOfInspection,
            String model,
            int currentStage,
            String requestorName,
            String customerPhoneNumber){
        this.id = id;
        this.vehicleNumber = vehicleNumber;
        this.customerName = customerName;
        this.remark = remark;
        this.inspectionTime = inspectionTime;
        this.insuranceCompany =  new InsuranceCompany(insuranceCompany);
        this.purposeOfInspection =  new PurposeOfInspection(purposeOfInspection);
        this.vehicle = new Vehicle(model);
        this.currentStage = currentStage;
        this.requestorName = requestorName;
        this.customerPhoneNumber = customerPhoneNumber;
    }
    
    
    public class InsuranceCompany{
        private final String name;
        
        public InsuranceCompany(String name){
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
    
    
    public class PurposeOfInspection{
        
        public PurposeOfInspection(String name){
            this.name = name;
        }
        
        private final String name;

        public String getName() {
            return name;
        }
    }
    
    public class Vehicle{
        private final String model;
        
        public Vehicle(String model){
            
            if(model == null)
                model="";
            this.model = model;
        }

        public String getModel() {
            return model;
        }
        
        
    }

    public long getId() {
        return id;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getRemark() {
        return remark;
    }

    public Date getInspectionTime() {
        return inspectionTime;
    }

    public InsuranceCompany getInsuranceCompany() {
        return insuranceCompany;
    }

    public PurposeOfInspection getPurposeOfInspection() {
        return purposeOfInspection;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public int getCurrentStage() {
        return currentStage;
    }

    public String getRequestorName() {
        return requestorName;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }
    
    
}
