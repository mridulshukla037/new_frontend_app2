package com.jadu.dto;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jadu.model.MakeModelVariant;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ClaimMasterData implements Serializable {

	private Long id;

	private String partType;

	private Integer partTypeId;

	private String partSide;

	private Integer partSideId;

	private String partParticularType;

	private Integer partParticularTypeId;

	private String damageType;

	private Integer damageTypeId;

	private String repairType;

	private Integer repairTypeId;

	private double partCost;

	private Integer partCostId;

	private double labourCost;

	private Integer labourCostId;

	private Integer makeModelVariantId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPartType() {
		return partType;
	}

	public void setPartType(String partType) {
		this.partType = partType;
	}

	public String getPartParticularType() {
		return partParticularType;
	}

	public void setPartParticularType(String partParticularType) {
		this.partParticularType = partParticularType;
	}

	public String getDamageType() {
		return damageType;
	}

	public void setDamageType(String damageType) {
		this.damageType = damageType;
	}

	public String getRepairType() {
		return repairType;
	}

	public void setRepairType(String repairType) {
		this.repairType = repairType;
	}

	public double getPartCost() {
		return partCost;
	}

	public void setPartCost(double partCost) {
		this.partCost = partCost;
	}

	public String getPartSide() {
		return partSide;
	}

	public void setPartSide(String partSide) {
		this.partSide = partSide;
	}

	public Integer getPartTypeId() {
		return partTypeId;
	}

	public void setPartTypeId(Integer partTypeId) {
		this.partTypeId = partTypeId;
	}

	public Integer getPartSideId() {
		return partSideId;
	}

	public void setPartSideId(Integer partSideId) {
		this.partSideId = partSideId;
	}

	public Integer getPartParticularTypeId() {
		return partParticularTypeId;
	}

	public void setPartParticularTypeId(Integer partParticularTypeId) {
		this.partParticularTypeId = partParticularTypeId;
	}

	public Integer getDamageTypeId() {
		return damageTypeId;
	}

	public void setDamageTypeId(Integer damageTypeId) {
		this.damageTypeId = damageTypeId;
	}

	public Integer getRepairTypeId() {
		return repairTypeId;
	}

	public void setRepairTypeId(Integer repairTypeId) {
		this.repairTypeId = repairTypeId;
	}

	public Integer getPartCostId() {
		return partCostId;
	}

	public void setPartCostId(Integer partCostId) {
		this.partCostId = partCostId;
	}

	public double getLabourCost() {
		return labourCost;
	}

	public void setLabourCost(double labourCost) {
		this.labourCost = labourCost;
	}

	public Integer getLabourCostId() {
		return labourCostId;
	}

	public void setLabourCostId(Integer labourCostId) {
		this.labourCostId = labourCostId;
	}

	public Integer getMakeModelVariantId() {
		return makeModelVariantId;
	}

	public void setMakeModelVariantId(Integer makeModelVariantId) {
		this.makeModelVariantId = makeModelVariantId;
	}

	@Override
	public String toString() {
		return "ClaimMasterData [id=" + id + ", partType=" + partType + ", partTypeId=" + partTypeId + ", partSide="
				+ partSide + ", partSideId=" + partSideId + ", partParticularType=" + partParticularType
				+ ", partParticularTypeId=" + partParticularTypeId + ", damageType=" + damageType + ", damageTypeId="
				+ damageTypeId + ", repairType=" + repairType + ", repairTypeId=" + repairTypeId + ", partCost="
				+ partCost + ", partCostId=" + partCostId + ", labourCost=" + labourCost + ", labourCostId="
				+ labourCostId + ", makeModelVariantId=" + makeModelVariantId + "]";
	}

}
