package com.jadu.dto;

import java.io.Serializable;
import java.util.List;

import com.jadu.model.ClaimCase;
import com.jadu.model.ClaimCasePhoto;
import com.jadu.model.LossAssessment;
import com.jadu.model.LossAssessmentPhotos;

public class ClaimCaseDto implements Serializable {
	private ClaimCase claimCase;
	private List<LossAssessment> lossAssessments;
	private List<ClaimCasePhoto> driverDetailsPhotos;
	private List<ClaimCasePhoto> accidentDetailsPhotos;
	private List<ClaimCasePhoto> insuranceDetailsPhotos;
	private List<ClaimCasePhoto> vehicleDetailsPhotos;
	private List<ClaimCasePhoto> otherPhotos;

	private List<LossAssessmentPhotos> lossAssessmentPhotos;
	private AssessmentSummary assessmentSummary;

	public ClaimCase getClaimCase() {
		return claimCase;
	}

	public void setClaimCase(ClaimCase claimCase) {
		this.claimCase = claimCase;
	}

	public List<LossAssessment> getLossAssessments() {
		return lossAssessments;
	}

	public void setLossAssessments(List<LossAssessment> lossAssessments) {
		this.lossAssessments = lossAssessments;
	}

	public List<ClaimCasePhoto> getDriverDetailsPhotos() {
		return driverDetailsPhotos;
	}

	public void setDriverDetailsPhotos(List<ClaimCasePhoto> driverDetailsPhotos) {
		this.driverDetailsPhotos = driverDetailsPhotos;
	}

	public List<ClaimCasePhoto> getAccidentDetailsPhotos() {
		return accidentDetailsPhotos;
	}

	public void setAccidentDetailsPhotos(List<ClaimCasePhoto> accidentDetailsPhotos) {
		this.accidentDetailsPhotos = accidentDetailsPhotos;
	}

	public List<ClaimCasePhoto> getInsuranceDetailsPhotos() {
		return insuranceDetailsPhotos;
	}

	public void setInsuranceDetailsPhotos(List<ClaimCasePhoto> insuranceDetailsPhotos) {
		this.insuranceDetailsPhotos = insuranceDetailsPhotos;
	}

	public List<ClaimCasePhoto> getVehicleDetailsPhotos() {
		return vehicleDetailsPhotos;
	}

	public void setVehicleDetailsPhotos(List<ClaimCasePhoto> vehicleDetailsPhotos) {
		this.vehicleDetailsPhotos = vehicleDetailsPhotos;
	}

	public AssessmentSummary getAssessmentSummary() {
		return assessmentSummary;
	}

	public void setAssessmentSummary(AssessmentSummary assessmentSummary) {
		this.assessmentSummary = assessmentSummary;
	}

	public List<LossAssessmentPhotos> getLossAssessmentPhotos() {
		return lossAssessmentPhotos;
	}

	public void setLossAssessmentPhotos(List<LossAssessmentPhotos> lossAssessmentPhotos) {
		this.lossAssessmentPhotos = lossAssessmentPhotos;
	}

	public List<ClaimCasePhoto> getOtherPhotos() {
		return otherPhotos;
	}

	public void setOtherPhotos(List<ClaimCasePhoto> otherPhotos) {
		this.otherPhotos = otherPhotos;
	}

}
