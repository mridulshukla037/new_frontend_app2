package com.jadu.dto;

import java.io.Serializable;

public class CasesSummary implements Serializable{
    private final Long totalCaseCount;
    private final Long completedCasesCount;
    private final Long cancelledCasesCount;
    private final Long twoWheelersCount;
    private final Long fourWheelersCount;
    private final Long commercialsCount;
    private final Long recommendedCount;
    private final Long notRecommendedCount;
    private final Long pendingcount;

    public CasesSummary(Long totalCaseCount, 
            Long completedCasesCount, 
            Long cancelledCasesCount, 
            Long twoWheelersCount, 
            Long fourWheelersCount,
            Long commercialsCount, 
            Long recommendedCount, 
            Long notRecommendedCount, 
            Long pendingcount) {
        this.totalCaseCount = totalCaseCount;
        this.completedCasesCount = completedCasesCount;
        this.cancelledCasesCount = cancelledCasesCount;
        this.twoWheelersCount = twoWheelersCount;
        this.fourWheelersCount = fourWheelersCount;
        this.commercialsCount = commercialsCount;
        this.recommendedCount = recommendedCount;
        this.notRecommendedCount = notRecommendedCount;
        this.pendingcount = pendingcount;
    }

    public Long getTotalCaseCount() {
        return totalCaseCount;
    }

    public Long getCompletedCasesCount() {
        return completedCasesCount;
    }

    public Long getCancelledCasesCount() {
        return cancelledCasesCount;
    }

    public Long getTwoWheelersCount() {
        return twoWheelersCount;
    }

    public Long getFourWheelersCount() {
        return fourWheelersCount;
    }

    public Long getCommercialsCount() {
        return commercialsCount;
    }

    public Long getRecommendedCount() {
        return recommendedCount;
    }

    public Long getNotRecommendedCount() {
        return notRecommendedCount;
    }

    public Long getPendingcount() {
        return pendingcount;
    }
}
