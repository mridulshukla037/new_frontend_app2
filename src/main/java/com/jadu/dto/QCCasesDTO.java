/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author gautam
 */
public class QCCasesDTO implements Serializable {
	private long id;
	private String vehicleNumber;
	private String customerName;
	private String customerPhoneNumber;
	private Date inspectionTime;
	private Date closeTime;
	private int currentStage;
	private String inspectionType;
	private String insuranceCompanyName;
	private String logoUrl;
	private String requestorPhoneNumber;
	private Date inspectionSubmitTime;
	private String purposeOfInspection;
	private String remark;
	private String downloadKey;
	private String comment;
	private String vehicleType;
	private String branch;
	private long countComments;

	public QCCasesDTO(long id, String vehicleNumber, String customerName, String customerPhoneNumber,
			Date inspectionTime, Date closeTime, int currentStage, String inspectionType, String insuranceCompanyName,
			String logoUrl, String requestorPhoneNumber, Date inspectionSubmitTime, String purposeOfInspection,
			String remark, String downloadKey, String comment, String vehicleType, String branch, long countComments) {
		this.id = id;
		this.vehicleNumber = vehicleNumber;
		this.customerName = customerName;
		this.customerPhoneNumber = customerPhoneNumber;
		this.inspectionTime = inspectionTime;
		this.closeTime = closeTime;
		this.currentStage = currentStage;
		this.inspectionType = inspectionType;
		this.insuranceCompanyName = insuranceCompanyName;
		this.logoUrl = logoUrl;
		this.requestorPhoneNumber = requestorPhoneNumber;
		this.inspectionSubmitTime = inspectionSubmitTime;
		this.purposeOfInspection = purposeOfInspection;
		this.remark = remark;
		this.downloadKey = downloadKey;
		this.comment = comment;
		this.vehicleType = vehicleType;
		this.branch = branch;
		this.countComments = countComments;
	}

	public long getId() {
		return id;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public String getCustomerPhoneNumber() {
		return customerPhoneNumber;
	}

	public Date getInspectionTime() {
		return inspectionTime;
	}

	public Date getCloseTime() {
		return closeTime;
	}

	public int getCurrentStage() {
		return currentStage;
	}

	public String getInspectionType() {
		return inspectionType;
	}

	public String getInsuranceCompanyName() {
		return insuranceCompanyName;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public String getRequestorPhoneNumber() {
		return requestorPhoneNumber;
	}

	public Date getInspectionSubmitTime() {
		return inspectionSubmitTime;
	}

	public String getPurposeOfInspection() {
		return purposeOfInspection;
	}

	public String getRemark() {
		return remark;
	}

	public String getDownloadKey() {
		return downloadKey;
	}

	public String getComment() {
		return comment;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	@Override
	public String toString() {
		return "QCCasesDTO [id=" + id + ", vehicleNumber=" + vehicleNumber + ", customerName=" + customerName
				+ ", customerPhoneNumber=" + customerPhoneNumber + ", inspectionTime=" + inspectionTime + ", closeTime="
				+ closeTime + ", currentStage=" + currentStage + ", inspectionType=" + inspectionType
				+ ", insuranceCompanyName=" + insuranceCompanyName + ", logoUrl=" + logoUrl + ", requestorPhoneNumber="
				+ requestorPhoneNumber + ", inspectionSubmitTime=" + inspectionSubmitTime + ", purposeOfInspection="
				+ purposeOfInspection + ", remark=" + remark + ", downloadKey=" + downloadKey + ", comment=" + comment
				+ ", vehicleType=" + vehicleType + ", branch=" + branch + "]";
	}

	public long getCountComments() {
		return countComments;
	}

	public void setCountComments(long countComments) {
		this.countComments = countComments;
	}

}
