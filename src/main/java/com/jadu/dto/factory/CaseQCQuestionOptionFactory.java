package com.jadu.dto.factory;

import com.jadu.model.CaseQCQuestionOption;
import java.util.List;

public class CaseQCQuestionOptionFactory {
    private final List<CaseQCQuestionOption> items;
    
    public CaseQCQuestionOptionFactory(List items){
        this.items = items;
    }
    
    public CaseQCQuestionOption get(int id){
        for(CaseQCQuestionOption  item: items)
            if(item.getId() == id)
                return item;
        
        return null;
    }
}
