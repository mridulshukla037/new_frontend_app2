package com.jadu.dto;

import java.io.Serializable;

public class RestWrapperDTO implements Serializable{
    protected boolean success = true;
    private final Object data;
    
    public RestWrapperDTO(Object data){
        this.data = data;
    }
    
    public boolean isSuccess() {
        return success;
    }
    public void setSuccess(boolean value) {
        success = value;
    }

    public Object getData() {
        return data;
    }
    
    
}
