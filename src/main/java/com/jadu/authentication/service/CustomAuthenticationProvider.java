/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jadu.authentication.service;

import com.jadu.dao.CaseDAO;
import com.jadu.dao.UserDAOImpl;
import com.jadu.model.InspectionCase;
import com.jadu.service.CaseService;
import com.jadu.service.SmsService;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 *
 * @author gauta
 */
public class CustomAuthenticationProvider implements AuthenticationProvider{
    
    @Autowired
    public UserDAOImpl userDAO2;
    
    @Autowired
    SmsService smsService;
    
    @Autowired
    CaseDAO caseDAO;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String userName = authentication.getName().trim();
        String password;
        if(authentication.getCredentials() == null)
            throw new BadCredentialsException("Invalid password!");
        
        password = authentication.getCredentials().toString().trim();
        Map<String, Object> otherDetails = (Map<String, Object>) authentication.getDetails();
        
        Set authorities = new HashSet<>();
        
        com.jadu.model.User dbUser = userDAO2.getUserByPhoneOrEmail(userName);
        
        if(dbUser == null)
            throw new BadCredentialsException("Invalid username/password!");
        
        if(!dbUser.isEnabled())
            throw new BadCredentialsException("Your account has been disabled. Please contact administrator!");
        
        if(otherDetails.containsKey("login_type") && otherDetails.get("login_type").equals("OTP")){
            try {
                if(!smsService.validateRegistrationOTP(dbUser, password, 720));
            } catch (Exception ex) {
                Logger.getLogger(CustomAuthenticationProvider.class.getName()).log(Level.SEVERE, null, ex);
                throw new BadCredentialsException("Invalid/Expired OTP!");
            }
        }else if(otherDetails.containsKey("login_type") && otherDetails.get("login_type").equals("CASE_REFERENCE")){
            try {
                InspectionCase ic = (InspectionCase) caseDAO.getCaseById(Long.valueOf(password));
                if(ic == null){
                    throw new BadCredentialsException("Invalid inspection case!");
                }
                
                if(ic.getCustomerPhoneNumber().equals(userName) || ic.getRequestorPhoneNumber().equals(userName)){
                }else{
                    throw new BadCredentialsException("Invalid inspection case!");
                }
                
            } catch (Exception ex) {
                Logger.getLogger(CustomAuthenticationProvider.class.getName()).log(Level.SEVERE, null, ex);
                throw new BadCredentialsException("Invalid inspection case!");
            }
        
        }else{
            if(!dbUser.getPassword().equals(password))
                throw new BadCredentialsException("Invalid username/password!");
        }
        
        
        if(dbUser.isDeviceLocked()){
            if(!otherDetails.containsKey("device_id"))
                throw new BadCredentialsException("Invalid Device ID/ Device ID missing");
            
            String deviceId = (String) otherDetails.get("device_id");
            
            if(!deviceId.equals(dbUser.getDeviceId()))
                throw new BadCredentialsException("Invalid device. Unauthorized!");
        }
        
        
        authorities.add(new SimpleGrantedAuthority(dbUser.getAuthority().getAuthority()));
        
        User appUser = new User(dbUser.getUsername(), dbUser.getPassword(), true, true, true, true,authorities);
        Authentication auth = new UsernamePasswordAuthenticationToken(appUser, password, authorities);
        return auth;
    }

    @Override
    public boolean supports(Class<?> type) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(type));
    }
}
