package com.jadu.authentication.service;

import java.util.HashSet;
import java.util.Set;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserDetailsServiceImp implements UserDetailsService {
    

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        /*Here we are using dummy data, you need to load user data from
        database or other third party application*/
       return findUserbyUername(username);
    }

    private User findUserbyUername(String username) {
        Set authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority("ADMIN"));
        if(username.equalsIgnoreCase("admin")) {
          return new User(username, "admin123", true, true, true, true, authorities);
        }
        
        throw new BadCredentialsException("Bad credentials");
      }
    
}
