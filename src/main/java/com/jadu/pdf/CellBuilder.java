package com.jadu.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;

public class CellBuilder {
    
    private final PdfPCell cell;
    
    public CellBuilder(){
        cell = new PdfPCell();
        
        //cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
    }
    
    public CellBuilder(PdfPCell cell){
        this.cell = cell;
    }
    
    public CellBuilder(Image image){
        cell = new PdfPCell(image, true);
    }
    
    public CellBuilder(Image image, boolean fit){
        cell = new PdfPCell(image, fit);
    }
    
    public CellBuilder setBorder(int border){
        this.cell.setBorder(border);
        return this;
    }
    
    public CellBuilder addElement(Element e){
        this.cell.addElement(e);
        return this;
    }
    
    public CellBuilder addParagraph(Paragraph e){
        Float fontSize = e.getFont().getSize();
        Float capHeight = e.getFont().getBaseFont().getFontDescriptor(BaseFont.CAPHEIGHT, fontSize);

        Float padding = 5f;    

        this.cell.addElement(e);
        //cell.setPadding(padding);
        //cell.setPaddingTop(capHeight - fontSize + padding);
        return this;
    }
    
    public CellBuilder setBackgroundColor(BaseColor color){
        if(color != null)
            this.cell.setBackgroundColor(color);
        return this;
    }
    
    public CellBuilder setHeight(int height){
        this.cell.setFixedHeight(height);
        return this;
    }
    
    
    public CellBuilder setColSpan(int colSpan){
        if(colSpan > 1)
            this.cell.setColspan(colSpan);
        return this;
    }
    
    public CellBuilder setPadding(int padding){
        if(padding > -1)
            this.cell.setPadding(padding);
        
        cell.setPaddingBottom(20);
        
        return this;
    }
    
    public  PdfPCell build(){
        return this.cell;
    }
    
    public CellBuilder setMiddle(){
        cell.setVerticalAlignment(Element.ALIGN_TOP);
        return this;
    }
}
