package com.jadu.pdf;

import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;

public class ParagraphBuilder {
    private final Paragraph p;
    
    public ParagraphBuilder(Phrase phrase){
        this.p =  new Paragraph(phrase);
    }
    
    public ParagraphBuilder setAlignment(int alignment){
        this.p.setAlignment(alignment);
        return this;
    }
    
    public Paragraph build(){
        return this.p;
    }
}
