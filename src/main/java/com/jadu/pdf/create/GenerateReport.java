package com.jadu.pdf.create;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfStream;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.ElementHandlerPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import com.jadu.dao.AppConfigurationDAO;
import com.jadu.dao.CasePhotoDAO;
import com.jadu.dao.CaseQCAnswerDAO;
import com.jadu.dao.CaseQCQuestionDAO;
import com.jadu.dao.CaseQCQuestionOptionDAO;
import com.jadu.dao.PhotoTypeDAO;
import com.jadu.dto.CasePhotoDTO;
import com.jadu.dto.CaseQCAnswerDTO;
import com.jadu.dto.QCQuestionDTO;
import com.jadu.dto.factory.CaseQCQuestionOptionFactory;
import com.jadu.helpers.ImageHelper;
import com.jadu.helpers.Pair;
import com.jadu.model.ClaimCase;
import com.jadu.model.InspectionCase;
import com.jadu.pdf.TableBuilder;
import com.jadu.reconcile.RedisReconcileController;
import com.jadu.service.AppConfigService;
import com.jadu.service.GoogleLocationService;
import com.jadu.service.ReduceSize;
import com.jadu.service.S3Service;
import com.jadu.service.UtilService;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import javax.imageio.ImageIO;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
@PropertySource("/WEB-INF/config.properties")
public class GenerateReport {
	@Autowired
	CaseQCQuestionDAO caseQCQuestionDAO;

	@Autowired
	CaseQCAnswerDAO caseQCAnswerDAO;

	@Autowired
	CaseQCQuestionOptionDAO caseQCQuestionOptionDAO;

	@Autowired
	CasePhotoDAO casePhotoDAO;

	@Autowired
	UtilService utilService;

	@Autowired
	GoogleLocationService googleLocationService;

	@Autowired
	Environment env;

	@Autowired
	S3Service s3Service;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	PhotoTypeDAO photoTypeDAO;

	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateReport.class);

	public static BaseColor myColor = WebColors.getRGBColor("#01315F");
	public static BaseColor myColor2 = WebColors.getRGBColor("#049FE1");
	public static BaseColor backgroundColor = WebColors.getRGBColor("#EAEBEF");
	public static BaseColor whiteColor = WebColors.getRGBColor("#ffffff");

	private String getFileNameByPhotoType(List<CasePhotoDTO> casePhotos, String id) {
		for (CasePhotoDTO c : casePhotos)
			if (c.getPhotoType().equals(id))
				return c.getFileName();
		return null;
	}

	public String getAnswerByPhotoType(List<CasePhotoDTO> casePhotos, String photoType) {
		for (CasePhotoDTO item : casePhotos) {
			if (item.getPhotoType().equals(photoType))
				return item.getQcAnswer();
		}

		return "";
	}

	public CasePhotoDTO getAnswerObjectByPhotoType(List<CasePhotoDTO> casePhotos, String photoType) {
		for (CasePhotoDTO item : casePhotos) {
			if (item.getPhotoType().equals(photoType))
				return item;
		}

		return null;
	}

	public CasePhotoDTO getNonZeroLatLongPhoto(List<CasePhotoDTO> casePhotos) {
		for (CasePhotoDTO item : casePhotos) {
			if (item.getLatitude() != 0.0 && item.getLongitude() != 0.0)
				return item;
		}

		return casePhotos.get(0);
	}

	public PdfPTable getTable(String cleanHTML) throws IOException {
		String CSS = "" + " table { background-color: white; width:100%; } "
				+ " tr { text-align: center; height: 20px; }" + " td { padding: 5px; font-size: 12px; }"
				+ " .blue { padding: 5px; background-color: #01315F; color: white; }";
		return getTable(cleanHTML, CSS);
	}

	public PdfPTable getTable(String cleanHTML, String CSS) throws IOException {

		CSSResolver cssResolver = new StyleAttrCSSResolver();
		CssFile cssFile = XMLWorkerHelper.getCSS(new ByteArrayInputStream(CSS.getBytes()));
		cssResolver.addCss(cssFile);

		// HTML
		HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
		htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());

		// Pipelines
		ElementList elements = new ElementList();
		ElementHandlerPipeline pdf = new ElementHandlerPipeline(elements, null);
		HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
		CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);

		// XML Worker
		XMLWorker worker = new XMLWorker(css, true);
		XMLParser p = new XMLParser(worker);
		p.parse(new ByteArrayInputStream(cleanHTML.getBytes()));

		return (PdfPTable) elements.get(0);
	}

	public void createPdf(InspectionCase ic, String fileName, String outputFilename, boolean addInspectionPhotos)
			throws Exception {

		File upFile = new File(env.getProperty("jadu.upload.root.url") + "/" + fileName);
		Files.deleteIfExists(upFile.toPath());

		List<QCQuestionDTO> qcQuestions = caseQCQuestionDAO.getQuestions(ic.getVehicle().getVehicleType().getId());
		List<CaseQCAnswerDTO> qcAnswers = caseQCAnswerDAO.getQCAnswers(ic.getId());
		List<String> photoTags = new ArrayList<>();
		List<CasePhotoDTO> casePhotos = casePhotoDAO.getCasePhotosDTOByCaseId(ic.getId());
		CaseQCQuestionOptionFactory qcOptionsFactory = new CaseQCQuestionOptionFactory(
				caseQCQuestionOptionDAO.getAllObjects());

		for (QCQuestionDTO t : qcQuestions) {
			if (photoTags.indexOf(t.getPhotoTag()) == -1)
				photoTags.add(t.getPhotoTag());
		}

		CasePhotoDTO latLngPhoto = this.getNonZeroLatLongPhoto(casePhotos);

		byte[] bytes = googleLocationService.getMapSnapshotByLatLng(latLngPhoto.getLatitude(),
				latLngPhoto.getLongitude());
		Rectangle pageSize = new Rectangle(PageSize.A4);
		pageSize.setBackgroundColor(backgroundColor);
		Document document = new Document(pageSize);
		PdfWriter writer = PdfWriter.getInstance(document,
				new FileOutputStream(env.getProperty("jadu.upload.root.url") + "/" + fileName));

		writer.setPdfVersion(PdfWriter.PDF_VERSION_1_5);
		writer.setCompressionLevel(PdfStream.BEST_COMPRESSION);

		try {
			document.open();

			document.add(new TableBuilder(2, 100, new int[] { 1, 2 })
					.addImageColumn(env.getProperty("jadu.upload.root.url") + "/logo.png")
					.addTextColumn("" + "HEDGEROW TECHNOLOGIES PRIVATE LIMITED \n" + "Phone Number: "
							+ appConfigService.getProperty("SUPPORT_CONTACT_NUMBER") + " \n" + "Email ID: "
							+ appConfigService.getProperty("SUPPORT_EMAIL") + " ", 8, Element.ALIGN_RIGHT)
					.setMarginAfter(5).build());

			LineSeparator ls = new LineSeparator();
			ls.setLineColor(myColor);
			ls.setLineWidth(4);

			LineSeparator ls2 = new LineSeparator();
			ls2.setLineColor(myColor2);
			ls2.setLineWidth(4);
			ls2.setOffset(-4);

			document.add(ls);
			document.add(ls2);

			document.add(new TableBuilder(3, 100, new int[] { 1, 1, 1 })
					.addTextColumn("Case Ref. Number: " + ic.getId(), 8, Element.ALIGN_LEFT)
					.addTextColumn("Inspection Report", 8, Element.ALIGN_CENTER)
					.addTextColumn("Vehicle Type: " + ic.getVehicle().getVehicleType().getName(), 8,
							Element.ALIGN_RIGHT)
					.setMarginBefore(2).setMarginAfter(4).build());

			LineSeparator ls3 = new LineSeparator();
			ls3.setLineColor(myColor);
			ls3.setLineWidth(1);
			document.add(ls3);

			CasePhotoDTO signPhoto = getAnswerObjectByPhotoType(casePhotos, "chachis-number");
			LinkedHashMap<String, String> hm = new LinkedHashMap<>();
			hm.put("Insurer", ic.getCompanyBranchDivision().getCompany());
			hm.put("Agent", ic.getRequestorName());
			hm.put("Branch", ic.getCompanyBranchDivision().getBranch());
			hm.put("Division", ic.getCompanyBranchDivision().getDivision());
			hm.put("Purpose", ic.getPurposeOfInspection().getName());
			hm.put("Inspection Request", ic.getCreationTime().toString());
			hm.put("Inspection Time", signPhoto.getSnapTime().toString());

			if (ic.getInspectionType().equals("ASSIGN_TO_CUSTOMER")) {
				hm.put("Inspection done by", ic.getCustomerName());// "Inspection done by"
			} else {
				hm.put("Inspection done by", ic.getRequestorName());
			}

			hm.put("QC Done by", ic.getQcName());
			hm.put("Inspection Status", ic.getRemark().toUpperCase());

			String chassisNumber = ic.getChassisNumber();
			String vinPlateNumber = ic.getEngineNumber();
			String odometerRpm = ic.getOdometerReading();

			if ("".equals(chassisNumber))
				chassisNumber = "NA";

			if ("".equals(vinPlateNumber))
				vinPlateNumber = "NA";

			if ("".equals(odometerRpm))
				odometerRpm = "NA";

			LinkedHashMap<String, String> hm2 = new LinkedHashMap<>();
			hm2.put("Vehicle No.", ic.getVehicleNumber());
			hm2.put("Owner Name", ic.getCustomerName());
			hm2.put("Owner Phone", ic.getCustomerPhoneNumber());
			hm2.put("Make", ic.getVehicle().getMake());
			hm2.put("Model", ic.getVehicle().getModel());
			hm2.put("Mfg. Year", String.valueOf(ic.getVehicleYOM()));
			hm2.put("Color", ic.getVehicleColor());
			hm2.put("Chasis Number", chassisNumber);
			hm2.put("Engine Number", vinPlateNumber);
			hm2.put("Odometer Reading", odometerRpm);
			hm2.put("Fuel Type", ic.getVehicleFuelType().getName());
			hm2.put("RC Verified", "Yes");

			LinkedHashMap<String, String> hm3 = new LinkedHashMap<>();
			hm3.put("Place",
					googleLocationService.getLocalityByLatLng(latLngPhoto.getLatitude(), latLngPhoto.getLongitude()));
			hm3.put("Latitude", String.valueOf(latLngPhoto.getLatitude()));
			hm3.put("Longitude", String.valueOf(latLngPhoto.getLongitude()));

			document.add(new TableBuilder(3, 100, new int[] { 3, 3, 3 }).setMarginBefore(2).setMarginAfter(4)
					.addTable(new TableBuilder(2, 100, new int[] { 3, 5 }).addTextColumns(hm, 7, Element.ALIGN_LEFT)
							.setBackgroundColor("#ffffff").build())
					.addTable(new TableBuilder(2, 100, new int[] { 3, 5 }).addTextColumns(hm2, 7, Element.ALIGN_LEFT)
							.setBackgroundColor("#ffffff").build())
					.addTable(new TableBuilder(2, 100, new int[] { 1, 2 }).setBackgroundColor("#ffffff")
							.setNumberOfHeaderColumns(2).setColspan(2)
							.addTextColumnVAlign("Inspection Location".toUpperCase(), 10, Element.ALIGN_CENTER, 2)
							.addImageColumn(bytes).addTextColumns(hm3, 7, Element.ALIGN_LEFT).build())
					.build());

			TableBuilder items = new TableBuilder(3, 100, new int[] { 1, 1, 1 }).setMarginBefore(2).setMarginAfter(4);

			for (String tag : photoTags) {
				LinkedHashMap<String, String> temp = new LinkedHashMap<>();
				TableBuilder tb = new TableBuilder(2, 100, new int[] { 1, 1 }).setNumberOfHeaderColumns(1)
						.setBackgroundColor("#ffffff").setHeaderColor("#01315F").setColspan(2)
						.addTextColumn(tag.toUpperCase(), 10, Element.ALIGN_CENTER, whiteColor);

				for (QCQuestionDTO t : qcQuestions) {
					if (t.getPhotoTag().equals(tag)) {
						String question = t.getValue();
						for (CaseQCAnswerDTO a : qcAnswers) {
							if (a.getQuestionId() == t.getId()) {
								String answer = qcOptionsFactory.get(a.getOptionId()).getValue();
								temp.put(question, answer);
							}
						}
					}
				}

				tb.addTextColumns(temp, 6, Element.ALIGN_LEFT, Element.ALIGN_CENTER);
				items.addTable(tb.build());
			}

			document.add(items.build());

			byte[] firstPhoto = utilService.getCaseImageBytes(getFileNameByPhotoType(casePhotos, "chachis-number"),
					ic.getId());
			byte[] secondPhoto = utilService.getCaseImageBytes(getFileNameByPhotoType(casePhotos, "number-plate"),
					ic.getId());
			byte[] thirdPhoto = utilService.getCaseImageBytes(getFileNameByPhotoType(casePhotos, "client-signature"),
					ic.getId());

			if (firstPhoto == null)
				throw new Exception("Chassis number photo not found!");

			if (secondPhoto == null)
				throw new Exception("Number plate photo not found" + "!");

			if (thirdPhoto == null)
				throw new Exception("Client Signature not found!");

			// BufferedImage scaledImage1 = Scalr.resize(ImageIO.read(new
			// ByteArrayInputStream(firstPhoto)), 300, 300);
			// BufferedImage scaledImage2 = Scalr.resize(ImageIO.read(new
			// ByteArrayInputStream(secondPhoto)), 300, 300);
			// BufferedImage scaledImage3 = Scalr.resize(ImageIO.read(new
			// ByteArrayInputStream(thirdPhoto)), 300, 300);

			Pair<Integer, Integer> firstPhotoResolution = ImageHelper.getImageWidthAndHeight(firstPhoto);
			Pair<Integer, Integer> secondPhotoResolution = ImageHelper.getImageWidthAndHeight(secondPhoto);
			Pair<Integer, Integer> thirdPhotoResolution = ImageHelper.getImageWidthAndHeight(thirdPhoto);

			if (firstPhotoResolution.getLeft() < firstPhotoResolution.getRight()) {
				firstPhoto = ImageHelper.rotateClockwise(firstPhoto);
			}

			if (secondPhotoResolution.getLeft() < secondPhotoResolution.getRight()) {
				secondPhoto = ImageHelper.rotateClockwise(secondPhoto);
			}

			if (thirdPhotoResolution.getLeft() < thirdPhotoResolution.getRight()) {
				thirdPhoto = ImageHelper.rotateClockwise(thirdPhoto);
			}

			if (firstPhoto == null)
				throw new Exception("Chassis number photo not available!");

			if (secondPhoto == null)
				throw new Exception("Number plate photo not available!");

			if (thirdPhoto == null)
				throw new Exception("Client signature not available!");

			document.add(new TableBuilder(3, 100, new int[] { 1, 1, 1 })
					.addTable(new TableBuilder(1, 100, new int[] { 1 }).setNumberOfHeaderColumns(1)
							.setHeaderColor("#01315F")
							.addTextColumn("Chasis Number Photo", 10, Element.ALIGN_CENTER, whiteColor)
							.addImageColumn(firstPhoto, 200, 100).build())
					.addTable(new TableBuilder(1, 100, new int[] { 1 }).setNumberOfHeaderColumns(1)
							.setHeaderColor("#01315F")
							.addTextColumn("Vehicle Number Photo", 10, Element.ALIGN_CENTER, whiteColor)
							.addImageColumn(secondPhoto, 200, 100).build())

					.addTable(new TableBuilder(1, 100, new int[] { 1 }).setNumberOfHeaderColumns(1)
							.setHeaderColor("#01315F")
							.addTextColumn("Customer Signature", 10, Element.ALIGN_CENTER, whiteColor)
							.addImageColumn((thirdPhoto), 200, 100).build())
					.build());

			Phrase phrase1 = new Phrase(10f, "Remarks: " + ic.getComment(), FontFactory.getFont("ARIAL", 6));

			Phrase phrase = new Phrase(10f,
					"Declaration of owner: I hereby confirm and declare that above-mentioned identification details of My Vehicle NO. "
							+ ic.getVehicleNumber()
							+ " as well as that of damage to the vehicle as noted by the inspecting official are correct. Nothing has been Hidden/ undisclosed. I further confirm & declare that the Motor Vehicle proposed for insurance after a break in coverage has not met with an accident giving rise to any  claims that Third Party for injury or death causes to any person or damage to any property/Insured vehicle during the period following the expiry​ ​of an Insurance, till the moment it is proposed for insurance. I also agree that the damages mentioned above shall be excluded /adjusted in the event of any claim being logged.",
					FontFactory.getFont("ARIAL", 6));

			document.add(new Paragraph(phrase1));
			document.add(new Paragraph(phrase));

			if (addInspectionPhotos) {
				int pageNo = writer.getPageNumber();

				if (pageNo == 1)
					document.newPage();

				addInspectionImagesToDocument(ic, casePhotos, document);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Something went wrong while genrating report, error={} ", e.getMessage());
		} finally {
			document.close();
		}

		ReduceSize.manipulatePdf(env.getProperty("jadu.upload.root.url") + "/" + fileName,
				env.getProperty("jadu.upload.root.url") + "/optimized." + fileName);

		File file = new File(env.getProperty("jadu.upload.root.url") + "/" + fileName);
		File file2 = new File(env.getProperty("jadu.upload.root.url") + "/optimized." + fileName);

		s3Service.uploadFile(file2, "cases/" + ic.getId() + "/" + outputFilename);
		file.delete();
	}

	private void addInspectionImagesToDocument(InspectionCase ic, List<CasePhotoDTO> casePhotos, Document document)
			throws DocumentException, IOException {

		List<String> photoTypesToBeAddedInReport = photoTypeDAO.getPhotoTypesToBeAddedInReport();

		TableBuilder caseImages = new TableBuilder(2, 100, new int[] { 1, 1 }).setBackgroundColor("#000000")
				.setPadding(5);

		for (CasePhotoDTO casePhoto : casePhotos) {
			byte[] image = utilService.getCaseImageBytes(casePhoto.getFileName(), ic.getId());
			if (image != null && photoTypesToBeAddedInReport.indexOf(casePhoto.getPhotoType()) > -1)
				caseImages.addImageColumn(image);
		}

		document.add(caseImages.build());
	}

	private void addInspectionImagesToDocument(ClaimCase ic, List<CasePhotoDTO> casePhotos, Document document)
			throws DocumentException, IOException {

		List<String> photoTypesToBeAddedInReport = photoTypeDAO.getPhotoTypesToBeAddedInReport();

		TableBuilder caseImages = new TableBuilder(2, 100, new int[] { 1, 1 }).setBackgroundColor("#000000")
				.setPadding(5);

		for (CasePhotoDTO casePhoto : casePhotos) {
			byte[] image = utilService.getCaseImageBytes(casePhoto.getFileName(), ic.getId());
			if (image != null && photoTypesToBeAddedInReport.indexOf(casePhoto.getPhotoType()) > -1)
				caseImages.addImageColumn(image);
		}

		document.add(caseImages.build());
	}

	public void createPdf(ClaimCase ic, String fileName, String outputFile1, boolean addInspectionPhotos)
			throws Exception {

		File upFile = new File(env.getProperty("jadu.upload.root.url") + "/" + fileName);
		Files.deleteIfExists(upFile.toPath());

		/*
		 * List<QCQuestionDTO> qcQuestions =
		 * caseQCQuestionDAO.getQuestions(ic.getVehicleDetails().getVehicleType());
		 * List<CaseQCAnswerDTO> qcAnswers = caseQCAnswerDAO.getQCAnswers(ic.getId());
		 */
		List<String> photoTags = new ArrayList<>();
		List<CasePhotoDTO> casePhotos = casePhotoDAO.getCasePhotosDTOByCaseId(ic.getId());
		CaseQCQuestionOptionFactory qcOptionsFactory = new CaseQCQuestionOptionFactory(
				caseQCQuestionOptionDAO.getAllObjects());

		/*
		 * for (QCQuestionDTO t : qcQuestions) { if (photoTags.indexOf(t.getPhotoTag())
		 * == -1) photoTags.add(t.getPhotoTag()); }
		 */
		CasePhotoDTO latLngPhoto = this.getNonZeroLatLongPhoto(casePhotos);

		byte[] bytes = googleLocationService.getMapSnapshotByLatLng(latLngPhoto.getLatitude(),
				latLngPhoto.getLongitude());
		Rectangle pageSize = new Rectangle(PageSize.A4);
		pageSize.setBackgroundColor(backgroundColor);
		Document document = new Document(pageSize);
		PdfWriter writer = PdfWriter.getInstance(document,
				new FileOutputStream(env.getProperty("jadu.upload.root.url") + "/" + fileName));

		writer.setPdfVersion(PdfWriter.PDF_VERSION_1_5);
		writer.setCompressionLevel(PdfStream.BEST_COMPRESSION);

		try {
			document.open();

			document.add(new TableBuilder(2, 100, new int[] { 1, 2 })
					.addImageColumn(env.getProperty("jadu.upload.root.url") + "/logo.png")
					.addTextColumn("" + "HEDGEROW TECHNOLOGIES PRIVATE LIMITED \n" + "Phone Number: "
							+ appConfigService.getProperty("SUPPORT_CONTACT_NUMBER") + " \n" + "Email ID: "
							+ appConfigService.getProperty("SUPPORT_EMAIL") + " ", 8, Element.ALIGN_RIGHT)
					.setMarginAfter(5).build());

			LineSeparator ls = new LineSeparator();
			ls.setLineColor(myColor);
			ls.setLineWidth(4);

			LineSeparator ls2 = new LineSeparator();
			ls2.setLineColor(myColor2);
			ls2.setLineWidth(4);
			ls2.setOffset(-4);

			document.add(ls);
			document.add(ls2);

			document.add(new TableBuilder(3, 100, new int[] { 1, 1, 1 })
					.addTextColumn("Case Ref. Number: " + ic.getId(), 8, Element.ALIGN_LEFT)
					.addTextColumn("Inspection Report", 8, Element.ALIGN_CENTER)
					//.addTextColumn("Vehicle Type: " + ic.getVehicleDetails().getVehicleType(), 8, Element.ALIGN_RIGHT)
					.setMarginBefore(2).setMarginAfter(4).build());

			LineSeparator ls3 = new LineSeparator();
			ls3.setLineColor(myColor);
			ls3.setLineWidth(1);
			document.add(ls3);

			CasePhotoDTO signPhoto = getAnswerObjectByPhotoType(casePhotos, "chachis-number");
			LinkedHashMap<String, String> hm = new LinkedHashMap<>();
			hm.put("Insurer", ic.getCompanyBranchDivision().getCompany());
			hm.put("Agent", ic.getRequestor().getFirstName());
			hm.put("Branch", ic.getCompanyBranchDivision().getBranch());
			hm.put("Division", ic.getCompanyBranchDivision().getDivision());
			hm.put("Purpose", ic.getPurposeOfSurvey().getName());
			hm.put("Inspection Request", ic.getCreationTime().toString());
			hm.put("Inspection Time", signPhoto.getSnapTime().toString());

			if (ic.getInspectionType().equals("ASSIGN_TO_CUSTOMER")) {
				hm.put("Inspection done by", ic.getCustomerName());// "Inspection done by"
			} else {
				hm.put("Inspection done by", ic.getRequestor().getFirstName());
			}

			hm.put("QC Done by", ic.getQc().getFirstName() + " " + ic.getQc().getLastName());
			hm.put("Inspection Status", ic.getRemark().toUpperCase());

			String chassisNumber = ic.getVehicleDetails().getChassisNumber();
			String vinPlateNumber = ic.getVehicleDetails().getEngineNumber();
			String odometerRpm = ic.getVehicleDetails().getOdometerReading();

			if ("".equals(chassisNumber))
				chassisNumber = "NA";

			if ("".equals(vinPlateNumber))
				vinPlateNumber = "NA";

			if ("".equals(odometerRpm))
				odometerRpm = "NA";

			LinkedHashMap<String, String> hm2 = new LinkedHashMap<>();
			hm2.put("Vehicle No.", ic.getVehicleNumber());
			hm2.put("Owner Name", ic.getCustomerName());
			hm2.put("Owner Phone", ic.getCustomerPhoneNumber());
//			hm2.put("Make", ic.getVehicleDetails().getVehicleMake());
//			hm2.put("Model", ic.getVehicleDetails().getVehicleModel());
			hm2.put("Mfg. Year", String.valueOf(ic.getVehicleDetails().getVehicleYOM()));
			hm2.put("Color", ic.getVehicleDetails().getVehicleColor());
			hm2.put("Chasis Number", chassisNumber);
			hm2.put("Engine Number", vinPlateNumber);
			hm2.put("Odometer Reading", odometerRpm);
			hm2.put("Fuel Type", ic.getVehicleFuelType().getName());
			hm2.put("RC Verified", "Yes");

			LinkedHashMap<String, String> hm3 = new LinkedHashMap<>();
			hm3.put("Place",
					googleLocationService.getLocalityByLatLng(latLngPhoto.getLatitude(), latLngPhoto.getLongitude()));
			hm3.put("Latitude", String.valueOf(latLngPhoto.getLatitude()));
			hm3.put("Longitude", String.valueOf(latLngPhoto.getLongitude()));

			document.add(new TableBuilder(3, 100, new int[] { 3, 3, 3 }).setMarginBefore(2).setMarginAfter(4)
					.addTable(new TableBuilder(2, 100, new int[] { 3, 5 }).addTextColumns(hm, 7, Element.ALIGN_LEFT)
							.setBackgroundColor("#ffffff").build())
					.addTable(new TableBuilder(2, 100, new int[] { 3, 5 }).addTextColumns(hm2, 7, Element.ALIGN_LEFT)
							.setBackgroundColor("#ffffff").build())
					.addTable(new TableBuilder(2, 100, new int[] { 1, 2 }).setBackgroundColor("#ffffff")
							.setNumberOfHeaderColumns(2).setColspan(2)
							.addTextColumnVAlign("Inspection Location".toUpperCase(), 10, Element.ALIGN_CENTER, 2)
							.addImageColumn(bytes).addTextColumns(hm3, 7, Element.ALIGN_LEFT).build())
					.build());

			TableBuilder items = new TableBuilder(3, 100, new int[] { 1, 1, 1 }).setMarginBefore(2).setMarginAfter(4);

			/*
			 * for (String tag : photoTags) { LinkedHashMap<String, String> temp = new
			 * LinkedHashMap<>(); TableBuilder tb = new TableBuilder(2, 100, new int[] { 1,
			 * 1 }).setNumberOfHeaderColumns(1)
			 * .setBackgroundColor("#ffffff").setHeaderColor("#01315F").setColspan(2)
			 * .addTextColumn(tag.toUpperCase(), 10, Element.ALIGN_CENTER, whiteColor);
			 * 
			 * 
			 * for (QCQuestionDTO t : qcQuestions) { if (t.getPhotoTag().equals(tag)) {
			 * String question = t.getValue(); for (CaseQCAnswerDTO a : qcAnswers) { if
			 * (a.getQuestionId() == t.getId()) { String answer =
			 * qcOptionsFactory.get(a.getOptionId()).getValue(); temp.put(question, answer);
			 * } } } }
			 * 
			 * 
			 * // tb.addTextColumns(temp, 6, Element.ALIGN_LEFT, Element.ALIGN_CENTER); //
			 * items.addTable(tb.build()); }
			 */

			document.add(items.build());

			byte[] firstPhoto = utilService.getCaseImageBytes(getFileNameByPhotoType(casePhotos, "chachis-number"),
					ic.getId());
			byte[] secondPhoto = utilService.getCaseImageBytes(getFileNameByPhotoType(casePhotos, "number-plate"),
					ic.getId());
			byte[] thirdPhoto = utilService.getCaseImageBytes(getFileNameByPhotoType(casePhotos, "client-signature"),
					ic.getId());

			if (firstPhoto == null)
				throw new Exception("Chassis number photo not found!");

			if (secondPhoto == null)
				throw new Exception("Number plate photo not found" + "!");

			if (thirdPhoto == null)
				throw new Exception("Client Signature not found!");

			// BufferedImage scaledImage1 = Scalr.resize(ImageIO.read(new
			// ByteArrayInputStream(firstPhoto)), 300, 300);
			// BufferedImage scaledImage2 = Scalr.resize(ImageIO.read(new
			// ByteArrayInputStream(secondPhoto)), 300, 300);
			// BufferedImage scaledImage3 = Scalr.resize(ImageIO.read(new
			// ByteArrayInputStream(thirdPhoto)), 300, 300);

			Pair<Integer, Integer> firstPhotoResolution = ImageHelper.getImageWidthAndHeight(firstPhoto);
			Pair<Integer, Integer> secondPhotoResolution = ImageHelper.getImageWidthAndHeight(secondPhoto);
			Pair<Integer, Integer> thirdPhotoResolution = ImageHelper.getImageWidthAndHeight(thirdPhoto);

			if (firstPhotoResolution.getLeft() < firstPhotoResolution.getRight()) {
				firstPhoto = ImageHelper.rotateClockwise(firstPhoto);
			}

			if (secondPhotoResolution.getLeft() < secondPhotoResolution.getRight()) {
				secondPhoto = ImageHelper.rotateClockwise(secondPhoto);
			}

			if (thirdPhotoResolution.getLeft() < thirdPhotoResolution.getRight()) {
				thirdPhoto = ImageHelper.rotateClockwise(thirdPhoto);
			}

			if (firstPhoto == null)
				throw new Exception("Chassis number photo not available!");

			if (secondPhoto == null)
				throw new Exception("Number plate photo not available!");

			if (thirdPhoto == null)
				throw new Exception("Client signature not available!");

			document.add(new TableBuilder(3, 100, new int[] { 1, 1, 1 })
					.addTable(new TableBuilder(1, 100, new int[] { 1 }).setNumberOfHeaderColumns(1)
							.setHeaderColor("#01315F")
							.addTextColumn("Chasis Number Photo", 10, Element.ALIGN_CENTER, whiteColor)
							.addImageColumn(firstPhoto, 200, 100).build())
					.addTable(new TableBuilder(1, 100, new int[] { 1 }).setNumberOfHeaderColumns(1)
							.setHeaderColor("#01315F")
							.addTextColumn("Vehicle Number Photo", 10, Element.ALIGN_CENTER, whiteColor)
							.addImageColumn(secondPhoto, 200, 100).build())

					.addTable(new TableBuilder(1, 100, new int[] { 1 }).setNumberOfHeaderColumns(1)
							.setHeaderColor("#01315F")
							.addTextColumn("Customer Signature", 10, Element.ALIGN_CENTER, whiteColor)
							.addImageColumn((thirdPhoto), 200, 100).build())
					.build());

			Phrase phrase1 = new Phrase(10f, "Remarks: " + ic.getComment(), FontFactory.getFont("ARIAL", 6));

			Phrase phrase = new Phrase(10f,
					"Declaration of owner: I hereby confirm and declare that above-mentioned identification details of My Vehicle NO. "
							+ ic.getVehicleNumber()
							+ " as well as that of damage to the vehicle as noted by the inspecting official are correct. Nothing has been Hidden/ undisclosed. I further confirm & declare that the Motor Vehicle proposed for insurance after a break in coverage has not met with an accident giving rise to any  claims that Third Party for injury or death causes to any person or damage to any property/Insured vehicle during the period following the expiry​ ​of an Insurance, till the moment it is proposed for insurance. I also agree that the damages mentioned above shall be excluded /adjusted in the event of any claim being logged.",
					FontFactory.getFont("ARIAL", 6));

			document.add(new Paragraph(phrase1));
			document.add(new Paragraph(phrase));

			if (addInspectionPhotos) {
				int pageNo = writer.getPageNumber();

				if (pageNo == 1)
					document.newPage();

				addInspectionImagesToDocument(ic, casePhotos, document);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Something went wrong while genrating report, error={} ", e.getMessage());
		} finally {
			document.close();
		}

		ReduceSize.manipulatePdf(env.getProperty("jadu.upload.root.url") + "/" + fileName,
				env.getProperty("jadu.upload.root.url") + "/optimized." + fileName);

		File file = new File(env.getProperty("jadu.upload.root.url") + "/" + fileName);
		File file2 = new File(env.getProperty("jadu.upload.root.url") + "/optimized." + fileName);

		s3Service.uploadFile(file2, "cases/" + ic.getId() + "/" + outputFile1);
		file.delete();

	}
}
